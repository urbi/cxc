'use strict';

angular.module('cxcApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });

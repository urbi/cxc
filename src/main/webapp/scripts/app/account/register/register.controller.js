'use strict';

angular.module('cxcApp')
    .controller('RegisterController', function ($scope, $translate, $timeout, Auth, PlazaSearch) {
        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.errorUserExists = null;
        $scope.searchQuery ="*";
        $scope.plazas = PlazaSearch.query({query: $scope.searchQuery});
        console.log($scope.plazas);
        $scope.errorEmailDomain = null;
        $scope.registerAccount = {};
        $timeout(function (){angular.element('[ng-model="registerAccount.login"]').focus();});

        $scope.register = function () {
            $scope.errorEmailDomain= null;
            if ($scope.registerAccount.password !== $scope.confirmPassword) {
                $scope.doNotMatch = 'ERROR';
            } else if($scope.registerAccount.email.indexOf("urbi.com")<=0){
                $scope.errorEmailDomain="ERROR";

            }else{
                    $scope.registerAccount.langKey = $translate.use();
                    $scope.doNotMatch = null;
                    $scope.error = null;
                    $scope.errorUserExists = null;
                    $scope.errorEmailExists = null;

                    Auth.createAccount($scope.registerAccount).then(function () {
                        $scope.success = 'OK';
                    }).catch(function (response) {
                        $scope.success = null;
                        if (response.status === 400 && response.data === 'login already in use') {
                            $scope.errorUserExists = 'ERROR';
                        } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                            $scope.errorEmailExists = 'ERROR';
                        } else {
                            $scope.error = 'ERROR';
                        }
                    });
                }
        };
    });

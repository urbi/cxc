'use strict';

angular.module('cxcApp')
    .controller('EmpresaDestinoPagoDetailController', function ($scope, $rootScope, $stateParams, entity, EmpresaDestinoPago, DestinoPago, Empresa, Plaza) {
        $scope.empresaDestinoPago = entity;
        $scope.load = function (id) {
            EmpresaDestinoPago.get({id: id}, function(result) {
                $scope.empresaDestinoPago = result;
            });
        };
        $rootScope.$on('cxcApp:empresaDestinoPagoUpdate', function(event, result) {
            $scope.empresaDestinoPago = result;
        });
    });

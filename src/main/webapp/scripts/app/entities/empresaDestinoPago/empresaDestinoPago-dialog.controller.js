'use strict';

angular.module('cxcApp').controller('EmpresaDestinoPagoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'EmpresaDestinoPago', 'DestinoPago', 'Empresa', 'Plaza',
        function($scope, $stateParams, $modalInstance, entity, EmpresaDestinoPago, DestinoPago, Empresa, Plaza) {

        $scope.empresaDestinoPago = entity;
        $scope.destinopagos = DestinoPago.query();
        $scope.empresas = Empresa.query();
        $scope.plazas = Plaza.query();
        $scope.load = function(id) {
            EmpresaDestinoPago.get({id : id}, function(result) {
                $scope.empresaDestinoPago = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:empresaDestinoPagoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.empresaDestinoPago.id != null) {
                EmpresaDestinoPago.update($scope.empresaDestinoPago, onSaveFinished);
            } else {
                EmpresaDestinoPago.save($scope.empresaDestinoPago, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

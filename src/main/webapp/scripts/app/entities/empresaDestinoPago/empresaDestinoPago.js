'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('empresaDestinoPago', {
                parent: 'entity',
                url: '/empresaDestinoPagos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.empresaDestinoPago.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/empresaDestinoPago/empresaDestinoPagos.html',
                        controller: 'EmpresaDestinoPagoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('empresaDestinoPago');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('empresaDestinoPago.detail', {
                parent: 'entity',
                url: '/empresaDestinoPago/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.empresaDestinoPago.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/empresaDestinoPago/empresaDestinoPago-detail.html',
                        controller: 'EmpresaDestinoPagoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('empresaDestinoPago');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'EmpresaDestinoPago', function($stateParams, EmpresaDestinoPago) {
                        return EmpresaDestinoPago.get({id : $stateParams.id});
                    }]
                }
            })
            .state('empresaDestinoPago.new', {
                parent: 'empresaDestinoPago',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/empresaDestinoPago/empresaDestinoPago-dialog.html',
                        controller: 'EmpresaDestinoPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {generadorReferencia: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('empresaDestinoPago', null, { reload: true });
                    }, function() {
                        $state.go('empresaDestinoPago');
                    })
                }]
            })
            .state('empresaDestinoPago.edit', {
                parent: 'empresaDestinoPago',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/empresaDestinoPago/empresaDestinoPago-dialog.html',
                        controller: 'EmpresaDestinoPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['EmpresaDestinoPago', function(EmpresaDestinoPago) {
                                return EmpresaDestinoPago.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('empresaDestinoPago', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp')
    .controller('EmpresaDestinoPagoController', function ($scope, EmpresaDestinoPago, EmpresaDestinoPagoSearch, ParseLinks) {
        $scope.empresaDestinoPagos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            EmpresaDestinoPago.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.empresaDestinoPagos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            EmpresaDestinoPago.get({id: id}, function(result) {
                $scope.empresaDestinoPago = result;
                $('#deleteEmpresaDestinoPagoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            EmpresaDestinoPago.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmpresaDestinoPagoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmpresaDestinoPagoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.empresaDestinoPagos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.empresaDestinoPago = {generadorReferencia: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('entity2', {
                abstract: true,
                parent: 'site'
            });
    });

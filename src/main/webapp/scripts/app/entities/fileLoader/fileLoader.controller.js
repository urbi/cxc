'use strict';

angular.module('cxcApp')
    .controller('FileLoaderController', function ($scope, FileLoader, FileLoaderSearch) {
        $scope.fileLoaders = [];
        $scope.loadAll = function() {
            FileLoader.query(function(result) {
               $scope.fileLoaders = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            FileLoader.get({id: id}, function(result) {
                $scope.fileLoader = result;
                $('#deleteFileLoaderConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            FileLoader.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteFileLoaderConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            FileLoaderSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.fileLoaders = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.fileLoader = {tipoDato: null, file: null, id: null};
        };

        $scope.abbreviate = function (text) {
            if (!angular.isString(text)) {
                return '';
            }
            if (text.length < 30) {
                return text;
            }
            return text ? (text.substring(0, 15) + '...' + text.slice(-10)) : '';
        };

        $scope.byteSize = function (base64String) {
            if (!angular.isString(base64String)) {
                return '';
            }
            function endsWith(suffix, str) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            function paddingSize(base64String) {
                if (endsWith('==', base64String)) {
                    return 2;
                }
                if (endsWith('=', base64String)) {
                    return 1;
                }
                return 0;
            }
            function size(base64String) {
                return base64String.length / 4 * 3 - paddingSize(base64String);
            }
            function formatAsBytes(size) {
                return size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " bytes";
            }

            return formatAsBytes(size(base64String));
        };
    });

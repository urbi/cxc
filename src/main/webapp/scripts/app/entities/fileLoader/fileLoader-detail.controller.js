'use strict';

angular.module('cxcApp')
    .controller('FileLoaderDetailController', function ($scope, $rootScope, $stateParams, entity, FileLoader) {
        $scope.fileLoader = entity;
        $scope.load = function (id) {
            FileLoader.get({id: id}, function(result) {
                $scope.fileLoader = result;
            });
        };
        $rootScope.$on('cxcApp:fileLoaderUpdate', function(event, result) {
            $scope.fileLoader = result;
        });
    });

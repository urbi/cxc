'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('fileLoader', {
                parent: 'entity',
                url: '/fileLoaders',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.fileLoader.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/fileLoader/fileLoaders.html',
                        controller: 'FileLoaderController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('fileLoader');
                        $translatePartialLoader.addPart('objectNameEnum');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('fileLoader.detail', {
                parent: 'entity',
                url: '/fileLoader/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.fileLoader.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/fileLoader/fileLoader-detail.html',
                        controller: 'FileLoaderDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('fileLoader');
                        $translatePartialLoader.addPart('objectNameEnum');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'FileLoader', function($stateParams, FileLoader) {
                        return FileLoader.get({id : $stateParams.id});
                    }]
                }
            })
            .state('fileLoader.new', {
                parent: 'fileLoader',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/fileLoader/fileLoader-dialog.html',
                        controller: 'FileLoaderDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {tipoDato: null, file: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('fileLoader', null, { reload: true });
                    }, function() {
                        $state.go('fileLoader');
                    })
                }]
            })
            .state('fileLoader.edit', {
                parent: 'fileLoader',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/fileLoader/fileLoader-dialog.html',
                        controller: 'FileLoaderDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['FileLoader', function(FileLoader) {
                                return FileLoader.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('fileLoader', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp')
    .controller('ClienteController', function ($scope, Cliente,  ClienteSearch, ParseLinks) {
        $scope.clientes = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Cliente.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.clientes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Cliente.get({id: id}, function(result) {
                $scope.cliente = result;
                $('#deleteClienteConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Cliente.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteClienteConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ClienteSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.clientes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cliente = {nombres: null, apellidoPaterno: null, apellidoMaterno: null, rfc: null, curp: null, sexo: null, nss: null, correo: null, telefono1: null, telefono2: null, celular: null, fechaNacimiento: null, numeroRefExterna: null, sistemaOrigen: null, tipoCliente: null, calle: null, numero: null, codigoPostal: null, colonia: null, ciudad: null, estado: null, municipio: null, geoLocalizacion: null, createdId: null, createdDate: null, updatedId: null, lastModifiedDate: null, secuenciaCliente: null, plazaId: null, id: null};
        };
    });

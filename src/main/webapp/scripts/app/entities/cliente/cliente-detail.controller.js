'use strict';

angular.module('cxcApp')
    .controller('ClienteDetailController', function ($scope, $rootScope, $stateParams, entity, Cliente, Cuenta, Plaza) {
        $scope.cliente = entity;
        $scope.load = function (id) {
            Cliente.get({id: id}, function(result) {
                $scope.cliente = result;
            });
        };
        $rootScope.$on('cxcApp:clienteUpdate', function(event, result) {
            $scope.cliente = result;
        });
    });

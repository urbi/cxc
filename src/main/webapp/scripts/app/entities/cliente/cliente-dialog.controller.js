'use strict';

angular.module('cxcApp').controller('ClienteDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Cliente', 'Cuenta', 'PlazaSearchByUser',
        function($scope, $stateParams, $modalInstance, entity, Cliente, Cuenta, PlazaSearchByUser) {

        $scope.cliente = entity;
        $scope.cuentas = Cuenta.query();
        $scope.plazas = PlazaSearchByUser.query();

        $scope.load = function(id) {
            Cliente.get({id : id}, function(result) {
                $scope.cliente = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:clienteUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.cliente.id != null) {
                Cliente.update($scope.cliente, onSaveFinished);
            } else {
                Cliente.save($scope.cliente, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };

            $scope.remoteUrlRequestFn = function(str) {
                str = str +'*';
                console.log(str);
                return {q: str};
            };
            console.log($scope.cliente);
            console.log(entity);
            console.log('Cliente: ' + $scope.cliente);

           // $scope.initialMunicipioSelected = entity2;
            $scope.municipioSelected = function(selected) {
                try{
                    $scope.cliente.municipio = selected.originalObject.nombre;
                }catch(e){
                    $scope.cliente.municipio = null;

                }
            };








}]);

'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cliente', {
                parent: 'entity',
                url: '/clientes',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cliente.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cliente/clientes.html',
                        controller: 'ClienteController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cliente');
                        $translatePartialLoader.addPart('sexoType');
                        $translatePartialLoader.addPart('sistemaOrigenType');
                        $translatePartialLoader.addPart('tipoClienteType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('cliente.detail', {
                parent: 'entity',
                url: '/cliente/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cliente.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cliente/cliente-detail.html',
                        controller: 'ClienteDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cliente');
                        $translatePartialLoader.addPart('sexoType');
                        $translatePartialLoader.addPart('sistemaOrigenType');
                        $translatePartialLoader.addPart('tipoClienteType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Cliente', function($stateParams, Cliente) {
                        return Cliente.get({id : $stateParams.id});
                    }]
                }
            })
            .state('cliente.new', {
                parent: 'cliente',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cliente/cliente-dialog.html',
                        controller: 'ClienteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombres: null, apellidoPaterno: null, apellidoMaterno: null, rfc: null, curp: null, sexo: null, nss: null, correo: null, telefono1: null, telefono2: null, celular: null, fechaNacimiento: null, numeroRefExterna: null, sistemaOrigen: null, tipoCliente: null, calle: null, numero: null, codigoPostal: null, colonia: null, ciudad: null, estado: null, municipio: null, geoLocalizacion: null, createdBy: null, createdDate: null, lastModifiedBy: null, lastModifiedDate: null, secuenciaCliente: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('cliente', null, { reload: true });
                    }, function() {
                        $state.go('cliente');
                    })
                }]
            })
            .state('cliente.edit', {
                parent: 'cliente',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cliente/cliente-dialog.html',
                        controller: 'ClienteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Cliente', function(Cliente) {
                                return Cliente.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('cliente', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp').controller('EmpresaDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Empresa', 'Cuenta', 'EmpresaDestinoPago',
        function($scope, $stateParams, $modalInstance, entity, Empresa, Cuenta, EmpresaDestinoPago) {

        $scope.empresa = entity;
        $scope.cuentas = Cuenta.query();
        $scope.empresadestinopagos = EmpresaDestinoPago.query();
        $scope.load = function(id) {
            Empresa.get({id : id}, function(result) {
                $scope.empresa = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:empresaUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.empresa.id != null) {
                Empresa.update($scope.empresa, onSaveFinished);
            } else {
                Empresa.save($scope.empresa, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

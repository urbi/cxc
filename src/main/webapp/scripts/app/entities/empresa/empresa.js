'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('empresa', {
                parent: 'entity',
                url: '/empresas',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.empresa.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/empresa/empresas.html',
                        controller: 'EmpresaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('empresa');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('empresa.detail', {
                parent: 'entity',
                url: '/empresa/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.empresa.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/empresa/empresa-detail.html',
                        controller: 'EmpresaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('empresa');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Empresa', function($stateParams, Empresa) {
                        return Empresa.get({id : $stateParams.id});
                    }]
                }
            })
            .state('empresa.new', {
                parent: 'empresa',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/empresa/empresa-dialog.html',
                        controller: 'EmpresaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, rfc: null, telefono: null, correo: null, direccion: null, codigoPostal: null, ciudad: null, municipio: null, estado: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('empresa', null, { reload: true });
                    }, function() {
                        $state.go('empresa');
                    })
                }]
            })
            .state('empresa.edit', {
                parent: 'empresa',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/empresa/empresa-dialog.html',
                        controller: 'EmpresaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Empresa', function(Empresa) {
                                return Empresa.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('empresa', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

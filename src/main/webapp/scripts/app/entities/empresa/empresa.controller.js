'use strict';

angular.module('cxcApp')
    .controller('EmpresaController', function ($scope, Empresa, EmpresaSearch, ParseLinks) {
        $scope.empresas = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Empresa.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.empresas = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Empresa.get({id: id}, function(result) {
                $scope.empresa = result;
                $('#deleteEmpresaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Empresa.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEmpresaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EmpresaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.empresas = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.empresa = {nombre: null, rfc: null, telefono: null, correo: null, direccion: null, codigoPostal: null, ciudad: null, municipio: null, estado: null, id: null};
        };
    });

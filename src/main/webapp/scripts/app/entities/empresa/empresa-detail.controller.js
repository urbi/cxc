'use strict';

angular.module('cxcApp')
    .controller('EmpresaDetailController', function ($scope, $rootScope, $stateParams, entity, Empresa, Cuenta, EmpresaDestinoPago) {
        $scope.empresa = entity;
        $scope.load = function (id) {
            Empresa.get({id: id}, function(result) {
                $scope.empresa = result;
            });
        };
        $rootScope.$on('cxcApp:empresaUpdate', function(event, result) {
            $scope.empresa = result;
        });
    });

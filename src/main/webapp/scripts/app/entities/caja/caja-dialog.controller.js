'use strict';

angular.module('cxcApp').controller('CajaDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Caja', 'User', 'Plaza', 'Pago',
        function($scope, $stateParams, $modalInstance, entity, Caja, User, Plaza, Pago) {

        $scope.caja = entity;
        $scope.users = User.query();
        $scope.plazas = Plaza.query();
        $scope.pagos = Pago.query();
        $scope.load = function(id) {
            Caja.get({id : id}, function(result) {
                $scope.caja = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:cajaUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.caja.id != null) {
                Caja.update($scope.caja, onSaveFinished);
            } else {
                Caja.save($scope.caja, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

'use strict';

angular.module('cxcApp')
    .controller('CajaController', function ($scope, Caja, CajaSearch, ParseLinks) {
        $scope.cajas = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Caja.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cajas = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Caja.get({id: id}, function(result) {
                $scope.caja = result;
                $('#deleteCajaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Caja.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCajaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CajaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.cajas = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.caja = {nombre: null, descripcion: null, ubicacion: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('caja', {
                parent: 'entity',
                url: '/cajas',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.caja.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/caja/cajas.html',
                        controller: 'CajaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('caja');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('caja.detail', {
                parent: 'entity',
                url: '/caja/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.caja.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/caja/caja-detail.html',
                        controller: 'CajaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('caja');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Caja', function($stateParams, Caja) {
                        return Caja.get({id : $stateParams.id});
                    }]
                }
            })
            .state('caja.new', {
                parent: 'caja',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/caja/caja-dialog.html',
                        controller: 'CajaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, descripcion: null, ubicacion: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('caja', null, { reload: true });
                    }, function() {
                        $state.go('caja');
                    })
                }]
            })
            .state('caja.edit', {
                parent: 'caja',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/caja/caja-dialog.html',
                        controller: 'CajaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Caja', function(Caja) {
                                return Caja.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('caja', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp')
    .controller('CajaDetailController', function ($scope, $rootScope, $stateParams, entity, Caja, User, Plaza, Pago) {
        $scope.caja = entity;
        $scope.load = function (id) {
            Caja.get({id: id}, function(result) {
                $scope.caja = result;
            });
        };
        $rootScope.$on('cxcApp:cajaUpdate', function(event, result) {
            $scope.caja = result;
        });
    });

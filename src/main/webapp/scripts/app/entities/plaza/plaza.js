'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('plaza', {
                parent: 'entity',
                url: '/plazas',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.plaza.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/plaza/plazas.html',
                        controller: 'PlazaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('plaza');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('plaza.detail', {
                parent: 'entity',
                url: '/plaza/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.plaza.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/plaza/plaza-detail.html',
                        controller: 'PlazaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('plaza');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Plaza', function($stateParams, Plaza) {
                        return Plaza.get({id : $stateParams.id});
                    }]
                }
            })
            .state('plaza.new', {
                parent: 'plaza',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/plaza/plaza-dialog.html',
                        controller: 'PlazaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, direccion: null, codigoPostal: null, ciudad: null, municipio: null, estado: null, region: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('plaza', null, { reload: true });
                    }, function() {
                        $state.go('plaza');
                    })
                }]
            })
            .state('plaza.edit', {
                parent: 'plaza',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/plaza/plaza-dialog.html',
                        controller: 'PlazaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Plaza', function(Plaza) {
                                return Plaza.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('plaza', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp')
    .controller('PlazaController', function ($scope, Plaza, PlazaSearch, ParseLinks) {
        $scope.plazas = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Plaza.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.plazas = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Plaza.get({id: id}, function(result) {
                $scope.plaza = result;
                $('#deletePlazaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Plaza.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePlazaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PlazaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.plazas = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.plaza = {nombre: null, direccion: null, codigoPostal: null, ciudad: null, municipio: null, estado: null, region: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp').controller('PlazaDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Plaza', 'Cliente', 'Cuenta', 'PagoReferenciado', 'Caja', 'User', 'EmpresaDestinoPago',
        function($scope, $stateParams, $modalInstance, entity, Plaza, Cliente, Cuenta, PagoReferenciado, Caja, User, EmpresaDestinoPago) {

        $scope.plaza = entity;
        $scope.clientes = Cliente.query();
        $scope.cuentas = Cuenta.query();
        $scope.pagoreferenciados = PagoReferenciado.query();
        $scope.cajas = Caja.query();
        $scope.users = User.query();
        $scope.empresadestinopagos = EmpresaDestinoPago.query();
        $scope.load = function(id) {
            Plaza.get({id : id}, function(result) {
                $scope.plaza = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:plazaUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.plaza.id != null) {
                Plaza.update($scope.plaza, onSaveFinished);
            } else {
                Plaza.save($scope.plaza, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

'use strict';

angular.module('cxcApp')
    .controller('PlazaDetailController', function ($scope, $rootScope, $stateParams, entity, Plaza, Cliente, Cuenta, PagoReferenciado, Caja, User, EmpresaDestinoPago) {
        $scope.plaza = entity;
        $scope.load = function (id) {
            Plaza.get({id: id}, function(result) {
                $scope.plaza = result;
            });
        };
        $rootScope.$on('cxcApp:plazaUpdate', function(event, result) {
            $scope.plaza = result;
        });
    });

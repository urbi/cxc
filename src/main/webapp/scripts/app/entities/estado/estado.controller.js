'use strict';

angular.module('cxcApp')
    .controller('EstadoController', function ($scope, Estado, EstadoSearch, ParseLinks) {
        $scope.estados = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Estado.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.estados = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Estado.get({id: id}, function(result) {
                $scope.estado = result;
                $('#deleteEstadoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Estado.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteEstadoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            EstadoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.estados = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.estado = {nombre: null, region: null, id: null};
        };
    });

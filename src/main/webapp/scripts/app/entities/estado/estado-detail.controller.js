'use strict';

angular.module('cxcApp')
    .controller('EstadoDetailController', function ($scope, $rootScope, $stateParams, entity, Estado, Municipio) {
        $scope.estado = entity;
        $scope.load = function (id) {
            Estado.get({id: id}, function(result) {
                $scope.estado = result;
            });
        };
        $rootScope.$on('cxcApp:estadoUpdate', function(event, result) {
            $scope.estado = result;
        });
    });

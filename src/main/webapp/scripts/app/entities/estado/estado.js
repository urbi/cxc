'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('estado', {
                parent: 'entity',
                url: '/estados',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.estado.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/estado/estados.html',
                        controller: 'EstadoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('estado');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('estado.detail', {
                parent: 'entity',
                url: '/estado/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.estado.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/estado/estado-detail.html',
                        controller: 'EstadoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('estado');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Estado', function($stateParams, Estado) {
                        return Estado.get({id : $stateParams.id});
                    }]
                }
            })
            .state('estado.new', {
                parent: 'estado',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/estado/estado-dialog.html',
                        controller: 'EstadoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, region: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('estado', null, { reload: true });
                    }, function() {
                        $state.go('estado');
                    })
                }]
            })
            .state('estado.edit', {
                parent: 'estado',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/estado/estado-dialog.html',
                        controller: 'EstadoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Estado', function(Estado) {
                                return Estado.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('estado', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

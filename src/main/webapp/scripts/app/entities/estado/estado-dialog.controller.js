'use strict';

angular.module('cxcApp').controller('EstadoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Estado', 'Municipio',
        function($scope, $stateParams, $modalInstance, entity, Estado, Municipio) {

        $scope.estado = entity;
        $scope.municipios = Municipio.query();
        $scope.load = function(id) {
            Estado.get({id : id}, function(result) {
                $scope.estado = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:estadoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.estado.id != null) {
                Estado.update($scope.estado, onSaveFinished);
            } else {
                Estado.save($scope.estado, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

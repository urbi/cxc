'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('secuenciaCliente', {
                parent: 'entity',
                url: '/secuenciaClientes',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.secuenciaCliente.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/secuenciaCliente/secuenciaClientes.html',
                        controller: 'SecuenciaClienteController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('secuenciaCliente');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('secuenciaCliente.detail', {
                parent: 'entity',
                url: '/secuenciaCliente/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.secuenciaCliente.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/secuenciaCliente/secuenciaCliente-detail.html',
                        controller: 'SecuenciaClienteDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('secuenciaCliente');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SecuenciaCliente', function($stateParams, SecuenciaCliente) {
                        return SecuenciaCliente.get({id : $stateParams.id});
                    }]
                }
            })
            .state('secuenciaCliente.new', {
                parent: 'secuenciaCliente',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/secuenciaCliente/secuenciaCliente-dialog.html',
                        controller: 'SecuenciaClienteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {tipoCliente: null, secuencia: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('secuenciaCliente', null, { reload: true });
                    }, function() {
                        $state.go('secuenciaCliente');
                    })
                }]
            })
            .state('secuenciaCliente.edit', {
                parent: 'secuenciaCliente',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/secuenciaCliente/secuenciaCliente-dialog.html',
                        controller: 'SecuenciaClienteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['SecuenciaCliente', function(SecuenciaCliente) {
                                return SecuenciaCliente.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('secuenciaCliente', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

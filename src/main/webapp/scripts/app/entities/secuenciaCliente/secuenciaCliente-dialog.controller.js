'use strict';

angular.module('cxcApp').controller('SecuenciaClienteDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'SecuenciaCliente',
        function($scope, $stateParams, $modalInstance, entity, SecuenciaCliente) {

        $scope.secuenciaCliente = entity;
        $scope.load = function(id) {
            SecuenciaCliente.get({id : id}, function(result) {
                $scope.secuenciaCliente = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:secuenciaClienteUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.secuenciaCliente.id != null) {
                SecuenciaCliente.update($scope.secuenciaCliente, onSaveFinished);
            } else {
                SecuenciaCliente.save($scope.secuenciaCliente, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

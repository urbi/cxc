'use strict';

angular.module('cxcApp')
    .controller('SecuenciaClienteDetailController', function ($scope, $rootScope, $stateParams, entity, SecuenciaCliente) {
        $scope.secuenciaCliente = entity;
        $scope.load = function (id) {
            SecuenciaCliente.get({id: id}, function(result) {
                $scope.secuenciaCliente = result;
            });
        };
        $rootScope.$on('cxcApp:secuenciaClienteUpdate', function(event, result) {
            $scope.secuenciaCliente = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .controller('SecuenciaClienteController', function ($scope, SecuenciaCliente, SecuenciaClienteSearch, ParseLinks) {
        $scope.secuenciaClientes = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            SecuenciaCliente.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.secuenciaClientes = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            SecuenciaCliente.get({id: id}, function(result) {
                $scope.secuenciaCliente = result;
                $('#deleteSecuenciaClienteConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            SecuenciaCliente.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSecuenciaClienteConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SecuenciaClienteSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.secuenciaClientes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.secuenciaCliente = {tipoCliente: null, secuencia: null, id: null};
        };
    });

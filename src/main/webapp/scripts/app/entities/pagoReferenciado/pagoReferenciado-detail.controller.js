'use strict';

angular.module('cxcApp')
    .controller('PagoReferenciadoDetailController', function ($scope, $rootScope, $stateParams, entity, PagoReferenciado, Plaza, Pago, Cuenta, DestinoPago) {
        $scope.pagoReferenciado = entity;
        $scope.load = function (id) {
            PagoReferenciado.get({id: id}, function(result) {
                $scope.pagoReferenciado = result;
            });
        };
        $rootScope.$on('cxcApp:pagoReferenciadoUpdate', function(event, result) {
            $scope.pagoReferenciado = result;
        });
    });

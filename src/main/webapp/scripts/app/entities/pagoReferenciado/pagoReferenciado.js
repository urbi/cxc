'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('pagoReferenciado', {
                parent: 'entity',
                url: '/pagoReferenciados',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.pagoReferenciado.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/pagoReferenciado/pagoReferenciados.html',
                        controller: 'PagoReferenciadoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pagoReferenciado');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('pagoReferenciado.detail', {
                parent: 'entity',
                url: '/pagoReferenciado/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.pagoReferenciado.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/pagoReferenciado/pagoReferenciado-detail.html',
                        controller: 'PagoReferenciadoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('pagoReferenciado');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'PagoReferenciado', function($stateParams, PagoReferenciado) {
                        return PagoReferenciado.get({id : $stateParams.id});
                    }]
                }
            })
            .state('pagoReferenciado.new', {
                parent: 'pagoReferenciado',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pagoReferenciado/pagoReferenciado-dialog.html',
                        controller: 'PagoReferenciadoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {montoPago: null, referencia: null, cuentaClabe: null, observaciones: null, fechaPago: null, fechaCargaPago: null, folioExternoConciliacion: null, folioCarga: null, convenio: null, fechaAplicacionPago: null, comentario: null, estatusPago: null, createdBy: null, createdDate: null, lastModifiedBy: null, lastModifiedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('pagoReferenciado', null, { reload: true });
                    }, function() {
                        $state.go('pagoReferenciado');
                    })
                }]
            })
            .state('pagoReferenciado.edit', {
                parent: 'pagoReferenciado',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/pagoReferenciado/pagoReferenciado-dialog.html',
                        controller: 'PagoReferenciadoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['PagoReferenciado', function(PagoReferenciado) {
                                return PagoReferenciado.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('pagoReferenciado', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

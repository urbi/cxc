'use strict';

angular.module('cxcApp').controller('PagoReferenciadoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'PagoReferenciado', 'Plaza', 'Pago', 'Cuenta', 'DestinoPago',
        function($scope, $stateParams, $modalInstance, entity, PagoReferenciado, Plaza, Pago, Cuenta, DestinoPago) {

        $scope.pagoReferenciado = entity;
        $scope.plazas = Plaza.query();
        $scope.pagos = Pago.query();
        $scope.cuentas = Cuenta.query();
        $scope.destinopagos = DestinoPago.query();
        $scope.load = function(id) {
            PagoReferenciado.get({id : id}, function(result) {
                $scope.pagoReferenciado = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:pagoReferenciadoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.pagoReferenciado.id != null) {
                PagoReferenciado.update($scope.pagoReferenciado, onSaveFinished);
            } else {
                PagoReferenciado.save($scope.pagoReferenciado, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

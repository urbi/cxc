'use strict';

angular.module('cxcApp')
    .controller('PagoReferenciadoController', function ($scope, PagoReferenciado, PagoReferenciadoSearch, ParseLinks) {
        $scope.pagoReferenciados = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            PagoReferenciado.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.pagoReferenciados = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            PagoReferenciado.get({id: id}, function(result) {
                $scope.pagoReferenciado = result;
                $('#deletePagoReferenciadoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            PagoReferenciado.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePagoReferenciadoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PagoReferenciadoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.pagoReferenciados = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.pagoReferenciado = {montoPago: null, referencia: null, cuentaClabe: null, observaciones: null, fechaPago: null, fechaCargaPago: null, folioExternoConciliacion: null, folioCarga: null, convenio: null, fechaAplicacionPago: null, comentario: null, estatusPago: null, createdBy: null, createdDate: null, lastModifiedBy: null, lastModifiedDate: null, id: null};
        };
    });

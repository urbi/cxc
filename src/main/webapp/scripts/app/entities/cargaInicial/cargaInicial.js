'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cargaInicial', {
                parent: 'entity',
                url: '/cargaInicials',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'cxcApp.cargaInicial.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cargaInicial/cargaInicials.html',
                        controller: 'CargaInicialController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cargaInicial');
                        $translatePartialLoader.addPart('tipoCargaType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('cargaInicial.detail', {
                parent: 'entity',
                url: '/cargaInicial/{id}',
                data: {
                    roles: ['ROLE_ADMIN'],
                    pageTitle: 'cxcApp.cargaInicial.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cargaInicial/cargaInicial-detail.html',
                        controller: 'CargaInicialDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cargaInicial');
                        $translatePartialLoader.addPart('tipoCargaType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'CargaInicial', function($stateParams, CargaInicial) {
                        return CargaInicial.get({id : $stateParams.id});
                    }]
                }
            })
            .state('cargaInicial.new', {
                parent: 'cargaInicial',
                url: '/new',
                data: {
                    roles: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cargaInicial/cargaInicial-dialog.html',
                        controller: 'CargaInicialDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {tipoCarga: null, archivo: null, mensaje: null, fechaCarga: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('cargaInicial', null, { reload: true });
                    }, function() {
                        $state.go('cargaInicial');
                    })
                }]
            })
            .state('cargaInicial.edit', {
                parent: 'cargaInicial',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_ADMIN'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cargaInicial/cargaInicial-dialog.html',
                        controller: 'CargaInicialDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['CargaInicial', function(CargaInicial) {
                                return CargaInicial.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('cargaInicial', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

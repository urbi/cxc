'use strict';

angular.module('cxcApp')
    .controller('CargaInicialDetailController', function ($scope, $rootScope, $stateParams, entity, CargaInicial) {
        $scope.cargaInicial = entity;
        $scope.load = function (id) {
            CargaInicial.get({id: id}, function(result) {
                $scope.cargaInicial = result;
            });
        };
        $rootScope.$on('cxcApp:cargaInicialUpdate', function(event, result) {
            $scope.cargaInicial = result;
        });
    });

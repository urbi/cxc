'use strict';

angular.module('cxcApp')
    .controller('FoliadorReciboPagoController', function ($scope, FoliadorReciboPago, FoliadorReciboPagoSearch, ParseLinks) {
        $scope.foliadorReciboPagos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            FoliadorReciboPago.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.foliadorReciboPagos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            FoliadorReciboPago.get({id: id}, function(result) {
                $scope.foliadorReciboPago = result;
                $('#deleteFoliadorReciboPagoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            FoliadorReciboPago.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteFoliadorReciboPagoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            FoliadorReciboPagoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.foliadorReciboPagos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.foliadorReciboPago = {secuencia: null, empresa: null, plaza: null, id: null};
        };
    });

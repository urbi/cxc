'use strict';

angular.module('cxcApp').controller('FoliadorReciboPagoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'FoliadorReciboPago',
        function($scope, $stateParams, $modalInstance, entity, FoliadorReciboPago) {

        $scope.foliadorReciboPago = entity;
        $scope.load = function(id) {
            FoliadorReciboPago.get({id : id}, function(result) {
                $scope.foliadorReciboPago = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:foliadorReciboPagoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.foliadorReciboPago.id != null) {
                FoliadorReciboPago.update($scope.foliadorReciboPago, onSaveFinished);
            } else {
                FoliadorReciboPago.save($scope.foliadorReciboPago, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

'use strict';

angular.module('cxcApp')
    .controller('FoliadorReciboPagoDetailController', function ($scope, $rootScope, $stateParams, entity, FoliadorReciboPago) {
        $scope.foliadorReciboPago = entity;
        $scope.load = function (id) {
            FoliadorReciboPago.get({id: id}, function(result) {
                $scope.foliadorReciboPago = result;
            });
        };
        $rootScope.$on('cxcApp:foliadorReciboPagoUpdate', function(event, result) {
            $scope.foliadorReciboPago = result;
        });
    });

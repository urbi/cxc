'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('foliadorReciboPago', {
                parent: 'entity',
                url: '/foliadorReciboPagos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.foliadorReciboPago.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/foliadorReciboPago/foliadorReciboPagos.html',
                        controller: 'FoliadorReciboPagoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('foliadorReciboPago');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('foliadorReciboPago.detail', {
                parent: 'entity',
                url: '/foliadorReciboPago/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.foliadorReciboPago.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/foliadorReciboPago/foliadorReciboPago-detail.html',
                        controller: 'FoliadorReciboPagoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('foliadorReciboPago');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'FoliadorReciboPago', function($stateParams, FoliadorReciboPago) {
                        return FoliadorReciboPago.get({id : $stateParams.id});
                    }]
                }
            })
            .state('foliadorReciboPago.new', {
                parent: 'foliadorReciboPago',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/foliadorReciboPago/foliadorReciboPago-dialog.html',
                        controller: 'FoliadorReciboPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {secuencia: null, empresa: null, plaza: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('foliadorReciboPago', null, { reload: true });
                    }, function() {
                        $state.go('foliadorReciboPago');
                    })
                }]
            })
            .state('foliadorReciboPago.edit', {
                parent: 'foliadorReciboPago',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/foliadorReciboPago/foliadorReciboPago-dialog.html',
                        controller: 'FoliadorReciboPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['FoliadorReciboPago', function(FoliadorReciboPago) {
                                return FoliadorReciboPago.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('foliadorReciboPago', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

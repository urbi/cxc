'use strict';

angular.module('cxcApp')
    .controller('MunicipioDetailController', function ($scope, $rootScope, $stateParams, entity, Municipio, Estado) {
        $scope.municipio = entity;
        $scope.load = function (id) {
            Municipio.get({id: id}, function(result) {
                $scope.municipio = result;
            });
        };
        $rootScope.$on('cxcApp:municipioUpdate', function(event, result) {
            $scope.municipio = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .controller('SelectMunicipioController',
    ['$scope', '$stateParams', '$modalInstance', 'Municipio', 'MunicipioSearch', 'ParseLinks','MunicipioSelected',
        function ($scope, $stateParams, $modalInstance, Municipio, MunicipioSearch, ParseLinks, MunicipioSelected) {
        $scope.municipios = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Municipio.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.municipios = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.select = function (municipio) {
            console.log('municipio with id ' + municipio.id + ' selected');

            $scope.selectedMunicipio = municipio;
            $scope.clear();

        };



        $scope.search = function () {
            MunicipioSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.municipios = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };


        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
    }]);

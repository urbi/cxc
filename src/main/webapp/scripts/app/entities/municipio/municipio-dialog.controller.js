'use strict';

angular.module('cxcApp').controller('MunicipioDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Municipio', 'Estado',
        function($scope, $stateParams, $modalInstance, entity, Municipio, Estado) {

        $scope.municipio = entity;
        $scope.estados = Estado.query();
        $scope.load = function(id) {
            Municipio.get({id : id}, function(result) {
                $scope.municipio = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:municipioUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.municipio.id != null) {
                Municipio.update($scope.municipio, onSaveFinished);
            } else {
                Municipio.save($scope.municipio, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

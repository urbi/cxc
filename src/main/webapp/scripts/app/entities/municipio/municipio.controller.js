'use strict';

angular.module('cxcApp')
    .controller('MunicipioController', function ($scope, Municipio, MunicipioSearch, ParseLinks) {
        $scope.municipios = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Municipio.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.municipios = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Municipio.get({id: id}, function(result) {
                $scope.municipio = result;
                $('#deleteMunicipioConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Municipio.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteMunicipioConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            MunicipioSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.municipios = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.municipio = {nombre: null, id: null};
        };
    });

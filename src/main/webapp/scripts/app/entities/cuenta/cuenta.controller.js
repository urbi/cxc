'use strict';

angular.module('cxcApp')
    .controller('CuentaController', function ($scope, Cuenta, CuentaSearch, ParseLinks) {
        $scope.cuentas = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Cuenta.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cuentas = result;
            });
        };
        $scope.printShow = false;
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Cuenta.get({id: id}, function(result) {
                $scope.cuenta = result;
                $('#deleteCuentaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Cuenta.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCuentaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CuentaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.cuentas = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cuenta = {secuenciaCuenta: null, plazaId:null, empresaId:null, fechaCreacion: null, saldo: null, montoExigible: null, saldoInicial: null, fechaUltimoPago: null, numeroContratoOrigen: null, descripcion: null, attribute1: null, attribute2: null, tipoCuenta: null, estatusCuenta: null, createdDate: null, lastModifiedDate: null, createdBy: null, lastModifiedBy: null, id: null};
        };
    });

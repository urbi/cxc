'use strict';

angular.module('cxcApp').controller('CuentaDialogController',
    ['$scope','$rootScope', '$filter','$state', '$stateParams',  'entity','Account', 'Cuenta', 'Cliente', 'Plaza', 'Nota',  'Empresa', 'CuentaDestinoPago', 'Movimiento',
        function($scope, $rootScope, $filter, $state, $stateParams,  entity, Account, Cuenta, Cliente, Plaza, Nota,  Empresa, CuentaDestinoPago, Movimiento ) {



        $scope.cuenta = entity;
        $scope.clientes = Cliente.query();
        $scope.plazas = Plaza.query({page: 1, per_page:40});
        $scope.notas = Nota.query();

        $scope.empresas = Empresa.query();
        $scope.cuentadestinopagos = CuentaDestinoPago.query();
        //para diferencia el movimiento  de pago., cuando es un pago se pide campo adicional. qeu no se despliega con los otros

        $scope.printShow = false;
        $scope.isDisableSave = false;
        $scope.pagoRecibo =[];
        $scope.clienteRecibo = [];
        $scope.empresaRecibo = [];
        $scope.account = [];

        $scope.load = function(id) {

            Cuenta.get({id : id}, function(result) {
                $scope.cuenta = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:cuentaUpdate', result);
            //$modalInstance.close(result);
            $state.go('cuenta');

        };

        $scope.save = function () {
            console.log("que pddo" + $scope.isDisableSave);
            $scope.isDisableSave = true;
            if ($scope.cuenta.id != null) {
                Cuenta.update($scope.cuenta, onSaveFinished);
            } else {

                Cuenta.save($scope.cuenta, onSaveFinished);
            }
        };

        var onSaveReload = function (result){
            console.log(result);
            Cuenta.get({id : $scope.cuenta.id}, function(result) {
                $scope.cuenta = result;
            });
        };

        $scope.addPago = function (cuentaId, monto, conceptoMovimiento){

            //var pago = { 'numeroPagare':$scope.numeroPagare, 'conceptoMovimiento': $scope.conceptoMovimiento, 'tipoMovimiento':tipoMovimiento, 'monto':$scope.monto,'fechaVencimiento':$scope.fechaVencimiento, 'estatusMovimiento':estatusMovimiento}
            var pago = {cuentaId:null,monto: null,saldo:null, numeroPagare: null, numeroPago: null, fechaVencimiento: null, fechaPago: null,
                estatusMovimiento: null, secuenciaMovimiento: null, tipoPago: null, conceptoMovimiento: null, id: null};

            pago.monto = monto;
            pago.saldo = monto;
            pago.fechaVencimiento = $scope.fechaVencimiento;
            pago.fechaPago = $scope.fechaVencimiento;
            pago.estatusMovimiento = $scope.estatusMovimiento;
            pago.conceptoMovimiento = conceptoMovimiento;
            pago.tipoPago = $scope.tipoPago;
            pago.cuentaId = $scope.cuenta.id;
            console.log( pago);
            Movimiento.save(pago,onSaveReload);

            console.log($scope.cuenta);
            $scope.monto2 = null;
            $scope.tipoPago = null;
            $scope.monto = null;


        };

        $scope.clear = function() {
            $('#myModal2').modal('hide');
            $modalInstance.dismiss('cancel');
        };

        $scope.conceptoChange = function(){


            if($scope.conceptoMovimiento == "PAGO"){
                $scope.fechaVencimiento = new Date();
                $scope.checked = false;


            }else{
                $scope.checked = true;
            }


        };

            $scope.addMovimiento = function(){
                var index = -1;
                console.log("Cuenta : " +$scope.cuenta);
                //TODO: mejorar el manejo de la excepcion a lo mejor se puede hacer un trasnlate
                if($scope.monto == null || $scope.conceptoMovimiento ==  null|| $scope.fechaVencimiento ==  null ){

                    alert("Ninguno de los campos puede estar vacio");
                    return;
                }
                if($scope.conceptoMovimiento == "PAGO" && $scope.tipoPago==null){
                    alert("Ninguno de los campos puede estar vacio.");
                    return;
                }
               console.log($scope.monto2 != $scope.monto );
                console.log($scope.monto2 + " " + $scope.monto );
                if($scope.conceptoMovimiento == "PAGO" && $scope.monto2 !== $scope.monto ){
                    console.log("entro");
                    $scope.muestraDialogoPago = true;
                    return;
                }
                if($scope.conceptoMovimiento == "PAGO" && $scope.monto2 != null && $scope.confirmoPago == true){
                    $scope.muestraDialogoPago = false;
                    $scope.addPago($scope.cuenta.id, $scope.monto, $scope.conceptoMovimiento);
                    return;
                }




                var movimientoArray = eval( $scope.cuenta.movimientoDTOs );
                /*Inicicializamos valores por defult*/
                if(movimientoArray != null){
                    console.log('entro por primera vez');
                    $scope.numeroPagare = movimientoArray.length +1;

                    if($scope.numeroPagare == 1){
                        $scope.numeroPagare = 1;
                        $scope.cuenta.saldo = 0;
                        $scope.cuenta.saldoInicial = 0
                        $scope.cuenta.montoExigible =0;
                        $scope.cuenta.interesAnual = 0.0;

                    }

                }


                var tipoMovimiento = $scope.conceptoMovimiento == "PAGO" ? "ABONO" : "CARGO";

                $scope.getDatetime = new Date();
                $scope.getDatetime.setDate($scope.getDatetime.getDate()-1);
                console.log('Time : ' + $scope.getDatetime);
                var estatusMovimiento =  $scope.fechaVencimiento >= $scope.getDatetime ? 'CORRIENTE' : 'VENCIDO';

                $scope.cuenta.plazoMeses =$scope.numeroPagare;
                $scope.cuenta.saldo = $scope.cuenta.saldo + $scope.monto;

                $scope.cuenta.saldo =parseFloat( $scope.cuenta.saldo.toFixed(2));
                $scope.cuenta.saldoInicial = $scope.cuenta.saldo ;


                if($scope.estatusMovimiento =='VENCIDO') $scope.montoExigible = parseFloat($scope.montoExigible.toFixed(2) + $scope.monto.toFixed(2));
                $scope.cuenta.movimientoDTOs.push({ 'numeroPagare':$scope.numeroPagare, 'conceptoMovimiento': $scope.conceptoMovimiento, 'tipoMovimiento':tipoMovimiento, 'monto':$scope.monto,'saldo':$scope.monto,'fechaVencimiento':$scope.fechaVencimiento, 'estatusMovimiento':estatusMovimiento});


                $scope.numeroPagare='';
                $scope.conceptoMovimiento='';
                $scope.monto='';
                $scope.fechaVencimiento='';
                $scope.estatusMovimiento = '';
                $scope.movimientos = eval($scope.movimientos);

               // $scope.cuenta.movimientoDTOs = $scope.movimientos;

                console.log($scope.cuenta);
            };


            $scope.removeMovimiento = function(numeroPagare){
                var index = -1;
                var movimientoArray = eval( $scope.cuenta.movimientoDTOs );
                for( var i = 0; i < movimientoArray.length; i++ ) {
                    if( movimientoArray[i].numeroPagare === numeroPagare ) {
                        index = i;
                        break;
                    }
                }
                if( index === -1 ) {
                    alert( "Something gone wrong" );
                }
                $scope.cuenta.plazoMeses =$scope.numeroPagare;
                $scope.cuenta.saldo = $scope.cuenta.saldo - movimientoArray[index].monto;
                $scope.cuenta.saldo = parseFloat($scope.cuenta.saldo.toFixed(2) );
                $scope.cuenta.saldoInicial = $scope.cuenta.saldo ;
                if($scope.estatusMovimiento =='VENCIDO') $scope.montoExigible = parseFloat($scope.montoExigible.toFixed(2) -  movimientoArray[index].monto.toFixed(2));

                $scope.cuenta.movimientoDTOs.splice( index, 1 );
                $scope.cuenta.plazoMeses = movimientoArray.length

                for(var i= 0; i < $scope.cuenta.movimientoDTOs; i++ ) {
                    $scope.cuenta.movimientoDTOs[i].numeroPagare = i+1;
                }

            };

            function Comparator(a,b){
                console.log(a[1]);
                if (a[3] < b[3]) return -1;
                if (a[3] > b[3]) return 1;
                return 0;
            }

            $scope.remoteUrlRequestFn = function(str) {

                return {q: str+'*'};
            };


            // $scope.initialClienteSelected = entity2;
            $scope.clienteSelected = function(selected) {
                try{
                    console.log(selected.originalObject);
                    $scope.cuenta.clienteId= selected.originalObject.id;
                }catch(e){
                    $scope.cuenta.cliente= null;

                }
            };
            //TODO: EL recibo debe ser impreso en PDF para evitar hacks al mismo mediante la modificacion del DOM.
            $scope.impresionReciboPago = function(id){
                $scope.printShow = true;

                var index = -1;
                var movimientoArray = eval( $scope.cuenta.movimientoDTOs );
                for( var i = 0; i < movimientoArray.length; i++ ) {
                    if( movimientoArray[i].id === id ) {
                        index = i;
                        break;
                    }
                }
                if( index === -1 ) {
                    alert( "Something gone wrong" );
                }
                $scope.pagoRecibo = movimientoArray[index];

                //  $scope.printShow = true;
                  $scope.reciboPagoId =id;
                $('#myModal2').modal('show');

               // $scope.pago = Movimiento.get({id:$scope.pago.id});
                console.log("pago : "+$scope.pagoRecibo);
                $scope.empresaRecibo = Empresa.get({id:$scope.cuenta.empresaId});
                $scope.clienteRecibo = Cliente.get({id:$scope.cuenta.clienteId});
                $scope.plazaRecibo   = Plaza.get({id:$scope.cuenta.plazaId});
                $scope.account = Account.get();
                $scope.fechaActual = $filter('date')(new Date(), 'fullDate');
                console.log($scope.pagoRecibo.fechaPago);
                $scope.reciboFechaPago = $filter('date')($scope.pagoRecibo.fechaPago, 'fullDate');





            };
            $scope.closeRecibo = function(){

                //  $scope.printShow = true;

                $('#myModal2').modal('hide');





            };

            $scope.estructuraCuentaValid = function(){
                var movimientoArray = eval( $scope.movimientos );
                if(movimientoArray == null ) return false;

                if($scope.cuenta.tipoDocumento=="ENGANCHE"){
                    var hasCredito = false;
                    var hasEnganche = false;
                    var hasManyCreditos = false;
                    for( var i = 0; i < movimientoArray.length; i++ ) {
                        if(movimientoArray[i].conceptoMovimiento =="CREDITO"){
                            hasManyCreditos = hasCredito ? true : false;
                            hasCredito = true;
                        }
                    }

                    if(hasCredito && hasEnganche && hasManyCreditos) return true;
                    return false;
                }

                if($scope.cuenta.tipoDocumento=="ESCRITURA"){
                    var hasESCRITURA= false;
                    var hasEnganche = false;
                    var hasManyCreditos = false;
                    for( var i = 0; i < movimientoArray.length; i++ ) {
                        if(movimientoArray[i].conceptoMovimiento =="CREDITO"){
                            hasManyCreditos = hasCredito ? true : false;
                            hasCredito = true;
                        }
                    }

                    if(hasCredito && hasEnganche && hasManyCreditos) return true;
                    return false;
                }

            };
            $scope.orderByDate = function(){
                $scope.movimientos = $filter
                $scope.movimientos = $scope.movimientos.sortBy()
            };




            }]);

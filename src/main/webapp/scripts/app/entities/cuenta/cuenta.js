'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cuenta', {
                parent: 'entity',
                url: '/cuentas',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cuenta.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cuenta/cuentas.html',
                        controller: 'CuentaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cuenta');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('cuenta.detail', {
                parent: 'entity',
                url: '/cuenta/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cuenta.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cuenta/cuenta-detail.html',
                        controller: 'CuentaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cuenta');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Cuenta', function($stateParams, Cuenta) {
                        return Cuenta.get({id : $stateParams.id});
                    }]
                }
            }).state('cuenta.edit', {
                parent: 'cuenta',
                url: 'cuenta/{id}/edit',
                data: {
                    roles: ['ROLE_USER']
                },
                views: {
                    'content@' : {
                        templateUrl: 'scripts/app/entities/cuenta/cuenta-dialog.html',
                        controller: 'CuentaDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cuenta');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Cuenta', function($stateParams, Cuenta) {
                        return Cuenta.get({id : $stateParams.id});
                    }]
                }

            })
            .state('cuenta.enganche', {
                parent: 'cuenta',
                url: '/new',
                data: {
                    roles: ['ROLE_USER']
                },
                views: {
                    'content@' : {
                        templateUrl: 'scripts/app/entities/cuenta/cuenta-dialog.html',
                        controller: 'CuentaDialogController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cuenta');
                        return $translate.refresh();
                    }],
                    entity: function () {
                        return { saldo: null, montoExigible: null, saldoInicial: null, fechaUltimoPago: null,
                            numeroContratoOrigen: null, descripcion: null, attribute1: null, attribute2: null,
                            tipoDocumento: 'ENGANCHE', estatusCuenta: 'ACTIVADA', createdDate: null, lastModifiedDate: null,
                            plazaId: null, empresaId:null, isCuentaValid:false, created: null, lastModified: null, id: null,
                            movimientoDTOs:[],notaDTOs:[], cuentaDestinoPagoDTOs:[],isNew:true};
                    }
                }

            });



    });

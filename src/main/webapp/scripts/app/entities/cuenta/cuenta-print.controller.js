'use strict';

angular.module('cxcApp')
    .controller('CuentaDetailController', function ($scope, $rootScope, $stateParams, entity, Cuenta, Cliente, Plaza, Nota,  Empresa, CuentaDestinoPago, Movimiento, Documento, PagoReferenciado) {
        $scope.cuenta = entity;
        $scope.load = function (id) {
            Cuenta.get({id: id}, function(result) {
                $scope.cuenta = result;
            });
        };
        $rootScope.$on('cxcApp:cuentaUpdate', function(event, result) {
            $scope.cuenta = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .controller('CxcSecuenciaDetailController', function ($scope, $rootScope, $stateParams, entity, CxcSecuencia) {
        $scope.cxcSecuencia = entity;
        $scope.load = function (id) {
            CxcSecuencia.get({id: id}, function(result) {
                $scope.cxcSecuencia = result;
            });
        };
        $rootScope.$on('cxcApp:cxcSecuenciaUpdate', function(event, result) {
            $scope.cxcSecuencia = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .controller('CxcSecuenciaController', function ($scope, CxcSecuencia, CxcSecuenciaSearch) {
        $scope.cxcSecuencias = [];
        $scope.loadAll = function() {
            CxcSecuencia.query(function(result) {
               $scope.cxcSecuencias = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            CxcSecuencia.get({id: id}, function(result) {
                $scope.cxcSecuencia = result;
                $('#deleteCxcSecuenciaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            CxcSecuencia.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCxcSecuenciaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CxcSecuenciaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.cxcSecuencias = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cxcSecuencia = {nombre: null, secuencia: null, id: null};
        };
    });

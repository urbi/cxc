'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cxcSecuencia', {
                parent: 'entity',
                url: '/cxcSecuencias',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cxcSecuencia.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cxcSecuencia/cxcSecuencias.html',
                        controller: 'CxcSecuenciaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cxcSecuencia');
                        $translatePartialLoader.addPart('secuenciaType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('cxcSecuencia.detail', {
                parent: 'entity',
                url: '/cxcSecuencia/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cxcSecuencia.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cxcSecuencia/cxcSecuencia-detail.html',
                        controller: 'CxcSecuenciaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cxcSecuencia');
                        $translatePartialLoader.addPart('secuenciaType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'CxcSecuencia', function($stateParams, CxcSecuencia) {
                        return CxcSecuencia.get({id : $stateParams.id});
                    }]
                }
            })
            .state('cxcSecuencia.new', {
                parent: 'cxcSecuencia',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cxcSecuencia/cxcSecuencia-dialog.html',
                        controller: 'CxcSecuenciaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, secuencia: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('cxcSecuencia', null, { reload: true });
                    }, function() {
                        $state.go('cxcSecuencia');
                    })
                }]
            })
            .state('cxcSecuencia.edit', {
                parent: 'cxcSecuencia',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cxcSecuencia/cxcSecuencia-dialog.html',
                        controller: 'CxcSecuenciaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['CxcSecuencia', function(CxcSecuencia) {
                                return CxcSecuencia.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('cxcSecuencia', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

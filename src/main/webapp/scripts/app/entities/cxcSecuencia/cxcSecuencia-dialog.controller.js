'use strict';

angular.module('cxcApp').controller('CxcSecuenciaDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'CxcSecuencia',
        function($scope, $stateParams, $modalInstance, entity, CxcSecuencia) {

        $scope.cxcSecuencia = entity;
        $scope.load = function(id) {
            CxcSecuencia.get({id : id}, function(result) {
                $scope.cxcSecuencia = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:cxcSecuenciaUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.cxcSecuencia.id != null) {
                CxcSecuencia.update($scope.cxcSecuencia, onSaveFinished);
            } else {
                CxcSecuencia.save($scope.cxcSecuencia, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

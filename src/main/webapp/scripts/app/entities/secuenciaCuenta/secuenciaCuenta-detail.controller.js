'use strict';

angular.module('cxcApp')
    .controller('SecuenciaCuentaDetailController', function ($scope, $rootScope, $stateParams, entity, SecuenciaCuenta) {
        $scope.secuenciaCuenta = entity;
        $scope.load = function (id) {
            SecuenciaCuenta.get({id: id}, function(result) {
                $scope.secuenciaCuenta = result;
            });
        };
        $rootScope.$on('cxcApp:secuenciaCuentaUpdate', function(event, result) {
            $scope.secuenciaCuenta = result;
        });
    });

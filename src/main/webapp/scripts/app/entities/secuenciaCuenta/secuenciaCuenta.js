'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('secuenciaCuenta', {
                parent: 'entity',
                url: '/secuenciaCuentas',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.secuenciaCuenta.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/secuenciaCuenta/secuenciaCuentas.html',
                        controller: 'SecuenciaCuentaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('secuenciaCuenta');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('secuenciaCuenta.detail', {
                parent: 'entity',
                url: '/secuenciaCuenta/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.secuenciaCuenta.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/secuenciaCuenta/secuenciaCuenta-detail.html',
                        controller: 'SecuenciaCuentaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('secuenciaCuenta');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'SecuenciaCuenta', function($stateParams, SecuenciaCuenta) {
                        return SecuenciaCuenta.get({id : $stateParams.id});
                    }]
                }
            })
            .state('secuenciaCuenta.new', {
                parent: 'secuenciaCuenta',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/secuenciaCuenta/secuenciaCuenta-dialog.html',
                        controller: 'SecuenciaCuentaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {tipoCuenta: null, secuencia: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('secuenciaCuenta', null, { reload: true });
                    }, function() {
                        $state.go('secuenciaCuenta');
                    })
                }]
            })
            .state('secuenciaCuenta.edit', {
                parent: 'secuenciaCuenta',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/secuenciaCuenta/secuenciaCuenta-dialog.html',
                        controller: 'SecuenciaCuentaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['SecuenciaCuenta', function(SecuenciaCuenta) {
                                return SecuenciaCuenta.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('secuenciaCuenta', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

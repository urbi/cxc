'use strict';

angular.module('cxcApp')
    .controller('SecuenciaCuentaController', function ($scope, SecuenciaCuenta, SecuenciaCuentaSearch, ParseLinks) {
        $scope.secuenciaCuentas = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            SecuenciaCuenta.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.secuenciaCuentas = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            SecuenciaCuenta.get({id: id}, function(result) {
                $scope.secuenciaCuenta = result;
                $('#deleteSecuenciaCuentaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            SecuenciaCuenta.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSecuenciaCuentaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SecuenciaCuentaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.secuenciaCuentas = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.secuenciaCuenta = {tipoCuenta: null, secuencia: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp').controller('SecuenciaCuentaDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'SecuenciaCuenta',
        function($scope, $stateParams, $modalInstance, entity, SecuenciaCuenta) {

        $scope.secuenciaCuenta = entity;
        $scope.load = function(id) {
            SecuenciaCuenta.get({id : id}, function(result) {
                $scope.secuenciaCuenta = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:secuenciaCuentaUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.secuenciaCuenta.id != null) {
                SecuenciaCuenta.update($scope.secuenciaCuenta, onSaveFinished);
            } else {
                SecuenciaCuenta.save($scope.secuenciaCuenta, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

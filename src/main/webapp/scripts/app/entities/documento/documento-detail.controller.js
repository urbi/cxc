'use strict';

angular.module('cxcApp')
    .controller('DocumentoDetailController', function ($scope, $rootScope, $stateParams, entity, Documento, Cuenta) {
        $scope.documento = entity;
        $scope.load = function (id) {
            Documento.get({id: id}, function(result) {
                $scope.documento = result;
            });
        };
        $rootScope.$on('cxcApp:documentoUpdate', function(event, result) {
            $scope.documento = result;
        });
    });

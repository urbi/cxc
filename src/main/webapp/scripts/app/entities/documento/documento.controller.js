'use strict';

angular.module('cxcApp')
    .controller('DocumentoController', function ($scope, Documento, DocumentoSearch, ParseLinks) {
        $scope.documentos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Documento.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.documentos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Documento.get({id: id}, function(result) {
                $scope.documento = result;
                $('#deleteDocumentoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Documento.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDocumentoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            DocumentoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.documentos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.documento = {numeroDocumento: null, descripcion: null, tipoDocumento: null, attribute1: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('documento', {
                parent: 'entity',
                url: '/documentos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.documento.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/documento/documentos.html',
                        controller: 'DocumentoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('documento');
                        $translatePartialLoader.addPart('tipoDocumentoType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('documento.detail', {
                parent: 'entity',
                url: '/documento/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.documento.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/documento/documento-detail.html',
                        controller: 'DocumentoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('documento');
                        $translatePartialLoader.addPart('tipoDocumentoType');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Documento', function($stateParams, Documento) {
                        return Documento.get({id : $stateParams.id});
                    }]
                }
            })
            .state('documento.new', {
                parent: 'documento',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/documento/documento-dialog.html',
                        controller: 'DocumentoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {numeroDocumento: null, descripcion: null, tipoDocumento: null, attribute1: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('documento', null, { reload: true });
                    }, function() {
                        $state.go('documento');
                    })
                }]
            })
            .state('documento.edit', {
                parent: 'documento',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/documento/documento-dialog.html',
                        controller: 'DocumentoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Documento', function(Documento) {
                                return Documento.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('documento', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp').controller('DocumentoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Documento', 'Cuenta',
        function($scope, $stateParams, $modalInstance, entity, Documento, Cuenta) {

        $scope.documento = entity;
        $scope.cuentas = Cuenta.query();
        $scope.load = function(id) {
            Documento.get({id : id}, function(result) {
                $scope.documento = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:documentoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.documento.id != null) {
                Documento.update($scope.documento, onSaveFinished);
            } else {
                Documento.save($scope.documento, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

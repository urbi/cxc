'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('movimiento', {
                parent: 'entity',
                url: '/movimientos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.movimiento.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/movimiento/movimientos.html',
                        controller: 'MovimientoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('movimiento');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('movimiento.detail', {
                parent: 'entity',
                url: '/movimiento/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.movimiento.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/movimiento/movimiento-detail.html',
                        controller: 'MovimientoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('movimiento');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Movimiento', function($stateParams, Movimiento) {
                        return Movimiento.get({id : $stateParams.id});
                    }]
                }
            })
            .state('movimiento.print', {
                parent: 'entity',
                url: '/movimientoPrint/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.movimiento.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/movimiento/movimiento-print.html',
                        controller: 'MovimientoPrintController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('movimiento');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Movimiento', function($stateParams, Movimiento) {
                        var movimiento = Movimiento.get({id : $stateParams.id});

                        return movimiento;
                    }]
                }
            })
            .state('movimiento.new', {
                parent: 'movimiento',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/movimiento/movimiento-dialog.html',
                        controller: 'MovimientoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {monto: null, numeroPagare: null, numeroPago: null, fechaVencimiento: null, fechaPago: null, estatusMovimiento: null, secuenciaMovimiento: null, tipoPago: null, tipoMovimiento: null, conceptoMovimiento: null, createdDate: null, lastModifiedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('movimiento', null, { reload: true });
                    }, function() {
                        $state.go('movimiento');
                    })
                }]
            })
            .state('movimiento.edit', {
                parent: 'movimiento',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/movimiento/movimiento-dialog.html',
                        controller: 'MovimientoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Movimiento', function(Movimiento) {
                                return Movimiento.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('movimiento', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });


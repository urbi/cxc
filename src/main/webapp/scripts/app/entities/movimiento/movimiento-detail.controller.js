'use strict';

angular.module('cxcApp')
    .controller('MovimientoDetailController', function ($scope, $rootScope, $stateParams, entity, Movimiento, Cuenta) {
        $scope.movimiento = entity;
        $scope.load = function (id) {
            Movimiento.get({id: id}, function(result) {
                $scope.movimiento = result;
            });
        };
        $rootScope.$on('cxcApp:movimientoUpdate', function(event, result) {
            $scope.movimiento = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .controller('MovimientoController', function ($scope, Movimiento, MovimientoSearch, ParseLinks) {
        $scope.movimientos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Movimiento.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.movimientos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Movimiento.get({id: id}, function(result) {
                $scope.movimiento = result;
                $('#deleteMovimientoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Movimiento.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteMovimientoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            MovimientoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.movimientos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.movimiento = {monto: null, numeroPagare: null, numeroPago: null, fechaVencimiento: null, fechaPago: null, estatusMovimiento: null, secuenciaMovimiento: null, tipoPago: null, tipoMovimiento: null, conceptoMovimiento: null, createdDate: null, lastModifiedDate: null, id: null};
        };
    });

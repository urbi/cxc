'use strict';

angular.module('cxcApp').controller('MovimientoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Movimiento', 'Cuenta',
        function($scope, $stateParams, $modalInstance, entity, Movimiento, Cuenta) {

        $scope.movimiento = entity;
        $scope.cuentas = Cuenta.query();
        $scope.load = function(id) {
            Movimiento.get({id : id}, function(result) {
                $scope.movimiento = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:movimientoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.movimiento.id != null) {
                Movimiento.update($scope.movimiento, onSaveFinished);
            } else {
                Movimiento.save($scope.movimiento, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

'use strict';

angular.module('cxcApp')
    .controller('DestinoPagoController', function ($scope, DestinoPago, DestinoPagoSearch, ParseLinks) {
        $scope.destinoPagos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            DestinoPago.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.destinoPagos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            DestinoPago.get({id: id}, function(result) {
                $scope.destinoPago = result;
                $('#deleteDestinoPagoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            DestinoPago.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteDestinoPagoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            DestinoPagoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.destinoPagos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.destinoPago = {nombre: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp').controller('DestinoPagoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'DestinoPago', 'EmpresaDestinoPago', 'CuentaDestinoPago', 'PagoReferenciado',
        function($scope, $stateParams, $modalInstance, entity, DestinoPago, EmpresaDestinoPago, CuentaDestinoPago, PagoReferenciado) {

        $scope.destinoPago = entity;
        $scope.empresadestinopagos = EmpresaDestinoPago.query();
        $scope.cuentadestinopagos = CuentaDestinoPago.query();
        $scope.pagoreferenciados = PagoReferenciado.query();
        $scope.load = function(id) {
            DestinoPago.get({id : id}, function(result) {
                $scope.destinoPago = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:destinoPagoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.destinoPago.id != null) {
                DestinoPago.update($scope.destinoPago, onSaveFinished);
            } else {
                DestinoPago.save($scope.destinoPago, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

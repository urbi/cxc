'use strict';

angular.module('cxcApp')
    .controller('DestinoPagoDetailController', function ($scope, $rootScope, $stateParams, entity, DestinoPago, EmpresaDestinoPago, CuentaDestinoPago, PagoReferenciado) {
        $scope.destinoPago = entity;
        $scope.load = function (id) {
            DestinoPago.get({id: id}, function(result) {
                $scope.destinoPago = result;
            });
        };
        $rootScope.$on('cxcApp:destinoPagoUpdate', function(event, result) {
            $scope.destinoPago = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('destinoPago', {
                parent: 'entity',
                url: '/destinoPagos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.destinoPago.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/destinoPago/destinoPagos.html',
                        controller: 'DestinoPagoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('destinoPago');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('destinoPago.detail', {
                parent: 'entity',
                url: '/destinoPago/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.destinoPago.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/destinoPago/destinoPago-detail.html',
                        controller: 'DestinoPagoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('destinoPago');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'DestinoPago', function($stateParams, DestinoPago) {
                        return DestinoPago.get({id : $stateParams.id});
                    }]
                }
            })
            .state('destinoPago.new', {
                parent: 'destinoPago',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/destinoPago/destinoPago-dialog.html',
                        controller: 'DestinoPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('destinoPago', null, { reload: true });
                    }, function() {
                        $state.go('destinoPago');
                    })
                }]
            })
            .state('destinoPago.edit', {
                parent: 'destinoPago',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/destinoPago/destinoPago-dialog.html',
                        controller: 'DestinoPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['DestinoPago', function(DestinoPago) {
                                return DestinoPago.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('destinoPago', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp')
    .controller('NotaController', function ($scope, Nota, NotaSearch, ParseLinks) {
        $scope.notas = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Nota.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.notas = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Nota.get({id: id}, function(result) {
                $scope.nota = result;
                $('#deleteNotaConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Nota.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteNotaConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            NotaSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.notas = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.nota = {titulo: null, descripcion: null, fechaCreacion: null, id: null};
        };
    });

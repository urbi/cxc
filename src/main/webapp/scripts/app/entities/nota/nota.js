'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('nota', {
                parent: 'entity',
                url: '/notas',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.nota.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/nota/notas.html',
                        controller: 'NotaController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('nota');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('nota.detail', {
                parent: 'entity',
                url: '/nota/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.nota.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/nota/nota-detail.html',
                        controller: 'NotaDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('nota');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Nota', function($stateParams, Nota) {
                        return Nota.get({id : $stateParams.id});
                    }]
                }
            })
            .state('nota.new', {
                parent: 'nota',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/nota/nota-dialog.html',
                        controller: 'NotaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {titulo: null, descripcion: null, fechaCreacion: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('nota', null, { reload: true });
                    }, function() {
                        $state.go('nota');
                    })
                }]
            })
            .state('nota.edit', {
                parent: 'nota',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/nota/nota-dialog.html',
                        controller: 'NotaDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Nota', function(Nota) {
                                return Nota.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('nota', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

'use strict';

angular.module('cxcApp')
    .controller('NotaDetailController', function ($scope, $rootScope, $stateParams, entity, Nota, Cuenta) {
        $scope.nota = entity;
        $scope.load = function (id) {
            Nota.get({id: id}, function(result) {
                $scope.nota = result;
            });
        };
        $rootScope.$on('cxcApp:notaUpdate', function(event, result) {
            $scope.nota = result;
        });
    });

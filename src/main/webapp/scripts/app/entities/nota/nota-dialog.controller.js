'use strict';

angular.module('cxcApp').controller('NotaDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Nota', 'Cuenta',
        function($scope, $stateParams, $modalInstance, entity, Nota, Cuenta) {

        $scope.nota = entity;
        $scope.cuentas = Cuenta.query();
        $scope.load = function(id) {
            Nota.get({id : id}, function(result) {
                $scope.nota = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:notaUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.nota.id != null) {
                Nota.update($scope.nota, onSaveFinished);
            } else {
                Nota.save($scope.nota, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

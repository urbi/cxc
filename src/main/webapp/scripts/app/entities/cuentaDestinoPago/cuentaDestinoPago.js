'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cuentaDestinoPago', {
                parent: 'entity',
                url: '/cuentaDestinoPagos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cuentaDestinoPago.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cuentaDestinoPago/cuentaDestinoPagos.html',
                        controller: 'CuentaDestinoPagoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cuentaDestinoPago');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('cuentaDestinoPago.detail', {
                parent: 'entity',
                url: '/cuentaDestinoPago/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.cuentaDestinoPago.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cuentaDestinoPago/cuentaDestinoPago-detail.html',
                        controller: 'CuentaDestinoPagoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cuentaDestinoPago');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'CuentaDestinoPago', function($stateParams, CuentaDestinoPago) {
                        return CuentaDestinoPago.get({id : $stateParams.id});
                    }]
                }
            })
            .state('cuentaDestinoPago.new', {
                parent: 'cuentaDestinoPago',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cuentaDestinoPago/cuentaDestinoPago-dialog.html',
                        controller: 'CuentaDestinoPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {convenio: null, cuentaClabe: null, referencia: null, banco: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('cuentaDestinoPago', null, { reload: true });
                    }, function() {
                        $state.go('cuentaDestinoPago');
                    })
                }]
            })
            .state('cuentaDestinoPago.edit', {
                parent: 'cuentaDestinoPago',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cuentaDestinoPago/cuentaDestinoPago-dialog.html',
                        controller: 'CuentaDestinoPagoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['CuentaDestinoPago', function(CuentaDestinoPago) {
                                return CuentaDestinoPago.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('cuentaDestinoPago', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

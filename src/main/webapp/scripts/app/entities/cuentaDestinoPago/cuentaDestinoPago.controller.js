'use strict';

angular.module('cxcApp')
    .controller('CuentaDestinoPagoController', function ($scope, CuentaDestinoPago, CuentaDestinoPagoSearch, ParseLinks) {
        $scope.cuentaDestinoPagos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            CuentaDestinoPago.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.cuentaDestinoPagos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            CuentaDestinoPago.get({id: id}, function(result) {
                $scope.cuentaDestinoPago = result;
                $('#deleteCuentaDestinoPagoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            CuentaDestinoPago.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCuentaDestinoPagoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CuentaDestinoPagoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.cuentaDestinoPagos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cuentaDestinoPago = {convenio: null, cuentaClabe: null, referencia: null, banco: null, id: null};
        };
    });

'use strict';

angular.module('cxcApp').controller('CuentaDestinoPagoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'CuentaDestinoPago', 'Cuenta', 'DestinoPago',
        function($scope, $stateParams, $modalInstance, entity, CuentaDestinoPago, Cuenta, DestinoPago) {

        $scope.cuentaDestinoPago = entity;
        $scope.cuentas = Cuenta.query();
        $scope.destinopagos = DestinoPago.query();
        $scope.load = function(id) {
            CuentaDestinoPago.get({id : id}, function(result) {
                $scope.cuentaDestinoPago = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:cuentaDestinoPagoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.cuentaDestinoPago.id != null) {
                CuentaDestinoPago.update($scope.cuentaDestinoPago, onSaveFinished);
            } else {
                CuentaDestinoPago.save($scope.cuentaDestinoPago, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

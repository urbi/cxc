'use strict';

angular.module('cxcApp')
    .controller('CuentaDestinoPagoDetailController', function ($scope, $rootScope, $stateParams, entity, CuentaDestinoPago, Cuenta, DestinoPago) {
        $scope.cuentaDestinoPago = entity;
        $scope.load = function (id) {
            CuentaDestinoPago.get({id: id}, function(result) {
                $scope.cuentaDestinoPago = result;
            });
        };
        $rootScope.$on('cxcApp:cuentaDestinoPagoUpdate', function(event, result) {
            $scope.cuentaDestinoPago = result;
        });
    });

'use strict';

angular.module('cxcApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('conceptoMovimiento', {
                parent: 'entity',
                url: '/conceptoMovimientos',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.conceptoMovimiento.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/conceptoMovimiento/conceptoMovimientos.html',
                        controller: 'ConceptoMovimientoController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('conceptoMovimiento');
                        $translatePartialLoader.addPart('');
                        $translatePartialLoader.addPart('');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('conceptoMovimiento.detail', {
                parent: 'entity',
                url: '/conceptoMovimiento/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'cxcApp.conceptoMovimiento.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/conceptoMovimiento/conceptoMovimiento-detail.html',
                        controller: 'ConceptoMovimientoDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('conceptoMovimiento');
                        $translatePartialLoader.addPart('');
                        $translatePartialLoader.addPart('');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ConceptoMovimiento', function($stateParams, ConceptoMovimiento) {
                        return ConceptoMovimiento.get({id : $stateParams.id});
                    }]
                }
            })
            .state('conceptoMovimiento.new', {
                parent: 'conceptoMovimiento',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/conceptoMovimiento/conceptoMovimiento-dialog.html',
                        controller: 'ConceptoMovimientoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {nombre: null, tipoMovimiento: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('conceptoMovimiento', null, { reload: true });
                    }, function() {
                        $state.go('conceptoMovimiento');
                    })
                }]
            })
            .state('conceptoMovimiento.edit', {
                parent: 'conceptoMovimiento',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/conceptoMovimiento/conceptoMovimiento-dialog.html',
                        controller: 'ConceptoMovimientoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ConceptoMovimiento', function(ConceptoMovimiento) {
                                return ConceptoMovimiento.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('conceptoMovimiento', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });

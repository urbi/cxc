'use strict';

angular.module('cxcApp')
    .controller('ConceptoMovimientoController', function ($scope, ConceptoMovimiento, ConceptoMovimientoSearch, ParseLinks) {
        $scope.conceptoMovimientos = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            ConceptoMovimiento.query({page: $scope.page, per_page: 20}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.conceptoMovimientos = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ConceptoMovimiento.get({id: id}, function(result) {
                $scope.conceptoMovimiento = result;
                $('#deleteConceptoMovimientoConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ConceptoMovimiento.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteConceptoMovimientoConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ConceptoMovimientoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.conceptoMovimientos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.conceptoMovimiento = {nombre: null, tipoMovimiento: null, id: null};
        };
    });

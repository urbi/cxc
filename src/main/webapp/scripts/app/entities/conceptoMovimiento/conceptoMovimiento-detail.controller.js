'use strict';

angular.module('cxcApp')
    .controller('ConceptoMovimientoDetailController', function ($scope, $rootScope, $stateParams, entity, ConceptoMovimiento, Movimiento) {
        $scope.conceptoMovimiento = entity;
        $scope.load = function (id) {
            ConceptoMovimiento.get({id: id}, function(result) {
                $scope.conceptoMovimiento = result;
            });
        };
        $rootScope.$on('cxcApp:conceptoMovimientoUpdate', function(event, result) {
            $scope.conceptoMovimiento = result;
        });
    });

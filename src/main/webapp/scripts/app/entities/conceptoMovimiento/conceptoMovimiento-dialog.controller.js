'use strict';

angular.module('cxcApp').controller('ConceptoMovimientoDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ConceptoMovimiento', 'Movimiento',
        function($scope, $stateParams, $modalInstance, entity, ConceptoMovimiento, Movimiento) {

        $scope.conceptoMovimiento = entity;
        $scope.movimientos = Movimiento.query();
        $scope.load = function(id) {
            ConceptoMovimiento.get({id : id}, function(result) {
                $scope.conceptoMovimiento = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('cxcApp:conceptoMovimientoUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.conceptoMovimiento.id != null) {
                ConceptoMovimiento.update($scope.conceptoMovimiento, onSaveFinished);
            } else {
                ConceptoMovimiento.save($scope.conceptoMovimiento, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

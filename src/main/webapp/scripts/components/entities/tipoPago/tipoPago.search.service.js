'use strict';

angular.module('cxcApp')
    .factory('TipoPagoSearch', function ($resource) {
        return $resource('api/_search/tipoPagos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

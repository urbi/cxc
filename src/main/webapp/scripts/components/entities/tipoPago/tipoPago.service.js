'use strict';

angular.module('cxcApp')
    .factory('TipoPago', function ($resource, DateUtils) {
        return $resource('api/tipoPagos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

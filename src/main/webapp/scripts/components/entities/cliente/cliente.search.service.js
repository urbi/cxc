'use strict';

angular.module('cxcApp')
    .factory('ClienteSearch', function ($resource) {
        return $resource('api/_search/clientes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('TipoClienteSearch', function ($resource) {
        return $resource('api/_search/tipoClientes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('EstatusPagoSearch', function ($resource) {
        return $resource('api/_search/estatusPagos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

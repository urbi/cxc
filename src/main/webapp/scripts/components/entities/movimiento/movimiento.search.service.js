'use strict';

angular.module('cxcApp')
    .factory('MovimientoSearch', function ($resource) {
        return $resource('api/_search/movimientos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

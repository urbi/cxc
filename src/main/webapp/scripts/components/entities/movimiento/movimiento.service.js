'use strict';

angular.module('cxcApp')
    .factory('Movimiento', function ($resource, DateUtils) {
        return $resource('api/movimientos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaVencimiento = DateUtils.convertLocaleDateFromServer(data.fechaVencimiento);
                    data.fechaPago = DateUtils.convertLocaleDateFromServer(data.fechaPago);
                    data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    data.lastModifiedDate = DateUtils.convertDateTimeFromServer(data.lastModifiedDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.fechaVencimiento = DateUtils.convertLocaleDateToServer(data.fechaVencimiento);
                    data.fechaPago = DateUtils.convertLocaleDateToServer(data.fechaPago);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.fechaVencimiento = DateUtils.convertLocaleDateToServer(data.fechaVencimiento);
                    data.fechaPago = DateUtils.convertLocaleDateToServer(data.fechaPago);
                    return angular.toJson(data);
                }
            }
        });
    });

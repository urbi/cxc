'use strict';

angular.module('cxcApp')
    .factory('EmpresaSearch', function ($resource) {
        return $resource('api/_search/empresas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

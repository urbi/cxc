'use strict';

angular.module('cxcApp')
    .factory('FileLoaderSearch', function ($resource) {
        return $resource('api/_search/fileLoaders/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

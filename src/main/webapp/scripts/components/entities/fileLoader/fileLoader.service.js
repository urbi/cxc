'use strict';

angular.module('cxcApp')
    .factory('FileLoader', function ($resource, DateUtils) {
        return $resource('api/fileLoaders/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

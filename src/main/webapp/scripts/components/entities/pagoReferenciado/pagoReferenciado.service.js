'use strict';

angular.module('cxcApp')
    .factory('PagoReferenciado', function ($resource, DateUtils) {
        return $resource('api/pagoReferenciados/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaPago = DateUtils.convertLocaleDateFromServer(data.fechaPago);
                    data.fechaCargaPago = DateUtils.convertDateTimeFromServer(data.fechaCargaPago);
                    data.fechaAplicacionPago = DateUtils.convertDateTimeFromServer(data.fechaAplicacionPago);
                    data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    data.lastModifiedDate = DateUtils.convertDateTimeFromServer(data.lastModifiedDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.fechaPago = DateUtils.convertLocaleDateToServer(data.fechaPago);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.fechaPago = DateUtils.convertLocaleDateToServer(data.fechaPago);
                    return angular.toJson(data);
                }
            }
        });
    });

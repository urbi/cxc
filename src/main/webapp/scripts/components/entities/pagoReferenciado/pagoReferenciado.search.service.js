'use strict';

angular.module('cxcApp')
    .factory('PagoReferenciadoSearch', function ($resource) {
        return $resource('api/_search/pagoReferenciados/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

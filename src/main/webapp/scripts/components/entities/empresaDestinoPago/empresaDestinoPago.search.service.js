'use strict';

angular.module('cxcApp')
    .factory('EmpresaDestinoPagoSearch', function ($resource) {
        return $resource('api/_search/empresaDestinoPagos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

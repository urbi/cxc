'use strict';

angular.module('cxcApp')
    .factory('SecuenciaClienteSearch', function ($resource) {
        return $resource('api/_search/secuenciaClientes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

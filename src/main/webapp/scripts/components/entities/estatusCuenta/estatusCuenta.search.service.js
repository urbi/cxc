'use strict';

angular.module('cxcApp')
    .factory('EstatusCuentaSearch', function ($resource) {
        return $resource('api/_search/estatusCuentas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

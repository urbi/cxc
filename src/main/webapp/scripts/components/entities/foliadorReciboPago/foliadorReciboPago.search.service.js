'use strict';

angular.module('cxcApp')
    .factory('FoliadorReciboPagoSearch', function ($resource) {
        return $resource('api/_search/foliadorReciboPagos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

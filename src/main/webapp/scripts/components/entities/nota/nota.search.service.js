'use strict';

angular.module('cxcApp')
    .factory('NotaSearch', function ($resource) {
        return $resource('api/_search/notas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

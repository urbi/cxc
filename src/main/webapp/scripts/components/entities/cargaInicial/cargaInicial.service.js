'use strict';

angular.module('cxcApp')
    .factory('CargaInicial', function ($resource, DateUtils) {
        return $resource('api/cargaInicials/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaCarga = DateUtils.convertDateTimeFromServer(data.fechaCarga);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

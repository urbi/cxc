'use strict';

angular.module('cxcApp')
    .factory('CargaInicialSearch', function ($resource) {
        return $resource('api/_search/cargaInicials/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

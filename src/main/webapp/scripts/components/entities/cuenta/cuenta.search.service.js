'use strict';

angular.module('cxcApp')
    .factory('CuentaSearch', function ($resource) {
        return $resource('api/_search/cuentas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('Cuenta', function ($resource, DateUtils) {
        return $resource('api/cuentas/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.fechaCreacion = DateUtils.convertDateTimeFromServer(data.fechaCreacion);
                    data.fechaUltimoPago = DateUtils.convertLocaleDateFromServer(data.fechaUltimoPago);
                    data.createdDate = DateUtils.convertDateTimeFromServer(data.createdDate);
                    data.lastModifiedDate = DateUtils.convertDateTimeFromServer(data.lastModifiedDate);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.fechaUltimoPago = DateUtils.convertLocaleDateToServer(data.fechaUltimoPago);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.fechaUltimoPago = DateUtils.convertLocaleDateToServer(data.fechaUltimoPago);
                    console.log(data);
                    console.log(angular.toJson(data));
                    return angular.toJson(data);
                }
            }
        });
    });

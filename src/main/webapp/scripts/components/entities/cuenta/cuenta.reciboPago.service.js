'use strict';

angular.module('cxcApp')
    .factory('CuentaReciboPago', function ($resource) {
        return $resource('api/_searchByUser/plazas' , {
            'query': { method: 'GET', isArray: true}
        });
    });

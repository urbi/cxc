'use strict';

angular.module('cxcApp')
    .factory('PlazaSearch', function ($resource) {
        return $resource('api/_search/plazas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

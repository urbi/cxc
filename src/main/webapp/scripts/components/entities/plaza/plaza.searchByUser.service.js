'use strict';

angular.module('cxcApp')
    .factory('PlazaSearchByUser', function ($resource) {
        return $resource('api/_searchByUser/plazas' , {
            'query': { method: 'GET', isArray: true}
        });
    });

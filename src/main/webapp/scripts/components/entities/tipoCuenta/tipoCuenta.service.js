'use strict';

angular.module('cxcApp')
    .factory('TipoCuenta', function ($resource, DateUtils) {
        return $resource('api/tipoCuentas/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('TipoCuentaSearch', function ($resource) {
        return $resource('api/_search/tipoCuentas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

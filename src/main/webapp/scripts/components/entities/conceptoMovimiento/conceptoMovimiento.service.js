'use strict';

angular.module('cxcApp')
    .factory('ConceptoMovimiento', function ($resource, DateUtils) {
        return $resource('api/conceptoMovimientos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

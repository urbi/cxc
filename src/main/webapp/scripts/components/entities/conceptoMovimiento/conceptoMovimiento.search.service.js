'use strict';

angular.module('cxcApp')
    .factory('ConceptoMovimientoSearch', function ($resource) {
        return $resource('api/_search/conceptoMovimientos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

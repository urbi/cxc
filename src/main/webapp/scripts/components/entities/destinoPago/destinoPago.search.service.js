'use strict';

angular.module('cxcApp')
    .factory('DestinoPagoSearch', function ($resource) {
        return $resource('api/_search/destinoPagos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

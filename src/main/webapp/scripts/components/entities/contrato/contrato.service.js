'use strict';

angular.module('cxcApp')
    .factory('Contrato', function ($resource, DateUtils) {
        return $resource('api/contratos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

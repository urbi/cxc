'use strict';

angular.module('cxcApp')
    .factory('ContratoSearch', function ($resource) {
        return $resource('api/_search/contratos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('TipoMovimientoSearch', function ($resource) {
        return $resource('api/_search/tipoMovimientos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('DocumentoSearch', function ($resource) {
        return $resource('api/_search/documentos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

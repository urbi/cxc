'use strict';

angular.module('cxcApp')
    .factory('Caja', function ($resource, DateUtils) {
        return $resource('api/cajas/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

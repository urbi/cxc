'use strict';

angular.module('cxcApp')
    .factory('CajaSearch', function ($resource) {
        return $resource('api/_search/cajas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

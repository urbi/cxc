'use strict';

angular.module('cxcApp')
    .factory('MunicipioSearch', function ($resource) {
        return $resource('api/_search/municipios/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

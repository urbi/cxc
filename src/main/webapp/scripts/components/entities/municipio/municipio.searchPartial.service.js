'use strict';

angular.module('cxcApp')
    .factory('MunicipioSearchPartial', function ($resource) {
        return $resource('api/_searchPartial/municipios/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

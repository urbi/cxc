'use strict';

angular.module('cxcApp')
    .factory('CxcSecuenciaSearch', function ($resource) {
        return $resource('api/_search/cxcSecuencias/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

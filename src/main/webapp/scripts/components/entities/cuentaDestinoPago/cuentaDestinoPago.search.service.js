'use strict';

angular.module('cxcApp')
    .factory('CuentaDestinoPagoSearch', function ($resource) {
        return $resource('api/_search/cuentaDestinoPagos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

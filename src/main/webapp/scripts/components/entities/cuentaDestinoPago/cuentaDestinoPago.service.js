'use strict';

angular.module('cxcApp')
    .factory('CuentaDestinoPago', function ($resource, DateUtils) {
        return $resource('api/cuentaDestinoPagos/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

'use strict';

angular.module('cxcApp')
    .factory('SecuenciaCuentaSearch', function ($resource) {
        return $resource('api/_search/secuenciaCuentas/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });

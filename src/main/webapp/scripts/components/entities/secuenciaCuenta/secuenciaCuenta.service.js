'use strict';

angular.module('cxcApp')
    .factory('SecuenciaCuenta', function ($resource, DateUtils) {
        return $resource('api/secuenciaCuentas/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

/* globals $ */
'use strict';

angular.module('cxcApp')
    .directive('cxcAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });

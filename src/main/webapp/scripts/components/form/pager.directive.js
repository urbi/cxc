/* globals $ */
'use strict';

angular.module('cxcApp')
    .directive('cxcAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });

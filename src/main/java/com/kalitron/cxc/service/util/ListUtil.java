package com.kalitron.cxc.service.util;

import java.util.*;

/**
 * Created by narciso.parra on 23/07/2015.
 */
public class ListUtil {
    public static
    <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<T>(c);
        java.util.Collections.sort(list);
        return list;
    }


}

package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.*;

import com.kalitron.cxc.repository.CuentaDestinoPagoRepository;

import com.kalitron.cxc.repository.search.CuentaDestinoPagoSearchRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Set;

@Service
@Transactional
public class CuentaDestinoPagoService {

    private final Logger log = LoggerFactory.getLogger(CuentaDestinoPagoService.class);


    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private CuentaDestinoPagoSearchRepository cuentaDestinoPagoSearchRepository;

    @Inject
    private UserService userService;


    public CuentaDestinoPago create(CuentaDestinoPago cuentaDestinoPago) {

        CuentaDestinoPago result = cuentaDestinoPagoRepository.save(cuentaDestinoPago);
        cuentaDestinoPagoSearchRepository.save(result);
        return cuentaDestinoPago;
    }

    public CuentaDestinoPago update(CuentaDestinoPago cuentaDestinoPago) {


        CuentaDestinoPago result = cuentaDestinoPagoRepository.save(cuentaDestinoPago);
        cuentaDestinoPagoSearchRepository.save(result);
        return cuentaDestinoPago;
    }

    public void delete(Long id) throws Exception {
        User user = userService.getUserWithAuthorities();
        Set<Authority> authorities = user.getAuthorities();
        Boolean hasAuthority = false;
        System.out.println("autorities isEmpty : " + authorities.isEmpty());

        for (Authority authority : authorities) {
            System.out.println("Role: " + authority.toString());
            hasAuthority = authority.getName().equals("ROLE_ADMIN") ? true : false;
            if (hasAuthority.equals(true)) break;
        }
        if (!hasAuthority)
            throw new Exception("El Usuario: " + user.getFirstName() + " " + user.getLastName() + " no tiene privilegios para eliminar objetos ");


        cuentaDestinoPagoRepository.delete(id);
        cuentaDestinoPagoSearchRepository.delete(id);
    }

    public void deleteByCuenta(Long cuentaId) {
        cuentaDestinoPagoSearchRepository.delete(cuentaDestinoPagoRepository.removeByCuentaId(cuentaId));
    }


}

package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.Authority;
import com.kalitron.cxc.domain.Movimiento;

import com.kalitron.cxc.domain.User;
import com.kalitron.cxc.domain.enumeration.*;
import com.kalitron.cxc.repository.MovimientoRepository;

import com.kalitron.cxc.repository.search.MovimientoSearchRepository;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.util.List;
import java.util.Set;

@Service
@Transactional
public class MovimientoService {

    private final Logger log = LoggerFactory.getLogger(MovimientoService.class);


    @Inject
    private MovimientoRepository movimientoRepository;

    @Inject
    private MovimientoSearchRepository movimientoSearchRepository;

    @Inject
    private UserService userService;


    @Inject
    private CxcSecuenciaService cxcSecuenciaService;
    /*
    Este movimiento solo va a ser llamado desde los servicios
     */
    private Movimiento create(Movimiento movimiento){
        log.debug("Movimiento Create{}", movimiento);
        movimiento = beforeCreateOrUpdate(movimiento);
        movimiento.setSecuenciaMovimiento(getSecuenciaMovimiento(movimiento.getTipoMovimiento()));

        Movimiento result  = movimientoRepository.save(movimiento);
        movimientoSearchRepository.save(result);
        return movimiento;
    }

    public Movimiento AddPago(Movimiento pago){
        if(pago.getConceptoMovimiento() == ConceptoMovimientoType.PAGO){
            pago.setEstatusMovimiento(EstatusMovimientoType.NO_APLICA);
            pago.setTipoMovimiento(TipoMovimientoType.ABONO);
            if(pago.getFechaVencimiento()== null)
                pago.setFechaVencimiento( LocalDate.now() );
        }
        Movimiento movimiento =create(pago);




        return null;
    }


    public Movimiento update(Movimiento movimiento){

        movimiento = beforeCreateOrUpdate(movimiento);

        Movimiento result  =movimientoRepository.save(movimiento);
        movimientoSearchRepository.save(result);
        return movimiento;
    }

    public void delete(Long id) throws  Exception{
        User user = userService.getUserWithAuthorities();
        Set<Authority> authorities = user.getAuthorities();
        Boolean hasAuthority =false;
        System.out.println("autorities isEmpty : " + authorities.isEmpty());

        for(Authority authority :authorities){
            System.out.println("Role: " + authority.toString());
            hasAuthority  = authority.getName().equals("ROLE_ADMIN") ? true : false;
            if(hasAuthority.equals(true)) break;
        }
        if (!hasAuthority) throw new Exception("El Usuario: " +user.getFirstName()+" "+ user.getLastName()+" no tiene privilegios para eliminar objetos ");



        movimientoRepository.delete(id);
        movimientoSearchRepository.delete(id);
    }

    public void deleteByCuenta(Long cuentaId){
       movimientoSearchRepository.delete(movimientoRepository.removeByCuentaId(cuentaId));
    }

    private Movimiento beforeCreateOrUpdate(Movimiento movimiento){

        User user = userService.getUserWithAuthorities();



        DateTime now = new DateTime();
        if(movimiento.getId() == null) {
            movimiento.setCreated(user);
            movimiento.setCreatedDate( now );

        }
        movimiento.setLastModifiedDate(now);
        movimiento.setUpdated(user);


        return movimiento;
    }

    private Long getSecuenciaMovimiento(TipoMovimientoType tipoMovimientoType){
        Long newSequence = 0L;
        switch (tipoMovimientoType){
            case ABONO: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.MOVIMIENTO_ABONO);break;
            case CARGO: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.MOVIMIENTO_CARGO);break;

        }

        return newSequence;



    }

    @Async
    @Transactional
    public void indexToElastickSearch(){
        log.debug("indexToElastickSearch Movimeintos '{}'");
        List<Movimiento> movimientos = movimientoRepository.findAll();
        movimientoSearchRepository.save(movimientos);

        log.debug("FIN indexToElastickSearch Movimientos'{}'");

    }







}

package com.kalitron.cxc.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.domain.enumeration.EstatusPagoType;
import com.kalitron.cxc.domain.enumeration.SecuenciaType;
import com.kalitron.cxc.repository.*;
import com.kalitron.cxc.repository.search.*;
import com.kalitron.cxc.web.rest.dto.CuentaCargaInicialDTO;
import com.kalitron.cxc.web.rest.dto.PagoReferenciadoDTO;
import com.kalitron.cxc.web.rest.mapper.ClienteMapper;
import com.kalitron.cxc.web.rest.mapper.PagoReferenciadoMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service

public class CargaInicialService {

    private final Logger log = LoggerFactory.getLogger(CargaInicialService.class);

    @Inject
    private ClienteMapper clienteMapper;

    @Inject
    private ClienteService clienteService;

    @Inject CuentaService cuentaService;

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private EmpresaSearchRepository empresaSearchRepository;

    @Inject
    private EmpresaDestinoPagoRepository empresaDestinoPagoRepository;

    @Inject
    private EmpresaDestinoPagoSearchRepository empresaDestinoPagoSearchRepository;

    @Inject
    private PlazaRepository plazaRepository;

    @Inject
    private PlazaSearchRepository plazaSearchRepository;

    @Inject
    private CxcSecuenciaService cxcSecuenciaService;

    @Inject
    private PagoReferenciadoService pagoReferenciadoService;


    @Inject
    private PagoReferenciadoMapper pagoReferenciadoMapper;

    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private CuentaDestinoPagoSearchRepository cuentaDestinoPagoSearchRepository;



    private CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
    private CsvMapper csvMapper = new CsvMapper();
    //TODO: generar una bitocora  de carga para tener un control de lo que se cargo y en que paquete o folio  y presentarle al cliente los registros cargados y en que paquete se encuentra
    //TODO: otra opcion es regresar un archivo de excel con los registros cargados y el id que le corresponde por si los utilizan en alguna parte

   public CargaInicial procesaCargaInicial(CargaInicial cargaInicial){

       switch (cargaInicial.getTipoCarga()){
           case CUENTA: return processCuentas(cargaInicial);
           case EMPRESA: return processEmpresas(cargaInicial);
           case PLAZA: return processPlazas(cargaInicial);
           case EMPRESA_DESTINO_PAGO: return processEmpresaDestinoPago(cargaInicial);
           case PAGO_REFERENCIADO:; return processPagoReferenciados(cargaInicial);
           case CUENTA_DESTINO_PAGO: return processCuentaDestinoPago(cargaInicial);
       }
       return cargaInicial;

   }

    private CargaInicial processCuentaDestinoPago(CargaInicial cargaInicial){
        List<CuentaDestinoPago> cuentaDestinoPago= new ArrayList<>();
        try {
            MappingIterator<CuentaDestinoPago> mappingIterator = csvMapper.reader(CuentaDestinoPago.class).with(bootstrap).readValues(cargaInicial.getArchivo());

            List<CuentaDestinoPago> cuentas = mappingIterator.readAll();
            int row = 0;
            for (CuentaDestinoPago cuenta :  cuentas){

                try{
                    CuentaDestinoPago result = cuentaDestinoPagoSearchRepository.save(cuentaDestinoPagoRepository.save(cuenta));


                    cuentaDestinoPago.add(result);
                    row++;
                }catch (Exception e){
                    log.debug("ERRORRRRRRR ROW :" + row + "   - Message : " + e.getMessage());
                    break;
                }


            }

            cargaInicial.setMensaje("Se Crearon: " + cuentaDestinoPago.size() + " Empresas con la referencia : x");


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return cargaInicial;
    }

    private CargaInicial processPagoReferenciados(CargaInicial cargaInicial){
        List<PagoReferenciado> pagoReferenciados= new ArrayList<>();
        try {
            MappingIterator<PagoReferenciadoDTO> mappingIterator = csvMapper.reader(PagoReferenciado.class).with(bootstrap).readValues(cargaInicial.getArchivo());

            List<PagoReferenciadoDTO> pagos = mappingIterator.readAll();

            Long folioDeCarga = cxcSecuenciaService.getSecuencia(SecuenciaType.PAGO_REFERENCIADO);
            for (PagoReferenciadoDTO pagoReferenciadoDTO :  pagos){
                pagoReferenciadoDTO.setFolioCarga(String.valueOf(folioDeCarga));
                pagoReferenciadoDTO.setFechaCargaPago(new DateTime());
                pagoReferenciadoDTO.setEstatusPago(EstatusPagoType.POR_APLICAR);
                PagoReferenciado pagoReferenciado = pagoReferenciadoMapper.pagoReferenciadoDTOToPagoReferenciado(pagoReferenciadoDTO);

                PagoReferenciado result = pagoReferenciadoService.create(pagoReferenciado);


                pagoReferenciados.add(result);

            }
            pagoReferenciadoService.aplicaPagosReferenciados(pagoReferenciados);

            cargaInicial.setMensaje("Se Crearon: " + pagoReferenciados.size() + " Empresas con la referencia : x");


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return cargaInicial;
    }


    private CargaInicial processEmpresaDestinoPago(CargaInicial cargaInicial){
        List<EmpresaDestinoPago> empresaDestinoPagos= new ArrayList<>();
        try {
            MappingIterator<EmpresaDestinoPago> mappingIterator = csvMapper.reader(EmpresaDestinoPago.class).with(bootstrap).readValues(cargaInicial.getArchivo());

            List<EmpresaDestinoPago> empresas = mappingIterator.readAll();
            for (EmpresaDestinoPago empresa :  empresas){


                EmpresaDestinoPago result = empresaDestinoPagoSearchRepository.save(empresaDestinoPagoRepository.save(empresa));


                empresaDestinoPagos.add(result);

            }

            cargaInicial.setMensaje("Se Crearon: " + empresaDestinoPagos.size() + " Empresas con la referencia : x");


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return cargaInicial;
    }

    private CargaInicial processEmpresas(CargaInicial cargaInicial){
        List<Empresa> cuentasCreadas= new ArrayList<>();
        try {
            MappingIterator<Empresa> mappingIterator = csvMapper.reader(Empresa.class).with(bootstrap).readValues(cargaInicial.getArchivo());

            List<Empresa> empresas = mappingIterator.readAll();
            for (Empresa empresa :  empresas){


               Empresa result = empresaSearchRepository.save(empresaRepository.save(empresa));


                cuentasCreadas.add(result);

            }

            cargaInicial.setMensaje("Se Crearon: " + cuentasCreadas.size() + " Empresas con la referencia : x");


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return cargaInicial;
    }

    private CargaInicial processPlazas(CargaInicial cargaInicial){
        List<Plaza> cuentasCreadas= new ArrayList<>();
        try {
            MappingIterator<Plaza> mappingIterator = csvMapper.reader(Plaza.class).with(bootstrap).readValues(cargaInicial.getArchivo());

            List<Plaza> plazas = mappingIterator.readAll();
            for (Plaza plaza :  plazas){


                Plaza result = plazaSearchRepository.save(plazaRepository.save(plaza));


                cuentasCreadas.add(result);

            }

            cargaInicial.setMensaje("Se Crearon: " + cuentasCreadas.size() + " Plazas con la referencia : x");


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return cargaInicial;
    }

    private CargaInicial processCuentas(CargaInicial cargaInicial){
        List<Cuenta> cuentasCreadas= new ArrayList<>();
        try {
            MappingIterator<CuentaCargaInicialDTO> mappingIterator = csvMapper.reader(CuentaCargaInicialDTO.class).with(bootstrap).readValues(cargaInicial.getArchivo());

            List<CuentaCargaInicialDTO> cuentas = mappingIterator.readAll();
            for (CuentaCargaInicialDTO cuentaCargaInicialDTO :  cuentas){
                Cuenta cuenta = cuentaCargaInicialDTO.convertToCuenta();

                Cliente result = clienteService.create(cuenta.getCliente());
                Cliente cliente2 = new Cliente();
                cliente2.setId(result.getId());
                cuenta.setCliente(cliente2);
                Cuenta cuentaResult = cuentaService.create(cuenta);
                cuentasCreadas.add(cuentaResult);

            }

            cargaInicial.setMensaje("Se Crearon: " + cuentasCreadas.size() + " clientes con la referencia : x");


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }



        return cargaInicial;


    }


}

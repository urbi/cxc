package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.domain.enumeration.*;
import com.kalitron.cxc.repository.CuentaDestinoPagoRepository;
import com.kalitron.cxc.repository.CxcSecuenciaRepository;
import com.kalitron.cxc.repository.MovimientoRepository;
import com.kalitron.cxc.repository.PagoReferenciadoRepository;
import com.kalitron.cxc.repository.search.PagoReferenciadoSearchRepository;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;


@Service
public class PagoReferenciadoService {
    private final Logger log = LoggerFactory.getLogger(PagoReferenciadoService.class);

    @Inject
    private PagoReferenciadoRepository pagoReferenciadoRepository;
    @Inject
    private PagoReferenciadoSearchRepository pagoReferenciadoSearchRepository;

    @Inject
    private UserService userService;

    @Inject
    private CxcSecuenciaRepository cxcSecuenciaRepository;

    @Inject CuentaService cuentaService;

    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private MovimientoService movimientoService;

    @Async
    @Transactional
    public void indexToElastickSearch(){
        log.debug("indexToElastickSearch PagoReferenciado '{}'");
        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        pagoReferenciadoSearchRepository.save(pagoReferenciados);

        log.debug("FIN indexToElastickSearch Movimientos'{}'");

    }

    @Transactional(rollbackFor=Exception.class)
    public PagoReferenciado create(PagoReferenciado pagoReferenciado) throws Exception {
        log.debug("Create PagoReferenciado {}", pagoReferenciado);
        pagoReferenciado = beforeCreateOrUpdate(pagoReferenciado);

        pagoReferenciado.setFechaCargaPago(new DateTime());

        pagoReferenciadoSearchRepository.save(pagoReferenciadoRepository.save(pagoReferenciado));
        return pagoReferenciado;
    }

    private  PagoReferenciado  beforeCreateOrUpdate(PagoReferenciado cuenta){
        log.debug("beforeCreateOrUpdate");
        User user = userService.getUserWithAuthorities();



        DateTime now = new DateTime();
        if(cuenta.getId() == null) {
            cuenta.setCreated(user);
            cuenta.setCreatedDate( now );

        }
        cuenta.setLastModifiedDate(now);
        cuenta.setUpdated(user);


        return cuenta;
    }


    @Async
    public void aplicaPagosReferenciados(String folioCarga){

    }

    @Async
    public void aplicaPagosReferenciados(List<PagoReferenciado> pagosReferenciados){

        for (PagoReferenciado pagoReferenciado : pagosReferenciados){
            CuentaDestinoPago cuentaDestinoPago = cuentaDestinoPagoRepository.findByReferencia(pagoReferenciado.getReferencia());
            if(cuentaDestinoPago!= null){
                Movimiento movimiento = new Movimiento();
                movimiento.setSaldo(pagoReferenciado.getMontoPago());
                movimiento.setMonto(pagoReferenciado.getMontoPago());
                movimiento.setFechaVencimiento(pagoReferenciado.getFechaPago());
                movimiento.setFechaPago(pagoReferenciado.getFechaPago());
                movimiento.setEstatusMovimiento(EstatusMovimientoType.NO_APLICA);
                movimiento.setConceptoMovimiento(ConceptoMovimientoType.PAGO);
                movimiento.setTipoMovimiento(TipoMovimientoType.ABONO);
                movimiento.setPagoReferenciado(pagoReferenciado);
                movimiento.setCuenta(cuentaDestinoPago.getCuenta());

                pagoReferenciado.setPlaza(cuentaDestinoPago.getCuenta().getPlaza());
                pagoReferenciado.setDestinoPago(cuentaDestinoPago.getDestinoPago());
                pagoReferenciado.setFechaAplicacionPago( new DateTime());
                try {
                    Movimiento result =  movimientoService.AddPago(movimiento);
                    pagoReferenciado.setMovimiento(movimiento);

                    pagoReferenciadoSearchRepository.save(pagoReferenciadoRepository.save(pagoReferenciado));
                }catch (Exception e){
                    pagoReferenciado.setEstatusPago(EstatusPagoType.REVISION);
                    pagoReferenciado.setComentario("ERROR DESCONOCIDO AL APLICAR PAGO");
                    pagoReferenciado.setMovimiento(null);

                    pagoReferenciadoSearchRepository.save(pagoReferenciadoRepository.save(pagoReferenciado));
                }


            }else{
                pagoReferenciado.setEstatusPago(EstatusPagoType.REVISION);
                pagoReferenciado.setComentario("Referencia no encontrada");
                pagoReferenciadoSearchRepository.save(pagoReferenciadoRepository.save(pagoReferenciado));
            }


        }
    }



}

package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.CuentaDestinoPago;
import com.kalitron.cxc.domain.EmpresaDestinoPago;
import com.kalitron.cxc.repository.*;
import com.kalitron.cxc.repository.search.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by narciso.parra on 18/07/2015.
 */
@Service
@Transactional
public class InitializationDataService {

    private final Logger log = LoggerFactory.getLogger(ClienteService.class);

    @Inject
    private EstadoRepository estadoRepository;

    @Inject
    private EstadoSearchRepository estadoSearchRepository;

    @Inject
    private MunicipioRepository municipioRepository;

    @Inject
    private MunicipioSearchRepository municipioSearchRepository;

    @Inject
    private ClienteRepository clienteRepository;

    @Inject
    private ClienteSearchRepository clienteSearchRepository;

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private EmpresaSearchRepository empresaSearchRepository;

    @Inject
    private PlazaRepository plazaRepository;

    @Inject
    private PlazaSearchRepository plazaSearchRepository;

    @Inject
    private DocumentoRepository documentoRepository;

    @Inject
    private DocumentoSearchRepository documentoSearchRepository;

    @Inject
    private CajaRepository cajaRepository;

    @Inject
    private CajaSearchRepository cajaSearchRepository;

    @Inject
    private CuentaRepository cuentaRepository;

    @Inject
    private CuentaSearchRepository cuentaSearchRepository;

    @Inject
    private MovimientoRepository movimientoRepository;

    @Inject
    private MovimientoSearchRepository movimientoSearchRepository;

    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private CuentaDestinoPagoSearchRepository cuentaDestinoPagoSearchRepository;

    @Inject
    private PagoReferenciadoRepository pagoReferenciadoRepository;

    @Inject
    private PagoReferenciadoSearchRepository pagoReferenciadoSearchRepository;

    @Inject
    private DestinoPagoRepository destinoPagoRepository;

    @Inject
    private DestinoPagoSearchRepository  destinoPagoSearchRepository;

    @Inject
    private EmpresaDestinoPagoRepository empresaDestinoPagoRepository;

    @Inject
    private EmpresaDestinoPagoSearchRepository empresaDestinoPagoSearchRepository;

    @Inject
    private ConceptoMovimientoRepository conceptoMovimientoRepository;

    @Inject
    private ConceptoMovimientoSearchRepository conceptoMovimientoSearchRepository;






    /*TODO: agregar un metodo para cuando se arranque el servidor por primera vez indexe todo los catalogos
      a la base de elastick search*/
    public void indexAllData(){
        estadoRepository.findAll().forEach(estado ->estadoSearchRepository.save(estado));
        municipioRepository.findAll().forEach(municipio->municipioSearchRepository.save(municipio));
        plazaRepository.findAll().forEach(plaza->plazaSearchRepository.save(plaza));
        empresaRepository.findAll().forEach(empresa->empresaSearchRepository.save(empresa));
        destinoPagoRepository.findAll().forEach(destinoPago->destinoPagoSearchRepository.save(destinoPago));
        empresaDestinoPagoRepository.findAll().forEach(empresaDestinoPago->empresaDestinoPagoSearchRepository.save(empresaDestinoPago));
        documentoRepository.findAll().forEach(documento->documentoSearchRepository.save(documento));
        conceptoMovimientoRepository.findAll().forEach(conceptoMovimiento->conceptoMovimientoSearchRepository.save(conceptoMovimiento));
       // clienteRepository.findAll().forEach(cliente->clienteSearchRepository.save(cliente));
        cajaRepository.findAll().forEach(caja->cajaSearchRepository.save(caja));
        //cuentaRepository.findAll().forEach(cuenta->cuentaSearchRepository.save(cuenta));
       // movimientoRepository.findAll().forEach(movimiento->movimientoSearchRepository.save(movimiento));
       // cuentaDestinoPagoRepository.findAll().forEach(cuentaDestinoPago->cuentaDestinoPagoSearchRepository.save(cuentaDestinoPago));
        //pagoReferenciadoRepository.findAll().forEach(pagoReferenciado->pagoReferenciadoSearchRepository.save(pagoReferenciado));


    }

    public void indexCuentaYCliente(){
        cuentaRepository.findAll().forEach(cuenta->cuentaSearchRepository.save(cuenta));
        clienteRepository.findAll().forEach(cliente->clienteSearchRepository.save(cliente));

    }
    //TODO: eliminar una vez que este migrado
    public void deleteAllData(){
        cuentaDestinoPagoRepository.deleteAll();
        cuentaDestinoPagoSearchRepository.deleteAll();
        movimientoRepository.deleteAll();;
        movimientoSearchRepository.deleteAll();
        cuentaRepository.deleteAll();
        cuentaSearchRepository.deleteAll();
        clienteRepository.deleteAll();
        clienteSearchRepository.deleteAll();
    }


}

package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.CxcSecuencia;
import com.kalitron.cxc.domain.enumeration.SecuenciaType;
import com.kalitron.cxc.repository.CxcSecuenciaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

@Service
@Transactional
public class CxcSecuenciaService {

    @Inject
    private CxcSecuenciaRepository cxcSecuenciaRepository;
    private final Logger log = LoggerFactory.getLogger(CxcSecuenciaService.class);
    public Long getSecuencia(SecuenciaType secuenciaType){

        CxcSecuencia cxcSecuencia = cxcSecuenciaRepository.findByNombre(secuenciaType);

        Long newSequence = cxcSecuencia.getSecuencia().longValue() +1L;

        cxcSecuencia.setSecuencia(newSequence);
        cxcSecuenciaRepository.save(cxcSecuencia);
        return newSequence;



    }
}

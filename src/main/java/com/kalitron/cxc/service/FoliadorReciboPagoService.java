package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.FoliadorReciboPago;
import com.kalitron.cxc.repository.FoliadorReciboPagoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

@Service
@Transactional
public class FoliadorReciboPagoService {


    private final Logger log = LoggerFactory.getLogger(FoliadorReciboPagoService.class);

    @Inject
    private FoliadorReciboPagoRepository foliadorReciboPagoRepository;

    public Long getFolio( Long plazaId, Long empresaId){
        log.debug("getFolio {}");

        FoliadorReciboPago foliadorReciboPago= foliadorReciboPagoRepository.findByPlazaAndEmpresa(plazaId, empresaId);
        Long newSequence = foliadorReciboPago.getSecuencia().longValue() +1L;

        foliadorReciboPago.setSecuencia( newSequence);
        foliadorReciboPagoRepository.save(foliadorReciboPago);
        return newSequence;



    }


}

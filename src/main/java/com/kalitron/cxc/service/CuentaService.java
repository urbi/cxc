package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.*;

import com.kalitron.cxc.domain.enumeration.*;
import com.kalitron.cxc.repository.*;
import com.kalitron.cxc.repository.search.CuentaDestinoPagoSearchRepository;
import com.kalitron.cxc.repository.search.CuentaSearchRepository;
import com.kalitron.cxc.repository.search.MovimientoSearchRepository;
import com.kalitron.cxc.service.util.ListUtil;
import com.kalitron.cxc.web.rest.dto.MovimientoDTO;
import com.kalitron.cxc.web.rest.dto.ReciboPagoDTO;
import com.kalitron.cxc.web.rest.mapper.ClienteMapper;
import com.kalitron.cxc.web.rest.mapper.CuentaMapper;
import com.kalitron.cxc.web.rest.mapper.EmpresaMapper;
import com.kalitron.cxc.web.rest.mapper.MovimientoMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.math.BigDecimal;
import org.joda.time.LocalDate;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import java.util.*;

@Service
@Transactional
public class CuentaService {

    private final Logger log = LoggerFactory.getLogger(CuentaService.class);

    @Inject
    private CxcSecuenciaService cxcSecuenciaService;

    @Inject
    private UserService userService;

    @Inject
    private CuentaRepository cuentaRepository;

    @Inject
    private NotaRepository notaRepository;

    @Inject
    private CuentaSearchRepository cuentaSearchRepository;

    @Inject
    private EmpresaDestinoPagoRepository empresaDestinoPagoRepository;



    @Inject
    private MovimientoRepository movimientoRepository;

    @Inject
    private MovimientoSearchRepository movimientoSearchRepository;



    @Inject
    private DestinoPagoRepository destinoPagoRepository;



    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private CuentaDestinoPagoSearchRepository cuentaDestinoPagoSearchRepository;

    @Inject
    private SecuenciaMovimientoRepository secuenciaMovimientoRepository;

    @Inject
    private CuentaMapper cuentaMapper;

    @Inject
    private MovimientoMapper movimientoMapper;

    @Inject
    private ClienteMapper clienteMapper;

    @Inject
    private EmpresaMapper empresaMapper;

    @Inject
    private FoliadorReciboPagoService foliadorReciboPagoService;

    @Inject
    private ClienteRepository clienteRepository;

    @Inject
    private PlazaRepository plazaRepository;





    //TODO:debemos hacer una transaccion por si falla algo no grabe naad
    /*Falta implementar los metodos para crear las cuentas
    Directrices
        que al grabar el movimeinto exista un servicio de movimiento.
        que se valide si el concepto de movimiento es al que le corresponde el cargo y el abono


     */
    //TODO: revisar la funcionalidad de cascade para ver si al gragar el objeto cuenta se pueden guardar todos sus objetos relacionados
    //TODO: cual es la mejor forma de utilizar la Transaccionalidad
    //TODO: Directriz todos los repositorios solo podrán se llamados desde la capa de servicios.
    @Transactional(rollbackFor=Exception.class)
    public Cuenta create(Cuenta cuenta) throws Exception{
        log.debug("Create Cuenta {}", cuenta);
       // cuenta.getMovimientos().forEach(movimiento-> log.debug("Movimiento {}" + movimiento));


        cuenta = beforeCreateOrUpdate(cuenta);
        if(cuenta.getId() == null)
            cuenta.setSecuenciaCuenta(getSecuenciaCuenta(cuenta.getTipoDocumento()));
        cuenta = getSaldosInicialAndMontoExigible(cuenta);
        if(cuenta.getId() == null)
            cuenta.setEstatusCuenta(EstatusCuentaType.ACTIVADA);
        cuenta = getCuentaDestinosPagos(cuenta);
        cuenta.setFechaUltimoPago(getFechaUltimoPago(cuenta));

        switch (cuenta.getTipoDocumento()) {

            case ENGANCHE:
                cuenta = addMovimientosCuentaEnganche(cuenta);
                break;

            case CONTRATO_RENTA:
               // System.out.println("Fridays are better.");
                break;

            case ESCRITURA:
               // System.out.println("Weekends are best.");
                break;

            case PAGARE_SIMPLE:
                cuenta = addMovimientosPagareSimple(cuenta);
                break;


        }
        log.debug("Saving Cuenta Cuenta  {}");
        List<Movimiento> movimientos = cuenta.getMovimientos();
        List<CuentaDestinoPago> cuentaDestinoPagos = cuenta.getCuentaDestinoPagos();

        Cuenta result  = cuentaRepository.save(cuenta);
        Cliente cliente = clienteRepository.findOne(result.getCliente().getId());
        result.setCliente(cliente);
        result.setPlaza(plazaRepository.findOne(result.getPlaza().getId()));

        cuentaSearchRepository.save(result);
        log.debug("Get Cuenta ID {}", cuenta);
        for (Movimiento movimiento : movimientos) {
            log.debug("Add Movimientos");
            Cuenta thisCuenta = new Cuenta();
            thisCuenta.setId(cuenta.getId());
            movimiento.setCuenta(thisCuenta);
            movimiento.setLastModifiedDate(cuenta.getLastModifiedDate());
            movimiento.setCreated(cuenta.getCreated());
            movimiento.setUpdated(cuenta.getUpdated());
            movimiento.setCreatedDate(cuenta.getCreatedDate());
            movimiento.setSecuenciaMovimiento(getSecuenciaMovimiento(movimiento.getTipoMovimiento()));
            movimientoSearchRepository.save(movimientoSearchRepository.save( movimientoRepository.save(movimiento)));
        }

        for (CuentaDestinoPago cuentaDestinoPago : cuentaDestinoPagos){
            log.debug("CuentaDestinoPagos para Cuenta ID {}", result.getId());
            Cuenta thisCuenta = new Cuenta();
            thisCuenta.setId(cuenta.getId());
            cuentaDestinoPago.setCuenta(thisCuenta);
            CuentaDestinoPago cuentaDestinoPagoResult = cuentaDestinoPagoRepository.save(cuentaDestinoPago);
            cuentaDestinoPagoSearchRepository.save(cuentaDestinoPagoResult);
        }

        log.debug("Save Movimientos para Cuenta ID {}", result.getId());
        return cuenta;
    }

    public Cuenta update(Cuenta cuenta){

        cuenta = beforeCreateOrUpdate(cuenta);

        Cuenta result  =cuentaRepository.save(cuenta);
        cuentaSearchRepository.save(result);


        for (Movimiento movimiento : cuenta.getMovimientos()) {

            movimientoSearchRepository.save(movimientoRepository.save(movimiento));
        }

        for (CuentaDestinoPago cuentaDestinoPago : cuenta.getCuentaDestinoPagos()){
            CuentaDestinoPago cuentaDestinoPagoResult = cuentaDestinoPagoRepository.save(cuentaDestinoPago);
            cuentaDestinoPagoSearchRepository.save(cuentaDestinoPagoResult);
        }



        return cuenta;
    }
    @Transactional(rollbackFor=Exception.class)
    public void delete(Long id)  throws  Exception{

        User user = userService.getUserWithAuthorities();
        Set<Authority> authorities = user.getAuthorities();
        Boolean hasAuthority =false;
        System.out.println("autorities isEmpty : " + authorities.isEmpty());

        for(Authority authority :authorities){
            System.out.println("Role: " + authority.toString());
            hasAuthority  = authority.getName().equals("ROLE_ADMIN") ? true : false;
            if(hasAuthority.equals(true)) break;
        }
        if (!hasAuthority) throw new Exception("El Usuario: " +user.getFirstName()+" "+ user.getLastName()+" no tiene privilegios para eliminar objetos ");

        movimientoSearchRepository.delete(movimientoRepository.removeByCuentaId(id));
        cuentaDestinoPagoSearchRepository.delete( cuentaDestinoPagoRepository.removeByCuentaId(id));


        cuentaRepository.delete(id);
        cuentaSearchRepository.delete(id);

    }
    @Transactional(readOnly = true)
    public Cuenta findOne(Long id){
        Cuenta cuenta = cuentaRepository.findOne(id);
        cuenta.getMovimientos().size(); // eagerly load the association
        cuenta.getCuentaDestinoPagos().size();
        cuenta.getNotas().size();

        return cuenta;
    }

    private  Cuenta  beforeCreateOrUpdate(Cuenta cuenta){
        log.debug("beforeCreateOrUpdate");
        User user = userService.getUserWithAuthorities();

        DateTime now = new DateTime();
        if(cuenta.getId() == null) {
            cuenta.setCreated(user);
            cuenta.setCreatedDate( now );

        }else{
            Cuenta cuentaActual = cuentaRepository.findOne(cuenta.getId());
            cuenta.setCreated(cuentaActual.getCreated());
            cuenta.setCreatedDate(cuentaActual.getCreatedDate());
        }
        cuenta.setLastModifiedDate(now);
        cuenta.setUpdated(user);


        return cuenta;
    }

    private Long getSecuenciaCuenta(TipoDocumentoType tipoDocumentoType){
        Long newSequence = 0L;
        switch (tipoDocumentoType){
            case ENGANCHE: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CUENTA_ENGANCHE); break;
            case PAGARE_SIMPLE: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CUENTA_PAGARE_SIMPLE); break;
            case ESCRITURA: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CUENTA_ESCRITURA); break;
            case CONTRATO_RENTA: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CUENTA_RENTA); break;
        }

        return newSequence;



    }

    private Cuenta getSaldosInicialAndMontoExigible(Cuenta cuenta){
        log.debug("getSaldosInicialAndMontoExigible {} " + cuenta);
        List<Movimiento> movimientos = cuenta.getMovimientos();
        BigDecimal cargo = new BigDecimal(0);
        BigDecimal abono = new BigDecimal(0);
        BigDecimal montoExigible = new BigDecimal(0);
        for(Movimiento movimiento :  movimientos){

            if(movimiento.getTipoMovimiento().equals(TipoMovimientoType.CARGO)){
                cargo = cargo.add(movimiento.getMonto());
                if(movimiento.getFechaVencimiento().isBefore(org.joda.time.LocalDate.now())){
                    montoExigible.add(movimiento.getMonto());
                }

            }else{
                abono = abono.add(movimiento.getMonto());
            }

        }
        cuenta.setMontoExigible(montoExigible);
        cuenta.setSaldo(cargo.subtract(abono));
        cuenta.setSaldoInicial(cuenta.getSaldo());


        return cuenta;
    };

    private Cuenta getCuentaDestinosPagos(Cuenta cuenta){
        log.debug("getCuentaDestinosPagos" + cuenta.getCuentaDestinoPagos().size() );
        //si la applicacion no cuenta con ninguna referencia de pago entonces entra
        //TODO: AGRER LOGICA ANTE LA POSIBILIDAD DE AGREGAR UNA CUENTA DESTINO DE PAGO DESPUES DE CREADA LA CUENTA
        if(cuenta.getCuentaDestinoPagos().size() ==0){
            Set<CuentaDestinoPago> cuentaDestinoPagos =  new HashSet<>();
            Set<EmpresaDestinoPago> empresaDestinoPagos = empresaDestinoPagoRepository.findByPlazaAndEmpresa(cuenta.getPlaza(), cuenta.getEmpresa());
            CuentaDestinoPago cuentaDestinoPago = new CuentaDestinoPago();
            for(EmpresaDestinoPago empresaDestinoPago: empresaDestinoPagos){
                switch (empresaDestinoPago.getDestinoPago().getNombre()){
                    case "CAJA":
                        cuentaDestinoPago = getReferenciaCaja(cuenta);
                        break;
                    //TODO: NO DEBERIA ENTRAR AQUI SI LA CUENTA ES DE CARGA INICIAL
                    case "HSBC":
                        cuentaDestinoPago = getReferenciaHSBC(cuenta);
                        break;



                }
                cuentaDestinoPagos.add(cuentaDestinoPago);

            }
            cuenta.setCuentaDestinoPagos(new ArrayList<CuentaDestinoPago>(cuentaDestinoPagos));
        }
        return cuenta;
    }

    private Cuenta createCuentaEnganche(Cuenta cuenta){


        return cuenta;
    };
    //TODO: asegurar que todos los movimientos vengan ordenados
    //TODO: revisar si debe existir una fecha de movimiento generica para fecha de vencimiento y fecha de pago y diferenciarla  por cargo y abono
    //TODO: revisar que tipo de validaciones debemos tener aquí tomando en cuenta que en la Capa de DTO y en la vista se denen de hacer todas las validaciones

    /*
    1.-Para cada movimiento revisar si tiene correctamente configurados el tipo de movimiento
    2.- Por Implementar
     */
    private Cuenta addMovimientosCuentaEnganche(Cuenta cuenta) throws Exception{
        log.debug("addMovimientosCuentaEnganche {} ");

        /*
        Nos aseguramos que los movimientos que se agreguen o eliminen al hacer una actualizacion de la cuenta.
        se reflejen en la base de datos.
         */
     //  List movimientos  = movimientoRepository.removeByIdNotIn(cuenta.getMovimientos());
       // movimientoSearchRepository.delete(movimientos);


        return cuenta;
    };
    private Cuenta createCuentaEscritura(Cuenta cuenta){
        return cuenta;
    };

    //TODO: implementar la funcionalidad de creación
    private Cuenta addMovimientosPagareSimple(Cuenta cuenta){
        //Si ya cuenta con movimientos se deja tal como viene
        //que es el caso de la migracion
        return cuenta;
    };
    private Cuenta createCuentaFinanciamientoSaldos(Cuenta cuenta){
        return cuenta;
    };

    private CuentaDestinoPago getReferenciaCaja(Cuenta cuenta){
        log.debug("getReferenciaCaja {} " + cuenta);
        CuentaDestinoPago cuentaDestinoPago = new CuentaDestinoPago();
        cuentaDestinoPago.setReferencia(cuenta.getSecuenciaCuenta().toString());
        //TODO:Hacer enum destino pago
        cuentaDestinoPago.setDestinoPago(destinoPagoRepository.findOne(1062l));
        cuentaDestinoPago.setBanco("CAJA");
        cuentaDestinoPago.setConvenio("NO APLICA");
        cuentaDestinoPago.setCuentaClabe("NO APLICA");
     return cuentaDestinoPago;
    }

    private CuentaDestinoPago getReferenciaHSBC(Cuenta cuenta){
        //TODO: falta implementar método de obtención de referencias
        return null;
    }

    private LocalDate getFechaUltimoPago(Cuenta cuenta){
        log.debug("getFechaUltimoPago {} " );
        LocalDate fechaUltimoPago = null;

        List<Movimiento> movimientos = cuenta.getMovimientos();
        Boolean isFirstTime = true;
        for(Movimiento movimiento :  movimientos){

            if(movimiento.getTipoMovimiento().equals(TipoMovimientoType.ABONO)){

                if(isFirstTime) {
                    fechaUltimoPago = movimiento.getFechaPago();
                    isFirstTime = false;

                }
                if (movimiento.getFechaPago().isAfter(fechaUltimoPago)) {
                    fechaUltimoPago = movimiento.getFechaPago();
                }

            }

        }

       return  fechaUltimoPago;
    }
    private Long getSecuenciaMovimiento(TipoMovimientoType tipoMovimiento){
        Long newSequence = 0L;
        switch (tipoMovimiento){
            case CARGO: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.MOVIMIENTO_CARGO); break;
            case ABONO: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.MOVIMIENTO_ABONO); break;

        }

        return newSequence;



    }




    private Movimiento addMovimiento(Movimiento movimiento){
        log.debug("Movimiento addMovimiento{}", movimiento);
        movimiento = beforeCreateOrUpdate(movimiento);
        movimiento.setSecuenciaMovimiento(getSecuenciaMovimiento(movimiento.getTipoMovimiento()));

        Movimiento result  = movimientoRepository.save(movimiento);
        movimientoSearchRepository.save(result);
        return movimiento;
    }
    @Transactional(rollbackFor=Exception.class)
    public Movimiento addPago(Movimiento pago) throws Exception {
        log.debug("addPago" );
        if(pago.getConceptoMovimiento() == ConceptoMovimientoType.PAGO){
            pago.setEstatusMovimiento(EstatusMovimientoType.NO_APLICA);
            pago.setTipoMovimiento(TipoMovimientoType.ABONO);
            if(pago.getFechaVencimiento()== null)
                pago.setFechaVencimiento( LocalDate.now() );
            Integer numeroPagare  = movimientoRepository.numeroPagareMaxByCuentaId(pago.getCuenta().getId());
            pago.setNumeroPagare(numeroPagare == null ? 1 : numeroPagare + 1);



            Integer numeroPago = movimientoRepository.numeroPagoMaxByCuentaId(pago.getCuenta().getId());
            pago.setNumeroPago(numeroPago == null ? 1 : numeroPago +1);

            pago.setSaldo(pago.getMonto());
        }else throw new Exception("Asegurar el concepto de movimiento  PAGO");

        Cuenta cuenta = cuentaRepository.findOne(pago.getCuenta().getId());
        Long folio = foliadorReciboPagoService.getFolio(cuenta.getPlaza().getId(), cuenta.getEmpresa().getId());
        pago.setFolio(cuenta.getEmpresa().getId() +"-" + cuenta.getPlaza().getId() +"-"+ folio );
        Movimiento result =addMovimiento(pago);





        BigDecimal montoPago = pago.getMonto();
        List<Movimiento> movimientos = movimientoRepository.findByCuentaIdOrderByFechaVencimientoAsc(cuenta.getId());

        //TODO:AGREGAR VALIDADCION PARA QUE EL CONCEPTO DE CREDITO TENGA FECHA DE VENCIMEINTO MAYOR A todas las mensualidades
        log.debug("for movimientos size : " + movimientos.size() );

        for (Movimiento movimiento :  movimientos){


            //si el total del pago ya esta alicado salimos del ciclo y terminamos
            if (montoPago.compareTo(BigDecimal.ZERO) <= 0) break;

            //movimeintos en aclaracion o condonados no generan saldo no pasan por aqui
            //excluimos tambien los movimientos de ABONO
            if(movimiento.getTipoMovimiento().equals(TipoMovimientoType.ABONO)) continue;
            if(movimiento.getEstatusMovimiento().equals(EstatusMovimientoType.CONDONADO)) continue;

            //si detectamos movientos sin saldo avanzamos de movimiento
            if (movimiento.getSaldo().compareTo(BigDecimal.ZERO) <= 0) continue;


            log.debug("pasando las validaciones");
            //monto pago es menor al saldo del movimiento
            log.debug("Movimiento ID :" +movimiento.getId() + ", monto: " + movimiento.getMonto()+", saldo: " + movimiento.getSaldo()+", montoAplicar: " + montoPago);
            if(movimiento.getSaldo().compareTo(montoPago) <= 0){

                movimiento.setSaldo(movimiento.getSaldo().subtract(montoPago));
                montoPago = montoPago.subtract(montoPago);

            }else{
                BigDecimal residuo = montoPago.subtract(movimiento.getSaldo());
                movimiento.setSaldo(BigDecimal.ZERO);
                movimiento.setEstatusMovimiento(EstatusMovimientoType.PAGADO);
                montoPago = residuo;
            }

            movimientoSearchRepository.save(movimientoRepository.save(movimiento));

        }
        /*
        * Actualizamos la cuenta y sus saldos
         */

        cuenta.setSaldo(movimientoRepository.saldosByCuentaId(cuenta.getId()));
        LocalDate endOfMonth = LocalDate.fromDateFields(new
            Date()).dayOfMonth().withMaximumValue();
        BigDecimal montoExigible = movimientoRepository.MontoExigibleByCuentaId(cuenta.getId(), endOfMonth);
        log.debug("Monto Exigible fecha : ", endOfMonth);
        cuenta.setMontoExigible(montoExigible == null ? BigDecimal.ZERO : montoExigible);
        cuenta.setFechaUltimoPago(result.getFechaPago());
        cuenta.setMovimientos(movimientos);
        //TODO:aqui el foliador  ya que se usa actualmente ;
        cuentaSearchRepository.save(cuentaRepository.save(cuenta));




        return result;
    }

    private Movimiento beforeCreateOrUpdate(Movimiento movimiento){

        User user = userService.getUserWithAuthorities();

        DateTime now = new DateTime();
        if(movimiento.getId() == null) {
            movimiento.setCreated(user);
            movimiento.setCreatedDate( now );

        }
        movimiento.setLastModifiedDate(now);
        movimiento.setUpdated(user);


        return movimiento;
    }

    @Transactional
    public int updateEstatusMensualidadVencidas(){
        LocalDate endOfMonth = LocalDate.fromDateFields(new Date()).dayOfMonth().withMaximumValue();
        return movimientoRepository.updateEstatusVencidoByFechaVencimiento(endOfMonth);

    }
    //TODO: Desarrollar funcionalidad para enviar recibos de pago por  correo  o estado de cuenta
    public ReciboPagoDTO reciboPago(Long movimientoId){
        log.debug("Recibo Pago  '{}'", movimientoId);
        User user = userService.getUserWithAuthorities();
        Movimiento movimiento = movimientoRepository.findOne(movimientoId);
        Cuenta cuenta = cuentaRepository.findOne(movimiento.getCuenta().getId());
        Empresa empresa =  cuenta.getEmpresa();
        Cliente cliente = cuenta.getCliente();

        ReciboPagoDTO reciboPagoDTO = new ReciboPagoDTO();

        reciboPagoDTO.setCuentaDTO(cuentaMapper.cuentaToCuentaDTO(cuenta));
        reciboPagoDTO.setEmpresaDTO(empresaMapper.empresaToEmpresaDTO(cuenta.getEmpresa()));
        reciboPagoDTO.setClienteDTO(clienteMapper.clienteToClienteDTO(cuenta.getCliente()));
        reciboPagoDTO.setMovimientoDTO(movimientoMapper.movimientoToMovimientoDTO(movimiento));
        log.debug("ReciboPagoDTO '{}'", reciboPagoDTO);


        return reciboPagoDTO;




    }

    @Async
    @Transactional
    public void indexToElastickSearch(){
        log.debug("indexToElastickSearch '{}'");
        List<Cuenta> cuentas = cuentaRepository.findAll();
        for(Cuenta cuenta : cuentas){
            cuenta.getMovimientos().size();
            cuenta.getCuentaDestinoPagos().size();
            cuentaSearchRepository.save(cuenta);
        }
        log.debug("FIN indexToElastickSearch '{}'");

    }




}

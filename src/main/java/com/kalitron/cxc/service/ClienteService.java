package com.kalitron.cxc.service;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.domain.enumeration.SecuenciaType;
import com.kalitron.cxc.domain.enumeration.TipoClienteType;
import com.kalitron.cxc.repository.ClienteRepository;
import com.kalitron.cxc.repository.CuentaRepository;
import com.kalitron.cxc.repository.PlazaRepository;
import com.kalitron.cxc.repository.SecuenciaClienteRepository;
import com.kalitron.cxc.repository.search.ClienteSearchRepository;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ClienteService {

    private final Logger log = LoggerFactory.getLogger(ClienteService.class);

    @Inject
    private ClienteRepository clienteRepository;

    @Inject
    private ClienteSearchRepository clienteSearchRepository;

    @Inject
    private UserService userService;

    @Inject
    private CuentaRepository cuentaRepository;

    @Inject
    private PlazaRepository plazaRepository;




    @Inject
    private CxcSecuenciaService cxcSecuenciaService;

    public Cliente create(Cliente cliente){

        cliente = beforeCreateOrUpdate(cliente);
        cliente.setSecuenciaCliente(getSecuenciaCliente(cliente.getTipoCliente()));
        if(cliente.getCreated() == null) System.out.println("Valio Verga");
        if(cliente.getSecuenciaCliente() == null) System.out.println("Valio Verga la secuencia");


        Cliente result  =clienteRepository.save(cliente);

        result.setPlaza(plazaRepository.findOne(cliente.getPlaza().getId()));
        clienteSearchRepository.save(result);
        return cliente;
    }
    public Cliente update(Cliente cliente){

        cliente = beforeCreateOrUpdate(cliente);

        Cliente result  =clienteRepository.save(cliente);
        clienteSearchRepository.save(result);
        return cliente;
    }

    public void delete(Long id) throws  Exception{
        User user = userService.getUserWithAuthorities();
        Set<Authority> authorities = user.getAuthorities();
        Boolean hasAuthority =false;
        System.out.println("autorities isEmpty : " + authorities.isEmpty());

        for(Authority authority :authorities){
            System.out.println("Role: " + authority.toString());
            hasAuthority  = authority.getName().equals("ROLE_ADMIN") ? true : false;
            if(hasAuthority.equals(true)) break;
        }
        if (!hasAuthority) throw new Exception("El Usuario: " +user.getFirstName()+" "+ user.getLastName()+" no tiene privilegios para eliminar objetos ");

        List<Cuenta> cuentas =  cuentaRepository.findByClienteId(id);

        if(!cuentas.isEmpty()) throw new Exception("No se puede eliminar un cliente cuando tiene cuentas por cobrar asignadas");

        clienteRepository.delete(id);
        clienteSearchRepository.delete(id);
    }

    private Cliente beforeCreateOrUpdate(Cliente cliente){

        User user = userService.getUserWithAuthorities();



        DateTime now = new DateTime();
        if(cliente.getId() == null) {
            cliente.setCreated(user);
            cliente.setCreatedDate( now );

        }else{
            Cliente clienteActual = clienteRepository.findOne(cliente.getId());
            cliente.setCreated(clienteActual.getCreated());
            cliente.setCreatedDate(clienteActual.getCreatedDate());
        }
        cliente.setLastModifiedDate(now);
        cliente.setUpdated(user);


        return cliente;
    }

    private Long getSecuenciaCliente(TipoClienteType tipoCliente){
        Long newSequence = 0L;
        switch (tipoCliente){
            case CLIENTE: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CLIENTE);break;
            case EMPRESARIAL: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CLIENTE_EMPRESARIAL);break;
            case INSTITUCIONAL: newSequence = cxcSecuenciaService.getSecuencia(SecuenciaType.CLIENTE_INSTITUCIONAL);break;
        }

        return newSequence;



    }

    @Async
    @Transactional
    public void indexToElastickSearch(){
        log.debug("indexToElastickSearch Cliente '{}'");
        List<Cliente> clientes = clienteRepository.findAll();
        clienteSearchRepository.save(clientes);

        log.debug("FIN indexToElastickSearch Cliente'{}'");

    }

}

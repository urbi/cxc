package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.SecuenciaType;

/**
 * A CxcSecuencia.
 */
@Entity
@Table(name = "CXCSECUENCIA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="cxcsecuencia")
public class CxcSecuencia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull    

    
    @Enumerated(EnumType.STRING)
    @Column(name = "nombre", nullable = false)
    private SecuenciaType nombre;

    @NotNull
    @Min(value = 0)    

    
    @Column(name = "secuencia", nullable = false)
    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SecuenciaType getNombre() {
        return nombre;
    }

    public void setNombre(SecuenciaType nombre) {
        this.nombre = nombre;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CxcSecuencia cxcSecuencia = (CxcSecuencia) o;

        if ( ! Objects.equals(id, cxcSecuencia.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CxcSecuencia{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", secuencia='" + secuencia + "'" +
                '}';
    }
}

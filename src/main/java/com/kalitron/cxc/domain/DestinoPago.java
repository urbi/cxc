package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DestinoPago.
 */
@Entity
@Table(name = "DESTINOPAGO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="destinopago")
public class DestinoPago implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false, unique = true)
    private String nombre;

    @OneToMany(mappedBy = "destinoPago")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<EmpresaDestinoPago> empresaDestinoPagos = new HashSet<>();

    @OneToMany(mappedBy = "destinoPago")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<CuentaDestinoPago> cuentaDestinoPagos = new HashSet<>();

    @OneToMany(mappedBy = "destinoPago")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PagoReferenciado> pagoReferenciados = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<EmpresaDestinoPago> getEmpresaDestinoPagos() {
        return empresaDestinoPagos;
    }

    public void setEmpresaDestinoPagos(Set<EmpresaDestinoPago> empresaDestinoPagos) {
        this.empresaDestinoPagos = empresaDestinoPagos;
    }

    public Set<CuentaDestinoPago> getCuentaDestinoPagos() {
        return cuentaDestinoPagos;
    }

    public void setCuentaDestinoPagos(Set<CuentaDestinoPago> cuentaDestinoPagos) {
        this.cuentaDestinoPagos = cuentaDestinoPagos;
    }

    public Set<PagoReferenciado> getPagoReferenciados() {
        return pagoReferenciados;
    }

    public void setPagoReferenciados(Set<PagoReferenciado> pagoReferenciados) {
        this.pagoReferenciados = pagoReferenciados;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DestinoPago destinoPago = (DestinoPago) o;

        if ( ! Objects.equals(id, destinoPago.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DestinoPago{" +
            "id=" + id +
            ", nombre='" + nombre + '\'' +
            ", empresaDestinoPagos=" + empresaDestinoPagos +
            ", cuentaDestinoPagos=" + cuentaDestinoPagos +
            ", pagoReferenciados=" + pagoReferenciados +
            '}';
    }
}

package com.kalitron.cxc.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.EstatusPagoType;

/**
 * A PagoReferenciado.
 */
@Entity
@Table(name = "PAGOREFERENCIADO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="pagoreferenciado")
public class PagoReferenciado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "monto_pago", nullable = false,precision=10, scale=2)
    private BigDecimal montoPago;

    @NotNull
    @Column(name = "referencia", nullable = false)
    private String referencia;

    @NotNull
    @Column(name = "cuenta_clabe", nullable = false)
    private String cuentaClabe;

    @Column(name = "observaciones")
    private String observaciones;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "fecha_pago", nullable = false)
    private LocalDate fechaPago;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "fecha_carga_pago", nullable = false)
    private DateTime fechaCargaPago;

    @Column(name = "folio_externo_conciliacion")
    private String folioExternoConciliacion;

    @NotNull
    @Column(name = "folio_carga", nullable = false)
    private String folioCarga;

    @Column(name = "convenio")
    private String convenio;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "fecha_aplicacion_pago", nullable = true)
    private DateTime fechaAplicacionPago;

    @Column(name = "comentario")
    private String comentario;

    @Enumerated(EnumType.STRING)
    @Column(name = "estatus_pago", nullable = false)
    private EstatusPagoType estatusPago;

    @ManyToOne
    private User created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "created_date", nullable = false)
    private DateTime createdDate;

    @ManyToOne
    private User updated;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "last_modified_date", nullable = false)
    private DateTime lastModifiedDate;
    @ManyToOne
    private Plaza plaza;


    @ManyToOne
    private Cuenta cuenta;

    @ManyToOne
    private DestinoPago destinoPago;

    @OneToOne
    private Movimiento movimiento;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMontoPago() {
        return montoPago;
    }

    public void setMontoPago(BigDecimal montoPago) {
        this.montoPago = montoPago;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public DateTime getFechaCargaPago() {
        return fechaCargaPago;
    }

    public void setFechaCargaPago(DateTime fechaCargaPago) {
        this.fechaCargaPago = fechaCargaPago;
    }

    public String getFolioExternoConciliacion() {
        return folioExternoConciliacion;
    }

    public void setFolioExternoConciliacion(String folioExternoConciliacion) {
        this.folioExternoConciliacion = folioExternoConciliacion;
    }

    public String getFolioCarga() {
        return folioCarga;
    }

    public void setFolioCarga(String folioCarga) {
        this.folioCarga = folioCarga;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public DateTime getFechaAplicacionPago() {
        return fechaAplicacionPago;
    }

    public void setFechaAplicacionPago(DateTime fechaAplicacionPago) {
        this.fechaAplicacionPago = fechaAplicacionPago;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public EstatusPagoType getEstatusPago() {
        return estatusPago;
    }

    public void setEstatusPago(EstatusPagoType estatusPago) {
        this.estatusPago = estatusPago;
    }

    public User getCreated() {
        return created;
    }

    public void setCreated(User created) {
        this.created = created;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public User getUpdated() {
        return updated;
    }

    public void setUpdated(User updated) {
        this.updated = updated;
    }

    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public Movimiento getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Movimiento movimiento) {
        this.movimiento = movimiento;
    }

    @Override
    public String toString() {
        return "PagoReferenciado{" +
            "id=" + id +
            ", montoPago=" + montoPago +
            ", referencia='" + referencia + '\'' +
            ", cuentaClabe='" + cuentaClabe + '\'' +
            ", observaciones='" + observaciones + '\'' +
            ", fechaPago=" + fechaPago +
            ", fechaCargaPago=" + fechaCargaPago +
            ", folioExternoConciliacion='" + folioExternoConciliacion + '\'' +
            ", folioCarga='" + folioCarga + '\'' +
            ", convenio='" + convenio + '\'' +
            ", fechaAplicacionPago=" + fechaAplicacionPago +
            ", comentario='" + comentario + '\'' +
            ", estatusPago=" + estatusPago +
            ", created=" + created +
            ", createdDate=" + createdDate +
            ", updated=" + updated +
            ", lastModifiedDate=" + lastModifiedDate +
            ", plaza=" + plaza +
            ", cuenta=" + cuenta +
            ", destinoPago=" + destinoPago +
            '}';
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Plaza getPlaza() {
        return plaza;
    }

    public void setPlaza(Plaza plaza) {
        this.plaza = plaza;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public DestinoPago getDestinoPago() {
        return destinoPago;
    }

    public void setDestinoPago(DestinoPago destinoPago) {
        this.destinoPago = destinoPago;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PagoReferenciado pagoReferenciado = (PagoReferenciado) o;

        if ( ! Objects.equals(id, pagoReferenciado.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}

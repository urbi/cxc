package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.ObjectNameEnum;

/**
 * A FileLoader.
 */
@Entity
@Table(name = "FILELOADER")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="fileloader")
public class FileLoader implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull    

    
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_dato", nullable = false)
    private ObjectNameEnum tipoDato;

    @NotNull    

    
    @Lob
    @Column(name = "file", nullable = false)
    private byte[] file;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ObjectNameEnum getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(ObjectNameEnum tipoDato) {
        this.tipoDato = tipoDato;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FileLoader fileLoader = (FileLoader) o;

        if ( ! Objects.equals(id, fileLoader.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FileLoader{" +
                "id=" + id +
                ", tipoDato='" + tipoDato + "'" +
                ", file='" + file + "'" +
                '}';
    }
}

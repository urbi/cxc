package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A FoliadorReciboPago.
 */
@Entity
@Table(name = "FOLIADORRECIBOPAGO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="foliadorrecibopago")
public class FoliadorReciboPago implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Column(name = "secuencia")
    private Long secuencia;


    
    @Column(name = "empresa")
    private Long empresa;


    
    @Column(name = "plaza")
    private Long plaza;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    public Long getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Long empresa) {
        this.empresa = empresa;
    }

    public Long getPlaza() {
        return plaza;
    }

    public void setPlaza(Long plaza) {
        this.plaza = plaza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FoliadorReciboPago foliadorReciboPago = (FoliadorReciboPago) o;

        if ( ! Objects.equals(id, foliadorReciboPago.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FoliadorReciboPago{" +
                "id=" + id +
                ", secuencia='" + secuencia + "'" +
                ", empresa='" + empresa + "'" +
                ", plaza='" + plaza + "'" +
                '}';
    }
}

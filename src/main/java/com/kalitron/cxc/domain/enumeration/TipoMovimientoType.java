package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoMovimientoType enumeration.
 */
public enum TipoMovimientoType {
    CARGO,ABONO
}

package com.kalitron.cxc.domain.enumeration;

/**
 * The EstatusPagoType enumeration.
 */
public enum EstatusPagoType {
    APLICADO,DUPLICADO,POR_APLICAR,REVISION
}

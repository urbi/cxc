package com.kalitron.cxc.domain.enumeration;

/**
 * The SexType enumeration.
 */
public enum SexType {
    HOMBRE,MUJER
}

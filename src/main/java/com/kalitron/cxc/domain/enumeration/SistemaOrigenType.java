package com.kalitron.cxc.domain.enumeration;

/**
 * The SistemaOrigenType enumeration.
 */
public enum SistemaOrigenType {
    SVU2,SCU_MIGRACION,SVU2_MIGRACION
}

package com.kalitron.cxc.domain.enumeration;

/**
 * The SexoType enumeration.
 */
public enum SexoType {
    HOMBRE,MUJER
}

package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoPagoType enumeration.
 */
public enum TipoPagoType {
    CHEQUE,DEPOSITO_EFECTIVO,EFECTIVO,REFERENCIADO,TARJETA_CREDITO,TARJETA_DEBITO
}

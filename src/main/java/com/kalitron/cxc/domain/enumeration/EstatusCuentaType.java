package com.kalitron.cxc.domain.enumeration;

/**
 * The EstatusCuentaType enumeration.
 */
public enum EstatusCuentaType {
    ACTIVADA,CANCELADA,CONDONADA,PAGADA,ACLARACION
}

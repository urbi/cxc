package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoClienteType enumeration.
 */
public enum TipoClienteType {
    CLIENTE,EMPRESARIAL,INSTITUCIONAL
}

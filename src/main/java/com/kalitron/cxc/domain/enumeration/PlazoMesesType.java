package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoPagoType enumeration.
 */
public enum PlazoMesesType {
   NO_APLICA, _6_MESES
}

package com.kalitron.cxc.domain.enumeration;

/**
 * The EstatusMovimientoType enumeration.
 * El estatus de NO_APLICA es solo en caso de que sea un pago
 */
public enum EstatusMovimientoType {
    CORRIENTE,PAGADO,VENCIDO,CONDONADO,NO_APLICA
}

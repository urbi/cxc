package com.kalitron.cxc.domain.enumeration;

/**
 * The ConceptoMovimientoType enumeration.
 */
public enum ConceptoMovimientoType {
    APARTADO_INICIAL,PAGARE,RENTA,PAGO,INTERES,CREDITO,ENGANCHE
}

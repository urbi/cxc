package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoCargaType enumeration.
 */
public enum TipoCargaType {
    CLIENTE,CUENTA,PAGO_REFERENCIADO,EMPRESA,DESTINO_PAGO,PLAZA,EMPRESA_DESTINO_PAGO,CUENTA_DESTINO_PAGO
}

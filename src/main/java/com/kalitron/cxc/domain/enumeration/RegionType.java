package com.kalitron.cxc.domain.enumeration;

/**
 * The RegionType enumeration.
 */
public enum RegionType {
    PACIFICO,NORTE,CENTRO_SUR
}

package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoCuentaType enumeration.
 */
public enum TipoCuentaType {
    VENCIMIENTO_FIJO,SALDO_INSOLUTO,RENTA
}

package com.kalitron.cxc.domain.enumeration;

/**
 * The TipoDocumentoType enumeration.
 */
public enum TipoDocumentoType {
    ENGANCHE,ESCRITURA,PAGARE_SIMPLE,CONTRATO_RENTA
}

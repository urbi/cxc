package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A Empresa.
 */
@Entity
@Table(name = "EMPRESA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="empresa")
public class Empresa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull

    @Column(name = "nombre", nullable = false, unique = true)
    private String nombre;

    @NotNull


    @Column(name = "rfc", nullable = false)
    private String rfc;



    @Column(name = "telefono")
    private String telefono;



    @Column(name = "correo")
    private String correo;

    @NotNull


    @Column(name = "direccion", nullable = false)
    private String direccion;

    @NotNull
    @Size(min = 5, max = 5)
    @Pattern(regexp = "^[0-9]*$")


    @Column(name = "codigo_postal", length = 5, nullable = false)
    private String codigoPostal;



    @Column(name = "ciudad")
    private String ciudad;

    @NotNull


    @Column(name = "municipio", nullable = false)
    private String municipio;

    @NotNull


    @Column(name = "estado", nullable = false)
    private String estado;

    @OneToMany(mappedBy = "empresa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Cuenta> cuentas= new HashSet<>();

    @OneToMany(mappedBy = "empresa")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<EmpresaDestinoPago> empresaDestinoPagos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Set<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(Set<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public Set<EmpresaDestinoPago> getEmpresaDestinoPagos() {
        return empresaDestinoPagos;
    }

    public void setEmpresaDestinoPagos(Set<EmpresaDestinoPago> empresaDestinoPagos) {
        this.empresaDestinoPagos = empresaDestinoPagos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Empresa empresa = (Empresa) o;

        if ( ! Objects.equals(id, empresa.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Empresa{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", rfc='" + rfc + "'" +
                ", telefono='" + telefono + "'" +
                ", correo='" + correo + "'" +
                ", direccion='" + direccion + "'" +
                ", codigoPostal='" + codigoPostal + "'" +
                ", ciudad='" + ciudad + "'" +
                ", municipio='" + municipio + "'" +
                ", estado='" + estado + "'" +
                '}';
    }
}

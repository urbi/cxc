package com.kalitron.cxc.domain;

import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A SecuenciaMovimiento.
 */
@Entity
@Table(name = "SECUENCIAMOVIMIENTO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="secuenciamovimiento")
public class SecuenciaMovimiento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_movimiento")
    private TipoMovimientoType tipoMovimiento;

    @Column(name = "secuencia")
    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    public TipoMovimientoType getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimientoType tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    @Override
    public String toString() {
        return "SecuenciaMovimiento{" +
            "id=" + id +
            ", tipoMovimiento=" + tipoMovimiento +
            ", secuencia=" + secuencia +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecuenciaMovimiento secuenciaCuenta = (SecuenciaMovimiento) o;

        if ( ! Objects.equals(id, secuenciaCuenta.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }


}

package com.kalitron.cxc.domain;

import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoCuentaType;

/**
 * A SecuenciaCuenta.
 */
@Entity
@Table(name = "SECUENCIACUENTA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="secuenciacuenta")
public class SecuenciaCuenta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_documento")
    private TipoDocumentoType tipoDocumento;

    @Column(name = "secuencia")
    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoType tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecuenciaCuenta secuenciaCuenta = (SecuenciaCuenta) o;

        if ( ! Objects.equals(id, secuenciaCuenta.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SecuenciaCuenta{" +
            "id=" + id +
            ", tipoDocumento=" + tipoDocumento +
            ", secuencia=" + secuencia +
            '}';
    }
}

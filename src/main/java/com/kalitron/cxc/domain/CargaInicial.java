package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoCargaType;

/**
 * A CargaInicial.
 */
@Entity
@Table(name = "CARGAINICIAL")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="cargainicial")
public class CargaInicial implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull


    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_carga", nullable = false)
    private TipoCargaType tipoCarga;



    @Type(type="org.hibernate.type.BinaryType")
    @Column(name = "archivo")

    private byte[] archivo;



    @Column(name = "mensaje")
    private String mensaje;



    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "fecha_carga", nullable = false)
    private DateTime fechaCarga;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoCargaType getTipoCarga() {
        return tipoCarga;
    }

    public void setTipoCarga(TipoCargaType tipoCarga) {
        this.tipoCarga = tipoCarga;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public DateTime getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(DateTime fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CargaInicial cargaInicial = (CargaInicial) o;

        if ( ! Objects.equals(id, cargaInicial.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CargaInicial{" +
                "id=" + id +
                ", tipoCarga='" + tipoCarga + "'" +
                ", archivo='" + archivo + "'" +
                ", mensaje='" + mensaje + "'" +
                ", fechaCarga='" + fechaCarga + "'" +
                '}';
    }
}

package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;

/**
 * A Documento.
 */
//TODO: candidato a eliminar... revisar si se queda esta entidad y que soporte los datos adjuntados del contrato
@Entity
@Table(name = "DOCUMENTO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="documento")
public class Documento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;



    @Column(name = "numero_documento")
    private String numeroDocumento;



    @Column(name = "descripcion")
    private String descripcion;



    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_documento")
    private TipoDocumentoType tipoDocumento;



    @Column(name = "attribute1")
    private String attribute1;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoType tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

  /*  public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
*/
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Documento documento = (Documento) o;

        if ( ! Objects.equals(id, documento.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Documento{" +
                "id=" + id +
                ", numeroDocumento='" + numeroDocumento + "'" +
                ", descripcion='" + descripcion + "'" +
                ", tipoDocumento='" + tipoDocumento + "'" +
                ", attribute1='" + attribute1 + "'" +
                '}';
    }
}

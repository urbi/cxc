package com.kalitron.cxc.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.EstatusMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoPagoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;

/**
 * A Movimiento.
 */
@Entity
@Table(name = "MOVIMIENTO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="movimiento")
public class Movimiento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "monto", precision=10, scale=2, nullable = false)
    private BigDecimal monto;

    @NotNull
    @Column(name = "saldo", precision=10, scale=2)
    private BigDecimal saldo;

    @Column(name = "numero_pagare", nullable = true)
    private Integer numeroPagare;


    @Column(name = "numero_pago", nullable = true)
    private Integer numeroPago;


    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "fecha_vencimiento", nullable = true)
    private LocalDate fechaVencimiento;


    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "fecha_pago", nullable = true)
    private LocalDate fechaPago;

    @Enumerated(EnumType.STRING)
    @Column(name = "estatus_movimiento")
    private EstatusMovimientoType estatusMovimiento;

    @Column(name = "secuencia_movimiento", unique = true, nullable = false)
    private Long secuenciaMovimiento;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pago")
    private TipoPagoType tipoPago;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_movimiento")
    private TipoMovimientoType tipoMovimiento;

    @Enumerated(EnumType.STRING)
    @Column(name = "concepto_movimiento", nullable = false)
    private ConceptoMovimientoType conceptoMovimiento;


    @Column(name= "folio")
    private String folio;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "created_date", nullable = false)
    private DateTime createdDate;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "last_modified_date", nullable = false)
    private DateTime lastModifiedDate;

    @ManyToOne
    private Cuenta cuenta;


    @ManyToOne
    private User created;

    @ManyToOne
    private User updated;

    @OneToOne
    private PagoReferenciado pagoReferenciado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Integer getNumeroPagare() {
        return numeroPagare;
    }

    public void setNumeroPagare(Integer numeroPagare) {
        this.numeroPagare = numeroPagare;
    }

    public Integer getNumeroPago() {
        return numeroPago;
    }

    public void setNumeroPago(Integer numeroPago) {
        this.numeroPago = numeroPago;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public EstatusMovimientoType getEstatusMovimiento() {
        return estatusMovimiento;
    }

    public void setEstatusMovimiento(EstatusMovimientoType estatusMovimiento) {
        this.estatusMovimiento = estatusMovimiento;
    }

    public Long getSecuenciaMovimiento() {
        return secuenciaMovimiento;
    }

    public void setSecuenciaMovimiento(Long secuenciaMovimiento) {
        this.secuenciaMovimiento = secuenciaMovimiento;
    }

    public TipoPagoType getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPagoType tipoPago) {
        this.tipoPago = tipoPago;
    }

    public TipoMovimientoType getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimientoType tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public ConceptoMovimientoType getConceptoMovimiento() {
        return conceptoMovimiento;
    }

    public void setConceptoMovimiento(ConceptoMovimientoType conceptoMovimiento) {
        this.conceptoMovimiento = conceptoMovimiento;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public User getCreated() {
        return created;
    }

    public void setCreated(User created) {
        this.created = created;
    }

    public User getUpdated() {
        return updated;
    }

    public void setUpdated(User updated) {
        this.updated = updated;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public PagoReferenciado getPagoReferenciado() {
        return pagoReferenciado;
    }

    public void setPagoReferenciado(PagoReferenciado pagoReferenciado) {
        this.pagoReferenciado = pagoReferenciado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Movimiento movimiento = (Movimiento) o;

        if ( ! Objects.equals(id, movimiento.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Movimiento{" +
            "id=" + id +
            ", monto=" + monto +
            ", saldo=" + saldo +
            ", numeroPagare=" + numeroPagare +
            ", numeroPago=" + numeroPago +
            ", fechaVencimiento=" + fechaVencimiento +
            ", fechaPago=" + fechaPago +
            ", estatusMovimiento=" + estatusMovimiento +
            ", secuenciaMovimiento=" + secuenciaMovimiento +
            ", tipoPago=" + tipoPago +
            ", tipoMovimiento=" + tipoMovimiento +
            ", conceptoMovimiento=" + conceptoMovimiento +
            ", folio='" + folio + '\'' +
            ", createdDate=" + createdDate +
            ", lastModifiedDate=" + lastModifiedDate +
            ", cuenta=" + cuenta +
            ", created=" + created +
            ", updated=" + updated +
            '}';
    }
}

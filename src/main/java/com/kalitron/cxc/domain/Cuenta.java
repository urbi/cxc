package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.List;
import java.util.Objects;


import com.kalitron.cxc.domain.enumeration.EstatusCuentaType;

/**
 * A Cuenta.
 */
@Entity
@Table(name = "CUENTA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="cuenta")
public class Cuenta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "saldo", precision=10, scale=2, nullable = false)
    private BigDecimal saldo;

    @Column(name = "monto_exigible", precision=10, scale=2, nullable = false)
    private BigDecimal montoExigible;

    @Column(name = "saldo_inicial", precision=10, scale=2, nullable = false)
    private BigDecimal saldoInicial;
    //TODO: Candidato para enum o entity
    @NotNull
    @Column(name = "interes_anual", precision=10, scale=2, nullable = false )
    private BigDecimal interesAnual;
    //TODO: Candidato para enum o entity
    @NotNull
    @Column(name = "plazo_meses",  nullable = false )
    private Long plazoMeses;

    @Column(name= "pago_mensual")
    private BigDecimal pagoMensual;


    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "fecha_ultimo_pago", nullable = true)
    private LocalDate fechaUltimoPago;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "fecha_cobro", nullable = true)
    private LocalDate fechaCobro;

    @Column(name = "numero_contrato_origen")
    private String numeroContratoOrigen;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "attribute1")
    private String attribute1;

    @Column(name = "attribute2")
    private String attribute2;


    @Enumerated(EnumType.STRING)
    @Column(name = "estatus_cuenta")
    private EstatusCuentaType estatusCuenta;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_documento")
    private TipoDocumentoType tipoDocumento;


    @NotNull
    @ManyToOne
    private Cliente cliente;

    @ManyToOne
    private Plaza plaza;

    @OneToMany(mappedBy = "cuenta")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Nota> notas = new ArrayList<>();



    @ManyToOne
    private Empresa empresa;

    @OneToMany(mappedBy = "cuenta")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<CuentaDestinoPago> cuentaDestinoPagos = new ArrayList<>();

    @OneToMany(mappedBy = "cuenta")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<Movimiento> movimientos = new ArrayList<>();


    @OneToMany(mappedBy = "cuenta")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private List<PagoReferenciado> pagoReferenciados = new ArrayList<>();


    @ManyToOne
    private User created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "created_date", nullable = false)
    private DateTime createdDate;

    @ManyToOne
    private User updated;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "last_modified_date", nullable = false)
    private DateTime lastModifiedDate;

    @Column(name = "secuencia_cuenta", unique = true, nullable = false)
    private Long secuenciaCuenta;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getMontoExigible() {
        return montoExigible;
    }

    public void setMontoExigible(BigDecimal montoExigible) {
        this.montoExigible = montoExigible;
    }

    public BigDecimal getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(BigDecimal saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public LocalDate getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    public void setFechaUltimoPago(LocalDate fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    public String getNumeroContratoOrigen() {
        return numeroContratoOrigen;
    }

    public void setNumeroContratoOrigen(String numeroContratoOrigen) {
        this.numeroContratoOrigen = numeroContratoOrigen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }



    public EstatusCuentaType getEstatusCuenta() {
        return estatusCuenta;
    }

    public void setEstatusCuenta(EstatusCuentaType estatusCuenta) {
        this.estatusCuenta = estatusCuenta;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }


    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Plaza getPlaza() {
        return plaza;
    }

    public void setPlaza(Plaza plaza) {
        this.plaza = plaza;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<CuentaDestinoPago> getCuentaDestinoPagos() {
        return cuentaDestinoPagos;
    }

    public void setCuentaDestinoPagos(List<CuentaDestinoPago> cuentaDestinoPagos) {
        this.cuentaDestinoPagos = cuentaDestinoPagos;
    }

    public List<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }



    public List<PagoReferenciado> getPagoReferenciados() {
        return pagoReferenciados;
    }

    public void setPagoReferenciados(List<PagoReferenciado> pagoReferenciados) {
        this.pagoReferenciados = pagoReferenciados;
    }

    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoType tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public User getCreated() {
        return created;
    }

    public void setCreated(User created) {
        this.created = created;
    }

    public User getUpdated() {
        return updated;
    }

    public void setUpdated(User updated) {
        this.updated = updated;
    }

    public Long getSecuenciaCuenta() {
        return secuenciaCuenta;
    }

    public void setSecuenciaCuenta(Long secuenciaCuenta) {
        this.secuenciaCuenta = secuenciaCuenta;
    }

    public BigDecimal getInteresAnual() {
        return interesAnual;
    }

    public void setInteresAnual(BigDecimal interesAnual) {
        this.interesAnual = interesAnual;
    }

    public Long getPlazoMeses() {
        return plazoMeses;
    }

    public void setPlazoMeses(Long plazoMeses) {
        this.plazoMeses = plazoMeses;
    }

    public LocalDate getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(LocalDate fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public BigDecimal getPagoMensual() {
        return pagoMensual;
    }

    public void setPagoMensual(BigDecimal pagoMensual) {
        this.pagoMensual = pagoMensual;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Cuenta cuenta = (Cuenta) o;

        if ( ! Objects.equals(id, cuenta.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Cuenta{" +
            "id=" + id +
            ", saldo=" + saldo +
            ", montoExigible=" + montoExigible +
            ", saldoInicial=" + saldoInicial +
            ", pagoMensual=" + pagoMensual +
            ", interesAnual=" + interesAnual +
            ", plazoMeses=" + plazoMeses +
            ", fechaUltimoPago=" + fechaUltimoPago +
            ", fechaCobro=" + fechaCobro +
            ", numeroContratoOrigen='" + numeroContratoOrigen + '\'' +
            ", descripcion='" + descripcion + '\'' +
            ", attribute1='" + attribute1 + '\'' +
            ", attribute2='" + attribute2 + '\'' +
                       ", estatusCuenta=" + estatusCuenta +
            ", tipoDocumento=" + tipoDocumento +
            ", cliente=" + cliente +
            ", plaza=" + plaza +
           // ", notas=" + notaList() +
            ", empresa=" + empresa +
          //  ", cuentaDestinoPagos=" + cuentaDestinoPagoList() +
           // ", movimientos=" + movimientoList() +
           // ", pagoReferenciados=" + pagoReferenciados +
            ", created=" + created +
            ", createdDate=" + createdDate +
            ", updated=" + updated +
            ", lastModifiedDate=" + lastModifiedDate +
            ", secuenciaCuenta=" + secuenciaCuenta +
            '}';
    }

    private String movimientoList(){
        String str ="{";

        if(movimientos != null)
            for(Movimiento movimiento :  movimientos){
                str +=  movimiento.toString();
            }
        return  str + "}";
    }
    private String cuentaDestinoPagoList(){
        String str ="{";
        if(cuentaDestinoPagos != null)
            for(CuentaDestinoPago cuentaDestinoPago :  cuentaDestinoPagos){
                str += cuentaDestinoPago.toString();
            }
        return  str + "}";
    }

    private String notaList(){
        String str ="{";
        if(notas != null)
            for(Nota nota :  notas){
                str += nota.toString();
            }
        return  str + "}";
    }
}

package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A CuentaDestinoPago.
 */
@Entity
@Table(name = "CUENTADESTINOPAGO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="cuentadestinopago")
public class CuentaDestinoPago implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "convenio")
    private String convenio;

    @Column(name = "cuenta_clabe")
    private String cuentaClabe;

    @Column(name = "referencia")
    private String referencia;

    @Column(name = "banco")
    private String banco;

    @ManyToOne
    private Cuenta cuenta;

    @OneToOne
    private DestinoPago destinoPago;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public DestinoPago getDestinoPago() {
        return destinoPago;
    }

    public void setDestinoPago(DestinoPago destinoPago) {
        this.destinoPago = destinoPago;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CuentaDestinoPago cuentaDestinoPago = (CuentaDestinoPago) o;

        if ( ! Objects.equals(id, cuentaDestinoPago.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CuentaDestinoPago{" +
                "id=" + id +
                ", convenio='" + convenio + "'" +
                ", cuentaClabe='" + cuentaClabe + "'" +
                ", referencia='" + referencia + "'" +
                ", banco='" + banco + "'" +
                '}';
    }
}

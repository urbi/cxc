package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A EmpresaDestinoPago.
 */
@Entity
@Table(name = "EMPRESADESTINOPAGO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="empresadestinopago")
public class EmpresaDestinoPago implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "generador_referencia")
    private String generadorReferencia;

    @ManyToOne
    private DestinoPago destinoPago;

    @ManyToOne
    private Empresa empresa;

    @ManyToOne
    private Plaza plaza;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneradorReferencia() {
        return generadorReferencia;
    }

    public void setGeneradorReferencia(String generadorReferencia) {
        this.generadorReferencia = generadorReferencia;
    }

    public DestinoPago getDestinoPago() {
        return destinoPago;
    }

    public void setDestinoPago(DestinoPago destinoPago) {
        this.destinoPago = destinoPago;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Plaza getPlaza() {
        return plaza;
    }

    public void setPlaza(Plaza plaza) {
        this.plaza = plaza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmpresaDestinoPago empresaDestinoPago = (EmpresaDestinoPago) o;

        if ( ! Objects.equals(id, empresaDestinoPago.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EmpresaDestinoPago{" +
                "id=" + id +
                ", generadorReferencia='" + generadorReferencia + "'" +
                '}';
    }
}

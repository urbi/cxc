package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoClienteType;

/**
 * A SecuenciaCliente.
 */
@Entity
@Table(name = "SECUENCIACLIENTE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="secuenciacliente")
public class SecuenciaCliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_cliente")
    private TipoClienteType tipoCliente;

    @Column(name = "secuencia")
    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoClienteType getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoClienteType tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecuenciaCliente secuenciaCliente = (SecuenciaCliente) o;

        if ( ! Objects.equals(id, secuenciaCliente.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SecuenciaCliente{" +
                "id=" + id +
                ", tipoCliente='" + tipoCliente + "'" +
                ", secuencia='" + secuencia + "'" +
                '}';
    }
}

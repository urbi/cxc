package com.kalitron.cxc.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;

/**
 * A ConceptoMovimiento.
 */
@Entity
@Table(name = "CONCEPTOMOVIMIENTO")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="conceptomovimiento")
public class ConceptoMovimiento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Enumerated(EnumType.STRING)
    @Column(name = "nombre")
    private ConceptoMovimientoType nombre;


    
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_movimiento")
    private TipoMovimientoType tipoMovimiento;

    @ManyToOne
    private Movimiento movimiento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConceptoMovimientoType getNombre() {
        return nombre;
    }

    public void setNombre(ConceptoMovimientoType nombre) {
        this.nombre = nombre;
    }

    public TipoMovimientoType getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimientoType tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Movimiento getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Movimiento movimiento) {
        this.movimiento = movimiento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConceptoMovimiento conceptoMovimiento = (ConceptoMovimiento) o;

        if ( ! Objects.equals(id, conceptoMovimiento.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ConceptoMovimiento{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", tipoMovimiento='" + tipoMovimiento + "'" +
                '}';
    }
}

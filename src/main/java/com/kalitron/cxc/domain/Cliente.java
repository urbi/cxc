package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.SexoType;
import com.kalitron.cxc.domain.enumeration.SistemaOrigenType;
import com.kalitron.cxc.domain.enumeration.TipoClienteType;

/**
 * A Cliente.
 */
@Entity
@Table(name = "CLIENTE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="cliente")
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombres", nullable = false)
    private String nombres;

    @NotNull
    @Column(name = "apellido_paterno", nullable = false)
    private String apellidoPaterno;

    @Column(name = "apellido_materno")
    private String apellidoMaterno;

    @NotNull
    @Size(min = 10, max = 13)
    @Column(name = "rfc", length = 13, nullable = false)
    private String rfc;

    @Size(min = 18, max = 18)
    @Column(name = "curp", length = 18)
    private String curp;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexo")
    private SexoType sexo;

    @Size(min = 11, max = 11)
    @Column(name = "nss", length = 11)
    private String nss;

    @Column(name = "correo")
    private String correo;

    @Column(name = "telefono1")
    private String telefono1;

    @Column(name = "telefono2")
    private String telefono2;

    @Column(name = "celular")
    private String celular;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @Column(name = "numero_ref_externa")
    private String numeroRefExterna;

    @Enumerated(EnumType.STRING)
    @Column(name = "sistema_origen")
    private SistemaOrigenType sistemaOrigen;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_cliente")
    private TipoClienteType tipoCliente;

    @NotNull
    @Column(name = "calle", nullable = false)
    private String calle;

    @Column(name = "numero")
    private String numero;

    @NotNull
    @Size(min = 5, max = 5)
    @Pattern(regexp = "^[0-9]*$")
    @Column(name = "codigo_postal", length = 5, nullable = false)
    private String codigoPostal;

    @NotNull
    @Column(name = "colonia", nullable = false)
    private String colonia;

    @Column(name = "ciudad")
    private String ciudad;

    @NotNull
    @Column(name = "estado", nullable = false)
    private String estado;

    @NotNull
    @Column(name = "municipio", nullable = false)
    private String municipio;

    @Column(name = "geo_localizacion")
    private String geoLocalizacion;

    @ManyToOne
    private User created;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "created_date", nullable = false)
    private DateTime createdDate;

    @ManyToOne
    private User updated;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "last_modified_date", nullable = false)
    private DateTime lastModifiedDate;

    @Column(name = "secuencia_cliente", unique = true, nullable = false)
    private Long secuenciaCliente;

    @OneToMany(mappedBy = "cliente")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Cuenta> cuentas = new HashSet<>();

    @ManyToOne
    private Plaza plaza;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public SexoType getSexo() {
        return sexo;
    }

    public void setSexo(SexoType sexo) {
        this.sexo = sexo;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumeroRefExterna() {
        return numeroRefExterna;
    }

    public void setNumeroRefExterna(String numeroRefExterna) {
        this.numeroRefExterna = numeroRefExterna;
    }

    public SistemaOrigenType getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(SistemaOrigenType sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public TipoClienteType getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoClienteType tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getGeoLocalizacion() {
        return geoLocalizacion;
    }

    public void setGeoLocalizacion(String geoLocalizacion) {
        this.geoLocalizacion = geoLocalizacion;
    }

    public User getCreated() {
        return created;
    }

    public void setCreated(User created) {
        this.created = created;
    }

    public User getUpdated() {
        return updated;
    }

    public void setUpdated(User updated) {
        this.updated = updated;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }



    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getSecuenciaCliente() {
        return secuenciaCliente;
    }

    public void setSecuenciaCliente(Long secuenciaCliente) {
        this.secuenciaCliente = secuenciaCliente;
    }

    public Set<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(Set<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public Plaza getPlaza() {
        return plaza;
    }

    public void setPlaza(Plaza plaza) {
        this.plaza = plaza;
    }

    public String getFullName(){
        return (nombres + " " +apellidoPaterno +" " +apellidoMaterno).trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Cliente cliente = (Cliente) o;

        if ( ! Objects.equals(id, cliente.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nombres='" + nombres + "'" +
                ", apellidoPaterno='" + apellidoPaterno + "'" +
                ", apellidoMaterno='" + apellidoMaterno + "'" +
                ", rfc='" + rfc + "'" +
                ", curp='" + curp + "'" +
                ", sexo='" + sexo + "'" +
                ", nss='" + nss + "'" +
                ", correo='" + correo + "'" +
                ", telefono1='" + telefono1 + "'" +
                ", telefono2='" + telefono2 + "'" +
                ", celular='" + celular + "'" +
                ", fechaNacimiento='" + fechaNacimiento + "'" +
                ", numeroRefExterna='" + numeroRefExterna + "'" +
                ", sistemaOrigen='" + sistemaOrigen + "'" +
                ", tipoCliente='" + tipoCliente + "'" +
                ", calle='" + calle + "'" +
                ", numero='" + numero + "'" +
                ", codigoPostal='" + codigoPostal + "'" +
                ", colonia='" + colonia + "'" +
                ", ciudad='" + ciudad + "'" +
                ", estado='" + estado + "'" +
                ", municipio='" + municipio + "'" +
                ", geoLocalizacion='" + geoLocalizacion + "'" +
                ", created='" + created + "'" +
                ", createdDate='" + createdDate + "'" +
                ", updated='" + updated + "'" +
                ", lastModifiedDate='" + lastModifiedDate + "'" +
                ", secuenciaCliente='" + secuenciaCliente + "'" +
                '}';
    }
}

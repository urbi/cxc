package com.kalitron.cxc.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.RegionType;

/**
 * A Plaza.
 */
@Entity
@Table(name = "PLAZA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="plaza")
public class Plaza implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "codigo_postal")
    private String codigoPostal;

    @Column(name = "ciudad")
    private String ciudad;

    @Column(name = "municipio")
    private String municipio;

    @Column(name = "estado")
    private String estado;

    @Enumerated(EnumType.STRING)
    @Column(name = "region")
    private RegionType region;

    @OneToMany(mappedBy = "plaza")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Cliente> clientes = new HashSet<>();

    @OneToMany(mappedBy = "plaza")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Cuenta> cuentas = new HashSet<>();

    @OneToMany(mappedBy = "plaza")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PagoReferenciado> pagoReferenciados = new HashSet<>();

    @OneToMany(mappedBy = "plaza")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Caja> cajas = new HashSet<>();

    @ManyToMany(mappedBy = "plazas")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<User> users = new HashSet<>();

    @OneToMany(mappedBy = "plaza")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<EmpresaDestinoPago> empresaDestinoPagos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public RegionType getRegion() {
        return region;
    }

    public void setRegion(RegionType region) {
        this.region = region;
    }

    public Set<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(Set<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Set<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(Set<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public Set<PagoReferenciado> getPagoReferenciados() {
        return pagoReferenciados;
    }

    public void setPagoReferenciados(Set<PagoReferenciado> pagoReferenciados) {
        this.pagoReferenciados = pagoReferenciados;
    }

    public Set<Caja> getCajas() {
        return cajas;
    }

    public void setCajas(Set<Caja> cajas) {
        this.cajas = cajas;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<EmpresaDestinoPago> getEmpresaDestinoPagos() {
        return empresaDestinoPagos;
    }

    public void setEmpresaDestinoPagos(Set<EmpresaDestinoPago> empresaDestinoPagos) {
        this.empresaDestinoPagos = empresaDestinoPagos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Plaza plaza = (Plaza) o;

        if ( ! Objects.equals(id, plaza.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Plaza{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", direccion='" + direccion + "'" +
                ", codigoPostal='" + codigoPostal + "'" +
                ", ciudad='" + ciudad + "'" +
                ", municipio='" + municipio + "'" +
                ", estado='" + estado + "'" +
                ", region='" + region + "'" +
                '}';
    }
}

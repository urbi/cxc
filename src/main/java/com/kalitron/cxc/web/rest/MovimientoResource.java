package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Movimiento;
import com.kalitron.cxc.repository.MovimientoRepository;
import com.kalitron.cxc.repository.search.MovimientoSearchRepository;
import com.kalitron.cxc.service.CuentaService;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.MovimientoDTO;
import com.kalitron.cxc.web.rest.mapper.MovimientoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Movimiento.
 */
@RestController
@RequestMapping("/api")
public class MovimientoResource {

    private final Logger log = LoggerFactory.getLogger(MovimientoResource.class);

    @Inject
    private MovimientoRepository movimientoRepository;

    @Inject
    private MovimientoMapper movimientoMapper;

    @Inject
    private MovimientoSearchRepository movimientoSearchRepository;

    @Inject
    private CuentaService cuentaService;

    /**
     * POST  /movimientos -> Create a new movimiento.
     */
    @RequestMapping(value = "/movimientos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MovimientoDTO> create(@Valid @RequestBody MovimientoDTO movimientoDTO) throws URISyntaxException {
        log.debug("REST request to save Movimiento : {}", movimientoDTO);
        if (movimientoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new movimiento cannot already have an ID").body(null);
        }
        Movimiento movimiento = movimientoMapper.movimientoDTOToMovimiento(movimientoDTO);
        try {
            Movimiento result = cuentaService.addPago(movimiento);
            return ResponseEntity.created(new URI("/api/movimientos/" + movimientoDTO.getId())).body(movimientoMapper.movimientoToMovimientoDTO(result));
        }catch (Exception e){
            log.debug("REST request to save Movimiento : {}", e.getMessage());
        }
        return ResponseEntity.badRequest().header("Failure", "por razones desconocidas no se puede grabar el pago").body(null);
    }

    /**
     * PUT  /movimientos -> Updates an existing movimiento.
     */
    @RequestMapping(value = "/movimientos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MovimientoDTO> update(@Valid @RequestBody MovimientoDTO movimientoDTO) throws URISyntaxException {
        log.debug("REST request to update Movimiento : {}", movimientoDTO);
        if (movimientoDTO.getId() == null) {
            return create(movimientoDTO);
        }
        Movimiento movimiento = movimientoMapper.movimientoDTOToMovimiento(movimientoDTO);
        Movimiento result = movimientoRepository.save(movimiento);
        movimientoSearchRepository.save(movimiento);
        return ResponseEntity.ok().body(movimientoMapper.movimientoToMovimientoDTO(result));
    }

    /**
     * GET  /movimientos -> get all the movimientos.
     */
    @RequestMapping(value = "/movimientos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<MovimientoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Movimiento> page = movimientoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/movimientos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(movimientoMapper::movimientoToMovimientoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /movimientos/:id -> get the "id" movimiento.
     */
    @RequestMapping(value = "/movimientos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MovimientoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Movimiento : {}", id);
        return Optional.ofNullable(movimientoRepository.findOne(id))
            .map(movimientoMapper::movimientoToMovimientoDTO)
            .map(movimientoDTO -> new ResponseEntity<>(
                movimientoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /movimientos/:id -> delete the "id" movimiento.
     */
    @RequestMapping(value = "/movimientos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Movimiento : {}", id);
        movimientoRepository.delete(id);
        movimientoSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/movimientos/:query -> search for the movimiento corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/movimientos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Movimiento> search(@PathVariable String query) {
        return StreamSupport
            .stream(movimientoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

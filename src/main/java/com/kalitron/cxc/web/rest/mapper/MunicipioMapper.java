package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.MunicipioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Municipio and its DTO MunicipioDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MunicipioMapper {

    @Mapping(source = "estado.id", target = "estadoId")
    @Mapping(source = "estado.nombre", target = "estadoNombre")
    MunicipioDTO municipioToMunicipioDTO(Municipio municipio);

    @Mapping(source = "estadoId", target = "estado")
    Municipio municipioDTOToMunicipio(MunicipioDTO municipioDTO);

    default Estado estadoFromId(Long id) {
        if (id == null) {
            return null;
        }
        Estado estado = new Estado();
        estado.setId(id);
        return estado;
    }
}

package com.kalitron.cxc.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.RegionType;

/**
 * A DTO for the Plaza entity.
 */
public class PlazaDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombre;

    private String direccion;

    private String codigoPostal;

    private String ciudad;

    private String municipio;

    private String estado;

    private RegionType region;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public RegionType getRegion() {
        return region;
    }

    public void setRegion(RegionType region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlazaDTO plazaDTO = (PlazaDTO) o;

        if ( ! Objects.equals(id, plazaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PlazaDTO{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", direccion='" + direccion + "'" +
                ", codigoPostal='" + codigoPostal + "'" +
                ", ciudad='" + ciudad + "'" +
                ", municipio='" + municipio + "'" +
                ", estado='" + estado + "'" +
                ", region='" + region + "'" +
                '}';
    }
}

package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.EmpresaDestinoPago;
import com.kalitron.cxc.repository.EmpresaDestinoPagoRepository;
import com.kalitron.cxc.repository.search.EmpresaDestinoPagoSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.EmpresaDestinoPagoDTO;
import com.kalitron.cxc.web.rest.mapper.EmpresaDestinoPagoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing EmpresaDestinoPago.
 */
@RestController
@RequestMapping("/api")
public class EmpresaDestinoPagoResource {

    private final Logger log = LoggerFactory.getLogger(EmpresaDestinoPagoResource.class);

    @Inject
    private EmpresaDestinoPagoRepository empresaDestinoPagoRepository;

    @Inject
    private EmpresaDestinoPagoMapper empresaDestinoPagoMapper;

    @Inject
    private EmpresaDestinoPagoSearchRepository empresaDestinoPagoSearchRepository;

    /**
     * POST  /empresaDestinoPagos -> Create a new empresaDestinoPago.
     */
    @RequestMapping(value = "/empresaDestinoPagos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EmpresaDestinoPagoDTO> create(@RequestBody EmpresaDestinoPagoDTO empresaDestinoPagoDTO) throws URISyntaxException {
        log.debug("REST request to save EmpresaDestinoPago : {}", empresaDestinoPagoDTO);
        if (empresaDestinoPagoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new empresaDestinoPago cannot already have an ID").body(null);
        }
        EmpresaDestinoPago empresaDestinoPago = empresaDestinoPagoMapper.empresaDestinoPagoDTOToEmpresaDestinoPago(empresaDestinoPagoDTO);
        EmpresaDestinoPago result = empresaDestinoPagoRepository.save(empresaDestinoPago);
        empresaDestinoPagoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/empresaDestinoPagos/" + empresaDestinoPagoDTO.getId())).body(empresaDestinoPagoMapper.empresaDestinoPagoToEmpresaDestinoPagoDTO(result));
    }

    /**
     * PUT  /empresaDestinoPagos -> Updates an existing empresaDestinoPago.
     */
    @RequestMapping(value = "/empresaDestinoPagos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EmpresaDestinoPagoDTO> update(@RequestBody EmpresaDestinoPagoDTO empresaDestinoPagoDTO) throws URISyntaxException {
        log.debug("REST request to update EmpresaDestinoPago : {}", empresaDestinoPagoDTO);
        if (empresaDestinoPagoDTO.getId() == null) {
            return create(empresaDestinoPagoDTO);
        }
        EmpresaDestinoPago empresaDestinoPago = empresaDestinoPagoMapper.empresaDestinoPagoDTOToEmpresaDestinoPago(empresaDestinoPagoDTO);
        EmpresaDestinoPago result = empresaDestinoPagoRepository.save(empresaDestinoPago);
        empresaDestinoPagoSearchRepository.save(empresaDestinoPago);
        return ResponseEntity.ok().body(empresaDestinoPagoMapper.empresaDestinoPagoToEmpresaDestinoPagoDTO(result));
    }

    /**
     * GET  /empresaDestinoPagos -> get all the empresaDestinoPagos.
     */
    @RequestMapping(value = "/empresaDestinoPagos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<EmpresaDestinoPagoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<EmpresaDestinoPago> page = empresaDestinoPagoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/empresaDestinoPagos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(empresaDestinoPagoMapper::empresaDestinoPagoToEmpresaDestinoPagoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /empresaDestinoPagos/:id -> get the "id" empresaDestinoPago.
     */
    @RequestMapping(value = "/empresaDestinoPagos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EmpresaDestinoPagoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get EmpresaDestinoPago : {}", id);
        return Optional.ofNullable(empresaDestinoPagoRepository.findOne(id))
            .map(empresaDestinoPagoMapper::empresaDestinoPagoToEmpresaDestinoPagoDTO)
            .map(empresaDestinoPagoDTO -> new ResponseEntity<>(
                empresaDestinoPagoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /empresaDestinoPagos/:id -> delete the "id" empresaDestinoPago.
     */
    @RequestMapping(value = "/empresaDestinoPagos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete EmpresaDestinoPago : {}", id);
        empresaDestinoPagoRepository.delete(id);
        empresaDestinoPagoSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/empresaDestinoPagos/:query -> search for the empresaDestinoPago corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/empresaDestinoPagos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<EmpresaDestinoPago> search(@PathVariable String query) {
        return StreamSupport
            .stream(empresaDestinoPagoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

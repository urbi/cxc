package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.EmpresaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Empresa and its DTO EmpresaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmpresaMapper {

    EmpresaDTO empresaToEmpresaDTO(Empresa empresa);

    Empresa empresaDTOToEmpresa(EmpresaDTO empresaDTO);
}

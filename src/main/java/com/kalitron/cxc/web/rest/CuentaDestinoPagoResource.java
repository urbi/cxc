package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.CuentaDestinoPago;
import com.kalitron.cxc.repository.CuentaDestinoPagoRepository;
import com.kalitron.cxc.repository.search.CuentaDestinoPagoSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.CuentaDestinoPagoDTO;
import com.kalitron.cxc.web.rest.mapper.CuentaDestinoPagoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CuentaDestinoPago.
 */
//TODO: modificar el Resource para que las operaciones CRUD apunten al Service, como buena practica si existe el service que todas las operaciones se hagan de el.
@RestController
@RequestMapping("/api")
public class CuentaDestinoPagoResource {

    private final Logger log = LoggerFactory.getLogger(CuentaDestinoPagoResource.class);

    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private CuentaDestinoPagoMapper cuentaDestinoPagoMapper;

    @Inject
    private CuentaDestinoPagoSearchRepository cuentaDestinoPagoSearchRepository;

    /**
     * POST  /cuentaDestinoPagos -> Create a new cuentaDestinoPago.
     */
    @RequestMapping(value = "/cuentaDestinoPagos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CuentaDestinoPagoDTO> create(@RequestBody CuentaDestinoPagoDTO cuentaDestinoPagoDTO) throws URISyntaxException {
        log.debug("REST request to save CuentaDestinoPago : {}", cuentaDestinoPagoDTO);
        if (cuentaDestinoPagoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new cuentaDestinoPago cannot already have an ID").body(null);
        }
        CuentaDestinoPago cuentaDestinoPago = cuentaDestinoPagoMapper.cuentaDestinoPagoDTOToCuentaDestinoPago(cuentaDestinoPagoDTO);
        CuentaDestinoPago result = cuentaDestinoPagoRepository.save(cuentaDestinoPago);
        cuentaDestinoPagoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/cuentaDestinoPagos/" + cuentaDestinoPagoDTO.getId())).body(cuentaDestinoPagoMapper.cuentaDestinoPagoToCuentaDestinoPagoDTO(result));
    }

    /**
     * PUT  /cuentaDestinoPagos -> Updates an existing cuentaDestinoPago.
     */
    @RequestMapping(value = "/cuentaDestinoPagos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CuentaDestinoPagoDTO> update(@RequestBody CuentaDestinoPagoDTO cuentaDestinoPagoDTO) throws URISyntaxException {
        log.debug("REST request to update CuentaDestinoPago : {}", cuentaDestinoPagoDTO);
        if (cuentaDestinoPagoDTO.getId() == null) {
            return create(cuentaDestinoPagoDTO);
        }
        CuentaDestinoPago cuentaDestinoPago = cuentaDestinoPagoMapper.cuentaDestinoPagoDTOToCuentaDestinoPago(cuentaDestinoPagoDTO);
        CuentaDestinoPago result = cuentaDestinoPagoRepository.save(cuentaDestinoPago);
        cuentaDestinoPagoSearchRepository.save(cuentaDestinoPago);
        return ResponseEntity.ok().body(cuentaDestinoPagoMapper.cuentaDestinoPagoToCuentaDestinoPagoDTO(result));
    }

    /**
     * GET  /cuentaDestinoPagos -> get all the cuentaDestinoPagos.
     */
    @RequestMapping(value = "/cuentaDestinoPagos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CuentaDestinoPagoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<CuentaDestinoPago> page = cuentaDestinoPagoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cuentaDestinoPagos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(cuentaDestinoPagoMapper::cuentaDestinoPagoToCuentaDestinoPagoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /cuentaDestinoPagos/:id -> get the "id" cuentaDestinoPago.
     */
    @RequestMapping(value = "/cuentaDestinoPagos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CuentaDestinoPagoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get CuentaDestinoPago : {}", id);
        return Optional.ofNullable(cuentaDestinoPagoRepository.findOne(id))
            .map(cuentaDestinoPagoMapper::cuentaDestinoPagoToCuentaDestinoPagoDTO)
            .map(cuentaDestinoPagoDTO -> new ResponseEntity<>(
                cuentaDestinoPagoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cuentaDestinoPagos/:id -> delete the "id" cuentaDestinoPago.
     */
    @RequestMapping(value = "/cuentaDestinoPagos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete CuentaDestinoPago : {}", id);
        cuentaDestinoPagoRepository.delete(id);
        cuentaDestinoPagoSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/cuentaDestinoPagos/:query -> search for the cuentaDestinoPago corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/cuentaDestinoPagos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CuentaDestinoPago> search(@PathVariable String query) {
        return StreamSupport
            .stream(cuentaDestinoPagoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.FileLoaderDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FileLoader and its DTO FileLoaderDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FileLoaderMapper {

    FileLoaderDTO fileLoaderToFileLoaderDTO(FileLoader fileLoader);

    FileLoader fileLoaderDTOToFileLoader(FileLoaderDTO fileLoaderDTO);
}

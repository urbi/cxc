package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.SecuenciaClienteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SecuenciaCliente and its DTO SecuenciaClienteDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SecuenciaClienteMapper {

    SecuenciaClienteDTO secuenciaClienteToSecuenciaClienteDTO(SecuenciaCliente secuenciaCliente);

    SecuenciaCliente secuenciaClienteDTOToSecuenciaCliente(SecuenciaClienteDTO secuenciaClienteDTO);
}

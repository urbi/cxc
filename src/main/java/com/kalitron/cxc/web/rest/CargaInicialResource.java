package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.CargaInicial;
import com.kalitron.cxc.repository.CargaInicialRepository;
import com.kalitron.cxc.repository.search.CargaInicialSearchRepository;
import com.kalitron.cxc.service.CargaInicialService;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.dto.CargaInicialDTO;
import com.kalitron.cxc.web.rest.mapper.CargaInicialMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CargaInicial.
 */
@RestController
@RequestMapping("/api")
public class CargaInicialResource {

    private final Logger log = LoggerFactory.getLogger(CargaInicialResource.class);

    @Inject
    private CargaInicialRepository cargaInicialRepository;

    @Inject
    private CargaInicialMapper cargaInicialMapper;

    @Inject
    private CargaInicialSearchRepository cargaInicialSearchRepository;

    @Inject
    private CargaInicialService cargaInicialService;

    /**
     * POST  /cargaInicials -> Create a new cargaInicial.
     */
    @RequestMapping(value = "/cargaInicials",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CargaInicialDTO> create(@Valid @RequestBody CargaInicialDTO cargaInicialDTO) throws URISyntaxException {
        log.debug("REST request to save CargaInicial : {}", cargaInicialDTO);
        if (cargaInicialDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new cargaInicial cannot already have an ID").body(null);
        }
        CargaInicial cargaInicial = cargaInicialMapper.cargaInicialDTOToCargaInicial(cargaInicialDTO);
        cargaInicial = cargaInicialService.procesaCargaInicial(cargaInicial);
        cargaInicial.setFechaCarga(new DateTime());
        CargaInicial result = cargaInicialRepository.save(cargaInicial);
        cargaInicialSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/cargaInicials/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("cargaInicial", result.getId().toString()))
                .body(cargaInicialMapper.cargaInicialToCargaInicialDTO(result));
    }

    /**
     * PUT  /cargaInicials -> Updates an existing cargaInicial.
     */
    @RequestMapping(value = "/cargaInicials",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CargaInicialDTO> update(@Valid @RequestBody CargaInicialDTO cargaInicialDTO) throws URISyntaxException {
        log.debug("REST request to update CargaInicial : {}", cargaInicialDTO);
        if (cargaInicialDTO.getId() == null) {
            return create(cargaInicialDTO);
        }
        CargaInicial cargaInicial = cargaInicialMapper.cargaInicialDTOToCargaInicial(cargaInicialDTO);
        CargaInicial result = cargaInicialRepository.save(cargaInicial);
        cargaInicialSearchRepository.save(cargaInicial);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("cargaInicial", cargaInicialDTO.getId().toString()))
                .body(cargaInicialMapper.cargaInicialToCargaInicialDTO(result));
    }

    /**
     * GET  /cargaInicials -> get all the cargaInicials.
     */
    @RequestMapping(value = "/cargaInicials",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<CargaInicialDTO> getAll() {
        log.debug("REST request to get all CargaInicials");
        return cargaInicialRepository.findAll().stream()
            .map(cargaInicial -> cargaInicialMapper.cargaInicialToCargaInicialDTO(cargaInicial))
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * GET  /cargaInicials/:id -> get the "id" cargaInicial.
     */
    @RequestMapping(value = "/cargaInicials/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CargaInicialDTO> get(@PathVariable Long id) {
        log.debug("REST request to get CargaInicial : {}", id);
        return Optional.ofNullable(cargaInicialRepository.findOne(id))
            .map(cargaInicialMapper::cargaInicialToCargaInicialDTO)
            .map(cargaInicialDTO -> new ResponseEntity<>(
                cargaInicialDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cargaInicials/:id -> delete the "id" cargaInicial.
     */
    @RequestMapping(value = "/cargaInicials/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete CargaInicial : {}", id);
        cargaInicialRepository.delete(id);
        cargaInicialSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cargaInicial", id.toString())).build();
    }

    /**
     * SEARCH  /_search/cargaInicials/:query -> search for the cargaInicial corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/cargaInicials/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CargaInicial> search(@PathVariable String query) {
        return StreamSupport
            .stream(cargaInicialSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

package com.kalitron.cxc.web.rest.dto;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the FoliadorReciboPagos entity.
 */
public class FoliadorReciboPagosDTO implements Serializable {

    private Long id;

    private Long secuencia;

    private Long empresa;

    private Long plaza;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    public Long getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Long empresa) {
        this.empresa = empresa;
    }

    public Long getPlaza() {
        return plaza;
    }

    public void setPlaza(Long plaza) {
        this.plaza = plaza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FoliadorReciboPagosDTO foliadorReciboPagosDTO = (FoliadorReciboPagosDTO) o;

        if ( ! Objects.equals(id, foliadorReciboPagosDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FoliadorReciboPagosDTO{" +
                "id=" + id +
                ", secuencia='" + secuencia + "'" +
                ", empresa='" + empresa + "'" +
                ", plaza='" + plaza + "'" +
                '}';
    }
}

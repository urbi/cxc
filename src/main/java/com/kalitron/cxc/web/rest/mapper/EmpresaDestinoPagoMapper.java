package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.EmpresaDestinoPagoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EmpresaDestinoPago and its DTO EmpresaDestinoPagoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmpresaDestinoPagoMapper {

    @Mapping(source = "destinoPago.id", target = "destinoPagoId")
    @Mapping(source ="destinoPago.nombre", target = "destinoPagoNombre")
    @Mapping(source = "empresa.id", target = "empresaId")
    @Mapping(source ="empresa.nombre", target = "empresaNombre")
    @Mapping(source = "plaza.id", target = "plazaId")
    @Mapping(source ="plaza.nombre", target = "plazaNombre")
    EmpresaDestinoPagoDTO empresaDestinoPagoToEmpresaDestinoPagoDTO(EmpresaDestinoPago empresaDestinoPago);

    @Mapping(source = "plazaId", target = "plaza")
    @Mapping(source = "empresaId", target = "empresa")
    @Mapping(source = "destinoPagoId", target = "destinoPago")
    EmpresaDestinoPago empresaDestinoPagoDTOToEmpresaDestinoPago(EmpresaDestinoPagoDTO empresaDestinoPagoDTO);

    default Plaza plazaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Plaza plaza = new Plaza();
        plaza.setId(id);
        return plaza;
    }
    default Empresa empresaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Empresa empresa = new Empresa();
        empresa.setId(id);
        return empresa;
    }

    default DestinoPago destinoPagoFromId(Long id) {
        if (id == null) {
            return null;
        }
        DestinoPago destinoPago= new DestinoPago();
        destinoPago.setId(id);
        return destinoPago;
    }
}

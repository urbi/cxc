package com.kalitron.cxc.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the CuentaDestinoPago entity.
 */
public class CuentaDestinoPagoDTO implements Serializable {

    private Long id;

    private String convenio;

    private String cuentaClabe;

    private String referencia;

    private String banco;

    private Long cuentaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public Long getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(Long cuentaId) {
        this.cuentaId = cuentaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CuentaDestinoPagoDTO cuentaDestinoPagoDTO = (CuentaDestinoPagoDTO) o;

        if ( ! Objects.equals(id, cuentaDestinoPagoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CuentaDestinoPagoDTO{" +
                "id=" + id +
                ", convenio='" + convenio + "'" +
                ", cuentaClabe='" + cuentaClabe + "'" +
                ", referencia='" + referencia + "'" +
                ", banco='" + banco + "'" +
                '}';
    }
}

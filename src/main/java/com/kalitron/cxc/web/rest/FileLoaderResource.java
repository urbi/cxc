package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.FileLoader;
import com.kalitron.cxc.repository.FileLoaderRepository;
import com.kalitron.cxc.repository.search.FileLoaderSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.dto.FileLoaderDTO;
import com.kalitron.cxc.web.rest.mapper.FileLoaderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FileLoader.
 */
@RestController
@RequestMapping("/api")
public class FileLoaderResource {

    private final Logger log = LoggerFactory.getLogger(FileLoaderResource.class);

    @Inject
    private FileLoaderRepository fileLoaderRepository;

    @Inject
    private FileLoaderMapper fileLoaderMapper;

    @Inject
    private FileLoaderSearchRepository fileLoaderSearchRepository;

    /**
     * POST  /fileLoaders -> Create a new fileLoader.
     */
    @RequestMapping(value = "/fileLoaders",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FileLoaderDTO> create(@Valid @RequestBody FileLoaderDTO fileLoaderDTO) throws URISyntaxException {
        log.debug("REST request to save FileLoader : {}", fileLoaderDTO);
        if (fileLoaderDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new fileLoader cannot already have an ID").body(null);
        }
        FileLoader fileLoader = fileLoaderMapper.fileLoaderDTOToFileLoader(fileLoaderDTO);
        FileLoader result = fileLoaderRepository.save(fileLoader);
        fileLoaderSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/fileLoaders/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("fileLoader", result.getId().toString()))
                .body(fileLoaderMapper.fileLoaderToFileLoaderDTO(result));
    }

    /**
     * PUT  /fileLoaders -> Updates an existing fileLoader.
     */
    @RequestMapping(value = "/fileLoaders",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FileLoaderDTO> update(@Valid @RequestBody FileLoaderDTO fileLoaderDTO) throws URISyntaxException {
        log.debug("REST request to update FileLoader : {}", fileLoaderDTO);
        if (fileLoaderDTO.getId() == null) {
            return create(fileLoaderDTO);
        }
        FileLoader fileLoader = fileLoaderMapper.fileLoaderDTOToFileLoader(fileLoaderDTO);
        FileLoader result = fileLoaderRepository.save(fileLoader);
        fileLoaderSearchRepository.save(fileLoader);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("fileLoader", fileLoaderDTO.getId().toString()))
                .body(fileLoaderMapper.fileLoaderToFileLoaderDTO(result));
    }

    /**
     * GET  /fileLoaders -> get all the fileLoaders.
     */
    @RequestMapping(value = "/fileLoaders",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<FileLoaderDTO> getAll() {
        log.debug("REST request to get all FileLoaders");
        return fileLoaderRepository.findAll().stream()
            .map(fileLoader -> fileLoaderMapper.fileLoaderToFileLoaderDTO(fileLoader))
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * GET  /fileLoaders/:id -> get the "id" fileLoader.
     */
    @RequestMapping(value = "/fileLoaders/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FileLoaderDTO> get(@PathVariable Long id) {
        log.debug("REST request to get FileLoader : {}", id);
        return Optional.ofNullable(fileLoaderRepository.findOne(id))
            .map(fileLoaderMapper::fileLoaderToFileLoaderDTO)
            .map(fileLoaderDTO -> new ResponseEntity<>(
                fileLoaderDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /fileLoaders/:id -> delete the "id" fileLoader.
     */
    @RequestMapping(value = "/fileLoaders/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete FileLoader : {}", id);
        fileLoaderRepository.delete(id);
        fileLoaderSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("fileLoader", id.toString())).build();
    }

    /**
     * SEARCH  /_search/fileLoaders/:query -> search for the fileLoader corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/fileLoaders/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FileLoader> search(@PathVariable String query) {
        return StreamSupport
            .stream(fileLoaderSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.PagoReferenciado;
import com.kalitron.cxc.repository.PagoReferenciadoRepository;
import com.kalitron.cxc.repository.search.PagoReferenciadoSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.PagoReferenciadoDTO;
import com.kalitron.cxc.web.rest.mapper.PagoReferenciadoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PagoReferenciado.
 */
@RestController
@RequestMapping("/api")
public class PagoReferenciadoResource {

    private final Logger log = LoggerFactory.getLogger(PagoReferenciadoResource.class);

    @Inject
    private PagoReferenciadoRepository pagoReferenciadoRepository;

    @Inject
    private PagoReferenciadoMapper pagoReferenciadoMapper;

    @Inject
    private PagoReferenciadoSearchRepository pagoReferenciadoSearchRepository;

    /**
     * POST  /pagoReferenciados -> Create a new pagoReferenciado.
     */
    @RequestMapping(value = "/pagoReferenciados",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PagoReferenciadoDTO> create(@Valid @RequestBody PagoReferenciadoDTO pagoReferenciadoDTO) throws URISyntaxException {
        log.debug("REST request to save PagoReferenciado : {}", pagoReferenciadoDTO);
        if (pagoReferenciadoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new pagoReferenciado cannot already have an ID").body(null);
        }
        PagoReferenciado pagoReferenciado = pagoReferenciadoMapper.pagoReferenciadoDTOToPagoReferenciado(pagoReferenciadoDTO);
        PagoReferenciado result = pagoReferenciadoRepository.save(pagoReferenciado);
        pagoReferenciadoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pagoReferenciados/" + pagoReferenciadoDTO.getId())).body(pagoReferenciadoMapper.pagoReferenciadoToPagoReferenciadoDTO(result));
    }

    /**
     * PUT  /pagoReferenciados -> Updates an existing pagoReferenciado.
     */
    @RequestMapping(value = "/pagoReferenciados",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PagoReferenciadoDTO> update(@Valid @RequestBody PagoReferenciadoDTO pagoReferenciadoDTO) throws URISyntaxException {
        log.debug("REST request to update PagoReferenciado : {}", pagoReferenciadoDTO);
        if (pagoReferenciadoDTO.getId() == null) {
            return create(pagoReferenciadoDTO);
        }
        PagoReferenciado pagoReferenciado = pagoReferenciadoMapper.pagoReferenciadoDTOToPagoReferenciado(pagoReferenciadoDTO);
        PagoReferenciado result = pagoReferenciadoRepository.save(pagoReferenciado);
        pagoReferenciadoSearchRepository.save(pagoReferenciado);
        return ResponseEntity.ok().body(pagoReferenciadoMapper.pagoReferenciadoToPagoReferenciadoDTO(result));
    }

    /**
     * GET  /pagoReferenciados -> get all the pagoReferenciados.
     */
    @RequestMapping(value = "/pagoReferenciados",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PagoReferenciadoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<PagoReferenciado> page = pagoReferenciadoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pagoReferenciados", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(pagoReferenciadoMapper::pagoReferenciadoToPagoReferenciadoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /pagoReferenciados/:id -> get the "id" pagoReferenciado.
     */
    @RequestMapping(value = "/pagoReferenciados/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PagoReferenciadoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get PagoReferenciado : {}", id);
        return Optional.ofNullable(pagoReferenciadoRepository.findOne(id))
            .map(pagoReferenciadoMapper::pagoReferenciadoToPagoReferenciadoDTO)
            .map(pagoReferenciadoDTO -> new ResponseEntity<>(
                pagoReferenciadoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pagoReferenciados/:id -> delete the "id" pagoReferenciado.
     */
    @RequestMapping(value = "/pagoReferenciados/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete PagoReferenciado : {}", id);
        pagoReferenciadoRepository.delete(id);
        pagoReferenciadoSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/pagoReferenciados/:query -> search for the pagoReferenciado corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/pagoReferenciados/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<PagoReferenciado> search(@PathVariable String query) {
        return StreamSupport
            .stream(pagoReferenciadoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

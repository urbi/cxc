package com.kalitron.cxc.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.SecuenciaType;

/**
 * A DTO for the CxcSecuencia entity.
 */
public class CxcSecuenciaDTO implements Serializable {

    private Long id;

    @NotNull
    private SecuenciaType nombre;

    @NotNull
    @Min(value = 0)
    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SecuenciaType getNombre() {
        return nombre;
    }

    public void setNombre(SecuenciaType nombre) {
        this.nombre = nombre;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CxcSecuenciaDTO cxcSecuenciaDTO = (CxcSecuenciaDTO) o;

        if ( ! Objects.equals(id, cxcSecuenciaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CxcSecuenciaDTO{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", secuencia='" + secuencia + "'" +
                '}';
    }
}

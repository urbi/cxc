package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.CuentaDTO;

import com.kalitron.cxc.web.rest.dto.CuentaDestinoPagoDTO;
import com.kalitron.cxc.web.rest.dto.MovimientoDTO;
import com.kalitron.cxc.web.rest.dto.NotaDTO;
import javafx.scene.effect.SepiaTone;
import org.mapstruct.*;

import java.util.List;


/**
 * Mapper for the entity Cuenta and its DTO CuentaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CuentaMapper {

    @Mapping(source = "cliente.id", target = "clienteId")
    @Mapping(source = "cliente.fullName", target = "clienteNombre")
    @Mapping(source = "plaza.id", target = "plazaId")
    @Mapping(source ="plaza.nombre", target = "plazaNombre")
    @Mapping(source = "empresa.id", target = "empresaId")
    @Mapping(source ="empresa.nombre", target = "empresaNombre")
    @Mapping(source = "created.id", target = "createdId")
    @Mapping(source ="created.fullName", target = "createdName")
    @Mapping(source = "updated.id", target = "updatedId")
    @Mapping(source ="updated.fullName", target = "updatedName")
    CuentaDTO cuentaToCuentaDTO(Cuenta cuenta);

    List<MovimientoDTO> movimientosToMovimientoDTOs(List<Movimiento> movimientos);
    MovimientoDTO movimientoToMovimientoDTO(Movimiento movimiento);

    List<CuentaDestinoPagoDTO> cuentaDestinoPagosToCuentaDestinoPagoDTOs(List<CuentaDestinoPago> cuentaDestinoPagos);
    CuentaDestinoPagoDTO cuentaDestinoPagoToCuentaDestinoPagoDTO(CuentaDestinoPago cuentaDestinoPago);

    List<NotaDTO> notasToNotaDTOs(List<Nota> notas);
    NotaDTO notaToNotaDTO(Nota nota);


    @Mapping(source = "clienteId", target = "cliente")
    @Mapping(source = "plazaId", target = "plaza")
    @Mapping(source = "empresaId", target = "empresa")
    @Mapping(target = "movimientos", ignore = true)
    @Mapping(target = "notas", ignore = true)
    @Mapping(target = "cuentaDestinoPagos", ignore = true)
    Cuenta cuentaDTOToCuenta(CuentaDTO cuentaDTO);

    List<Movimiento> movimientoDTOsToMovimientos(List<MovimientoDTO> movimientoDTOs);

    Movimiento movimientoDTOToMovimiento(MovimientoDTO movimientoDTO);

    List<CuentaDestinoPago> cuentaDestinoPagoDTOsToCuentaDestinoPagos(List<CuentaDestinoPagoDTO> cuentaDestinoPagoDTOs);
    CuentaDestinoPago cuentaDestinoPagoDTOToCuentaDestinoPago(CuentaDestinoPagoDTO cuentaDestinoPagoDTO);

    List<Nota> notaDTOsToNotas(List<NotaDTO> notaDTOs);
    Nota notaDTOToNota(NotaDTO notaDTO);





    default Cliente clienteFromId(Long id) {
        if (id == null) {
            return null;
        }
        Cliente cliente = new Cliente();
        cliente.setId(id);
        return cliente;
    }

    default Plaza plazaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Plaza plaza = new Plaza();
        plaza.setId(id);
        return plaza;
    }
    default Empresa empresaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Empresa empresa = new Empresa();
        empresa.setId(id);
        return empresa;
    }

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}

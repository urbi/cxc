package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.CxcSecuenciaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CxcSecuencia and its DTO CxcSecuenciaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CxcSecuenciaMapper {

    CxcSecuenciaDTO cxcSecuenciaToCxcSecuenciaDTO(CxcSecuencia cxcSecuencia);

    CxcSecuencia cxcSecuenciaDTOToCxcSecuencia(CxcSecuenciaDTO cxcSecuenciaDTO);
}

package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Plaza;
import com.kalitron.cxc.domain.User;
import com.kalitron.cxc.repository.PlazaRepository;
import com.kalitron.cxc.repository.search.PlazaSearchRepository;
import com.kalitron.cxc.service.UserService;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.PlazaDTO;
import com.kalitron.cxc.web.rest.mapper.PlazaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Plaza.
 */
@RestController
@RequestMapping("/api")
public class PlazaResource {

    private final Logger log = LoggerFactory.getLogger(PlazaResource.class);

    @Inject
    private PlazaRepository plazaRepository;

    @Inject
    private PlazaMapper plazaMapper;

    @Inject
    private UserService userService;

    @Inject
    private PlazaSearchRepository plazaSearchRepository;

    /**
     * POST  /plazas -> Create a new plaza.
     */
    @RequestMapping(value = "/plazas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlazaDTO> create(@Valid @RequestBody PlazaDTO plazaDTO) throws URISyntaxException {
        log.debug("REST request to save Plaza : {}", plazaDTO);
        if (plazaDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new plaza cannot already have an ID").body(null);
        }
        Plaza plaza = plazaMapper.plazaDTOToPlaza(plazaDTO);
        Plaza result = plazaRepository.save(plaza);
        plazaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/plazas/" + plazaDTO.getId())).body(plazaMapper.plazaToPlazaDTO(result));
    }

    /**
     * PUT  /plazas -> Updates an existing plaza.
     */
    @RequestMapping(value = "/plazas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlazaDTO> update(@Valid @RequestBody PlazaDTO plazaDTO) throws URISyntaxException {
        log.debug("REST request to update Plaza : {}", plazaDTO);
        if (plazaDTO.getId() == null) {
            return create(plazaDTO);
        }
        Plaza plaza = plazaMapper.plazaDTOToPlaza(plazaDTO);
        Plaza result = plazaRepository.save(plaza);
        plazaSearchRepository.save(plaza);
        return ResponseEntity.ok().body(plazaMapper.plazaToPlazaDTO(result));
    }

    /**
     * GET  /plazas -> get all the plazas.
     */
    @RequestMapping(value = "/plazas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<PlazaDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Plaza> page = plazaRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/plazas", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(plazaMapper::plazaToPlazaDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /plazas/:id -> get the "id" plaza.
     */
    @RequestMapping(value = "/plazas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PlazaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Plaza : {}", id);
        return Optional.ofNullable(plazaRepository.findOne(id))
            .map(plazaMapper::plazaToPlazaDTO)
            .map(plazaDTO -> new ResponseEntity<>(
                plazaDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /plazas/:id -> delete the "id" plaza.
     */
    @RequestMapping(value = "/plazas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Plaza : {}", id);
        plazaRepository.delete(id);
        plazaSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/plazas/:query -> search for the plaza corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/plazas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Plaza> search(@PathVariable String query) {
        return StreamSupport
            .stream(plazaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * SEARCH  /_searchByUser/plazas/:query -> search for the plaza corresponding to a User
     * to the query.
     */
    @RequestMapping(value = "/_searchByUser/plazas",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Plaza> searchByUser() {
        User user = userService.getUserWithAuthoritiesAndPlazas();
        //TODO: agregar funcionalidad para usuarios mamalones
        Spliterator<Plaza> plazaSpliterator= user.getPlazas().spliterator();
        return StreamSupport
            .stream(plazaSpliterator, false)
            .collect(Collectors.toList());
    }
}

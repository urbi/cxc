package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.PlazaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Plaza and its DTO PlazaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlazaMapper {

    PlazaDTO plazaToPlazaDTO(Plaza plaza);

    @Mapping(target = "clientes", ignore = true)
    @Mapping(target = "cuentas", ignore = true)
    @Mapping(target = "pagoReferenciados", ignore = true)
    @Mapping(target = "users", ignore = true)
    Plaza plazaDTOToPlaza(PlazaDTO plazaDTO);
}

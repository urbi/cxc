package com.kalitron.cxc.web.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.EstatusPagoType;

/**
 * A DTO for the PagoReferenciado entity.
 */
public class PagoReferenciadoDTO implements Serializable {

    private Long id;
    @NotNull
    private BigDecimal montoPago;

    @NotNull
    private String referencia;

    @NotNull
    private String cuentaClabe;

    private String observaciones;

    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    private LocalDate fechaPago;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime fechaCargaPago;

    private String folioExternoConciliacion;


    private String folioCarga;

    private String convenio;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime fechaAplicacionPago;

    private String comentario;

    private EstatusPagoType estatusPago;

    private Long createdId;

    private Long updatedId;

    private String createdName;
    private String updatedName;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime createdDate;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime lastModifiedDate;

    private Long plazaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMontoPago() {
        return montoPago;
    }

    public void setMontoPago(BigDecimal montoPago) {
        this.montoPago = montoPago;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public DateTime getFechaCargaPago() {
        return fechaCargaPago;
    }

    public void setFechaCargaPago(DateTime fechaCargaPago) {
        this.fechaCargaPago = fechaCargaPago;
    }

    public String getFolioExternoConciliacion() {
        return folioExternoConciliacion;
    }

    public void setFolioExternoConciliacion(String folioExternoConciliacion) {
        this.folioExternoConciliacion = folioExternoConciliacion;
    }

    public String getFolioCarga() {
        return folioCarga;
    }

    public void setFolioCarga(String folioCarga) {
        this.folioCarga = folioCarga;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public DateTime getFechaAplicacionPago() {
        return fechaAplicacionPago;
    }

    public void setFechaAplicacionPago(DateTime fechaAplicacionPago) {
        this.fechaAplicacionPago = fechaAplicacionPago;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public EstatusPagoType getEstatusPago() {
        return estatusPago;
    }

    public void setEstatusPago(EstatusPagoType estatusPago) {
        this.estatusPago = estatusPago;
    }

    public Long getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Long createdId) {
        this.createdId = createdId;
    }

    public Long getUpdatedId() {
        return updatedId;
    }

    public void setUpdatedId(Long updatedId) {
        this.updatedId = updatedId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getPlazaId() {
        return plazaId;
    }

    public void setPlazaId(Long plazaId) {
        this.plazaId = plazaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PagoReferenciadoDTO pagoReferenciadoDTO = (PagoReferenciadoDTO) o;

        if ( ! Objects.equals(id, pagoReferenciadoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PagoReferenciadoDTO{" +
            "id=" + id +
            ", montoPago=" + montoPago +
            ", referencia='" + referencia + '\'' +
            ", cuentaClabe='" + cuentaClabe + '\'' +
            ", observaciones='" + observaciones + '\'' +
            ", fechaPago=" + fechaPago +
            ", fechaCargaPago=" + fechaCargaPago +
            ", folioExternoConciliacion='" + folioExternoConciliacion + '\'' +
            ", folioCarga='" + folioCarga + '\'' +
            ", convenio='" + convenio + '\'' +
            ", fechaAplicacionPago=" + fechaAplicacionPago +
            ", comentario='" + comentario + '\'' +
            ", estatusPago=" + estatusPago +
            ", createdId=" + createdId +
            ", updatedId=" + updatedId +
            ", createdName='" + createdName + '\'' +
            ", updatedName='" + updatedName + '\'' +
            ", createdDate=" + createdDate +
            ", lastModifiedDate=" + lastModifiedDate +
            ", plazaId=" + plazaId +
            '}';
    }
}

package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.EstadoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Estado and its DTO EstadoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EstadoMapper {

    EstadoDTO estadoToEstadoDTO(Estado estado);

    @Mapping(target = "municipios", ignore = true)
    Estado estadoDTOToEstado(EstadoDTO estadoDTO);
}

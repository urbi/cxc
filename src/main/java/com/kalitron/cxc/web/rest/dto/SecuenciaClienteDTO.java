package com.kalitron.cxc.web.rest.dto;

import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoClienteType;

/**
 * A DTO for the SecuenciaCliente entity.
 */
public class SecuenciaClienteDTO implements Serializable {

    private Long id;

    private TipoClienteType tipoCliente;

    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoClienteType getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoClienteType tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecuenciaClienteDTO secuenciaClienteDTO = (SecuenciaClienteDTO) o;

        if ( ! Objects.equals(id, secuenciaClienteDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SecuenciaClienteDTO{" +
                "id=" + id +
                ", tipoCliente='" + tipoCliente + "'" +
                ", secuencia='" + secuencia + "'" +
                '}';
    }
}

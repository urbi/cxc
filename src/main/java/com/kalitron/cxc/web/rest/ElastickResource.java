package com.kalitron.cxc.web.rest;

/**
 * Created by narciso.parra on 18/07/2015.
 */


import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Documento;
import com.kalitron.cxc.service.InitializationDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

/**
 * REST controller for indexing all tables in elastic search.
 */
@RestController
@RequestMapping("/api")
public class ElastickResource {

    private final Logger log = LoggerFactory.getLogger(DocumentoResource.class);

    @Inject
    private InitializationDataService initializationDataService;

    /**
     * INDEX  /Elastick/index
     */
    @RequestMapping(value = "/Elastick/index",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void index() {
        log.debug("REST request to Elastick Indes ");
         initializationDataService.indexAllData();
    }


    @RequestMapping(value = "/Elastick/indexCuentaYCliente",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void indexClienteYCuenta() {
        log.debug("REST request to Elastick Indes ");
        initializationDataService.indexCuentaYCliente();
    }



    @RequestMapping(value = "/Elastick/delete",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete() {
        log.debug("REST request to Elastick Indes ");
        initializationDataService.deleteAllData();
    }

}

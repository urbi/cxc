package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.CargaInicialDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CargaInicial and its DTO CargaInicialDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CargaInicialMapper {

    CargaInicialDTO cargaInicialToCargaInicialDTO(CargaInicial cargaInicial);

    CargaInicial cargaInicialDTOToCargaInicial(CargaInicialDTO cargaInicialDTO);
}

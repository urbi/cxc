package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.SecuenciaCuenta;
import com.kalitron.cxc.repository.SecuenciaCuentaRepository;
import com.kalitron.cxc.repository.search.SecuenciaCuentaSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.SecuenciaCuentaDTO;
import com.kalitron.cxc.web.rest.mapper.SecuenciaCuentaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SecuenciaCuenta.
 */
@RestController
@RequestMapping("/api")
public class SecuenciaCuentaResource {

    private final Logger log = LoggerFactory.getLogger(SecuenciaCuentaResource.class);

    @Inject
    private SecuenciaCuentaRepository secuenciaCuentaRepository;

    @Inject
    private SecuenciaCuentaMapper secuenciaCuentaMapper;

    @Inject
    private SecuenciaCuentaSearchRepository secuenciaCuentaSearchRepository;

    /**
     * POST  /secuenciaCuentas -> Create a new secuenciaCuenta.
     */
    @RequestMapping(value = "/secuenciaCuentas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecuenciaCuentaDTO> create(@RequestBody SecuenciaCuentaDTO secuenciaCuentaDTO) throws URISyntaxException {
        log.debug("REST request to save SecuenciaCuenta : {}", secuenciaCuentaDTO);
        if (secuenciaCuentaDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new secuenciaCuenta cannot already have an ID").body(null);
        }
        SecuenciaCuenta secuenciaCuenta = secuenciaCuentaMapper.secuenciaCuentaDTOToSecuenciaCuenta(secuenciaCuentaDTO);
        SecuenciaCuenta result = secuenciaCuentaRepository.save(secuenciaCuenta);
        secuenciaCuentaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/secuenciaCuentas/" + secuenciaCuentaDTO.getId())).body(secuenciaCuentaMapper.secuenciaCuentaToSecuenciaCuentaDTO(result));
    }

    /**
     * PUT  /secuenciaCuentas -> Updates an existing secuenciaCuenta.
     */
    @RequestMapping(value = "/secuenciaCuentas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecuenciaCuentaDTO> update(@RequestBody SecuenciaCuentaDTO secuenciaCuentaDTO) throws URISyntaxException {
        log.debug("REST request to update SecuenciaCuenta : {}", secuenciaCuentaDTO);
        if (secuenciaCuentaDTO.getId() == null) {
            return create(secuenciaCuentaDTO);
        }
        SecuenciaCuenta secuenciaCuenta = secuenciaCuentaMapper.secuenciaCuentaDTOToSecuenciaCuenta(secuenciaCuentaDTO);
        SecuenciaCuenta result = secuenciaCuentaRepository.save(secuenciaCuenta);
        secuenciaCuentaSearchRepository.save(secuenciaCuenta);
        return ResponseEntity.ok().body(secuenciaCuentaMapper.secuenciaCuentaToSecuenciaCuentaDTO(result));
    }

    /**
     * GET  /secuenciaCuentas -> get all the secuenciaCuentas.
     */
    @RequestMapping(value = "/secuenciaCuentas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SecuenciaCuentaDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<SecuenciaCuenta> page = secuenciaCuentaRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/secuenciaCuentas", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(secuenciaCuentaMapper::secuenciaCuentaToSecuenciaCuentaDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /secuenciaCuentas/:id -> get the "id" secuenciaCuenta.
     */
    @RequestMapping(value = "/secuenciaCuentas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecuenciaCuentaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get SecuenciaCuenta : {}", id);
        return Optional.ofNullable(secuenciaCuentaRepository.findOne(id))
            .map(secuenciaCuentaMapper::secuenciaCuentaToSecuenciaCuentaDTO)
            .map(secuenciaCuentaDTO -> new ResponseEntity<>(
                secuenciaCuentaDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /secuenciaCuentas/:id -> delete the "id" secuenciaCuenta.
     */
    @RequestMapping(value = "/secuenciaCuentas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete SecuenciaCuenta : {}", id);
        secuenciaCuentaRepository.delete(id);
        secuenciaCuentaSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/secuenciaCuentas/:query -> search for the secuenciaCuenta corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/secuenciaCuentas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SecuenciaCuenta> search(@PathVariable String query) {
        return StreamSupport
            .stream(secuenciaCuentaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

package com.kalitron.cxc.web.rest.dto;

import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the SecuenciaCuenta entity.
 */
public class SecuenciaMovimientoDTO implements Serializable {

    private Long id;

    private TipoMovimientoType tipoMovimientoType;

    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoMovimientoType getTipoMovimientoType() {
        return tipoMovimientoType;
    }

    public void setTipoMovimientoType(TipoMovimientoType tipoMovimientoType) {
        this.tipoMovimientoType = tipoMovimientoType;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecuenciaMovimientoDTO secuenciaCuentaDTO = (SecuenciaMovimientoDTO) o;

        if ( ! Objects.equals(id, secuenciaCuentaDTO.id)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "SecuenciaMovimientoDTO{" +
            "id=" + id +
            ", tipoMovimientoType=" + tipoMovimientoType +
            ", secuencia=" + secuencia +
            '}';
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}

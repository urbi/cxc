package com.kalitron.cxc.web.rest.dto;

import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoCuentaType;
import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;

/**
 * A DTO for the SecuenciaCuenta entity.
 */
public class SecuenciaCuentaDTO implements Serializable {

    private Long id;

    private TipoDocumentoType tipoDocumento;

    private Long secuencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoType tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Long secuencia) {
        this.secuencia = secuencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecuenciaCuentaDTO secuenciaCuentaDTO = (SecuenciaCuentaDTO) o;

        if ( ! Objects.equals(id, secuenciaCuentaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
    @Override
    public String toString() {
        return "SecuenciaCuentaDTO{" +
            "id=" + id +
            ", tipoDocumento=" + tipoDocumento +
            ", secuencia=" + secuencia +
            '}';
    }
}

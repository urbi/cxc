package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Caja;
import com.kalitron.cxc.repository.CajaRepository;
import com.kalitron.cxc.repository.search.CajaSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.CajaDTO;
import com.kalitron.cxc.web.rest.mapper.CajaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Caja.
 */
@RestController
@RequestMapping("/api")
public class CajaResource {

    private final Logger log = LoggerFactory.getLogger(CajaResource.class);

    @Inject
    private CajaRepository cajaRepository;

    @Inject
    private CajaMapper cajaMapper;

    @Inject
    private CajaSearchRepository cajaSearchRepository;

    /**
     * POST  /cajas -> Create a new caja.
     */
    @RequestMapping(value = "/cajas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CajaDTO> create(@Valid @RequestBody CajaDTO cajaDTO) throws URISyntaxException {
        log.debug("REST request to save Caja : {}", cajaDTO);
        if (cajaDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new caja cannot already have an ID").body(null);
        }
        Caja caja = cajaMapper.cajaDTOToCaja(cajaDTO);
        Caja result = cajaRepository.save(caja);
        cajaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/cajas/" + cajaDTO.getId())).body(cajaMapper.cajaToCajaDTO(result));
    }

    /**
     * PUT  /cajas -> Updates an existing caja.
     */
    @RequestMapping(value = "/cajas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CajaDTO> update(@Valid @RequestBody CajaDTO cajaDTO) throws URISyntaxException {
        log.debug("REST request to update Caja : {}", cajaDTO);
        if (cajaDTO.getId() == null) {
            return create(cajaDTO);
        }
        Caja caja = cajaMapper.cajaDTOToCaja(cajaDTO);
        Caja result = cajaRepository.save(caja);
        cajaSearchRepository.save(caja);
        return ResponseEntity.ok().body(cajaMapper.cajaToCajaDTO(result));
    }

    /**
     * GET  /cajas -> get all the cajas.
     */
    @RequestMapping(value = "/cajas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CajaDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Caja> page = cajaRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cajas", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(cajaMapper::cajaToCajaDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /cajas/:id -> get the "id" caja.
     */
    @RequestMapping(value = "/cajas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CajaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Caja : {}", id);
        return Optional.ofNullable(cajaRepository.findOne(id))
            .map(cajaMapper::cajaToCajaDTO)
            .map(cajaDTO -> new ResponseEntity<>(
                cajaDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cajas/:id -> delete the "id" caja.
     */
    @RequestMapping(value = "/cajas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Caja : {}", id);
        cajaRepository.delete(id);
        cajaSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/cajas/:query -> search for the caja corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/cajas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Caja> search(@PathVariable String query) {
        return StreamSupport
            .stream(cajaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

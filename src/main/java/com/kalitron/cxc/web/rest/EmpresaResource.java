package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Empresa;
import com.kalitron.cxc.repository.EmpresaRepository;
import com.kalitron.cxc.repository.search.EmpresaSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.EmpresaDTO;
import com.kalitron.cxc.web.rest.mapper.EmpresaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Empresa.
 */
@RestController
@RequestMapping("/api")
public class EmpresaResource {

    private final Logger log = LoggerFactory.getLogger(EmpresaResource.class);

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private EmpresaMapper empresaMapper;

    @Inject
    private EmpresaSearchRepository empresaSearchRepository;

    /**
     * POST  /empresas -> Create a new empresa.
     */
    @RequestMapping(value = "/empresas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EmpresaDTO> create(@Valid @RequestBody EmpresaDTO empresaDTO) throws URISyntaxException {
        log.debug("REST request to save Empresa : {}", empresaDTO);
        if (empresaDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new empresa cannot already have an ID").body(null);
        }
        Empresa empresa = empresaMapper.empresaDTOToEmpresa(empresaDTO);
        Empresa result = empresaRepository.save(empresa);
        empresaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/empresas/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("empresa", result.getId().toString()))
                .body(empresaMapper.empresaToEmpresaDTO(result));
    }

    /**
     * PUT  /empresas -> Updates an existing empresa.
     */
    @RequestMapping(value = "/empresas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EmpresaDTO> update(@Valid @RequestBody EmpresaDTO empresaDTO) throws URISyntaxException {
        log.debug("REST request to update Empresa : {}", empresaDTO);
        if (empresaDTO.getId() == null) {
            return create(empresaDTO);
        }
        Empresa empresa = empresaMapper.empresaDTOToEmpresa(empresaDTO);
        Empresa result = empresaRepository.save(empresa);
        empresaSearchRepository.save(empresa);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("empresa", empresaDTO.getId().toString()))
                .body(empresaMapper.empresaToEmpresaDTO(result));
    }

    /**
     * GET  /empresas -> get all the empresas.
     */
    @RequestMapping(value = "/empresas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<EmpresaDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Empresa> page = empresaRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/empresas", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(empresaMapper::empresaToEmpresaDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /empresas/:id -> get the "id" empresa.
     */
    @RequestMapping(value = "/empresas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EmpresaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Empresa : {}", id);
        return Optional.ofNullable(empresaRepository.findOne(id))
            .map(empresaMapper::empresaToEmpresaDTO)
            .map(empresaDTO -> new ResponseEntity<>(
                empresaDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /empresas/:id -> delete the "id" empresa.
     */
    @RequestMapping(value = "/empresas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Empresa : {}", id);
        empresaRepository.delete(id);
        empresaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("empresa", id.toString())).build();
    }

    /**
     * SEARCH  /_search/empresas/:query -> search for the empresa corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/empresas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Empresa> search(@PathVariable String query) {
        return StreamSupport
            .stream(empresaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

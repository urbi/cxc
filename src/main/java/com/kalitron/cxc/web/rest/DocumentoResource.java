package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Documento;
import com.kalitron.cxc.repository.DocumentoRepository;
import com.kalitron.cxc.repository.search.DocumentoSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.DocumentoDTO;
import com.kalitron.cxc.web.rest.mapper.DocumentoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Documento.
 */
@RestController
@RequestMapping("/api")
public class DocumentoResource {

    private final Logger log = LoggerFactory.getLogger(DocumentoResource.class);

    @Inject
    private DocumentoRepository documentoRepository;

    @Inject
    private DocumentoMapper documentoMapper;

    @Inject
    private DocumentoSearchRepository documentoSearchRepository;

    /**
     * POST  /documentos -> Create a new documento.
     */
    @RequestMapping(value = "/documentos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DocumentoDTO> create(@RequestBody DocumentoDTO documentoDTO) throws URISyntaxException {
        log.debug("REST request to save Documento : {}", documentoDTO);
        if (documentoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new documento cannot already have an ID").body(null);
        }
        Documento documento = documentoMapper.documentoDTOToDocumento(documentoDTO);
        Documento result = documentoRepository.save(documento);
        documentoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/documentos/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("documento", result.getId().toString()))
                .body(documentoMapper.documentoToDocumentoDTO(result));
    }

    /**
     * PUT  /documentos -> Updates an existing documento.
     */
    @RequestMapping(value = "/documentos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DocumentoDTO> update(@RequestBody DocumentoDTO documentoDTO) throws URISyntaxException {
        log.debug("REST request to update Documento : {}", documentoDTO);
        if (documentoDTO.getId() == null) {
            return create(documentoDTO);
        }
        Documento documento = documentoMapper.documentoDTOToDocumento(documentoDTO);
        Documento result = documentoRepository.save(documento);
        documentoSearchRepository.save(documento);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("documento", documentoDTO.getId().toString()))
                .body(documentoMapper.documentoToDocumentoDTO(result));
    }

    /**
     * GET  /documentos -> get all the documentos.
     */
    @RequestMapping(value = "/documentos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DocumentoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Documento> page = documentoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/documentos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(documentoMapper::documentoToDocumentoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /documentos/:id -> get the "id" documento.
     */
    @RequestMapping(value = "/documentos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DocumentoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Documento : {}", id);
        return Optional.ofNullable(documentoRepository.findOne(id))
            .map(documentoMapper::documentoToDocumentoDTO)
            .map(documentoDTO -> new ResponseEntity<>(
                documentoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /documentos/:id -> delete the "id" documento.
     */
    @RequestMapping(value = "/documentos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Documento : {}", id);
        documentoRepository.delete(id);
        documentoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("documento", id.toString())).build();
    }

    /**
     * SEARCH  /_search/documentos/:query -> search for the documento corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/documentos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Documento> search(@PathVariable String query) {
        return StreamSupport
            .stream(documentoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

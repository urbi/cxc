package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.NotaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Nota and its DTO NotaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NotaMapper {

    NotaDTO notaToNotaDTO(Nota nota);

    Nota notaDTOToNota(NotaDTO notaDTO);
}

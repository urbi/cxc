package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.SecuenciaCuentaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SecuenciaCuenta and its DTO SecuenciaCuentaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SecuenciaCuentaMapper {

    SecuenciaCuentaDTO secuenciaCuentaToSecuenciaCuentaDTO(SecuenciaCuenta secuenciaCuenta);

    SecuenciaCuenta secuenciaCuentaDTOToSecuenciaCuenta(SecuenciaCuentaDTO secuenciaCuentaDTO);
}

package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.SecuenciaCliente;
import com.kalitron.cxc.repository.SecuenciaClienteRepository;
import com.kalitron.cxc.repository.search.SecuenciaClienteSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.SecuenciaClienteDTO;
import com.kalitron.cxc.web.rest.mapper.SecuenciaClienteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SecuenciaCliente.
 */
@RestController
@RequestMapping("/api")
public class SecuenciaClienteResource {

    private final Logger log = LoggerFactory.getLogger(SecuenciaClienteResource.class);

    @Inject
    private SecuenciaClienteRepository secuenciaClienteRepository;

    @Inject
    private SecuenciaClienteMapper secuenciaClienteMapper;

    @Inject
    private SecuenciaClienteSearchRepository secuenciaClienteSearchRepository;

    /**
     * POST  /secuenciaClientes -> Create a new secuenciaCliente.
     */
    @RequestMapping(value = "/secuenciaClientes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecuenciaClienteDTO> create(@RequestBody SecuenciaClienteDTO secuenciaClienteDTO) throws URISyntaxException {
        log.debug("REST request to save SecuenciaCliente : {}", secuenciaClienteDTO);
        if (secuenciaClienteDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new secuenciaCliente cannot already have an ID").body(null);
        }
        SecuenciaCliente secuenciaCliente = secuenciaClienteMapper.secuenciaClienteDTOToSecuenciaCliente(secuenciaClienteDTO);
        SecuenciaCliente result = secuenciaClienteRepository.save(secuenciaCliente);
        secuenciaClienteSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/secuenciaClientes/" + secuenciaClienteDTO.getId())).body(secuenciaClienteMapper.secuenciaClienteToSecuenciaClienteDTO(result));
    }

    /**
     * PUT  /secuenciaClientes -> Updates an existing secuenciaCliente.
     */
    @RequestMapping(value = "/secuenciaClientes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecuenciaClienteDTO> update(@RequestBody SecuenciaClienteDTO secuenciaClienteDTO) throws URISyntaxException {
        log.debug("REST request to update SecuenciaCliente : {}", secuenciaClienteDTO);
        if (secuenciaClienteDTO.getId() == null) {
            return create(secuenciaClienteDTO);
        }
        SecuenciaCliente secuenciaCliente = secuenciaClienteMapper.secuenciaClienteDTOToSecuenciaCliente(secuenciaClienteDTO);
        SecuenciaCliente result = secuenciaClienteRepository.save(secuenciaCliente);
        secuenciaClienteSearchRepository.save(secuenciaCliente);
        return ResponseEntity.ok().body(secuenciaClienteMapper.secuenciaClienteToSecuenciaClienteDTO(result));
    }

    /**
     * GET  /secuenciaClientes -> get all the secuenciaClientes.
     */
    @RequestMapping(value = "/secuenciaClientes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SecuenciaClienteDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<SecuenciaCliente> page = secuenciaClienteRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/secuenciaClientes", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(secuenciaClienteMapper::secuenciaClienteToSecuenciaClienteDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /secuenciaClientes/:id -> get the "id" secuenciaCliente.
     */
    @RequestMapping(value = "/secuenciaClientes/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SecuenciaClienteDTO> get(@PathVariable Long id) {
        log.debug("REST request to get SecuenciaCliente : {}", id);
        return Optional.ofNullable(secuenciaClienteRepository.findOne(id))
            .map(secuenciaClienteMapper::secuenciaClienteToSecuenciaClienteDTO)
            .map(secuenciaClienteDTO -> new ResponseEntity<>(
                secuenciaClienteDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /secuenciaClientes/:id -> delete the "id" secuenciaCliente.
     */
    @RequestMapping(value = "/secuenciaClientes/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete SecuenciaCliente : {}", id);
        secuenciaClienteRepository.delete(id);
        secuenciaClienteSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/secuenciaClientes/:query -> search for the secuenciaCliente corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/secuenciaClientes/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SecuenciaCliente> search(@PathVariable String query) {
        return StreamSupport
            .stream(secuenciaClienteSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

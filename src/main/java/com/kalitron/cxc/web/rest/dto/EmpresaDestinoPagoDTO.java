package com.kalitron.cxc.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the EmpresaDestinoPago entity.
 */
public class EmpresaDestinoPagoDTO implements Serializable {

    private Long id;

    private String generadorReferencia;

    private Long destinoPagoId;
    private String destinoPagoNombre;

    private Long empresaId;
    private String empresaNombre;

    private Long plazaId;
    private String plazaNombre;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGeneradorReferencia() {
        return generadorReferencia;
    }

    public void setGeneradorReferencia(String generadorReferencia) {
        this.generadorReferencia = generadorReferencia;
    }

    public Long getDestinoPagoId() {
        return destinoPagoId;
    }

    public void setDestinoPagoId(Long destinoPagoId) {
        this.destinoPagoId = destinoPagoId;
    }

    public String getDestinoPagoNombre() {
        return destinoPagoNombre;
    }

    public void setDestinoPagoNombre(String destinoPagoNombre) {
        this.destinoPagoNombre = destinoPagoNombre;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public String getEmpresaNombre() {
        return empresaNombre;
    }

    public void setEmpresaNombre(String empresaNombre) {
        this.empresaNombre = empresaNombre;
    }

    public Long getPlazaId() {
        return plazaId;
    }

    public void setPlazaId(Long plazaId) {
        this.plazaId = plazaId;
    }

    public String getPlazaNombre() {
        return plazaNombre;
    }

    public void setPlazaNombre(String plazaNombre) {
        this.plazaNombre = plazaNombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmpresaDestinoPagoDTO empresaDestinoPagoDTO = (EmpresaDestinoPagoDTO) o;

        if ( ! Objects.equals(id, empresaDestinoPagoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EmpresaDestinoPagoDTO{" +
            "id=" + id +
            ", generadorReferencia='" + generadorReferencia + '\'' +
            ", destinoPagoId=" + destinoPagoId +
            ", destinoPagoNombre=" + destinoPagoNombre +
            ", empresaId=" + empresaId +
            ", empresaNombre=" + empresaNombre +
            ", plazaId=" + plazaId +
            ", plazaNombre=" + plazaNombre +
            '}';
    }
}

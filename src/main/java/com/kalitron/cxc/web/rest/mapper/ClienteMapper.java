package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.ClienteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Cliente and its DTO ClienteDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClienteMapper {

    @Mapping(source = "plaza.id", target = "plazaId")
    @Mapping(source ="plaza.nombre", target = "plazaNombre")
    @Mapping(source = "created.id", target = "createdId")
    @Mapping(source ="created.fullName", target = "createdName")
    @Mapping(source = "updated.id", target = "updatedId")
    @Mapping(source ="updated.fullName", target = "updatedName")
    ClienteDTO clienteToClienteDTO(Cliente cliente);

    @Mapping(target = "cuentas", ignore = true)
    @Mapping(source = "plazaId", target = "plaza")
    Cliente clienteDTOToCliente(ClienteDTO clienteDTO);

    default Plaza plazaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Plaza plaza = new Plaza();
        plaza.setId(id);
        return plaza;
    }
    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }

}

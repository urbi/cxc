package com.kalitron.cxc.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.domain.enumeration.*;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Cliente entity.
 */
public class CuentaCargaInicialDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombres;

    @NotNull
    private String apellidoPaterno;

    private String apellidoMaterno;


    @Size(min = 10, max = 13)
    private String rfc;

    //@Size(min = 18, max = 18)
    private String curp;

    private SexoType sexo;

    //@Size(min = 11, max = 11)
    private String nss;

    private String correo;

    private String telefono1;

    private String telefono2;

    private String celular;

    private String fechaNacimiento;

    private String numeroRefExterna;

    private SistemaOrigenType sistemaOrigen;

    private TipoClienteType tipoCliente;


    private String calle;

    private String numero;



    private String codigoPostal;

    @NotNull
    private String colonia;

    private String ciudad;

    @NotNull
    private String estado;

    @NotNull
    private String municipio;

    private String geoLocalizacion;
    @JsonIgnore
    private Long secuenciaCliente;
    @JsonIgnore
    private String createdDate;
    @JsonIgnore
    private Long createdId;
    @JsonIgnore
    private Long updatedId;
    @JsonIgnore
    private String lastModifiedDate;
    @JsonIgnore
    private String createdName;
    @JsonIgnore
    private String updatedName;

    @NotNull
    private Long plazaId;
    @JsonIgnore
    private String plazaNombre;


    private String nombreCompleto;

    private String autocompleteFormat;

/********************DATOS DE LA CUENTA*******************************/
    @NotNull
    private BigDecimal saldo;

    private BigDecimal montoExigible;

    private BigDecimal saldoInicial;

    @NotNull
    private BigDecimal interesAnual;

    @NotNull
    private Long plazoMeses;

    private BigDecimal pagoMensual;


    private String fechaUltimoPago;


    private String fechaCobro;

    private String numeroContratoOrigen;

    private String descripcion;

    private String attribute1;

    private String attribute2;


    private EstatusCuentaType estatusCuenta;

    private TipoDocumentoType tipoDocumento;




    private Long empresaId;
    private String empresaNombre;


    private Long secuenciaCuenta;



    private Boolean isCuentaValid;
    private Boolean hasPago;





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public SexoType getSexo() {
        return sexo;
    }

    public void setSexo(SexoType sexo) {
        this.sexo = sexo;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumeroRefExterna() {
        return numeroRefExterna;
    }

    public void setNumeroRefExterna(String numeroRefExterna) {
        this.numeroRefExterna = numeroRefExterna;
    }

    public SistemaOrigenType getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(SistemaOrigenType sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public TipoClienteType getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoClienteType tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getGeoLocalizacion() {
        return geoLocalizacion;
    }

    public void setGeoLocalizacion(String geoLocalizacion) {
        this.geoLocalizacion = geoLocalizacion;
    }



    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }



    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getSecuenciaCliente() {
        return secuenciaCliente;
    }

    public void setSecuenciaCliente(Long secuenciaCliente) {
        this.secuenciaCliente = secuenciaCliente;
    }

    public Long getPlazaId() {
        return plazaId;
    }

    public void setPlazaId(Long plazaId) {
        this.plazaId = plazaId;
    }

    public String getPlazaNombre() {
        return plazaNombre;
    }

    public void setPlazaNombre(String plazaNombre) {
        this.plazaNombre = plazaNombre;
    }



    public Long getUpdatedId() {
        return updatedId;
    }

    public void setUpdatedId(Long updatedId) {
        this.updatedId = updatedId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public String getNombreCompleto() {
        return (nombres + " " + apellidoPaterno +" " +  apellidoMaterno).trim();
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getAutocompleteFormat() {
        return rfc + " " +plazaNombre;
    }

    public String getAutocompleteIdAndNombre() {
        return secuenciaCliente + " : " +  getNombreCompleto();
    }

    public void setAutocompleteFormat(String autocompleteFormat) {
        this.autocompleteFormat = autocompleteFormat;
    }

    public Long getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Long createdId) {
        this.createdId = createdId;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getMontoExigible() {
        return montoExigible;
    }

    public void setMontoExigible(BigDecimal montoExigible) {
        this.montoExigible = montoExigible;
    }

    public BigDecimal getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(BigDecimal saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public BigDecimal getInteresAnual() {
        return interesAnual;
    }

    public void setInteresAnual(BigDecimal interesAnual) {
        this.interesAnual = interesAnual;
    }

    public Long getPlazoMeses() {
        return plazoMeses;
    }

    public void setPlazoMeses(Long plazoMeses) {
        this.plazoMeses = plazoMeses;
    }

    public String getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    public void setFechaUltimoPago(String fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    public String getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(String fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public String getNumeroContratoOrigen() {
        return numeroContratoOrigen;
    }

    public void setNumeroContratoOrigen(String numeroContratoOrigen) {
        this.numeroContratoOrigen = numeroContratoOrigen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public EstatusCuentaType getEstatusCuenta() {
        return estatusCuenta;
    }

    public void setEstatusCuenta(EstatusCuentaType estatusCuenta) {
        this.estatusCuenta = estatusCuenta;
    }

    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoType tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public String getEmpresaNombre() {
        return empresaNombre;
    }

    public void setEmpresaNombre(String empresaNombre) {
        this.empresaNombre = empresaNombre;
    }

    public Long getSecuenciaCuenta() {
        return secuenciaCuenta;
    }

    public void setSecuenciaCuenta(Long secuenciaCuenta) {
        this.secuenciaCuenta = secuenciaCuenta;
    }

    public Boolean getIsCuentaValid() {
        return isCuentaValid;
    }

    public void setIsCuentaValid(Boolean isCuentaValid) {
        this.isCuentaValid = isCuentaValid;
    }

    public Boolean getHasPago() {
        return hasPago;
    }

    public void setHasPago(Boolean hasPago) {
        this.hasPago = hasPago;
    }

    public BigDecimal getPagoMensual() {
        return pagoMensual;
    }

    public void setPagoMensual(BigDecimal pagoMensual) {
        this.pagoMensual = pagoMensual;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CuentaCargaInicialDTO clienteDTO = (CuentaCargaInicialDTO) o;

        if ( ! Objects.equals(id, clienteDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ClienteCargaInicialDTO{" +
            "id=" + id +
            ", nombres='" + nombres + '\'' +
            ", apellidoPaterno='" + apellidoPaterno + '\'' +
            ", apellidoMaterno='" + apellidoMaterno + '\'' +
            ", rfc='" + rfc + '\'' +
            ", curp='" + curp + '\'' +
            ", sexo=" + sexo +
            ", nss='" + nss + '\'' +
            ", correo='" + correo + '\'' +
            ", telefono1='" + telefono1 + '\'' +
            ", telefono2='" + telefono2 + '\'' +
            ", celular='" + celular + '\'' +
            ", fechaNacimiento='" + fechaNacimiento + '\'' +
            ", numeroRefExterna='" + numeroRefExterna + '\'' +
            ", sistemaOrigen=" + sistemaOrigen +
            ", tipoCliente=" + tipoCliente +
            ", calle='" + calle + '\'' +
            ", numero='" + numero + '\'' +
            ", codigoPostal='" + codigoPostal + '\'' +
            ", colonia='" + colonia + '\'' +
            ", ciudad='" + ciudad + '\'' +
            ", estado='" + estado + '\'' +
            ", municipio='" + municipio + '\'' +
            ", geoLocalizacion='" + geoLocalizacion + '\'' +
            ", secuenciaCliente=" + secuenciaCliente +
            ", createdDate='" + createdDate + '\'' +
            ", createdId=" + createdId +
            ", updatedId=" + updatedId +
            ", lastModifiedDate='" + lastModifiedDate + '\'' +
            ", createdName='" + createdName + '\'' +
            ", updatedName='" + updatedName + '\'' +
            ", plazaId=" + plazaId +
            ", plazaNombre='" + plazaNombre + '\'' +
            ", nombreCompleto='" + nombreCompleto + '\'' +
            ", autocompleteFormat='" + autocompleteFormat + '\'' +
            ", saldo=" + saldo +
            ", montoExigible=" + montoExigible +
            ", saldoInicial=" + saldoInicial +
            ", interesAnual=" + interesAnual +
            ", pagoMensual=" + pagoMensual +
            ", plazoMeses=" + plazoMeses +
            ", fechaUltimoPago='" + fechaUltimoPago + '\'' +
            ", fechaCobro='" + fechaCobro + '\'' +
            ", numeroContratoOrigen='" + numeroContratoOrigen + '\'' +
            ", descripcion='" + descripcion + '\'' +
            ", attribute1='" + attribute1 + '\'' +
            ", attribute2='" + attribute2 + '\'' +
            ", estatusCuenta=" + estatusCuenta +
            ", tipoDocumento=" + tipoDocumento +
            ", empresaId=" + empresaId +
            ", empresaNombre='" + empresaNombre + '\'' +
            ", secuenciaCuenta=" + secuenciaCuenta +
            ", isCuentaValid=" + isCuentaValid +
            ", hasPago=" + hasPago +
            '}';
    }

    public Cuenta convertToCuenta(){
        Cliente cliente = new Cliente();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-mm-dd");

        cliente.setNombres(nombres);
        cliente.setApellidoPaterno(apellidoPaterno);
        cliente.setApellidoMaterno(apellidoMaterno);
        if(rfc != null && rfc.length() >=10 && rfc.length() <= 13 )
            cliente.setRfc(rfc);
        if(curp!= null &&curp.length() == 18){
            cliente.setCurp(curp);
        }

        cliente.setSexo(sexo);
        if(nss != null && nss.length()==11)
            cliente.setNss(nss);

        cliente.setCorreo(correo);
        cliente.setTelefono1(telefono1);
        String fecha = fechaNacimiento;
        if(fecha != null && !fecha.equals("") && !fecha.equals("NULL"))
        cliente.setFechaNacimiento(new LocalDate(fechaNacimiento));

        cliente.setNumeroRefExterna(numeroRefExterna);
        cliente.setSistemaOrigen(sistemaOrigen);
        cliente.setTipoCliente(tipoCliente);
        cliente.setCalle(calle);
        cliente.setNumero(numero);

        if(codigoPostal!= null && codigoPostal.length()== 5)
            cliente.setCodigoPostal(codigoPostal);
        else
            cliente.setCodigoPostal("00000");
        cliente.setColonia(colonia);
        cliente.setCiudad(ciudad);
        cliente.setEstado(estado);
        cliente.setMunicipio(municipio);
        Plaza plaza = new Plaza();
        plaza.setId(plazaId);
        cliente.setPlaza(plaza);

        Cuenta cuenta = new Cuenta();
        cuenta.setSaldo(saldo);
        cuenta.setMontoExigible(montoExigible);
        cuenta.setSaldoInicial(saldoInicial);

        fecha = fechaUltimoPago;
        if(fecha != null && !fecha.equals("") && !fecha.equals("NULL"))
            cuenta.setFechaUltimoPago(new LocalDate(fecha));
        cuenta.setNumeroContratoOrigen(numeroContratoOrigen);
        cuenta.setDescripcion("FECHA CREACION ORIGINAL: " + descripcion);
        cuenta.setAttribute1("ORIGNAL: " + attribute1);
        cuenta.setAttribute2("MONTO PAGADO DE ORGINE: " + attribute2);
        cuenta.setEstatusCuenta(estatusCuenta);
        Empresa empresa =  new Empresa();
        empresa.setId(empresaId);
        cuenta.setEmpresa(empresa);
        cuenta.setPlazoMeses(plazoMeses);
        cuenta.setPagoMensual(pagoMensual);
        cuenta.setTipoDocumento(tipoDocumento);
        cuenta.setInteresAnual(interesAnual);
        cuenta.setPlaza(plaza);
        cuenta.setCliente(cliente);

        Movimiento movimiento = new Movimiento();
        movimiento.setMonto(saldo);
        movimiento.setSaldo(saldo);
        movimiento.setNumeroPagare(1);
        movimiento.setFechaVencimiento(new LocalDate());
        movimiento.setEstatusMovimiento(EstatusMovimientoType.CORRIENTE);
        movimiento.setTipoMovimiento(TipoMovimientoType.CARGO);
        movimiento.setConceptoMovimiento(ConceptoMovimientoType.CREDITO);
        List<Movimiento> movimientos = new ArrayList<>();
        movimientos.add(movimiento);
        cuenta.setMovimientos(movimientos);







        return cuenta;
    }
}

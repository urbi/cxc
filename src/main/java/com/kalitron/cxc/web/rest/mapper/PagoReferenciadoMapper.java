package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.PagoReferenciadoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PagoReferenciado and its DTO PagoReferenciadoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PagoReferenciadoMapper {

    @Mapping(source = "plaza.id", target = "plazaId")
    PagoReferenciadoDTO pagoReferenciadoToPagoReferenciadoDTO(PagoReferenciado pagoReferenciado);

    @Mapping(source = "plazaId", target = "plaza")
    PagoReferenciado pagoReferenciadoDTOToPagoReferenciado(PagoReferenciadoDTO pagoReferenciadoDTO);

    default Plaza plazaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Plaza plaza = new Plaza();
        plaza.setId(id);
        return plaza;
    }
}

package com.kalitron.cxc.web.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;


import java.util.Objects;
import java.util.List;


import com.kalitron.cxc.domain.enumeration.EstatusCuentaType;

/**
 * A DTO for the Cuenta entity.
 */
public class CuentaDTO implements Serializable {

    private Long id;


    @NotNull
    private BigDecimal saldo;

    private BigDecimal montoExigible;

    private BigDecimal saldoInicial;

    @NotNull
    private BigDecimal interesAnual;

    private BigDecimal pagoMensual;

    @NotNull
    private Long plazoMeses;

    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    private LocalDate fechaUltimoPago;

    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    private LocalDate fechaCobro;

    private String numeroContratoOrigen;

    private String descripcion;

    private String attribute1;

    private String attribute2;


    private EstatusCuentaType estatusCuenta;

    private TipoDocumentoType tipoDocumento;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime createdDate;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime lastModifiedDate;

    private Long createdId;

    private Long updatedId;

    private String createdName;
    private String updatedName;

    private Long clienteId;
    private String clienteNombre;

    private Long plazaId;

    private String plazaNombre;

    private Long empresaId;
    private String empresaNombre;


    private Long secuenciaCuenta;

    private List<MovimientoDTO> movimientoDTOs;

    private List<CuentaDestinoPagoDTO> cuentaDestinoPagoDTOs;

    private List<NotaDTO> notaDTOs;

    private Boolean isCuentaValid;
    private Boolean hasPago;

    public Boolean getHasPago() {
        return hasPago;
    }

    public void setHasPago(Boolean hasPago) {
        this.hasPago = hasPago;
    }

    public Boolean getIsCuentaValid() {
        return isCuentaValid;
    }

    public void setIsCuentaValid(Boolean isCuentaValid) {
        this.isCuentaValid = isCuentaValid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getMontoExigible() {
        return montoExigible;
    }

    public void setMontoExigible(BigDecimal montoExigible) {
        this.montoExigible = montoExigible;
    }

    public BigDecimal getSaldoInicial() {
        return saldoInicial;
    }

    public void setSaldoInicial(BigDecimal saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    public LocalDate getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    public void setFechaUltimoPago(LocalDate fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    public String getNumeroContratoOrigen() {
        return numeroContratoOrigen;
    }

    public void setNumeroContratoOrigen(String numeroContratoOrigen) {
        this.numeroContratoOrigen = numeroContratoOrigen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }


    public EstatusCuentaType getEstatusCuenta() {
        return estatusCuenta;
    }

    public void setEstatusCuenta(EstatusCuentaType estatusCuenta) {
        this.estatusCuenta = estatusCuenta;
    }

    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumentoType tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Long createdId) {
        this.createdId = createdId;
    }

    public Long getUpdatedId() {
        return updatedId;
    }

    public void setUpdatedId(Long updatedId) {
        this.updatedId = updatedId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public String getClienteNombre() {
        return clienteNombre;
    }

    public void setClienteNombre(String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }

    public Long getPlazaId() {
        return plazaId;
    }

    public void setPlazaId(Long plazaId) {
        this.plazaId = plazaId;
    }

    public String getPlazaNombre() {
        return plazaNombre;
    }

    public void setPlazaNombre(String plazaNombre) {
        this.plazaNombre = plazaNombre;
    }

    public Long getEmpresaId() {
        return empresaId;
    }

    public void setEmpresaId(Long empresaId) {
        this.empresaId = empresaId;
    }

    public String getEmpresaNombre() {
        return empresaNombre;
    }

    public void setEmpresaNombre(String empresaNombre) {
        this.empresaNombre = empresaNombre;
    }





    public BigDecimal getInteresAnual() {
        return interesAnual;
    }

    public void setInteresAnual(BigDecimal interesAnual) {
        this.interesAnual = interesAnual;
    }

    public Long getPlazoMeses() {
        return plazoMeses;
    }

    public void setPlazoMeses(Long plazoMeses) {
        this.plazoMeses = plazoMeses;
    }

    public LocalDate getFechaCobro() {
        return fechaCobro;
    }

    public void setFechaCobro(LocalDate fechaCobro) {
        this.fechaCobro = fechaCobro;
    }

    public Long getSecuenciaCuenta() {
        return secuenciaCuenta;
    }

    public void setSecuenciaCuenta(Long secuenciaCuenta) {
        this.secuenciaCuenta = secuenciaCuenta;
    }

    public List<MovimientoDTO> getMovimientoDTOs() {
        return movimientoDTOs;
    }

    public void setMovimientoDTOs(List<MovimientoDTO> movimientoDTOs) {
        this.movimientoDTOs = movimientoDTOs;
    }

    public List<CuentaDestinoPagoDTO> getCuentaDestinoPagoDTOs() {
        return cuentaDestinoPagoDTOs;
    }

    public void setCuentaDestinoPagoDTOs(List<CuentaDestinoPagoDTO> cuentaDestinoPagoDTOs) {
        this.cuentaDestinoPagoDTOs = cuentaDestinoPagoDTOs;
    }

    public List<NotaDTO> getNotaDTOs() {
        return notaDTOs;
    }

    public void setNotaDTOs(List<NotaDTO> notaDTOs) {
        this.notaDTOs = notaDTOs;
    }

    public BigDecimal getPagoMensual() {
        return pagoMensual;
    }

    public void setPagoMensual(BigDecimal pagoMensual) {
        this.pagoMensual = pagoMensual;
    }

    @Override
    public String toString() {
        return "CuentaDTO{" +
            "id=" + id +
            ", saldo=" + saldo +
            ", montoExigible=" + montoExigible +
            ", saldoInicial=" + saldoInicial +
            ", interesAnual=" + interesAnual +
            ", plazoMeses=" + plazoMeses +
            ", pagoMensual=" + pagoMensual +
            ", fechaUltimoPago=" + fechaUltimoPago +
            ", fechaCobro=" + fechaCobro +
            ", numeroContratoOrigen='" + numeroContratoOrigen + '\'' +
            ", descripcion='" + descripcion + '\'' +
            ", attribute1='" + attribute1 + '\'' +
            ", attribute2='" + attribute2 + '\'' +

            ", estatusCuenta=" + estatusCuenta +
            ", tipoDocumento=" + tipoDocumento +
            ", createdDate=" + createdDate +
            ", lastModifiedDate=" + lastModifiedDate +
            ", createdId=" + createdId +
            ", updatedId=" + updatedId +
            ", createdName='" + createdName + '\'' +
            ", updatedName='" + updatedName + '\'' +
            ", clienteId=" + clienteId +
            ", clienteNombre='" + clienteNombre + '\'' +
            ", plazaId=" + plazaId +
            ", plazaNombre='" + plazaNombre + '\'' +
            ", empresaId=" + empresaId +
            ", empresaNombre='" + empresaNombre + '\'' +
            ", secuenciaCuenta=" + secuenciaCuenta +
            ", isCuentaValid=" + isCuentaValid +
            ", movimientoDTOs=" + movimientoDTOList() +
            ", cuentaDestinoPagoDTOs=" + cuentaDestinoPagoList() +
            ", notaDTOs=" + notaList() +
            '}';
    }

    private String movimientoDTOList(){
        String str ="{";

        if(movimientoDTOs != null)

            for(MovimientoDTO movimientoDTO :  movimientoDTOs){
                str +=  movimientoDTO.toString();
            }
        return  str + "}";
    }
    private String cuentaDestinoPagoList(){
        String str ="{";
        if(cuentaDestinoPagoDTOs != null)
            for(CuentaDestinoPagoDTO cuentaDestinoPagoDTO :  cuentaDestinoPagoDTOs){
                str += cuentaDestinoPagoDTO.toString();
            }
        return  str + "}";
    }

    private String notaList(){
        String str ="{";
        if(notaDTOs != null)
            for(NotaDTO notaDTO :  notaDTOs){
                str += notaDTO.toString();
            }
        return  str + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CuentaDTO cuentaDTO = (CuentaDTO) o;

        if ( ! Objects.equals(id, cuentaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}

package com.kalitron.cxc.web.rest.dto;

import javax.persistence.Lob;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.ObjectNameEnum;
//TODO: Eliminar esta entidad y todas sus relaciones
/**
 * A DTO for the FileLoader entity.
 */
public class FileLoaderDTO implements Serializable {

    private Long id;

    @NotNull
    private ObjectNameEnum tipoDato;

    @NotNull
    @Lob
    private byte[] file;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ObjectNameEnum getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(ObjectNameEnum tipoDato) {
        this.tipoDato = tipoDato;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FileLoaderDTO fileLoaderDTO = (FileLoaderDTO) o;

        if ( ! Objects.equals(id, fileLoaderDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FileLoaderDTO{" +
                "id=" + id +
                ", tipoDato='" + tipoDato + "'" +
                ", file='" + file + "'" +
                '}';
    }
}

package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Estado;
import com.kalitron.cxc.repository.EstadoRepository;
import com.kalitron.cxc.repository.search.EstadoSearchRepository;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.EstadoDTO;
import com.kalitron.cxc.web.rest.mapper.EstadoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Estado.
 */
@RestController
@RequestMapping("/api")
public class EstadoResource {

    private final Logger log = LoggerFactory.getLogger(EstadoResource.class);

    @Inject
    private EstadoRepository estadoRepository;

    @Inject
    private EstadoMapper estadoMapper;

    @Inject
    private EstadoSearchRepository estadoSearchRepository;

    /**
     * POST  /estados -> Create a new estado.
     */
    @RequestMapping(value = "/estados",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EstadoDTO> create(@Valid @RequestBody EstadoDTO estadoDTO) throws URISyntaxException {
        log.debug("REST request to save Estado : {}", estadoDTO);
        if (estadoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new estado cannot already have an ID").body(null);
        }
        Estado estado = estadoMapper.estadoDTOToEstado(estadoDTO);
        Estado result = estadoRepository.save(estado);
        estadoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/estados/" + estadoDTO.getId())).body(estadoMapper.estadoToEstadoDTO(result));
    }

    /**
     * PUT  /estados -> Updates an existing estado.
     */
    @RequestMapping(value = "/estados",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EstadoDTO> update(@Valid @RequestBody EstadoDTO estadoDTO) throws URISyntaxException {
        log.debug("REST request to update Estado : {}", estadoDTO);
        if (estadoDTO.getId() == null) {
            return create(estadoDTO);
        }
        Estado estado = estadoMapper.estadoDTOToEstado(estadoDTO);
        Estado result = estadoRepository.save(estado);
        estadoSearchRepository.save(estado);
        return ResponseEntity.ok().body(estadoMapper.estadoToEstadoDTO(result));
    }

    /**
     * GET  /estados -> get all the estados.
     */
    @RequestMapping(value = "/estados",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<EstadoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Estado> page = estadoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/estados", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(estadoMapper::estadoToEstadoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /estados/:id -> get the "id" estado.
     */
    @RequestMapping(value = "/estados/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<EstadoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Estado : {}", id);
        return Optional.ofNullable(estadoRepository.findOne(id))
            .map(estadoMapper::estadoToEstadoDTO)
            .map(estadoDTO -> new ResponseEntity<>(
                estadoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /estados/:id -> delete the "id" estado.
     */
    @RequestMapping(value = "/estados/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Estado : {}", id);
        estadoRepository.delete(id);
        estadoSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/estados/:query -> search for the estado corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/estados/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Estado> search(@PathVariable String query) {
        return StreamSupport
            .stream(estadoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

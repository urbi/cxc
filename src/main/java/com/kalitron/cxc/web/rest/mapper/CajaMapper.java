package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.CajaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Caja and its DTO CajaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CajaMapper {

    CajaDTO cajaToCajaDTO(Caja caja);

    Caja cajaDTOToCaja(CajaDTO cajaDTO);
}

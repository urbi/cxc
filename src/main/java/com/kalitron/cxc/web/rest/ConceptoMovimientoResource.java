package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.ConceptoMovimiento;
import com.kalitron.cxc.repository.ConceptoMovimientoRepository;
import com.kalitron.cxc.repository.search.ConceptoMovimientoSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.ConceptoMovimientoDTO;
import com.kalitron.cxc.web.rest.mapper.ConceptoMovimientoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ConceptoMovimiento.
 */
@RestController
@RequestMapping("/api")
public class ConceptoMovimientoResource {

    private final Logger log = LoggerFactory.getLogger(ConceptoMovimientoResource.class);

    @Inject
    private ConceptoMovimientoRepository conceptoMovimientoRepository;

    @Inject
    private ConceptoMovimientoMapper conceptoMovimientoMapper;

    @Inject
    private ConceptoMovimientoSearchRepository conceptoMovimientoSearchRepository;

    /**
     * POST  /conceptoMovimientos -> Create a new conceptoMovimiento.
     */
    @RequestMapping(value = "/conceptoMovimientos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConceptoMovimientoDTO> create(@RequestBody ConceptoMovimientoDTO conceptoMovimientoDTO) throws URISyntaxException {
        log.debug("REST request to save ConceptoMovimiento : {}", conceptoMovimientoDTO);
        if (conceptoMovimientoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new conceptoMovimiento cannot already have an ID").body(null);
        }
        ConceptoMovimiento conceptoMovimiento = conceptoMovimientoMapper.conceptoMovimientoDTOToConceptoMovimiento(conceptoMovimientoDTO);
        ConceptoMovimiento result = conceptoMovimientoRepository.save(conceptoMovimiento);
        conceptoMovimientoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/conceptoMovimientos/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("conceptoMovimiento", result.getId().toString()))
                .body(conceptoMovimientoMapper.conceptoMovimientoToConceptoMovimientoDTO(result));
    }

    /**
     * PUT  /conceptoMovimientos -> Updates an existing conceptoMovimiento.
     */
    @RequestMapping(value = "/conceptoMovimientos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConceptoMovimientoDTO> update(@RequestBody ConceptoMovimientoDTO conceptoMovimientoDTO) throws URISyntaxException {
        log.debug("REST request to update ConceptoMovimiento : {}", conceptoMovimientoDTO);
        if (conceptoMovimientoDTO.getId() == null) {
            return create(conceptoMovimientoDTO);
        }
        ConceptoMovimiento conceptoMovimiento = conceptoMovimientoMapper.conceptoMovimientoDTOToConceptoMovimiento(conceptoMovimientoDTO);
        ConceptoMovimiento result = conceptoMovimientoRepository.save(conceptoMovimiento);
        conceptoMovimientoSearchRepository.save(conceptoMovimiento);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("conceptoMovimiento", conceptoMovimientoDTO.getId().toString()))
                .body(conceptoMovimientoMapper.conceptoMovimientoToConceptoMovimientoDTO(result));
    }

    /**
     * GET  /conceptoMovimientos -> get all the conceptoMovimientos.
     */
    @RequestMapping(value = "/conceptoMovimientos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ConceptoMovimientoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<ConceptoMovimiento> page = conceptoMovimientoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/conceptoMovimientos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(conceptoMovimientoMapper::conceptoMovimientoToConceptoMovimientoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /conceptoMovimientos/:id -> get the "id" conceptoMovimiento.
     */
    @RequestMapping(value = "/conceptoMovimientos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConceptoMovimientoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get ConceptoMovimiento : {}", id);
        return Optional.ofNullable(conceptoMovimientoRepository.findOne(id))
            .map(conceptoMovimientoMapper::conceptoMovimientoToConceptoMovimientoDTO)
            .map(conceptoMovimientoDTO -> new ResponseEntity<>(
                conceptoMovimientoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /conceptoMovimientos/:id -> delete the "id" conceptoMovimiento.
     */
    @RequestMapping(value = "/conceptoMovimientos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete ConceptoMovimiento : {}", id);
        conceptoMovimientoRepository.delete(id);
        conceptoMovimientoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("conceptoMovimiento", id.toString())).build();
    }

    /**
     * SEARCH  /_search/conceptoMovimientos/:query -> search for the conceptoMovimiento corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/conceptoMovimientos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ConceptoMovimiento> search(@PathVariable String query) {
        return StreamSupport
            .stream(conceptoMovimientoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

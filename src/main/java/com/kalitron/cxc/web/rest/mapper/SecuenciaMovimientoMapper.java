package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.SecuenciaMovimiento;
import com.kalitron.cxc.web.rest.dto.SecuenciaMovimientoDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity SecuenciaMovimiento and its DTO SecuenciaMovimientoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SecuenciaMovimientoMapper {

    SecuenciaMovimientoDTO SecuenciaMovimientoToSecuenciaMovimientoDTO(SecuenciaMovimiento SecuenciaMovimiento);

    SecuenciaMovimiento SecuenciaMovimientoDTOToSecuenciaMovimiento(SecuenciaMovimientoDTO SecuenciaMovimientoDTO);
}

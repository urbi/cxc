package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.DestinoPagoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DestinoPago and its DTO DestinoPagoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DestinoPagoMapper {

    DestinoPagoDTO destinoPagoToDestinoPagoDTO(DestinoPago destinoPago);

    DestinoPago destinoPagoDTOToDestinoPago(DestinoPagoDTO destinoPagoDTO);
}

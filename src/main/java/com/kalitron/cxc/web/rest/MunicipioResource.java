package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Municipio;
import com.kalitron.cxc.repository.MunicipioRepository;
import com.kalitron.cxc.repository.search.MunicipioSearchRepository;
import com.kalitron.cxc.service.InitializationDataService;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.MunicipioDTO;
import com.kalitron.cxc.web.rest.mapper.MunicipioMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Municipio.
 */
@RestController
@RequestMapping("/api")
public class MunicipioResource {

    private final Logger log = LoggerFactory.getLogger(MunicipioResource.class);

    @Inject
    private MunicipioRepository municipioRepository;

    @Inject
    private MunicipioMapper municipioMapper;

    @Inject
    private MunicipioSearchRepository municipioSearchRepository;

    @Inject
    private InitializationDataService initializationDataService;

    /**
     * POST  /municipios -> Create a new municipio.
     */
    @RequestMapping(value = "/municipios",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MunicipioDTO> create(@Valid @RequestBody MunicipioDTO municipioDTO) throws URISyntaxException {
        log.debug("REST request to save Municipio : {}", municipioDTO);
        if (municipioDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new municipio cannot already have an ID").body(null);
        }
        Municipio municipio = municipioMapper.municipioDTOToMunicipio(municipioDTO);
        Municipio result = municipioRepository.save(municipio);
        municipioSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/municipios/" + municipioDTO.getId())).body(municipioMapper.municipioToMunicipioDTO(result));
    }

    /**
     * PUT  /municipios -> Updates an existing municipio.
     */
    @RequestMapping(value = "/municipios",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MunicipioDTO> update(@Valid @RequestBody MunicipioDTO municipioDTO) throws URISyntaxException {
        log.debug("REST request to update Municipio : {}", municipioDTO);
        if (municipioDTO.getId() == null) {
            return create(municipioDTO);
        }
        Municipio municipio = municipioMapper.municipioDTOToMunicipio(municipioDTO);
        Municipio result = municipioRepository.save(municipio);
        municipioSearchRepository.save(municipio);
        return ResponseEntity.ok().body(municipioMapper.municipioToMunicipioDTO(result));
    }

    /**
     * GET  /municipios -> get all the municipios.
     */
    @RequestMapping(value = "/municipios",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<MunicipioDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {


        Page<Municipio> page = municipioRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/municipios", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(municipioMapper::municipioToMunicipioDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /municipios/:id -> get the "id" municipio.
     */
    @RequestMapping(value = "/municipios/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MunicipioDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Municipio : {}", id);
        return Optional.ofNullable(municipioRepository.findOne(id))
            .map(municipioMapper::municipioToMunicipioDTO)
            .map(municipioDTO -> new ResponseEntity<>(
                municipioDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /municipios/:id -> delete the "id" municipio.
     */
    @RequestMapping(value = "/municipios/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Municipio : {}", id);
        municipioRepository.delete(id);
        municipioSearchRepository.delete(id);
    }

    /**
     * SEARCH  /_search/municipios/:query -> search for the municipio corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/municipios/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<MunicipioDTO> search(@PathVariable String query) {

        Spliterator<Municipio> municipioSpliterator= municipioSearchRepository.search(queryString(query)).spliterator();

        return StreamSupport
            .stream(municipioSpliterator, false).map(municipioMapper::municipioToMunicipioDTO)
            .collect(Collectors.toList());
    }
}

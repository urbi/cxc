package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Cuenta;
import com.kalitron.cxc.domain.CuentaDestinoPago;
import com.kalitron.cxc.domain.Movimiento;
import com.kalitron.cxc.domain.Nota;
import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;
import com.kalitron.cxc.repository.CuentaRepository;
import com.kalitron.cxc.repository.search.CuentaSearchRepository;
import com.kalitron.cxc.service.CuentaService;


import com.kalitron.cxc.web.rest.dto.ReciboPagoDTO;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.CuentaDTO;
import com.kalitron.cxc.web.rest.mapper.CuentaMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Cuenta.
 */
@RestController
@RequestMapping("/api")
public class CuentaResource {

    private final Logger log = LoggerFactory.getLogger(CuentaResource.class);

    @Inject
    private CuentaRepository cuentaRepository;

    @Inject
    private CuentaMapper cuentaMapper;


    @Inject
    private CuentaSearchRepository cuentaSearchRepository;

    @Inject
    private CuentaService cuentaService;



    /**
     * POST  /cuentas -> Create a new cuenta.
     */

    @RequestMapping(value = "/cuentas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CuentaDTO> create( @RequestBody CuentaDTO cuentaDTO) throws URISyntaxException {
        log.debug("REST request to save CuentaDTO : {}", cuentaDTO);

        try {

            Cuenta cuenta = cuentaMapper.cuentaDTOToCuenta(cuentaDTO);
            cuenta.setMovimientos(cuentaMapper.movimientoDTOsToMovimientos(cuentaDTO.getMovimientoDTOs()));
            if(cuentaDTO.getCuentaDestinoPagoDTOs() != null)
                cuenta.setCuentaDestinoPagos(cuentaMapper.cuentaDestinoPagoDTOsToCuentaDestinoPagos(cuentaDTO.getCuentaDestinoPagoDTOs()));

            Cuenta result = cuentaService.create(cuenta);

            return ResponseEntity.created(new URI("/api/cuentas/" + cuentaDTO.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("cuenta",result.getId().toString()))
                .body(cuentaMapper.cuentaToCuentaDTO(result));
        }catch (Exception e){
            return ResponseEntity.badRequest().header("Failure", e.getMessage()).body(null);
        }
    }



    /**
     * PUT  /cuentas -> Updates an existing cuenta.
     * Todas las actualizaciones de una cuenta por cobrar se manejan como si fuera una nueva cuenta.
     * el servicio de cuenta por cobrar controla si el registro es nuevo o es una actualizacion
     * el servicio no lo borramos para mantener una consistencia con la arquitectura
     */
    @RequestMapping(value = "/cuentas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CuentaDTO> update(@Valid @RequestBody CuentaDTO cuentaDTO) throws URISyntaxException {
        log.debug("REST request to update Cuenta : {}", cuentaDTO);


            return create(cuentaDTO);

    }

    /**
     * GET  /cuentas -> get all the cuentas.
     */
     /*
    * TODO:no hace la conversion de todos los objetos que encuentra el mapper*/
    @RequestMapping(value = "/cuentas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CuentaDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        log.debug("REST Offest y limit : {}", offset +  " "  + limit);
        Page<Cuenta> page = cuentaRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));


        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cuentas", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(cuentaMapper::cuentaToCuentaDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /cuentas/:id -> get the "id" cuenta.
     */
    @RequestMapping(value = "/cuentas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<CuentaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Cuenta : {}", id);

        Cuenta cuenta = cuentaService.findOne(id);
        CuentaDTO cuentaDTO = null;
        if(cuenta != null){

            cuentaDTO = cuentaMapper.cuentaToCuentaDTO(cuenta);
            cuentaDTO.setMovimientoDTOs(cuentaMapper.movimientosToMovimientoDTOs(new ArrayList<Movimiento>(cuenta.getMovimientos())));
            cuentaDTO.setCuentaDestinoPagoDTOs(cuentaMapper.cuentaDestinoPagosToCuentaDestinoPagoDTOs(new ArrayList<CuentaDestinoPago>(cuenta.getCuentaDestinoPagos())));
            cuentaDTO.setHasPago(false);
            for (Movimiento movimiento :  cuenta.getMovimientos()){
                if(movimiento.getConceptoMovimiento() == ConceptoMovimientoType.PAGO) cuentaDTO.setHasPago(true);
            }
            if (cuenta.getNotas().size()>0)
                cuentaDTO.setNotaDTOs(cuentaMapper.notasToNotaDTOs(new ArrayList<Nota>(cuenta.getNotas())));

        }
        log.debug("REST request to get Movimientos Size: {}", cuenta.getMovimientos().size());


        return Optional.ofNullable(cuentaDTO)
            .map(cuentaDTO1 -> new ResponseEntity<>(
                cuentaDTO1,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cuentas/:id -> delete the "id" cuenta.
     */
    @RequestMapping(value = "/cuentas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Cuenta : {}", id);
        String messageHeader = id.toString();
        try{
            cuentaService.delete(id);
        }catch (Exception e){
            log.debug("REST request to delete Cuenta throws a exception: {}", id);
            messageHeader = e.getMessage() + "  - "  + id.toString();
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cuenta", messageHeader)).build();
    }

    /**
     * SEARCH  /_search/cuentas/:query -> search for the cuenta corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/cuentas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CuentaDTO> search(@PathVariable String query) {
        return StreamSupport
            .stream(cuentaSearchRepository.search(queryString(query)).spliterator(), false)
            .map(cuentaMapper::cuentaToCuentaDTO)
            .collect(Collectors.toList());
    }
//TODO: agregar funcionalidad para envio de correo al recibir pago
    /**
     * GET  /cuentas/:id -> get the "id" cuenta.

    @RequestMapping(value = "/cuentas/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<ReciboPagoDTO> reciboPago(@PathVariable Long id) {
        log.debug("REST request to get Recibo Pago : {}", id);

        ReciboPagoDTO reciboPagoDTO = cuentaService.reciboPago(id);



        return Optional.ofNullable(reciboPagoDTO)
            .map(reciboPagoDTO1 -> new ResponseEntity<>(
                reciboPagoDTO1,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }     */
}

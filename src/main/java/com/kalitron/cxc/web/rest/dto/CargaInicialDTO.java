package com.kalitron.cxc.web.rest.dto;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Lob;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.TipoCargaType;

/**
 * A DTO for the CargaInicial entity.
 */
public class CargaInicialDTO implements Serializable {

    private Long id;

    @NotNull
    private TipoCargaType tipoCarga;

    @Type(type="org.hibernate.type.BinaryType")
    @NotNull
    private byte[] archivo;

    private String mensaje;

    private DateTime fechaCarga;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoCargaType getTipoCarga() {
        return tipoCarga;
    }

    public void setTipoCarga(TipoCargaType tipoCarga) {
        this.tipoCarga = tipoCarga;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public DateTime getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(DateTime fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CargaInicialDTO cargaInicialDTO = (CargaInicialDTO) o;

        if ( ! Objects.equals(id, cargaInicialDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CargaInicialDTO{" +
                "id=" + id +
                ", tipoCarga='" + tipoCarga + "'" +
                ", archivo='" + archivo + "'" +
                ", mensaje='" + mensaje + "'" +
                ", fechaCarga='" + fechaCarga + "'" +
                '}';
    }
}

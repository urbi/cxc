package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Nota;
import com.kalitron.cxc.repository.NotaRepository;
import com.kalitron.cxc.repository.search.NotaSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.NotaDTO;
import com.kalitron.cxc.web.rest.mapper.NotaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Nota.
 */
@RestController
@RequestMapping("/api")
public class NotaResource {

    private final Logger log = LoggerFactory.getLogger(NotaResource.class);

    @Inject
    private NotaRepository notaRepository;

    @Inject
    private NotaMapper notaMapper;

    @Inject
    private NotaSearchRepository notaSearchRepository;

    /**
     * POST  /notas -> Create a new nota.
     */
    @RequestMapping(value = "/notas",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotaDTO> create(@Valid @RequestBody NotaDTO notaDTO) throws URISyntaxException {
        log.debug("REST request to save Nota : {}", notaDTO);
        if (notaDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new nota cannot already have an ID").body(null);
        }
        Nota nota = notaMapper.notaDTOToNota(notaDTO);
        Nota result = notaRepository.save(nota);
        notaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/notas/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("nota", result.getId().toString()))
                .body(notaMapper.notaToNotaDTO(result));
    }

    /**
     * PUT  /notas -> Updates an existing nota.
     */
    @RequestMapping(value = "/notas",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotaDTO> update(@Valid @RequestBody NotaDTO notaDTO) throws URISyntaxException {
        log.debug("REST request to update Nota : {}", notaDTO);
        if (notaDTO.getId() == null) {
            return create(notaDTO);
        }
        Nota nota = notaMapper.notaDTOToNota(notaDTO);
        Nota result = notaRepository.save(nota);
        notaSearchRepository.save(nota);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("nota", notaDTO.getId().toString()))
                .body(notaMapper.notaToNotaDTO(result));
    }

    /**
     * GET  /notas -> get all the notas.
     */
    @RequestMapping(value = "/notas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<NotaDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Nota> page = notaRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/notas", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(notaMapper::notaToNotaDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /notas/:id -> get the "id" nota.
     */
    @RequestMapping(value = "/notas/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<NotaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Nota : {}", id);
        return Optional.ofNullable(notaRepository.findOne(id))
            .map(notaMapper::notaToNotaDTO)
            .map(notaDTO -> new ResponseEntity<>(
                notaDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /notas/:id -> delete the "id" nota.
     */
    @RequestMapping(value = "/notas/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Nota : {}", id);
        notaRepository.delete(id);
        notaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("nota", id.toString())).build();
    }

    /**
     * SEARCH  /_search/notas/:query -> search for the nota corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/notas/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Nota> search(@PathVariable String query) {
        return StreamSupport
            .stream(notaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

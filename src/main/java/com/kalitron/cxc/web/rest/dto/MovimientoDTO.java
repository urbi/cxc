package com.kalitron.cxc.web.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import com.kalitron.cxc.service.util.NumberToLetterConverter;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.EstatusMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoPagoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;

/**
 * A DTO for the Movimiento entity.
 */
public class MovimientoDTO implements Serializable {

    private Long id;

    @NotNull
    private BigDecimal monto;


    private BigDecimal saldo;

    private Integer numeroPagare;


    private Integer numeroPago;

    private String montoLetra;



    @NotNull
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    private LocalDate fechaVencimiento;

    @NotNull
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @JsonDeserialize(using = ISO8601LocalDateDeserializer.class)
    private LocalDate fechaPago;

    private EstatusMovimientoType estatusMovimiento;

    private Long secuenciaMovimiento;

    private TipoPagoType tipoPago;

    private TipoMovimientoType tipoMovimiento;

    private ConceptoMovimientoType conceptoMovimiento;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime createdDate;

    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    private DateTime lastModifiedDate;

    private Long cuentaId;

    private String folio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Integer getNumeroPagare() {
        return numeroPagare;
    }

    public void setNumeroPagare(Integer numeroPagare) {
        this.numeroPagare = numeroPagare;
    }

    public Integer getNumeroPago() {
        return numeroPago;
    }

    public void setNumeroPago(Integer numeroPago) {
        this.numeroPago = numeroPago;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public LocalDate getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(LocalDate fechaPago) {
        this.fechaPago = fechaPago;
    }

    public EstatusMovimientoType getEstatusMovimiento() {
        return estatusMovimiento;
    }

    public void setEstatusMovimiento(EstatusMovimientoType estatusMovimiento) {
        this.estatusMovimiento = estatusMovimiento;
    }

    public Long getSecuenciaMovimiento() {
        return secuenciaMovimiento;
    }

    public void setSecuenciaMovimiento(Long secuenciaMovimiento) {
        this.secuenciaMovimiento = secuenciaMovimiento;
    }

    public TipoPagoType getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPagoType tipoPago) {
        this.tipoPago = tipoPago;
    }

    public TipoMovimientoType getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimientoType tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public ConceptoMovimientoType getConceptoMovimiento() {
        return conceptoMovimiento;
    }

    public void setConceptoMovimiento(ConceptoMovimientoType conceptoMovimiento) {
        this.conceptoMovimiento = conceptoMovimiento;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(Long cuentaId) {
        this.cuentaId = cuentaId;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getMontoLetra() {
        if(monto != null)
            return NumberToLetterConverter.convertNumberToLetter(monto.doubleValue());
        else
            return "";
    }

    public void setMontoLetra(String montoLetra) {
        this.montoLetra = montoLetra;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MovimientoDTO movimientoDTO = (MovimientoDTO) o;

        if ( ! Objects.equals(id, movimientoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MovimientoDTO{" +
            "id=" + id +
            ", monto=" + monto +
            ", saldo=" + saldo +
            ", numeroPagare=" + numeroPagare +
            ", numeroPago=" + numeroPago +
            ", fechaVencimiento=" + fechaVencimiento +
            ", fechaPago=" + fechaPago +
            ", estatusMovimiento=" + estatusMovimiento +
            ", secuenciaMovimiento=" + secuenciaMovimiento +
            ", tipoPago=" + tipoPago +
            ", tipoMovimiento=" + tipoMovimiento +
            ", conceptoMovimiento=" + conceptoMovimiento +
            ", createdDate=" + createdDate +
            ", lastModifiedDate=" + lastModifiedDate +
            ", cuentaId=" + cuentaId +
            '}';
    }
}

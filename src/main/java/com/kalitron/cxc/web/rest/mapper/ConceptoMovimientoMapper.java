package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.ConceptoMovimientoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ConceptoMovimiento and its DTO ConceptoMovimientoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConceptoMovimientoMapper {

    @Mapping(source = "movimiento.id", target = "movimientoId")
    ConceptoMovimientoDTO conceptoMovimientoToConceptoMovimientoDTO(ConceptoMovimiento conceptoMovimiento);

    @Mapping(source = "movimientoId", target = "movimiento")
    ConceptoMovimiento conceptoMovimientoDTOToConceptoMovimiento(ConceptoMovimientoDTO conceptoMovimientoDTO);

    default Movimiento movimientoFromId(Long id) {
        if (id == null) {
            return null;
        }
        Movimiento movimiento = new Movimiento();
        movimiento.setId(id);
        return movimiento;
    }
}

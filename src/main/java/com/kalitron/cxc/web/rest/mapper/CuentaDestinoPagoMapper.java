package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.CuentaDestinoPagoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CuentaDestinoPago and its DTO CuentaDestinoPagoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CuentaDestinoPagoMapper {

    @Mapping(source = "cuenta.id", target = "cuentaId")
    CuentaDestinoPagoDTO cuentaDestinoPagoToCuentaDestinoPagoDTO(CuentaDestinoPago cuentaDestinoPago);

    @Mapping(source = "cuentaId", target = "cuenta")
    CuentaDestinoPago cuentaDestinoPagoDTOToCuentaDestinoPago(CuentaDestinoPagoDTO cuentaDestinoPagoDTO);

    default Cuenta cuentaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Cuenta cuenta = new Cuenta();
        cuenta.setId(id);
        return cuenta;
    }
}

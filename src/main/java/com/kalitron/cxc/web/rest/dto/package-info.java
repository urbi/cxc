/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.kalitron.cxc.web.rest.dto;

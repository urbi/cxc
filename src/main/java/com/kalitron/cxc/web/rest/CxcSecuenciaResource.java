package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.CxcSecuencia;
import com.kalitron.cxc.repository.CxcSecuenciaRepository;
import com.kalitron.cxc.repository.search.CxcSecuenciaSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.dto.CxcSecuenciaDTO;
import com.kalitron.cxc.web.rest.mapper.CxcSecuenciaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CxcSecuencia.
 */
@RestController
@RequestMapping("/api")
public class CxcSecuenciaResource {

    private final Logger log = LoggerFactory.getLogger(CxcSecuenciaResource.class);

    @Inject
    private CxcSecuenciaRepository cxcSecuenciaRepository;

    @Inject
    private CxcSecuenciaMapper cxcSecuenciaMapper;

    @Inject
    private CxcSecuenciaSearchRepository cxcSecuenciaSearchRepository;

    /**
     * POST  /cxcSecuencias -> Create a new cxcSecuencia.
     */
    @RequestMapping(value = "/cxcSecuencias",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CxcSecuenciaDTO> create(@Valid @RequestBody CxcSecuenciaDTO cxcSecuenciaDTO) throws URISyntaxException {
        log.debug("REST request to save CxcSecuencia : {}", cxcSecuenciaDTO);
        if (cxcSecuenciaDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new cxcSecuencia cannot already have an ID").body(null);
        }
        CxcSecuencia cxcSecuencia = cxcSecuenciaMapper.cxcSecuenciaDTOToCxcSecuencia(cxcSecuenciaDTO);
        CxcSecuencia result = cxcSecuenciaRepository.save(cxcSecuencia);
        cxcSecuenciaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/cxcSecuencias/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("cxcSecuencia", result.getId().toString()))
                .body(cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(result));
    }

    /**
     * PUT  /cxcSecuencias -> Updates an existing cxcSecuencia.
     */
    @RequestMapping(value = "/cxcSecuencias",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CxcSecuenciaDTO> update(@Valid @RequestBody CxcSecuenciaDTO cxcSecuenciaDTO) throws URISyntaxException {
        log.debug("REST request to update CxcSecuencia : {}", cxcSecuenciaDTO);
        if (cxcSecuenciaDTO.getId() == null) {
            return create(cxcSecuenciaDTO);
        }
        CxcSecuencia cxcSecuencia = cxcSecuenciaMapper.cxcSecuenciaDTOToCxcSecuencia(cxcSecuenciaDTO);
        CxcSecuencia result = cxcSecuenciaRepository.save(cxcSecuencia);
        cxcSecuenciaSearchRepository.save(cxcSecuencia);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("cxcSecuencia", cxcSecuenciaDTO.getId().toString()))
                .body(cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(result));
    }

    /**
     * GET  /cxcSecuencias -> get all the cxcSecuencias.
     */
    @RequestMapping(value = "/cxcSecuencias",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public List<CxcSecuenciaDTO> getAll() {
        log.debug("REST request to get all CxcSecuencias");
        return cxcSecuenciaRepository.findAll().stream()
            .map(cxcSecuencia -> cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(cxcSecuencia))
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * GET  /cxcSecuencias/:id -> get the "id" cxcSecuencia.
     */
    @RequestMapping(value = "/cxcSecuencias/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CxcSecuenciaDTO> get(@PathVariable Long id) {
        log.debug("REST request to get CxcSecuencia : {}", id);
        return Optional.ofNullable(cxcSecuenciaRepository.findOne(id))
            .map(cxcSecuenciaMapper::cxcSecuenciaToCxcSecuenciaDTO)
            .map(cxcSecuenciaDTO -> new ResponseEntity<>(
                cxcSecuenciaDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /cxcSecuencias/:id -> delete the "id" cxcSecuencia.
     */
    @RequestMapping(value = "/cxcSecuencias/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete CxcSecuencia : {}", id);
        cxcSecuenciaRepository.delete(id);
        cxcSecuenciaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cxcSecuencia", id.toString())).build();
    }

    /**
     * SEARCH  /_search/cxcSecuencias/:query -> search for the cxcSecuencia corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/cxcSecuencias/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<CxcSecuencia> search(@PathVariable String query) {
        return StreamSupport
            .stream(cxcSecuenciaSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.Cliente;
import com.kalitron.cxc.repository.ClienteRepository;
import com.kalitron.cxc.repository.search.ClienteSearchRepository;
import com.kalitron.cxc.service.ClienteService;
import com.kalitron.cxc.service.InitializationDataService;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.ClienteDTO;
import com.kalitron.cxc.web.rest.mapper.ClienteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Cliente.
 */
@RestController
@RequestMapping("/api")
public class ClienteResource {

    private final Logger log = LoggerFactory.getLogger(ClienteResource.class);

    @Inject
    private ClienteRepository clienteRepository;

    @Inject
    private ClienteMapper clienteMapper;

    @Inject
    private ClienteSearchRepository clienteSearchRepository;

    @Inject
    private ClienteService clienteService;




    /**
     * POST  /clientes -> Create a new cliente.
     */
    @RequestMapping(value = "/clientes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ClienteDTO> create(@Valid @RequestBody ClienteDTO clienteDTO) throws URISyntaxException {
        log.debug("REST request to save Cliente : {}", clienteDTO);
        if (clienteDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new cliente cannot already have an ID").body(null);
        }
        try {
            Cliente result = clienteService.create(clienteMapper.clienteDTOToCliente(clienteDTO));
        return ResponseEntity.created(new URI("/api/clientes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("cliente", result.getId().toString()))
                .body(clienteMapper.clienteToClienteDTO(result));
        }catch (Exception e){
            return ResponseEntity.badRequest().header("Failure", e.getMessage()).body(null);
    }

    }

    /**
     * PUT  /clientes -> Updates an existing cliente.
     */
    @RequestMapping(value = "/clientes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ClienteDTO> update(@Valid @RequestBody ClienteDTO clienteDTO) throws URISyntaxException {
        log.debug("REST request to update Cliente : {}", clienteDTO);
        if (clienteDTO.getId() == null) {
            return create(clienteDTO);
        }
        Cliente result = clienteService.update(clienteMapper.clienteDTOToCliente(clienteDTO));
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("cliente", clienteDTO.getId().toString()))
                .body(clienteMapper.clienteToClienteDTO(result));
    }

    /**
     * GET  /clientes -> get all the clientes.
     */
    @RequestMapping(value = "/clientes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<ClienteDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<Cliente> page = clienteRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clientes", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(clienteMapper::clienteToClienteDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /clientes/:id -> get the "id" cliente.
     */
    @RequestMapping(value = "/clientes/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ClienteDTO> get(@PathVariable Long id) {
        log.debug("REST request to get Cliente : {}", id);
        return Optional.ofNullable(clienteRepository.findOne(id))
            .map(clienteMapper::clienteToClienteDTO)
            .map(clienteDTO -> new ResponseEntity<>(
                clienteDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /clientes/:id -> delete the "id" cliente.
     */
    @RequestMapping(value = "/clientes/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Cliente : {}", id);
        String headerMesaage = id.toString();
        try{
            clienteService.delete(id);
        }catch (Exception e){
            log.debug("REST request to delete Cliente throws a exception: {}", id);
            headerMesaage = "Sin autorización para eliminar un cliente. Id:  "+ id.toString();
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cliente", headerMesaage)).build();
    }

    /**
     * SEARCH  /_search/clientes/:query -> search for the cliente corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/clientes/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed

        public List<ClienteDTO> search(@PathVariable String query) {

            Spliterator<Cliente> clienteSpliterator= clienteSearchRepository.search(queryString(query)).spliterator();

            return StreamSupport
                .stream(clienteSpliterator, false).map(clienteMapper::clienteToClienteDTO)
                .collect(Collectors.toList());
        }

}

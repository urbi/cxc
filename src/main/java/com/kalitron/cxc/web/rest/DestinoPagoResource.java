package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.DestinoPago;
import com.kalitron.cxc.repository.DestinoPagoRepository;
import com.kalitron.cxc.repository.search.DestinoPagoSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.DestinoPagoDTO;
import com.kalitron.cxc.web.rest.mapper.DestinoPagoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DestinoPago.
 */
@RestController
@RequestMapping("/api")
public class DestinoPagoResource {

    private final Logger log = LoggerFactory.getLogger(DestinoPagoResource.class);

    @Inject
    private DestinoPagoRepository destinoPagoRepository;

    @Inject
    private DestinoPagoMapper destinoPagoMapper;

    @Inject
    private DestinoPagoSearchRepository destinoPagoSearchRepository;

    /**
     * POST  /destinoPagos -> Create a new destinoPago.
     */
    @RequestMapping(value = "/destinoPagos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DestinoPagoDTO> create(@Valid @RequestBody DestinoPagoDTO destinoPagoDTO) throws URISyntaxException {
        log.debug("REST request to save DestinoPago : {}", destinoPagoDTO);
        if (destinoPagoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new destinoPago cannot already have an ID").body(null);
        }
        DestinoPago destinoPago = destinoPagoMapper.destinoPagoDTOToDestinoPago(destinoPagoDTO);
        DestinoPago result = destinoPagoRepository.save(destinoPago);
        destinoPagoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/destinoPagos/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("destinoPago", result.getId().toString()))
                .body(destinoPagoMapper.destinoPagoToDestinoPagoDTO(result));
    }

    /**
     * PUT  /destinoPagos -> Updates an existing destinoPago.
     */
    @RequestMapping(value = "/destinoPagos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DestinoPagoDTO> update(@Valid @RequestBody DestinoPagoDTO destinoPagoDTO) throws URISyntaxException {
        log.debug("REST request to update DestinoPago : {}", destinoPagoDTO);
        if (destinoPagoDTO.getId() == null) {
            return create(destinoPagoDTO);
        }
        DestinoPago destinoPago = destinoPagoMapper.destinoPagoDTOToDestinoPago(destinoPagoDTO);
        DestinoPago result = destinoPagoRepository.save(destinoPago);
        destinoPagoSearchRepository.save(destinoPago);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("destinoPago", destinoPagoDTO.getId().toString()))
                .body(destinoPagoMapper.destinoPagoToDestinoPagoDTO(result));
    }

    /**
     * GET  /destinoPagos -> get all the destinoPagos.
     */
    @RequestMapping(value = "/destinoPagos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<DestinoPagoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<DestinoPago> page = destinoPagoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/destinoPagos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(destinoPagoMapper::destinoPagoToDestinoPagoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /destinoPagos/:id -> get the "id" destinoPago.
     */
    @RequestMapping(value = "/destinoPagos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DestinoPagoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get DestinoPago : {}", id);
        return Optional.ofNullable(destinoPagoRepository.findOne(id))
            .map(destinoPagoMapper::destinoPagoToDestinoPagoDTO)
            .map(destinoPagoDTO -> new ResponseEntity<>(
                destinoPagoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /destinoPagos/:id -> delete the "id" destinoPago.
     */
    @RequestMapping(value = "/destinoPagos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete DestinoPago : {}", id);
        destinoPagoRepository.delete(id);
        destinoPagoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("destinoPago", id.toString())).build();
    }

    /**
     * SEARCH  /_search/destinoPagos/:query -> search for the destinoPago corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/destinoPagos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<DestinoPago> search(@PathVariable String query) {
        return StreamSupport
            .stream(destinoPagoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

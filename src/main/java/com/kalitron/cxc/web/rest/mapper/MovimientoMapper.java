package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.MovimientoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Movimiento and its DTO MovimientoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MovimientoMapper {

    @Mapping(source = "cuenta.id", target = "cuentaId")
    MovimientoDTO movimientoToMovimientoDTO(Movimiento movimiento);

    @Mapping(source = "cuentaId", target = "cuenta")
    Movimiento movimientoDTOToMovimiento(MovimientoDTO movimientoDTO);

    default Cuenta cuentaFromId(Long id) {
        if (id == null) {
            return null;
        }
        Cuenta cuenta = new Cuenta();
        cuenta.setId(id);
        return cuenta;
    }
}

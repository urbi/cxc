package com.kalitron.cxc.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Empresa entity.
 */
public class EmpresaDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombre;

    @NotNull
    private String rfc;

    private String telefono;

    private String correo;

    @NotNull
    private String direccion;

    @NotNull
    @Size(min = 5, max = 5)
    @Pattern(regexp = "^[0-9]*$")
    private String codigoPostal;

    private String ciudad;

    @NotNull
    private String municipio;

    @NotNull
    private String estado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmpresaDTO empresaDTO = (EmpresaDTO) o;

        if ( ! Objects.equals(id, empresaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EmpresaDTO{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", rfc='" + rfc + "'" +
                ", telefono='" + telefono + "'" +
                ", correo='" + correo + "'" +
                ", direccion='" + direccion + "'" +
                ", codigoPostal='" + codigoPostal + "'" +
                ", ciudad='" + ciudad + "'" +
                ", municipio='" + municipio + "'" +
                ", estado='" + estado + "'" +
                '}';
    }
}

package com.kalitron.cxc.web.rest.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.RegionType;

/**
 * A DTO for the Estado entity.
 */
public class EstadoDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombre;

    private RegionType region;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public RegionType getRegion() {
        return region;
    }

    public void setRegion(RegionType region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EstadoDTO estadoDTO = (EstadoDTO) o;

        if ( ! Objects.equals(id, estadoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "EstadoDTO{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", region='" + region + "'" +
                '}';
    }
}

package com.kalitron.cxc.web.rest.dto;

import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.SexoType;
import com.kalitron.cxc.domain.enumeration.SistemaOrigenType;
import com.kalitron.cxc.domain.enumeration.TipoClienteType;

/**
 * A DTO for the Cliente entity.
 */
public class ClienteDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombres;

    @NotNull
    private String apellidoPaterno;

    private String apellidoMaterno;

    @NotNull
    @Size(min = 10, max = 13)
    private String rfc;

    @Size(min = 18, max = 18)
    private String curp;
   @NotNull
    private SexoType sexo;

    @Size(min = 11, max = 11)
    private String nss;

    private String correo;

    private String telefono1;

    private String telefono2;

    private String celular;

    private LocalDate fechaNacimiento;

    private String numeroRefExterna;
    @NotNull
    private SistemaOrigenType sistemaOrigen;
    @NotNull
    private TipoClienteType tipoCliente;

    @NotNull
    private String calle;

    private String numero;

    @NotNull
    @Size(min = 5, max = 5)
    @Pattern(regexp = "^[0-9]*$")
    private String codigoPostal;

    @NotNull
    private String colonia;

    private String ciudad;

    @NotNull
    private String estado;

    @NotNull
    private String municipio;

    private String geoLocalizacion;

    private Long secuenciaCliente;

    private DateTime createdDate;
    private Long createdId;
    private Long updatedId;

    private DateTime lastModifiedDate;

    private String createdName;
    private String updatedName;

    @NotNull
    private Long plazaId;

    private String plazaNombre;


    private String nombreCompleto;

    private String autocompleteFormat;





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public SexoType getSexo() {
        return sexo;
    }

    public void setSexo(SexoType sexo) {
        this.sexo = sexo;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumeroRefExterna() {
        return numeroRefExterna;
    }

    public void setNumeroRefExterna(String numeroRefExterna) {
        this.numeroRefExterna = numeroRefExterna;
    }

    public SistemaOrigenType getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(SistemaOrigenType sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public TipoClienteType getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(TipoClienteType tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getGeoLocalizacion() {
        return geoLocalizacion;
    }

    public void setGeoLocalizacion(String geoLocalizacion) {
        this.geoLocalizacion = geoLocalizacion;
    }



    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }



    public DateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(DateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getSecuenciaCliente() {
        return secuenciaCliente;
    }

    public void setSecuenciaCliente(Long secuenciaCliente) {
        this.secuenciaCliente = secuenciaCliente;
    }

    public Long getPlazaId() {
        return plazaId;
    }

    public void setPlazaId(Long plazaId) {
        this.plazaId = plazaId;
    }

    public String getPlazaNombre() {
        return plazaNombre;
    }

    public void setPlazaNombre(String plazaNombre) {
        this.plazaNombre = plazaNombre;
    }



    public Long getUpdatedId() {
        return updatedId;
    }

    public void setUpdatedId(Long updatedId) {
        this.updatedId = updatedId;
    }

    public String getCreatedName() {
        return createdName;
    }

    public void setCreatedName(String createdName) {
        this.createdName = createdName;
    }

    public String getUpdatedName() {
        return updatedName;
    }

    public void setUpdatedName(String updatedName) {
        this.updatedName = updatedName;
    }

    public String getNombreCompleto() {
        return (nombres + " " + apellidoPaterno +" " +  apellidoMaterno).trim();
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getAutocompleteFormat() {
        return rfc + " " +plazaNombre;
    }

    public String getAutocompleteIdAndNombre() {
        return secuenciaCliente + " : " +  getNombreCompleto();
    }

    public void setAutocompleteFormat(String autocompleteFormat) {
        this.autocompleteFormat = autocompleteFormat;
    }

    public Long getCreatedId() {
        return createdId;
    }

    public void setCreatedId(Long createdId) {
        this.createdId = createdId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClienteDTO clienteDTO = (ClienteDTO) o;

        if ( ! Objects.equals(id, clienteDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ClienteDTO{" +
                "id=" + id +
                ", nombres='" + nombres + "'" +
                ", apellidoPaterno='" + apellidoPaterno + "'" +
                ", apellidoMaterno='" + apellidoMaterno + "'" +
                ", rfc='" + rfc + "'" +
                ", curp='" + curp + "'" +
                ", sexo='" + sexo + "'" +
                ", nss='" + nss + "'" +
                ", correo='" + correo + "'" +
                ", telefono1='" + telefono1 + "'" +
                ", telefono2='" + telefono2 + "'" +
                ", celular='" + celular + "'" +
                ", fechaNacimiento='" + fechaNacimiento + "'" +
                ", numeroRefExterna='" + numeroRefExterna + "'" +
                ", sistemaOrigen='" + sistemaOrigen + "'" +
                ", tipoCliente='" + tipoCliente + "'" +
                ", calle='" + calle + "'" +
                ", numero='" + numero + "'" +
                ", codigoPostal='" + codigoPostal + "'" +
                ", colonia='" + colonia + "'" +
                ", ciudad='" + ciudad + "'" +
                ", estado='" + estado + "'" +
                ", municipio='" + municipio + "'" +
                ", geoLocalizacion='" + geoLocalizacion + "'" +
                ", created='" + createdId + "'" +
                ", createdDate='" + createdDate + "'" +
                ", updated='" + updatedId + "'" +
                ", lastModifiedDate='" + lastModifiedDate + "'" +
                ", secuenciaCliente='" + secuenciaCliente + "'" +
                ", plazaId='" + plazaId + "'" +

                '}';
    }
}

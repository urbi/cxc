package com.kalitron.cxc.web.rest.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;

/**
 * A DTO for the ConceptoMovimiento entity.
 */
public class ConceptoMovimientoDTO implements Serializable {

    private Long id;

    private ConceptoMovimientoType nombre;

    private TipoMovimientoType tipoMovimiento;

    private Long movimientoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConceptoMovimientoType getNombre() {
        return nombre;
    }

    public void setNombre(ConceptoMovimientoType nombre) {
        this.nombre = nombre;
    }

    public TipoMovimientoType getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimientoType tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public Long getMovimientoId() {
        return movimientoId;
    }

    public void setMovimientoId(Long movimientoId) {
        this.movimientoId = movimientoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConceptoMovimientoDTO conceptoMovimientoDTO = (ConceptoMovimientoDTO) o;

        if ( ! Objects.equals(id, conceptoMovimientoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ConceptoMovimientoDTO{" +
                "id=" + id +
                ", nombre='" + nombre + "'" +
                ", tipoMovimiento='" + tipoMovimiento + "'" +
                '}';
    }
}

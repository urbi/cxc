package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.FoliadorReciboPagoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FoliadorReciboPago and its DTO FoliadorReciboPagoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FoliadorReciboPagoMapper {

    FoliadorReciboPagoDTO foliadorReciboPagoToFoliadorReciboPagoDTO(FoliadorReciboPago foliadorReciboPago);

    FoliadorReciboPago foliadorReciboPagoDTOToFoliadorReciboPago(FoliadorReciboPagoDTO foliadorReciboPagoDTO);
}

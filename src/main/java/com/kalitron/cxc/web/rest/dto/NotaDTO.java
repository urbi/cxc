package com.kalitron.cxc.web.rest.dto;

import org.joda.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A DTO for the Nota entity.
 */
public class NotaDTO implements Serializable {

    private Long id;

    @NotNull
    private String titulo;

    private String descripcion;

    private LocalDate fechaCreacion;

    private Long cuentaId;
    private Long secuenciaCuenta;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(Long cuentaId) {
        this.cuentaId = cuentaId;
    }

    @Override
    public String toString() {
        return "NotaDTO{" +
            "id=" + id +
            ", titulo='" + titulo + '\'' +
            ", descripcion='" + descripcion + '\'' +
            ", fechaCreacion=" + fechaCreacion +
            ", cuentaId=" + cuentaId +
            ", secuenciaCuenta=" + secuenciaCuenta +
            '}';
    }

    public Long getSecuenciaCuenta() {
        return secuenciaCuenta;
    }

    public void setSecuenciaCuenta(Long secuenciaCuenta) {
        this.secuenciaCuenta = secuenciaCuenta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NotaDTO notaDTO = (NotaDTO) o;

        if ( ! Objects.equals(id, notaDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}

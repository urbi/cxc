package com.kalitron.cxc.web.rest.mapper;

import com.kalitron.cxc.domain.*;
import com.kalitron.cxc.web.rest.dto.DocumentoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Documento and its DTO DocumentoDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DocumentoMapper {

    DocumentoDTO documentoToDocumentoDTO(Documento documento);

    Documento documentoDTOToDocumento(DocumentoDTO documentoDTO);
}

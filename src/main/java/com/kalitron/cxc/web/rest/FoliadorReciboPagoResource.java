package com.kalitron.cxc.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kalitron.cxc.domain.FoliadorReciboPago;
import com.kalitron.cxc.repository.FoliadorReciboPagoRepository;
import com.kalitron.cxc.repository.search.FoliadorReciboPagoSearchRepository;
import com.kalitron.cxc.web.rest.util.HeaderUtil;
import com.kalitron.cxc.web.rest.util.PaginationUtil;
import com.kalitron.cxc.web.rest.dto.FoliadorReciboPagoDTO;
import com.kalitron.cxc.web.rest.mapper.FoliadorReciboPagoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing FoliadorReciboPago.
 */
@RestController
@RequestMapping("/api")
public class FoliadorReciboPagoResource {

    private final Logger log = LoggerFactory.getLogger(FoliadorReciboPagoResource.class);

    @Inject
    private FoliadorReciboPagoRepository foliadorReciboPagoRepository;

    @Inject
    private FoliadorReciboPagoMapper foliadorReciboPagoMapper;

    @Inject
    private FoliadorReciboPagoSearchRepository foliadorReciboPagoSearchRepository;

    /**
     * POST  /foliadorReciboPagos -> Create a new foliadorReciboPago.
     */
    @RequestMapping(value = "/foliadorReciboPagos",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FoliadorReciboPagoDTO> create(@RequestBody FoliadorReciboPagoDTO foliadorReciboPagoDTO) throws URISyntaxException {
        log.debug("REST request to save FoliadorReciboPago : {}", foliadorReciboPagoDTO);
        if (foliadorReciboPagoDTO.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new foliadorReciboPago cannot already have an ID").body(null);
        }
        FoliadorReciboPago foliadorReciboPago = foliadorReciboPagoMapper.foliadorReciboPagoDTOToFoliadorReciboPago(foliadorReciboPagoDTO);
        FoliadorReciboPago result = foliadorReciboPagoRepository.save(foliadorReciboPago);
        foliadorReciboPagoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/foliadorReciboPagos/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("foliadorReciboPago", result.getId().toString()))
                .body(foliadorReciboPagoMapper.foliadorReciboPagoToFoliadorReciboPagoDTO(result));
    }

    /**
     * PUT  /foliadorReciboPagos -> Updates an existing foliadorReciboPago.
     */
    @RequestMapping(value = "/foliadorReciboPagos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FoliadorReciboPagoDTO> update(@RequestBody FoliadorReciboPagoDTO foliadorReciboPagoDTO) throws URISyntaxException {
        log.debug("REST request to update FoliadorReciboPago : {}", foliadorReciboPagoDTO);
        if (foliadorReciboPagoDTO.getId() == null) {
            return create(foliadorReciboPagoDTO);
        }
        FoliadorReciboPago foliadorReciboPago = foliadorReciboPagoMapper.foliadorReciboPagoDTOToFoliadorReciboPago(foliadorReciboPagoDTO);
        FoliadorReciboPago result = foliadorReciboPagoRepository.save(foliadorReciboPago);
        foliadorReciboPagoSearchRepository.save(foliadorReciboPago);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("foliadorReciboPago", foliadorReciboPagoDTO.getId().toString()))
                .body(foliadorReciboPagoMapper.foliadorReciboPagoToFoliadorReciboPagoDTO(result));
    }

    /**
     * GET  /foliadorReciboPagos -> get all the foliadorReciboPagos.
     */
    @RequestMapping(value = "/foliadorReciboPagos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<FoliadorReciboPagoDTO>> getAll(@RequestParam(value = "page" , required = false) Integer offset,
                                  @RequestParam(value = "per_page", required = false) Integer limit)
        throws URISyntaxException {
        Page<FoliadorReciboPago> page = foliadorReciboPagoRepository.findAll(PaginationUtil.generatePageRequest(offset, limit));
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/foliadorReciboPagos", offset, limit);
        return new ResponseEntity<>(page.getContent().stream()
            .map(foliadorReciboPagoMapper::foliadorReciboPagoToFoliadorReciboPagoDTO)
            .collect(Collectors.toCollection(LinkedList::new)), headers, HttpStatus.OK);
    }

    /**
     * GET  /foliadorReciboPagos/:id -> get the "id" foliadorReciboPago.
     */
    @RequestMapping(value = "/foliadorReciboPagos/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FoliadorReciboPagoDTO> get(@PathVariable Long id) {
        log.debug("REST request to get FoliadorReciboPago : {}", id);
        return Optional.ofNullable(foliadorReciboPagoRepository.findOne(id))
            .map(foliadorReciboPagoMapper::foliadorReciboPagoToFoliadorReciboPagoDTO)
            .map(foliadorReciboPagoDTO -> new ResponseEntity<>(
                foliadorReciboPagoDTO,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /foliadorReciboPagos/:id -> delete the "id" foliadorReciboPago.
     */
    @RequestMapping(value = "/foliadorReciboPagos/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete FoliadorReciboPago : {}", id);
        foliadorReciboPagoRepository.delete(id);
        foliadorReciboPagoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("foliadorReciboPago", id.toString())).build();
    }

    /**
     * SEARCH  /_search/foliadorReciboPagos/:query -> search for the foliadorReciboPago corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/foliadorReciboPagos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FoliadorReciboPago> search(@PathVariable String query) {
        return StreamSupport
            .stream(foliadorReciboPagoSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}

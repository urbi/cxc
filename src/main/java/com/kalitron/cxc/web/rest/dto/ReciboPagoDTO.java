package com.kalitron.cxc.web.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;
import com.kalitron.cxc.domain.enumeration.EstatusMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoPagoType;
import com.kalitron.cxc.domain.util.CustomDateTimeDeserializer;
import com.kalitron.cxc.domain.util.CustomDateTimeSerializer;
import com.kalitron.cxc.domain.util.CustomLocalDateSerializer;
import com.kalitron.cxc.domain.util.ISO8601LocalDateDeserializer;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created by narciso.parra on 11/08/2015.
 */
public class ReciboPagoDTO {



    private CuentaDTO cuentaDTO;
    private EmpresaDTO empresaDTO;
    private ClienteDTO clienteDTO;
    private MovimientoDTO movimientoDTO;

    public CuentaDTO getCuentaDTO() {
        return cuentaDTO;
    }

    public void setCuentaDTO(CuentaDTO cuentaDTO) {
        this.cuentaDTO = cuentaDTO;
    }

    public EmpresaDTO getEmpresaDTO() {
        return empresaDTO;
    }

    public void setEmpresaDTO(EmpresaDTO empresaDTO) {
        this.empresaDTO = empresaDTO;
    }

    public ClienteDTO getClienteDTO() {
        return clienteDTO;
    }

    public void setClienteDTO(ClienteDTO clienteDTO) {
        this.clienteDTO = clienteDTO;
    }

    public MovimientoDTO getMovimientoDTO() {
        return movimientoDTO;
    }

    public void setMovimientoDTO(MovimientoDTO movimientoDTO) {
        this.movimientoDTO = movimientoDTO;
    }

    @Override
    public String toString() {
        return "ReciboPagoDTO{" +
            "cuentaDTO=" + cuentaDTO +
            ", empresaDTO=" + empresaDTO +
            ", clienteDTO=" + clienteDTO +
            ", movimientoDTO=" + movimientoDTO +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReciboPagoDTO)) return false;

        ReciboPagoDTO that = (ReciboPagoDTO) o;

        if (clienteDTO != null ? !clienteDTO.equals(that.clienteDTO) : that.clienteDTO != null) return false;
        if (cuentaDTO != null ? !cuentaDTO.equals(that.cuentaDTO) : that.cuentaDTO != null) return false;
        if (empresaDTO != null ? !empresaDTO.equals(that.empresaDTO) : that.empresaDTO != null) return false;
        if (movimientoDTO != null ? !movimientoDTO.equals(that.movimientoDTO) : that.movimientoDTO != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cuentaDTO != null ? cuentaDTO.hashCode() : 0;
        result = 31 * result + (empresaDTO != null ? empresaDTO.hashCode() : 0);
        result = 31 * result + (clienteDTO != null ? clienteDTO.hashCode() : 0);
        result = 31 * result + (movimientoDTO != null ? movimientoDTO.hashCode() : 0);
        return result;
    }
}

package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.CxcSecuencia;
import com.kalitron.cxc.domain.enumeration.SecuenciaType;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CxcSecuencia entity.
 */
public interface CxcSecuenciaRepository extends JpaRepository<CxcSecuencia,Long> {


    public CxcSecuencia findByNombre(SecuenciaType id);


}

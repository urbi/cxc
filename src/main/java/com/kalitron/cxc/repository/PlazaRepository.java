package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Plaza;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Plaza entity.
 */
public interface PlazaRepository extends JpaRepository<Plaza,Long> {

}

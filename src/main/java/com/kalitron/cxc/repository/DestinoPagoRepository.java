package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.DestinoPago;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the DestinoPago entity.
 */
public interface DestinoPagoRepository extends JpaRepository<DestinoPago,Long> {

}

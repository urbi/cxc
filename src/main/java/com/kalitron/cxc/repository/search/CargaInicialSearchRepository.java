package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.CargaInicial;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CargaInicial entity.
 */
public interface CargaInicialSearchRepository extends ElasticsearchRepository<CargaInicial, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.CuentaDestinoPago;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CuentaDestinoPago entity.
 */
public interface CuentaDestinoPagoSearchRepository extends ElasticsearchRepository<CuentaDestinoPago, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.EmpresaDestinoPago;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the EmpresaDestinoPago entity.
 */
public interface EmpresaDestinoPagoSearchRepository extends ElasticsearchRepository<EmpresaDestinoPago, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.CxcSecuencia;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the CxcSecuencia entity.
 */
public interface CxcSecuenciaSearchRepository extends ElasticsearchRepository<CxcSecuencia, Long> {
}

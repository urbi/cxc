package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.DestinoPago;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the DestinoPago entity.
 */
public interface DestinoPagoSearchRepository extends ElasticsearchRepository<DestinoPago, Long> {
}

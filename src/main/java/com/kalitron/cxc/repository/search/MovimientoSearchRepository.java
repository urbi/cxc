package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Movimiento;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Movimiento entity.
 */
public interface MovimientoSearchRepository extends ElasticsearchRepository<Movimiento, Long> {
}

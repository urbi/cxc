package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Caja;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Caja entity.
 */
public interface CajaSearchRepository extends ElasticsearchRepository<Caja, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Nota;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Nota entity.
 */
public interface NotaSearchRepository extends ElasticsearchRepository<Nota, Long> {
}

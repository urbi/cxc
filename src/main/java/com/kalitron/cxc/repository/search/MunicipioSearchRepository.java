package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Municipio;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Municipio entity.
 */
public interface MunicipioSearchRepository extends ElasticsearchRepository<Municipio, Long> {
}

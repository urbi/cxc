package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.PagoReferenciado;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the PagoReferenciado entity.
 */
public interface PagoReferenciadoSearchRepository extends ElasticsearchRepository<PagoReferenciado, Long> {
}

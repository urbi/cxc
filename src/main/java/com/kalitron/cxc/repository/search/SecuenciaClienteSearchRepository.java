package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.SecuenciaCliente;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SecuenciaCliente entity.
 */
public interface SecuenciaClienteSearchRepository extends ElasticsearchRepository<SecuenciaCliente, Long> {
}

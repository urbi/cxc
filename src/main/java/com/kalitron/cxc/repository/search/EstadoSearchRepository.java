package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Estado;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Estado entity.
 */
public interface EstadoSearchRepository extends ElasticsearchRepository<Estado, Long> {
}

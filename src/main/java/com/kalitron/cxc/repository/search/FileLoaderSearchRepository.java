package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.FileLoader;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the FileLoader entity.
 */
public interface FileLoaderSearchRepository extends ElasticsearchRepository<FileLoader, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.SecuenciaCuenta;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the SecuenciaCuenta entity.
 */
public interface SecuenciaCuentaSearchRepository extends ElasticsearchRepository<SecuenciaCuenta, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Cuenta;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Cuenta entity.
 */
public interface CuentaSearchRepository extends ElasticsearchRepository<Cuenta, Long> {
}

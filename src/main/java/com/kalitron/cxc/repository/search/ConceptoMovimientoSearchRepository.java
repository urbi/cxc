package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.ConceptoMovimiento;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ConceptoMovimiento entity.
 */
public interface ConceptoMovimientoSearchRepository extends ElasticsearchRepository<ConceptoMovimiento, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.Plaza;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Plaza entity.
 */
public interface PlazaSearchRepository extends ElasticsearchRepository<Plaza, Long> {
}

package com.kalitron.cxc.repository.search;

import com.kalitron.cxc.domain.FoliadorReciboPago;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the FoliadorReciboPagos entity.
 */
public interface FoliadorReciboPagosSearchRepository extends ElasticsearchRepository<FoliadorReciboPago, Long> {
}

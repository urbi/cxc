package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.FoliadorReciboPago;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FoliadorReciboPago entity.
 */
public interface FoliadorReciboPagoRepository extends JpaRepository<FoliadorReciboPago,Long> {

    public FoliadorReciboPago findByPlazaAndEmpresa(Long plazaId, Long empresaId);

}

package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Nota;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Nota entity.
 */
public interface NotaRepository extends JpaRepository<Nota,Long> {

}

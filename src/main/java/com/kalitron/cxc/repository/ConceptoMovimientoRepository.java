package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.ConceptoMovimiento;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ConceptoMovimiento entity.
 */
public interface ConceptoMovimientoRepository extends JpaRepository<ConceptoMovimiento,Long> {

}

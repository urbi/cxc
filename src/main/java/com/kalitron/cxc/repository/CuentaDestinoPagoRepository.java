package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.CuentaDestinoPago;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the CuentaDestinoPago entity.
 */
public interface CuentaDestinoPagoRepository extends JpaRepository<CuentaDestinoPago,Long> {

    public List<CuentaDestinoPago> removeByCuentaId(Long id);

    public Set<CuentaDestinoPago> findByCuentaId(Long id);

    public CuentaDestinoPago findByReferencia(String referencia);
}

package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Caja;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Caja entity.
 */
public interface CajaRepository extends JpaRepository<Caja,Long> {

}

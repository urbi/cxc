package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Movimiento;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import org.springframework.data.jpa.repository.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import org.joda.time.LocalDate;

/**
 * Spring Data JPA repository for the Movimiento entity.
 */
public interface MovimientoRepository extends JpaRepository<Movimiento,Long> {

    public List<Movimiento> removeByCuentaId(Long id);

    public Set<Movimiento> findByCuentaId(Long id);

    public List<Movimiento> findByIdNotIn(List<Movimiento> movimientos);

    @Query("Select Max(m.numeroPagare) from Movimiento m where m.cuenta.id = ?1")
    public Integer numeroPagareMaxByCuentaId(Long id);

    @Query("Select Max(m.numeroPago) from Movimiento m where m.cuenta.id = ?1")
    public Integer numeroPagoMaxByCuentaId(Long id);
    //TODO: revisar portabilidad del llamado CASE  podria perderse  la transparencia en una migracion de base de datos.
    @Query("Select sum(CASE WHEN m.tipoMovimiento = 'CARGO' THEN m.monto ELSE -m.monto END) as saldo from Movimiento m where m.cuenta.id= ?1")
    public BigDecimal saldosByCuentaId(Long id);

    @Query("Select  SUM(m.monto) from Movimiento m where m.cuenta.id= ?1 and m.tipoMovimiento <> 'ABONO'" +
        "and m.fechaVencimiento <= ?2 and m.estatusMovimiento in('CORRIENTE', 'VENCIDA')")
    public BigDecimal MontoExigibleByCuentaId(Long id, LocalDate fecha);

    public List<Movimiento> findByCuentaIdOrderByFechaVencimientoAsc(Long id);

    @Query("Select SUM(m.monto) from Movimiento m where m.cuenta.id = ?1 and  m.tipoMovimiento = ?2")
    public BigDecimal getMontoSumarizadoByCuentaIdAndTipoMovimiento(Long id, TipoMovimientoType tipoMovimientoType);



    @Modifying
    @Query("update Movimiento m set m.estatusMovimiento = 'VENCIDA' where m.tipoMovimiento='CARGO' and m.estatusMovimiento ='CORRIENTE' and  m.fechaVencimiento = ?1")
    int updateEstatusVencidoByFechaVencimiento(LocalDate  lastDayOfMonth);

}

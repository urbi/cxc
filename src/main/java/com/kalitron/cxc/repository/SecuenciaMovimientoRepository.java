package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.SecuenciaCuenta;
import com.kalitron.cxc.domain.SecuenciaMovimiento;
import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the SecuenciaCuenta entity.
 */
public interface SecuenciaMovimientoRepository extends JpaRepository<SecuenciaMovimiento,Long> {



    public SecuenciaMovimiento findByTipoMovimiento(TipoMovimientoType id);

}

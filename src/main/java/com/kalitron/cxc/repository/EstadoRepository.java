package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Estado;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Estado entity.
 */
public interface EstadoRepository extends JpaRepository<Estado,Long> {

}

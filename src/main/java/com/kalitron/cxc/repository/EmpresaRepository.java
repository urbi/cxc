package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Empresa;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Empresa entity.
 */
public interface EmpresaRepository extends JpaRepository<Empresa,Long> {

}

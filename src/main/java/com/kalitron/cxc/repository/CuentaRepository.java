package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Cuenta;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Cuenta entity.
 */
public interface CuentaRepository extends JpaRepository<Cuenta,Long> {
    public List<Cuenta> findByClienteId(Long id);

}

package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Municipio;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Municipio entity.
 */
public interface MunicipioRepository extends JpaRepository<Municipio,Long> {

}

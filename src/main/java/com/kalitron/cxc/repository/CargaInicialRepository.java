package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.CargaInicial;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CargaInicial entity.
 */
public interface CargaInicialRepository extends JpaRepository<CargaInicial,Long> {

}

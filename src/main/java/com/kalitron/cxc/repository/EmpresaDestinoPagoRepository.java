package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.Empresa;
import com.kalitron.cxc.domain.EmpresaDestinoPago;
import com.kalitron.cxc.domain.Plaza;
import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Set;

/**
 * Spring Data JPA repository for the EmpresaDestinoPago entity.
 */
public interface EmpresaDestinoPagoRepository extends JpaRepository<EmpresaDestinoPago,Long> {


    public Set<EmpresaDestinoPago> findByPlazaAndEmpresa(Plaza plaza, Empresa empresa);

}

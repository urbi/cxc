package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.FileLoader;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the FileLoader entity.
 */
public interface FileLoaderRepository extends JpaRepository<FileLoader,Long> {

}

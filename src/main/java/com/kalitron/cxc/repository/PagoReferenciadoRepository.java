package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.PagoReferenciado;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PagoReferenciado entity.
 */
public interface PagoReferenciadoRepository extends JpaRepository<PagoReferenciado,Long> {

    List<PagoReferenciado> findByFolioCarga(String folioCarga);

}

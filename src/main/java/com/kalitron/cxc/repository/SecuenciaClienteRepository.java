package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.SecuenciaCliente;
import com.kalitron.cxc.domain.enumeration.TipoClienteType;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SecuenciaCliente entity.
 */
public interface SecuenciaClienteRepository extends JpaRepository<SecuenciaCliente,Long> {



    public SecuenciaCliente findByTipoCliente(TipoClienteType id);

}

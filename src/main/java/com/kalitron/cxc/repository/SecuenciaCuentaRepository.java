package com.kalitron.cxc.repository;

import com.kalitron.cxc.domain.SecuenciaCuenta;
import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SecuenciaCuenta entity.
 */
public interface SecuenciaCuentaRepository extends JpaRepository<SecuenciaCuenta,Long> {



    public SecuenciaCuenta findByTipoDocumento(TipoDocumentoType id);

}

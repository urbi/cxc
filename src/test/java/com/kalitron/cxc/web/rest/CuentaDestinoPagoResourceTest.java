package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.CuentaDestinoPago;
import com.kalitron.cxc.repository.CuentaDestinoPagoRepository;
import com.kalitron.cxc.repository.search.CuentaDestinoPagoSearchRepository;
import com.kalitron.cxc.web.rest.mapper.CuentaDestinoPagoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CuentaDestinoPagoResource REST controller.
 *
 * @see CuentaDestinoPagoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CuentaDestinoPagoResourceTest {

    private static final String DEFAULT_CONVENIO = "SAMPLE_TEXT";
    private static final String UPDATED_CONVENIO = "UPDATED_TEXT";
    private static final String DEFAULT_CUENTA_CLABE = "SAMPLE_TEXT";
    private static final String UPDATED_CUENTA_CLABE = "UPDATED_TEXT";
    private static final String DEFAULT_REFERENCIA = "SAMPLE_TEXT";
    private static final String UPDATED_REFERENCIA = "UPDATED_TEXT";
    private static final String DEFAULT_BANCO = "SAMPLE_TEXT";
    private static final String UPDATED_BANCO = "UPDATED_TEXT";

    @Inject
    private CuentaDestinoPagoRepository cuentaDestinoPagoRepository;

    @Inject
    private CuentaDestinoPagoMapper cuentaDestinoPagoMapper;

    @Inject
    private CuentaDestinoPagoSearchRepository cuentaDestinoPagoSearchRepository;

    private MockMvc restCuentaDestinoPagoMockMvc;

    private CuentaDestinoPago cuentaDestinoPago;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CuentaDestinoPagoResource cuentaDestinoPagoResource = new CuentaDestinoPagoResource();
        ReflectionTestUtils.setField(cuentaDestinoPagoResource, "cuentaDestinoPagoRepository", cuentaDestinoPagoRepository);
        ReflectionTestUtils.setField(cuentaDestinoPagoResource, "cuentaDestinoPagoMapper", cuentaDestinoPagoMapper);
        ReflectionTestUtils.setField(cuentaDestinoPagoResource, "cuentaDestinoPagoSearchRepository", cuentaDestinoPagoSearchRepository);
        this.restCuentaDestinoPagoMockMvc = MockMvcBuilders.standaloneSetup(cuentaDestinoPagoResource).build();
    }

    @Before
    public void initTest() {
        cuentaDestinoPago = new CuentaDestinoPago();
        cuentaDestinoPago.setConvenio(DEFAULT_CONVENIO);
        cuentaDestinoPago.setCuentaClabe(DEFAULT_CUENTA_CLABE);
        cuentaDestinoPago.setReferencia(DEFAULT_REFERENCIA);
        cuentaDestinoPago.setBanco(DEFAULT_BANCO);
    }

    @Test
    @Transactional
    public void createCuentaDestinoPago() throws Exception {
        int databaseSizeBeforeCreate = cuentaDestinoPagoRepository.findAll().size();

        // Create the CuentaDestinoPago
        restCuentaDestinoPagoMockMvc.perform(post("/api/cuentaDestinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cuentaDestinoPago)))
                .andExpect(status().isCreated());

        // Validate the CuentaDestinoPago in the database
        List<CuentaDestinoPago> cuentaDestinoPagos = cuentaDestinoPagoRepository.findAll();
        assertThat(cuentaDestinoPagos).hasSize(databaseSizeBeforeCreate + 1);
        CuentaDestinoPago testCuentaDestinoPago = cuentaDestinoPagos.get(cuentaDestinoPagos.size() - 1);
        assertThat(testCuentaDestinoPago.getConvenio()).isEqualTo(DEFAULT_CONVENIO);
        assertThat(testCuentaDestinoPago.getCuentaClabe()).isEqualTo(DEFAULT_CUENTA_CLABE);
        assertThat(testCuentaDestinoPago.getReferencia()).isEqualTo(DEFAULT_REFERENCIA);
        assertThat(testCuentaDestinoPago.getBanco()).isEqualTo(DEFAULT_BANCO);
    }

    @Test
    @Transactional
    public void getAllCuentaDestinoPagos() throws Exception {
        // Initialize the database
        cuentaDestinoPagoRepository.saveAndFlush(cuentaDestinoPago);

        // Get all the cuentaDestinoPagos
        restCuentaDestinoPagoMockMvc.perform(get("/api/cuentaDestinoPagos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cuentaDestinoPago.getId().intValue())))
                .andExpect(jsonPath("$.[*].convenio").value(hasItem(DEFAULT_CONVENIO.toString())))
                .andExpect(jsonPath("$.[*].cuentaClabe").value(hasItem(DEFAULT_CUENTA_CLABE.toString())))
                .andExpect(jsonPath("$.[*].referencia").value(hasItem(DEFAULT_REFERENCIA.toString())))
                .andExpect(jsonPath("$.[*].banco").value(hasItem(DEFAULT_BANCO.toString())));
    }

    @Test
    @Transactional
    public void getCuentaDestinoPago() throws Exception {
        // Initialize the database
        cuentaDestinoPagoRepository.saveAndFlush(cuentaDestinoPago);

        // Get the cuentaDestinoPago
        restCuentaDestinoPagoMockMvc.perform(get("/api/cuentaDestinoPagos/{id}", cuentaDestinoPago.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cuentaDestinoPago.getId().intValue()))
            .andExpect(jsonPath("$.convenio").value(DEFAULT_CONVENIO.toString()))
            .andExpect(jsonPath("$.cuentaClabe").value(DEFAULT_CUENTA_CLABE.toString()))
            .andExpect(jsonPath("$.referencia").value(DEFAULT_REFERENCIA.toString()))
            .andExpect(jsonPath("$.banco").value(DEFAULT_BANCO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCuentaDestinoPago() throws Exception {
        // Get the cuentaDestinoPago
        restCuentaDestinoPagoMockMvc.perform(get("/api/cuentaDestinoPagos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCuentaDestinoPago() throws Exception {
        // Initialize the database
        cuentaDestinoPagoRepository.saveAndFlush(cuentaDestinoPago);

		int databaseSizeBeforeUpdate = cuentaDestinoPagoRepository.findAll().size();

        // Update the cuentaDestinoPago
        cuentaDestinoPago.setConvenio(UPDATED_CONVENIO);
        cuentaDestinoPago.setCuentaClabe(UPDATED_CUENTA_CLABE);
        cuentaDestinoPago.setReferencia(UPDATED_REFERENCIA);
        cuentaDestinoPago.setBanco(UPDATED_BANCO);
        restCuentaDestinoPagoMockMvc.perform(put("/api/cuentaDestinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cuentaDestinoPago)))
                .andExpect(status().isOk());

        // Validate the CuentaDestinoPago in the database
        List<CuentaDestinoPago> cuentaDestinoPagos = cuentaDestinoPagoRepository.findAll();
        assertThat(cuentaDestinoPagos).hasSize(databaseSizeBeforeUpdate);
        CuentaDestinoPago testCuentaDestinoPago = cuentaDestinoPagos.get(cuentaDestinoPagos.size() - 1);
        assertThat(testCuentaDestinoPago.getConvenio()).isEqualTo(UPDATED_CONVENIO);
        assertThat(testCuentaDestinoPago.getCuentaClabe()).isEqualTo(UPDATED_CUENTA_CLABE);
        assertThat(testCuentaDestinoPago.getReferencia()).isEqualTo(UPDATED_REFERENCIA);
        assertThat(testCuentaDestinoPago.getBanco()).isEqualTo(UPDATED_BANCO);
    }

    @Test
    @Transactional
    public void deleteCuentaDestinoPago() throws Exception {
        // Initialize the database
        cuentaDestinoPagoRepository.saveAndFlush(cuentaDestinoPago);

		int databaseSizeBeforeDelete = cuentaDestinoPagoRepository.findAll().size();

        // Get the cuentaDestinoPago
        restCuentaDestinoPagoMockMvc.perform(delete("/api/cuentaDestinoPagos/{id}", cuentaDestinoPago.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CuentaDestinoPago> cuentaDestinoPagos = cuentaDestinoPagoRepository.findAll();
        assertThat(cuentaDestinoPagos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

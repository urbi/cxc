package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Empresa;
import com.kalitron.cxc.repository.EmpresaRepository;
import com.kalitron.cxc.repository.search.EmpresaSearchRepository;
import com.kalitron.cxc.web.rest.dto.EmpresaDTO;
import com.kalitron.cxc.web.rest.mapper.EmpresaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the EmpresaResource REST controller.
 *
 * @see EmpresaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EmpresaResourceTest {

    private static final String DEFAULT_NOMBRE = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRE = "UPDATED_TEXT";
    private static final String DEFAULT_RFC = "SAMPLE_TEXT";
    private static final String UPDATED_RFC = "UPDATED_TEXT";
    private static final String DEFAULT_TELEFONO = "SAMPLE_TEXT";
    private static final String UPDATED_TELEFONO = "UPDATED_TEXT";
    private static final String DEFAULT_CORREO = "SAMPLE_TEXT";
    private static final String UPDATED_CORREO = "UPDATED_TEXT";
    private static final String DEFAULT_DIRECCION = "SAMPLE_TEXT";
    private static final String UPDATED_DIRECCION = "UPDATED_TEXT";
    private static final String DEFAULT_CODIGO_POSTAL = "SAMPLE_TEXT";
    private static final String UPDATED_CODIGO_POSTAL = "UPDATED_TEXT";
    private static final String DEFAULT_CIUDAD = "SAMPLE_TEXT";
    private static final String UPDATED_CIUDAD = "UPDATED_TEXT";
    private static final String DEFAULT_MUNICIPIO = "SAMPLE_TEXT";
    private static final String UPDATED_MUNICIPIO = "UPDATED_TEXT";
    private static final String DEFAULT_ESTADO = "SAMPLE_TEXT";
    private static final String UPDATED_ESTADO = "UPDATED_TEXT";

    @Inject
    private EmpresaRepository empresaRepository;

    @Inject
    private EmpresaMapper empresaMapper;

    @Inject
    private EmpresaSearchRepository empresaSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restEmpresaMockMvc;

    private Empresa empresa;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EmpresaResource empresaResource = new EmpresaResource();
        ReflectionTestUtils.setField(empresaResource, "empresaRepository", empresaRepository);
        ReflectionTestUtils.setField(empresaResource, "empresaMapper", empresaMapper);
        ReflectionTestUtils.setField(empresaResource, "empresaSearchRepository", empresaSearchRepository);
        this.restEmpresaMockMvc = MockMvcBuilders.standaloneSetup(empresaResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        empresa = new Empresa();
        empresa.setNombre(DEFAULT_NOMBRE);
        empresa.setRfc(DEFAULT_RFC);
        empresa.setTelefono(DEFAULT_TELEFONO);
        empresa.setCorreo(DEFAULT_CORREO);
        empresa.setDireccion(DEFAULT_DIRECCION);
        empresa.setCodigoPostal(DEFAULT_CODIGO_POSTAL);
        empresa.setCiudad(DEFAULT_CIUDAD);
        empresa.setMunicipio(DEFAULT_MUNICIPIO);
        empresa.setEstado(DEFAULT_ESTADO);
    }

    @Test
    @Transactional
    public void createEmpresa() throws Exception {
        int databaseSizeBeforeCreate = empresaRepository.findAll().size();

        // Create the Empresa
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isCreated());

        // Validate the Empresa in the database
        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeCreate + 1);
        Empresa testEmpresa = empresas.get(empresas.size() - 1);
        assertThat(testEmpresa.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testEmpresa.getRfc()).isEqualTo(DEFAULT_RFC);
        assertThat(testEmpresa.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
        assertThat(testEmpresa.getCorreo()).isEqualTo(DEFAULT_CORREO);
        assertThat(testEmpresa.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testEmpresa.getCodigoPostal()).isEqualTo(DEFAULT_CODIGO_POSTAL);
        assertThat(testEmpresa.getCiudad()).isEqualTo(DEFAULT_CIUDAD);
        assertThat(testEmpresa.getMunicipio()).isEqualTo(DEFAULT_MUNICIPIO);
        assertThat(testEmpresa.getEstado()).isEqualTo(DEFAULT_ESTADO);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = empresaRepository.findAll().size();
        // set the field null
        empresa.setNombre(null);

        // Create the Empresa, which fails.
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isBadRequest());

        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRfcIsRequired() throws Exception {
        int databaseSizeBeforeTest = empresaRepository.findAll().size();
        // set the field null
        empresa.setRfc(null);

        // Create the Empresa, which fails.
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isBadRequest());

        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDireccionIsRequired() throws Exception {
        int databaseSizeBeforeTest = empresaRepository.findAll().size();
        // set the field null
        empresa.setDireccion(null);

        // Create the Empresa, which fails.
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isBadRequest());

        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodigoPostalIsRequired() throws Exception {
        int databaseSizeBeforeTest = empresaRepository.findAll().size();
        // set the field null
        empresa.setCodigoPostal(null);

        // Create the Empresa, which fails.
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isBadRequest());

        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMunicipioIsRequired() throws Exception {
        int databaseSizeBeforeTest = empresaRepository.findAll().size();
        // set the field null
        empresa.setMunicipio(null);

        // Create the Empresa, which fails.
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isBadRequest());

        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = empresaRepository.findAll().size();
        // set the field null
        empresa.setEstado(null);

        // Create the Empresa, which fails.
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(post("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isBadRequest());

        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEmpresas() throws Exception {
        // Initialize the database
        empresaRepository.saveAndFlush(empresa);

        // Get all the empresas
        restEmpresaMockMvc.perform(get("/api/empresas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(empresa.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
                .andExpect(jsonPath("$.[*].rfc").value(hasItem(DEFAULT_RFC.toString())))
                .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO.toString())))
                .andExpect(jsonPath("$.[*].correo").value(hasItem(DEFAULT_CORREO.toString())))
                .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION.toString())))
                .andExpect(jsonPath("$.[*].codigoPostal").value(hasItem(DEFAULT_CODIGO_POSTAL.toString())))
                .andExpect(jsonPath("$.[*].ciudad").value(hasItem(DEFAULT_CIUDAD.toString())))
                .andExpect(jsonPath("$.[*].municipio").value(hasItem(DEFAULT_MUNICIPIO.toString())))
                .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.toString())));
    }

    @Test
    @Transactional
    public void getEmpresa() throws Exception {
        // Initialize the database
        empresaRepository.saveAndFlush(empresa);

        // Get the empresa
        restEmpresaMockMvc.perform(get("/api/empresas/{id}", empresa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(empresa.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.rfc").value(DEFAULT_RFC.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO.toString()))
            .andExpect(jsonPath("$.correo").value(DEFAULT_CORREO.toString()))
            .andExpect(jsonPath("$.direccion").value(DEFAULT_DIRECCION.toString()))
            .andExpect(jsonPath("$.codigoPostal").value(DEFAULT_CODIGO_POSTAL.toString()))
            .andExpect(jsonPath("$.ciudad").value(DEFAULT_CIUDAD.toString()))
            .andExpect(jsonPath("$.municipio").value(DEFAULT_MUNICIPIO.toString()))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEmpresa() throws Exception {
        // Get the empresa
        restEmpresaMockMvc.perform(get("/api/empresas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmpresa() throws Exception {
        // Initialize the database
        empresaRepository.saveAndFlush(empresa);

		int databaseSizeBeforeUpdate = empresaRepository.findAll().size();

        // Update the empresa
        empresa.setNombre(UPDATED_NOMBRE);
        empresa.setRfc(UPDATED_RFC);
        empresa.setTelefono(UPDATED_TELEFONO);
        empresa.setCorreo(UPDATED_CORREO);
        empresa.setDireccion(UPDATED_DIRECCION);
        empresa.setCodigoPostal(UPDATED_CODIGO_POSTAL);
        empresa.setCiudad(UPDATED_CIUDAD);
        empresa.setMunicipio(UPDATED_MUNICIPIO);
        empresa.setEstado(UPDATED_ESTADO);
        
        EmpresaDTO empresaDTO = empresaMapper.empresaToEmpresaDTO(empresa);

        restEmpresaMockMvc.perform(put("/api/empresas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDTO)))
                .andExpect(status().isOk());

        // Validate the Empresa in the database
        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeUpdate);
        Empresa testEmpresa = empresas.get(empresas.size() - 1);
        assertThat(testEmpresa.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testEmpresa.getRfc()).isEqualTo(UPDATED_RFC);
        assertThat(testEmpresa.getTelefono()).isEqualTo(UPDATED_TELEFONO);
        assertThat(testEmpresa.getCorreo()).isEqualTo(UPDATED_CORREO);
        assertThat(testEmpresa.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testEmpresa.getCodigoPostal()).isEqualTo(UPDATED_CODIGO_POSTAL);
        assertThat(testEmpresa.getCiudad()).isEqualTo(UPDATED_CIUDAD);
        assertThat(testEmpresa.getMunicipio()).isEqualTo(UPDATED_MUNICIPIO);
        assertThat(testEmpresa.getEstado()).isEqualTo(UPDATED_ESTADO);
    }

    @Test
    @Transactional
    public void deleteEmpresa() throws Exception {
        // Initialize the database
        empresaRepository.saveAndFlush(empresa);

		int databaseSizeBeforeDelete = empresaRepository.findAll().size();

        // Get the empresa
        restEmpresaMockMvc.perform(delete("/api/empresas/{id}", empresa.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Empresa> empresas = empresaRepository.findAll();
        assertThat(empresas).hasSize(databaseSizeBeforeDelete - 1);
    }
}

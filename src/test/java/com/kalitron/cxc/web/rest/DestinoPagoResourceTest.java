package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.DestinoPago;
import com.kalitron.cxc.repository.DestinoPagoRepository;
import com.kalitron.cxc.repository.search.DestinoPagoSearchRepository;
import com.kalitron.cxc.web.rest.dto.DestinoPagoDTO;
import com.kalitron.cxc.web.rest.mapper.DestinoPagoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the DestinoPagoResource REST controller.
 *
 * @see DestinoPagoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class DestinoPagoResourceTest {

    private static final String DEFAULT_NOMBRE = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRE = "UPDATED_TEXT";

    @Inject
    private DestinoPagoRepository destinoPagoRepository;

    @Inject
    private DestinoPagoMapper destinoPagoMapper;

    @Inject
    private DestinoPagoSearchRepository destinoPagoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restDestinoPagoMockMvc;

    private DestinoPago destinoPago;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DestinoPagoResource destinoPagoResource = new DestinoPagoResource();
        ReflectionTestUtils.setField(destinoPagoResource, "destinoPagoRepository", destinoPagoRepository);
        ReflectionTestUtils.setField(destinoPagoResource, "destinoPagoMapper", destinoPagoMapper);
        ReflectionTestUtils.setField(destinoPagoResource, "destinoPagoSearchRepository", destinoPagoSearchRepository);
        this.restDestinoPagoMockMvc = MockMvcBuilders.standaloneSetup(destinoPagoResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        destinoPago = new DestinoPago();
        destinoPago.setNombre(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createDestinoPago() throws Exception {
        int databaseSizeBeforeCreate = destinoPagoRepository.findAll().size();

        // Create the DestinoPago
        DestinoPagoDTO destinoPagoDTO = destinoPagoMapper.destinoPagoToDestinoPagoDTO(destinoPago);

        restDestinoPagoMockMvc.perform(post("/api/destinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(destinoPagoDTO)))
                .andExpect(status().isCreated());

        // Validate the DestinoPago in the database
        List<DestinoPago> destinoPagos = destinoPagoRepository.findAll();
        assertThat(destinoPagos).hasSize(databaseSizeBeforeCreate + 1);
        DestinoPago testDestinoPago = destinoPagos.get(destinoPagos.size() - 1);
        assertThat(testDestinoPago.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = destinoPagoRepository.findAll().size();
        // set the field null
        destinoPago.setNombre(null);

        // Create the DestinoPago, which fails.
        DestinoPagoDTO destinoPagoDTO = destinoPagoMapper.destinoPagoToDestinoPagoDTO(destinoPago);

        restDestinoPagoMockMvc.perform(post("/api/destinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(destinoPagoDTO)))
                .andExpect(status().isBadRequest());

        List<DestinoPago> destinoPagos = destinoPagoRepository.findAll();
        assertThat(destinoPagos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDestinoPagos() throws Exception {
        // Initialize the database
        destinoPagoRepository.saveAndFlush(destinoPago);

        // Get all the destinoPagos
        restDestinoPagoMockMvc.perform(get("/api/destinoPagos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(destinoPago.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }

    @Test
    @Transactional
    public void getDestinoPago() throws Exception {
        // Initialize the database
        destinoPagoRepository.saveAndFlush(destinoPago);

        // Get the destinoPago
        restDestinoPagoMockMvc.perform(get("/api/destinoPagos/{id}", destinoPago.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(destinoPago.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDestinoPago() throws Exception {
        // Get the destinoPago
        restDestinoPagoMockMvc.perform(get("/api/destinoPagos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDestinoPago() throws Exception {
        // Initialize the database
        destinoPagoRepository.saveAndFlush(destinoPago);

		int databaseSizeBeforeUpdate = destinoPagoRepository.findAll().size();

        // Update the destinoPago
        destinoPago.setNombre(UPDATED_NOMBRE);
        
        DestinoPagoDTO destinoPagoDTO = destinoPagoMapper.destinoPagoToDestinoPagoDTO(destinoPago);

        restDestinoPagoMockMvc.perform(put("/api/destinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(destinoPagoDTO)))
                .andExpect(status().isOk());

        // Validate the DestinoPago in the database
        List<DestinoPago> destinoPagos = destinoPagoRepository.findAll();
        assertThat(destinoPagos).hasSize(databaseSizeBeforeUpdate);
        DestinoPago testDestinoPago = destinoPagos.get(destinoPagos.size() - 1);
        assertThat(testDestinoPago.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void deleteDestinoPago() throws Exception {
        // Initialize the database
        destinoPagoRepository.saveAndFlush(destinoPago);

		int databaseSizeBeforeDelete = destinoPagoRepository.findAll().size();

        // Get the destinoPago
        restDestinoPagoMockMvc.perform(delete("/api/destinoPagos/{id}", destinoPago.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<DestinoPago> destinoPagos = destinoPagoRepository.findAll();
        assertThat(destinoPagos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

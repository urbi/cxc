package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.EmpresaDestinoPago;
import com.kalitron.cxc.repository.EmpresaDestinoPagoRepository;
import com.kalitron.cxc.repository.search.EmpresaDestinoPagoSearchRepository;
import com.kalitron.cxc.web.rest.mapper.EmpresaDestinoPagoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the EmpresaDestinoPagoResource REST controller.
 *
 * @see EmpresaDestinoPagoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EmpresaDestinoPagoResourceTest {

    private static final String DEFAULT_GENERADOR_REFERENCIA = "SAMPLE_TEXT";
    private static final String UPDATED_GENERADOR_REFERENCIA = "UPDATED_TEXT";

    @Inject
    private EmpresaDestinoPagoRepository empresaDestinoPagoRepository;

    @Inject
    private EmpresaDestinoPagoMapper empresaDestinoPagoMapper;

    @Inject
    private EmpresaDestinoPagoSearchRepository empresaDestinoPagoSearchRepository;

    private MockMvc restEmpresaDestinoPagoMockMvc;

    private EmpresaDestinoPago empresaDestinoPago;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EmpresaDestinoPagoResource empresaDestinoPagoResource = new EmpresaDestinoPagoResource();
        ReflectionTestUtils.setField(empresaDestinoPagoResource, "empresaDestinoPagoRepository", empresaDestinoPagoRepository);
        ReflectionTestUtils.setField(empresaDestinoPagoResource, "empresaDestinoPagoMapper", empresaDestinoPagoMapper);
        ReflectionTestUtils.setField(empresaDestinoPagoResource, "empresaDestinoPagoSearchRepository", empresaDestinoPagoSearchRepository);
        this.restEmpresaDestinoPagoMockMvc = MockMvcBuilders.standaloneSetup(empresaDestinoPagoResource).build();
    }

    @Before
    public void initTest() {
        empresaDestinoPago = new EmpresaDestinoPago();
        empresaDestinoPago.setGeneradorReferencia(DEFAULT_GENERADOR_REFERENCIA);
    }

    @Test
    @Transactional
    public void createEmpresaDestinoPago() throws Exception {
        int databaseSizeBeforeCreate = empresaDestinoPagoRepository.findAll().size();

        // Create the EmpresaDestinoPago
        restEmpresaDestinoPagoMockMvc.perform(post("/api/empresaDestinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDestinoPago)))
                .andExpect(status().isCreated());

        // Validate the EmpresaDestinoPago in the database
        List<EmpresaDestinoPago> empresaDestinoPagos = empresaDestinoPagoRepository.findAll();
        assertThat(empresaDestinoPagos).hasSize(databaseSizeBeforeCreate + 1);
        EmpresaDestinoPago testEmpresaDestinoPago = empresaDestinoPagos.get(empresaDestinoPagos.size() - 1);
        assertThat(testEmpresaDestinoPago.getGeneradorReferencia()).isEqualTo(DEFAULT_GENERADOR_REFERENCIA);
    }

    @Test
    @Transactional
    public void getAllEmpresaDestinoPagos() throws Exception {
        // Initialize the database
        empresaDestinoPagoRepository.saveAndFlush(empresaDestinoPago);

        // Get all the empresaDestinoPagos
        restEmpresaDestinoPagoMockMvc.perform(get("/api/empresaDestinoPagos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(empresaDestinoPago.getId().intValue())))
                .andExpect(jsonPath("$.[*].generadorReferencia").value(hasItem(DEFAULT_GENERADOR_REFERENCIA.toString())));
    }

    @Test
    @Transactional
    public void getEmpresaDestinoPago() throws Exception {
        // Initialize the database
        empresaDestinoPagoRepository.saveAndFlush(empresaDestinoPago);

        // Get the empresaDestinoPago
        restEmpresaDestinoPagoMockMvc.perform(get("/api/empresaDestinoPagos/{id}", empresaDestinoPago.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(empresaDestinoPago.getId().intValue()))
            .andExpect(jsonPath("$.generadorReferencia").value(DEFAULT_GENERADOR_REFERENCIA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEmpresaDestinoPago() throws Exception {
        // Get the empresaDestinoPago
        restEmpresaDestinoPagoMockMvc.perform(get("/api/empresaDestinoPagos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEmpresaDestinoPago() throws Exception {
        // Initialize the database
        empresaDestinoPagoRepository.saveAndFlush(empresaDestinoPago);

		int databaseSizeBeforeUpdate = empresaDestinoPagoRepository.findAll().size();

        // Update the empresaDestinoPago
        empresaDestinoPago.setGeneradorReferencia(UPDATED_GENERADOR_REFERENCIA);
        restEmpresaDestinoPagoMockMvc.perform(put("/api/empresaDestinoPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(empresaDestinoPago)))
                .andExpect(status().isOk());

        // Validate the EmpresaDestinoPago in the database
        List<EmpresaDestinoPago> empresaDestinoPagos = empresaDestinoPagoRepository.findAll();
        assertThat(empresaDestinoPagos).hasSize(databaseSizeBeforeUpdate);
        EmpresaDestinoPago testEmpresaDestinoPago = empresaDestinoPagos.get(empresaDestinoPagos.size() - 1);
        assertThat(testEmpresaDestinoPago.getGeneradorReferencia()).isEqualTo(UPDATED_GENERADOR_REFERENCIA);
    }

    @Test
    @Transactional
    public void deleteEmpresaDestinoPago() throws Exception {
        // Initialize the database
        empresaDestinoPagoRepository.saveAndFlush(empresaDestinoPago);

		int databaseSizeBeforeDelete = empresaDestinoPagoRepository.findAll().size();

        // Get the empresaDestinoPago
        restEmpresaDestinoPagoMockMvc.perform(delete("/api/empresaDestinoPagos/{id}", empresaDestinoPago.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<EmpresaDestinoPago> empresaDestinoPagos = empresaDestinoPagoRepository.findAll();
        assertThat(empresaDestinoPagos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

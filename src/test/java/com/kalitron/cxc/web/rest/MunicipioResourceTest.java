package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Municipio;
import com.kalitron.cxc.repository.MunicipioRepository;
import com.kalitron.cxc.repository.search.MunicipioSearchRepository;
import com.kalitron.cxc.web.rest.mapper.MunicipioMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the MunicipioResource REST controller.
 *
 * @see MunicipioResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class MunicipioResourceTest {

    private static final String DEFAULT_NOMBRE = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRE = "UPDATED_TEXT";

    @Inject
    private MunicipioRepository municipioRepository;

    @Inject
    private MunicipioMapper municipioMapper;

    @Inject
    private MunicipioSearchRepository municipioSearchRepository;

    private MockMvc restMunicipioMockMvc;

    private Municipio municipio;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MunicipioResource municipioResource = new MunicipioResource();
        ReflectionTestUtils.setField(municipioResource, "municipioRepository", municipioRepository);
        ReflectionTestUtils.setField(municipioResource, "municipioMapper", municipioMapper);
        ReflectionTestUtils.setField(municipioResource, "municipioSearchRepository", municipioSearchRepository);
        this.restMunicipioMockMvc = MockMvcBuilders.standaloneSetup(municipioResource).build();
    }

    @Before
    public void initTest() {
        municipio = new Municipio();
        municipio.setNombre(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void createMunicipio() throws Exception {
        int databaseSizeBeforeCreate = municipioRepository.findAll().size();

        // Create the Municipio
        restMunicipioMockMvc.perform(post("/api/municipios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(municipio)))
                .andExpect(status().isCreated());

        // Validate the Municipio in the database
        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeCreate + 1);
        Municipio testMunicipio = municipios.get(municipios.size() - 1);
        assertThat(testMunicipio.getNombre()).isEqualTo(DEFAULT_NOMBRE);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = municipioRepository.findAll().size();
        // set the field null
        municipio.setNombre(null);

        // Create the Municipio, which fails.
        restMunicipioMockMvc.perform(post("/api/municipios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(municipio)))
                .andExpect(status().isBadRequest());

        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMunicipios() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);

        // Get all the municipios
        restMunicipioMockMvc.perform(get("/api/municipios"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(municipio.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())));
    }

    @Test
    @Transactional
    public void getMunicipio() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);

        // Get the municipio
        restMunicipioMockMvc.perform(get("/api/municipios/{id}", municipio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(municipio.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMunicipio() throws Exception {
        // Get the municipio
        restMunicipioMockMvc.perform(get("/api/municipios/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMunicipio() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);

		int databaseSizeBeforeUpdate = municipioRepository.findAll().size();

        // Update the municipio
        municipio.setNombre(UPDATED_NOMBRE);
        restMunicipioMockMvc.perform(put("/api/municipios")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(municipio)))
                .andExpect(status().isOk());

        // Validate the Municipio in the database
        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeUpdate);
        Municipio testMunicipio = municipios.get(municipios.size() - 1);
        assertThat(testMunicipio.getNombre()).isEqualTo(UPDATED_NOMBRE);
    }

    @Test
    @Transactional
    public void deleteMunicipio() throws Exception {
        // Initialize the database
        municipioRepository.saveAndFlush(municipio);

		int databaseSizeBeforeDelete = municipioRepository.findAll().size();

        // Get the municipio
        restMunicipioMockMvc.perform(delete("/api/municipios/{id}", municipio.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Municipio> municipios = municipioRepository.findAll();
        assertThat(municipios).hasSize(databaseSizeBeforeDelete - 1);
    }
}

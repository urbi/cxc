package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.CargaInicial;
import com.kalitron.cxc.repository.CargaInicialRepository;
import com.kalitron.cxc.repository.search.CargaInicialSearchRepository;
import com.kalitron.cxc.web.rest.dto.CargaInicialDTO;
import com.kalitron.cxc.web.rest.mapper.CargaInicialMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.TipoCargaType;

/**
 * Test class for the CargaInicialResource REST controller.
 *
 * @see CargaInicialResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CargaInicialResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final TipoCargaType DEFAULT_TIPO_CARGA = TipoCargaType.CLIENTE;
    private static final TipoCargaType UPDATED_TIPO_CARGA = TipoCargaType.CUENTA;

    private static final byte[] DEFAULT_ARCHIVO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_ARCHIVO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_MENSAJE = "SAMPLE_TEXT";
    private static final String UPDATED_MENSAJE = "UPDATED_TEXT";

    private static final DateTime DEFAULT_FECHA_CARGA = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_FECHA_CARGA = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_FECHA_CARGA_STR = dateTimeFormatter.print(DEFAULT_FECHA_CARGA);

    @Inject
    private CargaInicialRepository cargaInicialRepository;

    @Inject
    private CargaInicialMapper cargaInicialMapper;

    @Inject
    private CargaInicialSearchRepository cargaInicialSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restCargaInicialMockMvc;

    private CargaInicial cargaInicial;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CargaInicialResource cargaInicialResource = new CargaInicialResource();
        ReflectionTestUtils.setField(cargaInicialResource, "cargaInicialRepository", cargaInicialRepository);
        ReflectionTestUtils.setField(cargaInicialResource, "cargaInicialMapper", cargaInicialMapper);
        ReflectionTestUtils.setField(cargaInicialResource, "cargaInicialSearchRepository", cargaInicialSearchRepository);
        this.restCargaInicialMockMvc = MockMvcBuilders.standaloneSetup(cargaInicialResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        cargaInicial = new CargaInicial();
        cargaInicial.setTipoCarga(DEFAULT_TIPO_CARGA);
        cargaInicial.setArchivo(DEFAULT_ARCHIVO);
        cargaInicial.setMensaje(DEFAULT_MENSAJE);
        cargaInicial.setFechaCarga(DEFAULT_FECHA_CARGA);
    }

    @Test
    @Transactional
    public void createCargaInicial() throws Exception {
        int databaseSizeBeforeCreate = cargaInicialRepository.findAll().size();

        // Create the CargaInicial
        CargaInicialDTO cargaInicialDTO = cargaInicialMapper.cargaInicialToCargaInicialDTO(cargaInicial);

        restCargaInicialMockMvc.perform(post("/api/cargaInicials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cargaInicialDTO)))
                .andExpect(status().isCreated());

        // Validate the CargaInicial in the database
        List<CargaInicial> cargaInicials = cargaInicialRepository.findAll();
        assertThat(cargaInicials).hasSize(databaseSizeBeforeCreate + 1);
        CargaInicial testCargaInicial = cargaInicials.get(cargaInicials.size() - 1);
        assertThat(testCargaInicial.getTipoCarga()).isEqualTo(DEFAULT_TIPO_CARGA);
        assertThat(testCargaInicial.getArchivo()).isEqualTo(DEFAULT_ARCHIVO);
        assertThat(testCargaInicial.getMensaje()).isEqualTo(DEFAULT_MENSAJE);
        assertThat(testCargaInicial.getFechaCarga().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_FECHA_CARGA);
    }

    @Test
    @Transactional
    public void checkTipoCargaIsRequired() throws Exception {
        int databaseSizeBeforeTest = cargaInicialRepository.findAll().size();
        // set the field null
        cargaInicial.setTipoCarga(null);

        // Create the CargaInicial, which fails.
        CargaInicialDTO cargaInicialDTO = cargaInicialMapper.cargaInicialToCargaInicialDTO(cargaInicial);

        restCargaInicialMockMvc.perform(post("/api/cargaInicials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cargaInicialDTO)))
                .andExpect(status().isBadRequest());

        List<CargaInicial> cargaInicials = cargaInicialRepository.findAll();
        assertThat(cargaInicials).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCargaInicials() throws Exception {
        // Initialize the database
        cargaInicialRepository.saveAndFlush(cargaInicial);

        // Get all the cargaInicials
        restCargaInicialMockMvc.perform(get("/api/cargaInicials"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cargaInicial.getId().intValue())))
                .andExpect(jsonPath("$.[*].tipoCarga").value(hasItem(DEFAULT_TIPO_CARGA.toString())))
                .andExpect(jsonPath("$.[*].archivo").value(hasItem(Base64Utils.encodeToString(DEFAULT_ARCHIVO))))
                .andExpect(jsonPath("$.[*].mensaje").value(hasItem(DEFAULT_MENSAJE.toString())))
                .andExpect(jsonPath("$.[*].fechaCarga").value(hasItem(DEFAULT_FECHA_CARGA_STR)));
    }

    @Test
    @Transactional
    public void getCargaInicial() throws Exception {
        // Initialize the database
        cargaInicialRepository.saveAndFlush(cargaInicial);

        // Get the cargaInicial
        restCargaInicialMockMvc.perform(get("/api/cargaInicials/{id}", cargaInicial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cargaInicial.getId().intValue()))
            .andExpect(jsonPath("$.tipoCarga").value(DEFAULT_TIPO_CARGA.toString()))
            .andExpect(jsonPath("$.archivo").value(Base64Utils.encodeToString(DEFAULT_ARCHIVO)))
            .andExpect(jsonPath("$.mensaje").value(DEFAULT_MENSAJE.toString()))
            .andExpect(jsonPath("$.fechaCarga").value(DEFAULT_FECHA_CARGA_STR));
    }

    @Test
    @Transactional
    public void getNonExistingCargaInicial() throws Exception {
        // Get the cargaInicial
        restCargaInicialMockMvc.perform(get("/api/cargaInicials/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCargaInicial() throws Exception {
        // Initialize the database
        cargaInicialRepository.saveAndFlush(cargaInicial);

		int databaseSizeBeforeUpdate = cargaInicialRepository.findAll().size();

        // Update the cargaInicial
        cargaInicial.setTipoCarga(UPDATED_TIPO_CARGA);
        cargaInicial.setArchivo(UPDATED_ARCHIVO);
        cargaInicial.setMensaje(UPDATED_MENSAJE);
        cargaInicial.setFechaCarga(UPDATED_FECHA_CARGA);
        
        CargaInicialDTO cargaInicialDTO = cargaInicialMapper.cargaInicialToCargaInicialDTO(cargaInicial);

        restCargaInicialMockMvc.perform(put("/api/cargaInicials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cargaInicialDTO)))
                .andExpect(status().isOk());

        // Validate the CargaInicial in the database
        List<CargaInicial> cargaInicials = cargaInicialRepository.findAll();
        assertThat(cargaInicials).hasSize(databaseSizeBeforeUpdate);
        CargaInicial testCargaInicial = cargaInicials.get(cargaInicials.size() - 1);
        assertThat(testCargaInicial.getTipoCarga()).isEqualTo(UPDATED_TIPO_CARGA);
        assertThat(testCargaInicial.getArchivo()).isEqualTo(UPDATED_ARCHIVO);
        assertThat(testCargaInicial.getMensaje()).isEqualTo(UPDATED_MENSAJE);
        assertThat(testCargaInicial.getFechaCarga().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_FECHA_CARGA);
    }

    @Test
    @Transactional
    public void deleteCargaInicial() throws Exception {
        // Initialize the database
        cargaInicialRepository.saveAndFlush(cargaInicial);

		int databaseSizeBeforeDelete = cargaInicialRepository.findAll().size();

        // Get the cargaInicial
        restCargaInicialMockMvc.perform(delete("/api/cargaInicials/{id}", cargaInicial.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CargaInicial> cargaInicials = cargaInicialRepository.findAll();
        assertThat(cargaInicials).hasSize(databaseSizeBeforeDelete - 1);
    }
}

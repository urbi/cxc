package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Documento;
import com.kalitron.cxc.repository.DocumentoRepository;
import com.kalitron.cxc.repository.search.DocumentoSearchRepository;
import com.kalitron.cxc.web.rest.dto.DocumentoDTO;
import com.kalitron.cxc.web.rest.mapper.DocumentoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.TipoDocumentoType;

/**
 * Test class for the DocumentoResource REST controller.
 *
 * @see DocumentoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class DocumentoResourceTest {

    private static final String DEFAULT_NUMERO_DOCUMENTO = "SAMPLE_TEXT";
    private static final String UPDATED_NUMERO_DOCUMENTO = "UPDATED_TEXT";
    private static final String DEFAULT_DESCRIPCION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPCION = "UPDATED_TEXT";

    private static final TipoDocumentoType DEFAULT_TIPO_DOCUMENTO = TipoDocumentoType.ENGANCHE;
    private static final TipoDocumentoType UPDATED_TIPO_DOCUMENTO = TipoDocumentoType.ESCRITURA;
    private static final String DEFAULT_ATTRIBUTE1 = "SAMPLE_TEXT";
    private static final String UPDATED_ATTRIBUTE1 = "UPDATED_TEXT";

    @Inject
    private DocumentoRepository documentoRepository;

    @Inject
    private DocumentoMapper documentoMapper;

    @Inject
    private DocumentoSearchRepository documentoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restDocumentoMockMvc;

    private Documento documento;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DocumentoResource documentoResource = new DocumentoResource();
        ReflectionTestUtils.setField(documentoResource, "documentoRepository", documentoRepository);
        ReflectionTestUtils.setField(documentoResource, "documentoMapper", documentoMapper);
        ReflectionTestUtils.setField(documentoResource, "documentoSearchRepository", documentoSearchRepository);
        this.restDocumentoMockMvc = MockMvcBuilders.standaloneSetup(documentoResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        documento = new Documento();
        documento.setNumeroDocumento(DEFAULT_NUMERO_DOCUMENTO);
        documento.setDescripcion(DEFAULT_DESCRIPCION);
        documento.setTipoDocumento(DEFAULT_TIPO_DOCUMENTO);
        documento.setAttribute1(DEFAULT_ATTRIBUTE1);
    }

    @Test
    @Transactional
    public void createDocumento() throws Exception {
        int databaseSizeBeforeCreate = documentoRepository.findAll().size();

        // Create the Documento
        DocumentoDTO documentoDTO = documentoMapper.documentoToDocumentoDTO(documento);

        restDocumentoMockMvc.perform(post("/api/documentos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
                .andExpect(status().isCreated());

        // Validate the Documento in the database
        List<Documento> documentos = documentoRepository.findAll();
        assertThat(documentos).hasSize(databaseSizeBeforeCreate + 1);
        Documento testDocumento = documentos.get(documentos.size() - 1);
        assertThat(testDocumento.getNumeroDocumento()).isEqualTo(DEFAULT_NUMERO_DOCUMENTO);
        assertThat(testDocumento.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testDocumento.getTipoDocumento()).isEqualTo(DEFAULT_TIPO_DOCUMENTO);
        assertThat(testDocumento.getAttribute1()).isEqualTo(DEFAULT_ATTRIBUTE1);
    }

    @Test
    @Transactional
    public void getAllDocumentos() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

        // Get all the documentos
        restDocumentoMockMvc.perform(get("/api/documentos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(documento.getId().intValue())))
                .andExpect(jsonPath("$.[*].numeroDocumento").value(hasItem(DEFAULT_NUMERO_DOCUMENTO.toString())))
                .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
                .andExpect(jsonPath("$.[*].tipoDocumento").value(hasItem(DEFAULT_TIPO_DOCUMENTO.toString())))
                .andExpect(jsonPath("$.[*].attribute1").value(hasItem(DEFAULT_ATTRIBUTE1.toString())));
    }

    @Test
    @Transactional
    public void getDocumento() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

        // Get the documento
        restDocumentoMockMvc.perform(get("/api/documentos/{id}", documento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(documento.getId().intValue()))
            .andExpect(jsonPath("$.numeroDocumento").value(DEFAULT_NUMERO_DOCUMENTO.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.tipoDocumento").value(DEFAULT_TIPO_DOCUMENTO.toString()))
            .andExpect(jsonPath("$.attribute1").value(DEFAULT_ATTRIBUTE1.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDocumento() throws Exception {
        // Get the documento
        restDocumentoMockMvc.perform(get("/api/documentos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDocumento() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

		int databaseSizeBeforeUpdate = documentoRepository.findAll().size();

        // Update the documento
        documento.setNumeroDocumento(UPDATED_NUMERO_DOCUMENTO);
        documento.setDescripcion(UPDATED_DESCRIPCION);
        documento.setTipoDocumento(UPDATED_TIPO_DOCUMENTO);
        documento.setAttribute1(UPDATED_ATTRIBUTE1);
        
        DocumentoDTO documentoDTO = documentoMapper.documentoToDocumentoDTO(documento);

        restDocumentoMockMvc.perform(put("/api/documentos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(documentoDTO)))
                .andExpect(status().isOk());

        // Validate the Documento in the database
        List<Documento> documentos = documentoRepository.findAll();
        assertThat(documentos).hasSize(databaseSizeBeforeUpdate);
        Documento testDocumento = documentos.get(documentos.size() - 1);
        assertThat(testDocumento.getNumeroDocumento()).isEqualTo(UPDATED_NUMERO_DOCUMENTO);
        assertThat(testDocumento.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testDocumento.getTipoDocumento()).isEqualTo(UPDATED_TIPO_DOCUMENTO);
        assertThat(testDocumento.getAttribute1()).isEqualTo(UPDATED_ATTRIBUTE1);
    }

    @Test
    @Transactional
    public void deleteDocumento() throws Exception {
        // Initialize the database
        documentoRepository.saveAndFlush(documento);

		int databaseSizeBeforeDelete = documentoRepository.findAll().size();

        // Get the documento
        restDocumentoMockMvc.perform(delete("/api/documentos/{id}", documento.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Documento> documentos = documentoRepository.findAll();
        assertThat(documentos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

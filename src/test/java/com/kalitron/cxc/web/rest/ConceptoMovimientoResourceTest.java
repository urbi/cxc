package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.ConceptoMovimiento;
import com.kalitron.cxc.repository.ConceptoMovimientoRepository;
import com.kalitron.cxc.repository.search.ConceptoMovimientoSearchRepository;
import com.kalitron.cxc.web.rest.dto.ConceptoMovimientoDTO;
import com.kalitron.cxc.web.rest.mapper.ConceptoMovimientoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;

/**
 * Test class for the ConceptoMovimientoResource REST controller.
 *
 * @see ConceptoMovimientoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ConceptoMovimientoResourceTest {


    private static final ConceptoMovimientoType DEFAULT_NOMBRE = ConceptoMovimientoType.APARTADO_INICIAL;
    private static final ConceptoMovimientoType UPDATED_NOMBRE = ConceptoMovimientoType.PAGARE;

    private static final TipoMovimientoType DEFAULT_TIPO_MOVIMIENTO = TipoMovimientoType.CARGO;
    private static final TipoMovimientoType UPDATED_TIPO_MOVIMIENTO = TipoMovimientoType.ABONO;

    @Inject
    private ConceptoMovimientoRepository conceptoMovimientoRepository;

    @Inject
    private ConceptoMovimientoMapper conceptoMovimientoMapper;

    @Inject
    private ConceptoMovimientoSearchRepository conceptoMovimientoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restConceptoMovimientoMockMvc;

    private ConceptoMovimiento conceptoMovimiento;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ConceptoMovimientoResource conceptoMovimientoResource = new ConceptoMovimientoResource();
        ReflectionTestUtils.setField(conceptoMovimientoResource, "conceptoMovimientoRepository", conceptoMovimientoRepository);
        ReflectionTestUtils.setField(conceptoMovimientoResource, "conceptoMovimientoMapper", conceptoMovimientoMapper);
        ReflectionTestUtils.setField(conceptoMovimientoResource, "conceptoMovimientoSearchRepository", conceptoMovimientoSearchRepository);
        this.restConceptoMovimientoMockMvc = MockMvcBuilders.standaloneSetup(conceptoMovimientoResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        conceptoMovimiento = new ConceptoMovimiento();
        conceptoMovimiento.setNombre(DEFAULT_NOMBRE);
        conceptoMovimiento.setTipoMovimiento(DEFAULT_TIPO_MOVIMIENTO);
    }

    @Test
    @Transactional
    public void createConceptoMovimiento() throws Exception {
        int databaseSizeBeforeCreate = conceptoMovimientoRepository.findAll().size();

        // Create the ConceptoMovimiento
        ConceptoMovimientoDTO conceptoMovimientoDTO = conceptoMovimientoMapper.conceptoMovimientoToConceptoMovimientoDTO(conceptoMovimiento);

        restConceptoMovimientoMockMvc.perform(post("/api/conceptoMovimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(conceptoMovimientoDTO)))
                .andExpect(status().isCreated());

        // Validate the ConceptoMovimiento in the database
        List<ConceptoMovimiento> conceptoMovimientos = conceptoMovimientoRepository.findAll();
        assertThat(conceptoMovimientos).hasSize(databaseSizeBeforeCreate + 1);
        ConceptoMovimiento testConceptoMovimiento = conceptoMovimientos.get(conceptoMovimientos.size() - 1);
        assertThat(testConceptoMovimiento.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testConceptoMovimiento.getTipoMovimiento()).isEqualTo(DEFAULT_TIPO_MOVIMIENTO);
    }

    @Test
    @Transactional
    public void getAllConceptoMovimientos() throws Exception {
        // Initialize the database
        conceptoMovimientoRepository.saveAndFlush(conceptoMovimiento);

        // Get all the conceptoMovimientos
        restConceptoMovimientoMockMvc.perform(get("/api/conceptoMovimientos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(conceptoMovimiento.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
                .andExpect(jsonPath("$.[*].tipoMovimiento").value(hasItem(DEFAULT_TIPO_MOVIMIENTO.toString())));
    }

    @Test
    @Transactional
    public void getConceptoMovimiento() throws Exception {
        // Initialize the database
        conceptoMovimientoRepository.saveAndFlush(conceptoMovimiento);

        // Get the conceptoMovimiento
        restConceptoMovimientoMockMvc.perform(get("/api/conceptoMovimientos/{id}", conceptoMovimiento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(conceptoMovimiento.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.tipoMovimiento").value(DEFAULT_TIPO_MOVIMIENTO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingConceptoMovimiento() throws Exception {
        // Get the conceptoMovimiento
        restConceptoMovimientoMockMvc.perform(get("/api/conceptoMovimientos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConceptoMovimiento() throws Exception {
        // Initialize the database
        conceptoMovimientoRepository.saveAndFlush(conceptoMovimiento);

		int databaseSizeBeforeUpdate = conceptoMovimientoRepository.findAll().size();

        // Update the conceptoMovimiento
        conceptoMovimiento.setNombre(UPDATED_NOMBRE);
        conceptoMovimiento.setTipoMovimiento(UPDATED_TIPO_MOVIMIENTO);
        
        ConceptoMovimientoDTO conceptoMovimientoDTO = conceptoMovimientoMapper.conceptoMovimientoToConceptoMovimientoDTO(conceptoMovimiento);

        restConceptoMovimientoMockMvc.perform(put("/api/conceptoMovimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(conceptoMovimientoDTO)))
                .andExpect(status().isOk());

        // Validate the ConceptoMovimiento in the database
        List<ConceptoMovimiento> conceptoMovimientos = conceptoMovimientoRepository.findAll();
        assertThat(conceptoMovimientos).hasSize(databaseSizeBeforeUpdate);
        ConceptoMovimiento testConceptoMovimiento = conceptoMovimientos.get(conceptoMovimientos.size() - 1);
        assertThat(testConceptoMovimiento.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testConceptoMovimiento.getTipoMovimiento()).isEqualTo(UPDATED_TIPO_MOVIMIENTO);
    }

    @Test
    @Transactional
    public void deleteConceptoMovimiento() throws Exception {
        // Initialize the database
        conceptoMovimientoRepository.saveAndFlush(conceptoMovimiento);

		int databaseSizeBeforeDelete = conceptoMovimientoRepository.findAll().size();

        // Get the conceptoMovimiento
        restConceptoMovimientoMockMvc.perform(delete("/api/conceptoMovimientos/{id}", conceptoMovimiento.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ConceptoMovimiento> conceptoMovimientos = conceptoMovimientoRepository.findAll();
        assertThat(conceptoMovimientos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.CxcSecuencia;
import com.kalitron.cxc.repository.CxcSecuenciaRepository;
import com.kalitron.cxc.repository.search.CxcSecuenciaSearchRepository;
import com.kalitron.cxc.web.rest.dto.CxcSecuenciaDTO;
import com.kalitron.cxc.web.rest.mapper.CxcSecuenciaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.SecuenciaType;

/**
 * Test class for the CxcSecuenciaResource REST controller.
 *
 * @see CxcSecuenciaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CxcSecuenciaResourceTest {


    private static final SecuenciaType DEFAULT_NOMBRE = SecuenciaType.CLIENTE;
    private static final SecuenciaType UPDATED_NOMBRE = SecuenciaType.CUENTA_RENTA;

    private static final Long DEFAULT_SECUENCIA = 0L;
    private static final Long UPDATED_SECUENCIA = 1L;

    @Inject
    private CxcSecuenciaRepository cxcSecuenciaRepository;

    @Inject
    private CxcSecuenciaMapper cxcSecuenciaMapper;

    @Inject
    private CxcSecuenciaSearchRepository cxcSecuenciaSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restCxcSecuenciaMockMvc;

    private CxcSecuencia cxcSecuencia;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CxcSecuenciaResource cxcSecuenciaResource = new CxcSecuenciaResource();
        ReflectionTestUtils.setField(cxcSecuenciaResource, "cxcSecuenciaRepository", cxcSecuenciaRepository);
        ReflectionTestUtils.setField(cxcSecuenciaResource, "cxcSecuenciaMapper", cxcSecuenciaMapper);
        ReflectionTestUtils.setField(cxcSecuenciaResource, "cxcSecuenciaSearchRepository", cxcSecuenciaSearchRepository);
        this.restCxcSecuenciaMockMvc = MockMvcBuilders.standaloneSetup(cxcSecuenciaResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        cxcSecuencia = new CxcSecuencia();
        cxcSecuencia.setNombre(DEFAULT_NOMBRE);
        cxcSecuencia.setSecuencia(DEFAULT_SECUENCIA);
    }

    @Test
    @Transactional
    public void createCxcSecuencia() throws Exception {
        int databaseSizeBeforeCreate = cxcSecuenciaRepository.findAll().size();

        // Create the CxcSecuencia
        CxcSecuenciaDTO cxcSecuenciaDTO = cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(cxcSecuencia);

        restCxcSecuenciaMockMvc.perform(post("/api/cxcSecuencias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cxcSecuenciaDTO)))
                .andExpect(status().isCreated());

        // Validate the CxcSecuencia in the database
        List<CxcSecuencia> cxcSecuencias = cxcSecuenciaRepository.findAll();
        assertThat(cxcSecuencias).hasSize(databaseSizeBeforeCreate + 1);
        CxcSecuencia testCxcSecuencia = cxcSecuencias.get(cxcSecuencias.size() - 1);
        assertThat(testCxcSecuencia.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testCxcSecuencia.getSecuencia()).isEqualTo(DEFAULT_SECUENCIA);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = cxcSecuenciaRepository.findAll().size();
        // set the field null
        cxcSecuencia.setNombre(null);

        // Create the CxcSecuencia, which fails.
        CxcSecuenciaDTO cxcSecuenciaDTO = cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(cxcSecuencia);

        restCxcSecuenciaMockMvc.perform(post("/api/cxcSecuencias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cxcSecuenciaDTO)))
                .andExpect(status().isBadRequest());

        List<CxcSecuencia> cxcSecuencias = cxcSecuenciaRepository.findAll();
        assertThat(cxcSecuencias).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSecuenciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = cxcSecuenciaRepository.findAll().size();
        // set the field null
        cxcSecuencia.setSecuencia(null);

        // Create the CxcSecuencia, which fails.
        CxcSecuenciaDTO cxcSecuenciaDTO = cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(cxcSecuencia);

        restCxcSecuenciaMockMvc.perform(post("/api/cxcSecuencias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cxcSecuenciaDTO)))
                .andExpect(status().isBadRequest());

        List<CxcSecuencia> cxcSecuencias = cxcSecuenciaRepository.findAll();
        assertThat(cxcSecuencias).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCxcSecuencias() throws Exception {
        // Initialize the database
        cxcSecuenciaRepository.saveAndFlush(cxcSecuencia);

        // Get all the cxcSecuencias
        restCxcSecuenciaMockMvc.perform(get("/api/cxcSecuencias"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cxcSecuencia.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
                .andExpect(jsonPath("$.[*].secuencia").value(hasItem(DEFAULT_SECUENCIA.intValue())));
    }

    @Test
    @Transactional
    public void getCxcSecuencia() throws Exception {
        // Initialize the database
        cxcSecuenciaRepository.saveAndFlush(cxcSecuencia);

        // Get the cxcSecuencia
        restCxcSecuenciaMockMvc.perform(get("/api/cxcSecuencias/{id}", cxcSecuencia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cxcSecuencia.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.secuencia").value(DEFAULT_SECUENCIA.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCxcSecuencia() throws Exception {
        // Get the cxcSecuencia
        restCxcSecuenciaMockMvc.perform(get("/api/cxcSecuencias/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCxcSecuencia() throws Exception {
        // Initialize the database
        cxcSecuenciaRepository.saveAndFlush(cxcSecuencia);

		int databaseSizeBeforeUpdate = cxcSecuenciaRepository.findAll().size();

        // Update the cxcSecuencia
        cxcSecuencia.setNombre(UPDATED_NOMBRE);
        cxcSecuencia.setSecuencia(UPDATED_SECUENCIA);

        CxcSecuenciaDTO cxcSecuenciaDTO = cxcSecuenciaMapper.cxcSecuenciaToCxcSecuenciaDTO(cxcSecuencia);

        restCxcSecuenciaMockMvc.perform(put("/api/cxcSecuencias")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cxcSecuenciaDTO)))
                .andExpect(status().isOk());

        // Validate the CxcSecuencia in the database
        List<CxcSecuencia> cxcSecuencias = cxcSecuenciaRepository.findAll();
        assertThat(cxcSecuencias).hasSize(databaseSizeBeforeUpdate);
        CxcSecuencia testCxcSecuencia = cxcSecuencias.get(cxcSecuencias.size() - 1);
        assertThat(testCxcSecuencia.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testCxcSecuencia.getSecuencia()).isEqualTo(UPDATED_SECUENCIA);
    }

    @Test
    @Transactional
    public void deleteCxcSecuencia() throws Exception {
        // Initialize the database
        cxcSecuenciaRepository.saveAndFlush(cxcSecuencia);

		int databaseSizeBeforeDelete = cxcSecuenciaRepository.findAll().size();

        // Get the cxcSecuencia
        restCxcSecuenciaMockMvc.perform(delete("/api/cxcSecuencias/{id}", cxcSecuencia.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CxcSecuencia> cxcSecuencias = cxcSecuenciaRepository.findAll();
        assertThat(cxcSecuencias).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Nota;
import com.kalitron.cxc.repository.NotaRepository;
import com.kalitron.cxc.repository.search.NotaSearchRepository;
import com.kalitron.cxc.web.rest.dto.NotaDTO;
import com.kalitron.cxc.web.rest.mapper.NotaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the NotaResource REST controller.
 *
 * @see NotaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class NotaResourceTest {

    private static final String DEFAULT_TITULO = "SAMPLE_TEXT";
    private static final String UPDATED_TITULO = "UPDATED_TEXT";
    private static final String DEFAULT_DESCRIPCION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPCION = "UPDATED_TEXT";

    private static final LocalDate DEFAULT_FECHA_CREACION = new LocalDate(0L);
    private static final LocalDate UPDATED_FECHA_CREACION = new LocalDate();

    @Inject
    private NotaRepository notaRepository;

    @Inject
    private NotaMapper notaMapper;

    @Inject
    private NotaSearchRepository notaSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restNotaMockMvc;

    private Nota nota;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NotaResource notaResource = new NotaResource();
        ReflectionTestUtils.setField(notaResource, "notaRepository", notaRepository);
        ReflectionTestUtils.setField(notaResource, "notaMapper", notaMapper);
        ReflectionTestUtils.setField(notaResource, "notaSearchRepository", notaSearchRepository);
        this.restNotaMockMvc = MockMvcBuilders.standaloneSetup(notaResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        nota = new Nota();
        nota.setTitulo(DEFAULT_TITULO);
        nota.setDescripcion(DEFAULT_DESCRIPCION);
        nota.setFechaCreacion(DEFAULT_FECHA_CREACION);
    }

    @Test
    @Transactional
    public void createNota() throws Exception {
        int databaseSizeBeforeCreate = notaRepository.findAll().size();

        // Create the Nota
        NotaDTO notaDTO = notaMapper.notaToNotaDTO(nota);

        restNotaMockMvc.perform(post("/api/notas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notaDTO)))
                .andExpect(status().isCreated());

        // Validate the Nota in the database
        List<Nota> notas = notaRepository.findAll();
        assertThat(notas).hasSize(databaseSizeBeforeCreate + 1);
        Nota testNota = notas.get(notas.size() - 1);
        assertThat(testNota.getTitulo()).isEqualTo(DEFAULT_TITULO);
        assertThat(testNota.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testNota.getFechaCreacion()).isEqualTo(DEFAULT_FECHA_CREACION);
    }

    @Test
    @Transactional
    public void checkTituloIsRequired() throws Exception {
        int databaseSizeBeforeTest = notaRepository.findAll().size();
        // set the field null
        nota.setTitulo(null);

        // Create the Nota, which fails.
        NotaDTO notaDTO = notaMapper.notaToNotaDTO(nota);

        restNotaMockMvc.perform(post("/api/notas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notaDTO)))
                .andExpect(status().isBadRequest());

        List<Nota> notas = notaRepository.findAll();
        assertThat(notas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotas() throws Exception {
        // Initialize the database
        notaRepository.saveAndFlush(nota);

        // Get all the notas
        restNotaMockMvc.perform(get("/api/notas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(nota.getId().intValue())))
                .andExpect(jsonPath("$.[*].titulo").value(hasItem(DEFAULT_TITULO.toString())))
                .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
                .andExpect(jsonPath("$.[*].fechaCreacion").value(hasItem(DEFAULT_FECHA_CREACION.toString())));
    }

    @Test
    @Transactional
    public void getNota() throws Exception {
        // Initialize the database
        notaRepository.saveAndFlush(nota);

        // Get the nota
        restNotaMockMvc.perform(get("/api/notas/{id}", nota.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(nota.getId().intValue()))
            .andExpect(jsonPath("$.titulo").value(DEFAULT_TITULO.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.fechaCreacion").value(DEFAULT_FECHA_CREACION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNota() throws Exception {
        // Get the nota
        restNotaMockMvc.perform(get("/api/notas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNota() throws Exception {
        // Initialize the database
        notaRepository.saveAndFlush(nota);

		int databaseSizeBeforeUpdate = notaRepository.findAll().size();

        // Update the nota
        nota.setTitulo(UPDATED_TITULO);
        nota.setDescripcion(UPDATED_DESCRIPCION);
        nota.setFechaCreacion(UPDATED_FECHA_CREACION);
        
        NotaDTO notaDTO = notaMapper.notaToNotaDTO(nota);

        restNotaMockMvc.perform(put("/api/notas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notaDTO)))
                .andExpect(status().isOk());

        // Validate the Nota in the database
        List<Nota> notas = notaRepository.findAll();
        assertThat(notas).hasSize(databaseSizeBeforeUpdate);
        Nota testNota = notas.get(notas.size() - 1);
        assertThat(testNota.getTitulo()).isEqualTo(UPDATED_TITULO);
        assertThat(testNota.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testNota.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
    }

    @Test
    @Transactional
    public void deleteNota() throws Exception {
        // Initialize the database
        notaRepository.saveAndFlush(nota);

		int databaseSizeBeforeDelete = notaRepository.findAll().size();

        // Get the nota
        restNotaMockMvc.perform(delete("/api/notas/{id}", nota.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Nota> notas = notaRepository.findAll();
        assertThat(notas).hasSize(databaseSizeBeforeDelete - 1);
    }
}

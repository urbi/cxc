package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Caja;
import com.kalitron.cxc.repository.CajaRepository;
import com.kalitron.cxc.repository.search.CajaSearchRepository;
import com.kalitron.cxc.web.rest.mapper.CajaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CajaResource REST controller.
 *
 * @see CajaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CajaResourceTest {

    private static final String DEFAULT_NOMBRE = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRE = "UPDATED_TEXT";
    private static final String DEFAULT_DESCRIPCION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPCION = "UPDATED_TEXT";
    private static final String DEFAULT_UBICACION = "SAMPLE_TEXT";
    private static final String UPDATED_UBICACION = "UPDATED_TEXT";

    @Inject
    private CajaRepository cajaRepository;

    @Inject
    private CajaMapper cajaMapper;

    @Inject
    private CajaSearchRepository cajaSearchRepository;

    private MockMvc restCajaMockMvc;

    private Caja caja;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CajaResource cajaResource = new CajaResource();
        ReflectionTestUtils.setField(cajaResource, "cajaRepository", cajaRepository);
        ReflectionTestUtils.setField(cajaResource, "cajaMapper", cajaMapper);
        ReflectionTestUtils.setField(cajaResource, "cajaSearchRepository", cajaSearchRepository);
        this.restCajaMockMvc = MockMvcBuilders.standaloneSetup(cajaResource).build();
    }

    @Before
    public void initTest() {
        caja = new Caja();
        caja.setNombre(DEFAULT_NOMBRE);
        caja.setDescripcion(DEFAULT_DESCRIPCION);
        caja.setUbicacion(DEFAULT_UBICACION);
    }

    @Test
    @Transactional
    public void createCaja() throws Exception {
        int databaseSizeBeforeCreate = cajaRepository.findAll().size();

        // Create the Caja
        restCajaMockMvc.perform(post("/api/cajas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(caja)))
                .andExpect(status().isCreated());

        // Validate the Caja in the database
        List<Caja> cajas = cajaRepository.findAll();
        assertThat(cajas).hasSize(databaseSizeBeforeCreate + 1);
        Caja testCaja = cajas.get(cajas.size() - 1);
        assertThat(testCaja.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testCaja.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testCaja.getUbicacion()).isEqualTo(DEFAULT_UBICACION);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = cajaRepository.findAll().size();
        // set the field null
        caja.setNombre(null);

        // Create the Caja, which fails.
        restCajaMockMvc.perform(post("/api/cajas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(caja)))
                .andExpect(status().isBadRequest());

        List<Caja> cajas = cajaRepository.findAll();
        assertThat(cajas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCajas() throws Exception {
        // Initialize the database
        cajaRepository.saveAndFlush(caja);

        // Get all the cajas
        restCajaMockMvc.perform(get("/api/cajas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(caja.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
                .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
                .andExpect(jsonPath("$.[*].ubicacion").value(hasItem(DEFAULT_UBICACION.toString())));
    }

    @Test
    @Transactional
    public void getCaja() throws Exception {
        // Initialize the database
        cajaRepository.saveAndFlush(caja);

        // Get the caja
        restCajaMockMvc.perform(get("/api/cajas/{id}", caja.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(caja.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.ubicacion").value(DEFAULT_UBICACION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCaja() throws Exception {
        // Get the caja
        restCajaMockMvc.perform(get("/api/cajas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCaja() throws Exception {
        // Initialize the database
        cajaRepository.saveAndFlush(caja);

		int databaseSizeBeforeUpdate = cajaRepository.findAll().size();

        // Update the caja
        caja.setNombre(UPDATED_NOMBRE);
        caja.setDescripcion(UPDATED_DESCRIPCION);
        caja.setUbicacion(UPDATED_UBICACION);
        restCajaMockMvc.perform(put("/api/cajas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(caja)))
                .andExpect(status().isOk());

        // Validate the Caja in the database
        List<Caja> cajas = cajaRepository.findAll();
        assertThat(cajas).hasSize(databaseSizeBeforeUpdate);
        Caja testCaja = cajas.get(cajas.size() - 1);
        assertThat(testCaja.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testCaja.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testCaja.getUbicacion()).isEqualTo(UPDATED_UBICACION);
    }

    @Test
    @Transactional
    public void deleteCaja() throws Exception {
        // Initialize the database
        cajaRepository.saveAndFlush(caja);

		int databaseSizeBeforeDelete = cajaRepository.findAll().size();

        // Get the caja
        restCajaMockMvc.perform(delete("/api/cajas/{id}", caja.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Caja> cajas = cajaRepository.findAll();
        assertThat(cajas).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Plaza;
import com.kalitron.cxc.repository.PlazaRepository;
import com.kalitron.cxc.repository.search.PlazaSearchRepository;
import com.kalitron.cxc.web.rest.mapper.PlazaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.RegionType;

/**
 * Test class for the PlazaResource REST controller.
 *
 * @see PlazaResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PlazaResourceTest {

    private static final String DEFAULT_NOMBRE = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRE = "UPDATED_TEXT";
    private static final String DEFAULT_DIRECCION = "SAMPLE_TEXT";
    private static final String UPDATED_DIRECCION = "UPDATED_TEXT";
    private static final String DEFAULT_CODIGO_POSTAL = "SAMPLE_TEXT";
    private static final String UPDATED_CODIGO_POSTAL = "UPDATED_TEXT";
    private static final String DEFAULT_CIUDAD = "SAMPLE_TEXT";
    private static final String UPDATED_CIUDAD = "UPDATED_TEXT";
    private static final String DEFAULT_MUNICIPIO = "SAMPLE_TEXT";
    private static final String UPDATED_MUNICIPIO = "UPDATED_TEXT";
    private static final String DEFAULT_ESTADO = "SAMPLE_TEXT";
    private static final String UPDATED_ESTADO = "UPDATED_TEXT";

    private static final RegionType DEFAULT_REGION = RegionType.PACIFICO;
    private static final RegionType UPDATED_REGION = RegionType.NORTE;

    @Inject
    private PlazaRepository plazaRepository;

    @Inject
    private PlazaMapper plazaMapper;

    @Inject
    private PlazaSearchRepository plazaSearchRepository;

    private MockMvc restPlazaMockMvc;

    private Plaza plaza;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PlazaResource plazaResource = new PlazaResource();
        ReflectionTestUtils.setField(plazaResource, "plazaRepository", plazaRepository);
        ReflectionTestUtils.setField(plazaResource, "plazaMapper", plazaMapper);
        ReflectionTestUtils.setField(plazaResource, "plazaSearchRepository", plazaSearchRepository);
        this.restPlazaMockMvc = MockMvcBuilders.standaloneSetup(plazaResource).build();
    }

    @Before
    public void initTest() {
        plaza = new Plaza();
        plaza.setNombre(DEFAULT_NOMBRE);
        plaza.setDireccion(DEFAULT_DIRECCION);
        plaza.setCodigoPostal(DEFAULT_CODIGO_POSTAL);
        plaza.setCiudad(DEFAULT_CIUDAD);
        plaza.setMunicipio(DEFAULT_MUNICIPIO);
        plaza.setEstado(DEFAULT_ESTADO);
        plaza.setRegion(DEFAULT_REGION);
    }

    @Test
    @Transactional
    public void createPlaza() throws Exception {
        int databaseSizeBeforeCreate = plazaRepository.findAll().size();

        // Create the Plaza
        restPlazaMockMvc.perform(post("/api/plazas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(plaza)))
                .andExpect(status().isCreated());

        // Validate the Plaza in the database
        List<Plaza> plazas = plazaRepository.findAll();
        assertThat(plazas).hasSize(databaseSizeBeforeCreate + 1);
        Plaza testPlaza = plazas.get(plazas.size() - 1);
        assertThat(testPlaza.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testPlaza.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testPlaza.getCodigoPostal()).isEqualTo(DEFAULT_CODIGO_POSTAL);
        assertThat(testPlaza.getCiudad()).isEqualTo(DEFAULT_CIUDAD);
        assertThat(testPlaza.getMunicipio()).isEqualTo(DEFAULT_MUNICIPIO);
        assertThat(testPlaza.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testPlaza.getRegion()).isEqualTo(DEFAULT_REGION);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = plazaRepository.findAll().size();
        // set the field null
        plaza.setNombre(null);

        // Create the Plaza, which fails.
        restPlazaMockMvc.perform(post("/api/plazas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(plaza)))
                .andExpect(status().isBadRequest());

        List<Plaza> plazas = plazaRepository.findAll();
        assertThat(plazas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlazas() throws Exception {
        // Initialize the database
        plazaRepository.saveAndFlush(plaza);

        // Get all the plazas
        restPlazaMockMvc.perform(get("/api/plazas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(plaza.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
                .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION.toString())))
                .andExpect(jsonPath("$.[*].codigoPostal").value(hasItem(DEFAULT_CODIGO_POSTAL.toString())))
                .andExpect(jsonPath("$.[*].ciudad").value(hasItem(DEFAULT_CIUDAD.toString())))
                .andExpect(jsonPath("$.[*].municipio").value(hasItem(DEFAULT_MUNICIPIO.toString())))
                .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.toString())))
                .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION.toString())));
    }

    @Test
    @Transactional
    public void getPlaza() throws Exception {
        // Initialize the database
        plazaRepository.saveAndFlush(plaza);

        // Get the plaza
        restPlazaMockMvc.perform(get("/api/plazas/{id}", plaza.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(plaza.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.direccion").value(DEFAULT_DIRECCION.toString()))
            .andExpect(jsonPath("$.codigoPostal").value(DEFAULT_CODIGO_POSTAL.toString()))
            .andExpect(jsonPath("$.ciudad").value(DEFAULT_CIUDAD.toString()))
            .andExpect(jsonPath("$.municipio").value(DEFAULT_MUNICIPIO.toString()))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.toString()))
            .andExpect(jsonPath("$.region").value(DEFAULT_REGION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlaza() throws Exception {
        // Get the plaza
        restPlazaMockMvc.perform(get("/api/plazas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlaza() throws Exception {
        // Initialize the database
        plazaRepository.saveAndFlush(plaza);

		int databaseSizeBeforeUpdate = plazaRepository.findAll().size();

        // Update the plaza
        plaza.setNombre(UPDATED_NOMBRE);
        plaza.setDireccion(UPDATED_DIRECCION);
        plaza.setCodigoPostal(UPDATED_CODIGO_POSTAL);
        plaza.setCiudad(UPDATED_CIUDAD);
        plaza.setMunicipio(UPDATED_MUNICIPIO);
        plaza.setEstado(UPDATED_ESTADO);
        plaza.setRegion(UPDATED_REGION);
        restPlazaMockMvc.perform(put("/api/plazas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(plaza)))
                .andExpect(status().isOk());

        // Validate the Plaza in the database
        List<Plaza> plazas = plazaRepository.findAll();
        assertThat(plazas).hasSize(databaseSizeBeforeUpdate);
        Plaza testPlaza = plazas.get(plazas.size() - 1);
        assertThat(testPlaza.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testPlaza.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testPlaza.getCodigoPostal()).isEqualTo(UPDATED_CODIGO_POSTAL);
        assertThat(testPlaza.getCiudad()).isEqualTo(UPDATED_CIUDAD);
        assertThat(testPlaza.getMunicipio()).isEqualTo(UPDATED_MUNICIPIO);
        assertThat(testPlaza.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testPlaza.getRegion()).isEqualTo(UPDATED_REGION);
    }

    @Test
    @Transactional
    public void deletePlaza() throws Exception {
        // Initialize the database
        plazaRepository.saveAndFlush(plaza);

		int databaseSizeBeforeDelete = plazaRepository.findAll().size();

        // Get the plaza
        restPlazaMockMvc.perform(delete("/api/plazas/{id}", plaza.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Plaza> plazas = plazaRepository.findAll();
        assertThat(plazas).hasSize(databaseSizeBeforeDelete - 1);
    }
}

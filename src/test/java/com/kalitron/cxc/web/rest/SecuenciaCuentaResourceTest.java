package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.SecuenciaCuenta;
import com.kalitron.cxc.repository.SecuenciaCuentaRepository;
import com.kalitron.cxc.repository.search.SecuenciaCuentaSearchRepository;
import com.kalitron.cxc.web.rest.mapper.SecuenciaCuentaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.TipoCuentaType;

/**
 * Test class for the SecuenciaCuentaResource REST controller.
 *
 * @see SecuenciaCuentaResource
 */
//TODO: actualizar test
//TODO: agregar test secuencia movimiento
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SecuenciaCuentaResourceTest {


    private static final TipoCuentaType DEFAULT_TIPO_CUENTA = TipoCuentaType.VENCIMIENTO_FIJO;
    private static final TipoCuentaType UPDATED_TIPO_CUENTA = TipoCuentaType.SALDO_INSOLUTO;

    private static final Long DEFAULT_SECUENCIA = 0L;
    private static final Long UPDATED_SECUENCIA = 1L;

    @Inject
    private SecuenciaCuentaRepository secuenciaCuentaRepository;

    @Inject
    private SecuenciaCuentaMapper secuenciaCuentaMapper;

    @Inject
    private SecuenciaCuentaSearchRepository secuenciaCuentaSearchRepository;

    private MockMvc restSecuenciaCuentaMockMvc;

    private SecuenciaCuenta secuenciaCuenta;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SecuenciaCuentaResource secuenciaCuentaResource = new SecuenciaCuentaResource();
        ReflectionTestUtils.setField(secuenciaCuentaResource, "secuenciaCuentaRepository", secuenciaCuentaRepository);
        ReflectionTestUtils.setField(secuenciaCuentaResource, "secuenciaCuentaMapper", secuenciaCuentaMapper);
        ReflectionTestUtils.setField(secuenciaCuentaResource, "secuenciaCuentaSearchRepository", secuenciaCuentaSearchRepository);
        this.restSecuenciaCuentaMockMvc = MockMvcBuilders.standaloneSetup(secuenciaCuentaResource).build();
    }

    @Before
    public void initTest() {
        secuenciaCuenta = new SecuenciaCuenta();
       // secuenciaCuenta.setTipoCuenta(DEFAULT_TIPO_CUENTA);
        secuenciaCuenta.setSecuencia(DEFAULT_SECUENCIA);
    }

    @Test
    @Transactional
    public void createSecuenciaCuenta() throws Exception {
        int databaseSizeBeforeCreate = secuenciaCuentaRepository.findAll().size();

        // Create the SecuenciaCuenta
        restSecuenciaCuentaMockMvc.perform(post("/api/secuenciaCuentas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(secuenciaCuenta)))
                .andExpect(status().isCreated());

        // Validate the SecuenciaCuenta in the database
        List<SecuenciaCuenta> secuenciaCuentas = secuenciaCuentaRepository.findAll();
        assertThat(secuenciaCuentas).hasSize(databaseSizeBeforeCreate + 1);
        SecuenciaCuenta testSecuenciaCuenta = secuenciaCuentas.get(secuenciaCuentas.size() - 1);
       // assertThat(testSecuenciaCuenta.getTipoCuenta()).isEqualTo(DEFAULT_TIPO_CUENTA);
        assertThat(testSecuenciaCuenta.getSecuencia()).isEqualTo(DEFAULT_SECUENCIA);
    }

    @Test
    @Transactional
    public void getAllSecuenciaCuentas() throws Exception {
        // Initialize the database
        secuenciaCuentaRepository.saveAndFlush(secuenciaCuenta);

        // Get all the secuenciaCuentas
        restSecuenciaCuentaMockMvc.perform(get("/api/secuenciaCuentas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(secuenciaCuenta.getId().intValue())))
                .andExpect(jsonPath("$.[*].tipoCuenta").value(hasItem(DEFAULT_TIPO_CUENTA.toString())))
                .andExpect(jsonPath("$.[*].secuencia").value(hasItem(DEFAULT_SECUENCIA.intValue())));
    }

    @Test
    @Transactional
    public void getSecuenciaCuenta() throws Exception {
        // Initialize the database
        secuenciaCuentaRepository.saveAndFlush(secuenciaCuenta);

        // Get the secuenciaCuenta
        restSecuenciaCuentaMockMvc.perform(get("/api/secuenciaCuentas/{id}", secuenciaCuenta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(secuenciaCuenta.getId().intValue()))
            .andExpect(jsonPath("$.tipoCuenta").value(DEFAULT_TIPO_CUENTA.toString()))
            .andExpect(jsonPath("$.secuencia").value(DEFAULT_SECUENCIA.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSecuenciaCuenta() throws Exception {
        // Get the secuenciaCuenta
        restSecuenciaCuentaMockMvc.perform(get("/api/secuenciaCuentas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSecuenciaCuenta() throws Exception {
        // Initialize the database
        secuenciaCuentaRepository.saveAndFlush(secuenciaCuenta);

		int databaseSizeBeforeUpdate = secuenciaCuentaRepository.findAll().size();

        // Update the secuenciaCuenta
        //secuenciaCuenta.setTipoCuenta(UPDATED_TIPO_CUENTA);
        secuenciaCuenta.setSecuencia(UPDATED_SECUENCIA);
        restSecuenciaCuentaMockMvc.perform(put("/api/secuenciaCuentas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(secuenciaCuenta)))
                .andExpect(status().isOk());

        // Validate the SecuenciaCuenta in the database
        List<SecuenciaCuenta> secuenciaCuentas = secuenciaCuentaRepository.findAll();
        assertThat(secuenciaCuentas).hasSize(databaseSizeBeforeUpdate);
        SecuenciaCuenta testSecuenciaCuenta = secuenciaCuentas.get(secuenciaCuentas.size() - 1);
        //assertThat(testSecuenciaCuenta.getTipoCuenta()).isEqualTo(UPDATED_TIPO_CUENTA);
        assertThat(testSecuenciaCuenta.getSecuencia()).isEqualTo(UPDATED_SECUENCIA);
    }

    @Test
    @Transactional
    public void deleteSecuenciaCuenta() throws Exception {
        // Initialize the database
        secuenciaCuentaRepository.saveAndFlush(secuenciaCuenta);

		int databaseSizeBeforeDelete = secuenciaCuentaRepository.findAll().size();

        // Get the secuenciaCuenta
        restSecuenciaCuentaMockMvc.perform(delete("/api/secuenciaCuentas/{id}", secuenciaCuenta.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SecuenciaCuenta> secuenciaCuentas = secuenciaCuentaRepository.findAll();
        assertThat(secuenciaCuentas).hasSize(databaseSizeBeforeDelete - 1);
    }
}

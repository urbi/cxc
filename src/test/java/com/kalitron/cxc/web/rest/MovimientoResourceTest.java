package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Movimiento;
import com.kalitron.cxc.repository.MovimientoRepository;
import com.kalitron.cxc.repository.search.MovimientoSearchRepository;
import com.kalitron.cxc.web.rest.mapper.MovimientoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.EstatusMovimientoType;
import com.kalitron.cxc.domain.enumeration.TipoPagoType;
import com.kalitron.cxc.domain.enumeration.TipoMovimientoType;
import com.kalitron.cxc.domain.enumeration.ConceptoMovimientoType;

/**
 * Test class for the MovimientoResource REST controller.
 *
 * @see MovimientoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class MovimientoResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final BigDecimal DEFAULT_MONTO = new BigDecimal(0);
    private static final BigDecimal UPDATED_MONTO = new BigDecimal(1);

    private static final Integer DEFAULT_NUMERO_PAGARE = 0;
    private static final Integer UPDATED_NUMERO_PAGARE = 1;

    private static final Integer DEFAULT_NUMERO_PAGO = 0;
    private static final Integer UPDATED_NUMERO_PAGO = 1;

    private static final LocalDate DEFAULT_FECHA_VENCIMIENTO = new LocalDate(0L);
    private static final LocalDate UPDATED_FECHA_VENCIMIENTO = new LocalDate();

    private static final LocalDate DEFAULT_FECHA_PAGO = new LocalDate(0L);
    private static final LocalDate UPDATED_FECHA_PAGO = new LocalDate();

    private static final EstatusMovimientoType DEFAULT_ESTATUS_MOVIMIENTO = EstatusMovimientoType.CORRIENTE;
    private static final EstatusMovimientoType UPDATED_ESTATUS_MOVIMIENTO = EstatusMovimientoType.PAGADO;

    private static final Long DEFAULT_SECUENCIA_MOVIMIENTO = 0L;
    private static final Long UPDATED_SECUENCIA_MOVIMIENTO = 1L;

    private static final TipoPagoType DEFAULT_TIPO_PAGO = TipoPagoType.CHEQUE;
    private static final TipoPagoType UPDATED_TIPO_PAGO = TipoPagoType.DEPOSITO_EFECTIVO;

    private static final TipoMovimientoType DEFAULT_TIPO_MOVIMIENTO = TipoMovimientoType.CARGO;
    private static final TipoMovimientoType UPDATED_TIPO_MOVIMIENTO = TipoMovimientoType.ABONO;

    private static final ConceptoMovimientoType DEFAULT_CONCEPTO_MOVIMIENTO = ConceptoMovimientoType.APARTADO_INICIAL;
    private static final ConceptoMovimientoType UPDATED_CONCEPTO_MOVIMIENTO = ConceptoMovimientoType.PAGARE;

    private static final DateTime DEFAULT_CREATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATED_DATE);

    private static final DateTime DEFAULT_LAST_MODIFIED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_LAST_MODIFIED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_LAST_MODIFIED_DATE_STR = dateTimeFormatter.print(DEFAULT_LAST_MODIFIED_DATE);

    @Inject
    private MovimientoRepository movimientoRepository;

    @Inject
    private MovimientoMapper movimientoMapper;

    @Inject
    private MovimientoSearchRepository movimientoSearchRepository;

    private MockMvc restMovimientoMockMvc;

    private Movimiento movimiento;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MovimientoResource movimientoResource = new MovimientoResource();
        ReflectionTestUtils.setField(movimientoResource, "movimientoRepository", movimientoRepository);
        ReflectionTestUtils.setField(movimientoResource, "movimientoMapper", movimientoMapper);
        ReflectionTestUtils.setField(movimientoResource, "movimientoSearchRepository", movimientoSearchRepository);
        this.restMovimientoMockMvc = MockMvcBuilders.standaloneSetup(movimientoResource).build();
    }

    @Before
    public void initTest() {
        movimiento = new Movimiento();
        movimiento.setMonto(DEFAULT_MONTO);
        movimiento.setNumeroPagare(DEFAULT_NUMERO_PAGARE);
        movimiento.setNumeroPago(DEFAULT_NUMERO_PAGO);
        movimiento.setFechaVencimiento(DEFAULT_FECHA_VENCIMIENTO);
        movimiento.setFechaPago(DEFAULT_FECHA_PAGO);
        movimiento.setEstatusMovimiento(DEFAULT_ESTATUS_MOVIMIENTO);
        movimiento.setSecuenciaMovimiento(DEFAULT_SECUENCIA_MOVIMIENTO);
        movimiento.setTipoPago(DEFAULT_TIPO_PAGO);
        movimiento.setTipoMovimiento(DEFAULT_TIPO_MOVIMIENTO);
        movimiento.setConceptoMovimiento(DEFAULT_CONCEPTO_MOVIMIENTO);
        movimiento.setCreatedDate(DEFAULT_CREATED_DATE);
        movimiento.setLastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createMovimiento() throws Exception {
        int databaseSizeBeforeCreate = movimientoRepository.findAll().size();

        // Create the Movimiento
        restMovimientoMockMvc.perform(post("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isCreated());

        // Validate the Movimiento in the database
        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeCreate + 1);
        Movimiento testMovimiento = movimientos.get(movimientos.size() - 1);
        assertThat(testMovimiento.getMonto()).isEqualTo(DEFAULT_MONTO);
        assertThat(testMovimiento.getNumeroPagare()).isEqualTo(DEFAULT_NUMERO_PAGARE);
        assertThat(testMovimiento.getNumeroPago()).isEqualTo(DEFAULT_NUMERO_PAGO);
        assertThat(testMovimiento.getFechaVencimiento()).isEqualTo(DEFAULT_FECHA_VENCIMIENTO);
        assertThat(testMovimiento.getFechaPago()).isEqualTo(DEFAULT_FECHA_PAGO);
        assertThat(testMovimiento.getEstatusMovimiento()).isEqualTo(DEFAULT_ESTATUS_MOVIMIENTO);
        assertThat(testMovimiento.getSecuenciaMovimiento()).isEqualTo(DEFAULT_SECUENCIA_MOVIMIENTO);
        assertThat(testMovimiento.getTipoPago()).isEqualTo(DEFAULT_TIPO_PAGO);
        assertThat(testMovimiento.getTipoMovimiento()).isEqualTo(DEFAULT_TIPO_MOVIMIENTO);
        assertThat(testMovimiento.getConceptoMovimiento()).isEqualTo(DEFAULT_CONCEPTO_MOVIMIENTO);
        assertThat(testMovimiento.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMovimiento.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void checkMontoIsRequired() throws Exception {
        int databaseSizeBeforeTest = movimientoRepository.findAll().size();
        // set the field null
        movimiento.setMonto(null);

        // Create the Movimiento, which fails.
        restMovimientoMockMvc.perform(post("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isBadRequest());

        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumeroPagareIsRequired() throws Exception {
        int databaseSizeBeforeTest = movimientoRepository.findAll().size();
        // set the field null
        movimiento.setNumeroPagare(null);

        // Create the Movimiento, which fails.
        restMovimientoMockMvc.perform(post("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isBadRequest());

        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNumeroPagoIsRequired() throws Exception {
        int databaseSizeBeforeTest = movimientoRepository.findAll().size();
        // set the field null
        movimiento.setNumeroPago(null);

        // Create the Movimiento, which fails.
        restMovimientoMockMvc.perform(post("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isBadRequest());

        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaVencimientoIsRequired() throws Exception {
        int databaseSizeBeforeTest = movimientoRepository.findAll().size();
        // set the field null
        movimiento.setFechaVencimiento(null);

        // Create the Movimiento, which fails.
        restMovimientoMockMvc.perform(post("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isBadRequest());

        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFechaPagoIsRequired() throws Exception {
        int databaseSizeBeforeTest = movimientoRepository.findAll().size();
        // set the field null
        movimiento.setFechaPago(null);

        // Create the Movimiento, which fails.
        restMovimientoMockMvc.perform(post("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isBadRequest());

        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMovimientos() throws Exception {
        // Initialize the database
        movimientoRepository.saveAndFlush(movimiento);

        // Get all the movimientos
        restMovimientoMockMvc.perform(get("/api/movimientos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(movimiento.getId().intValue())))
                .andExpect(jsonPath("$.[*].monto").value(hasItem(DEFAULT_MONTO.intValue())))
                .andExpect(jsonPath("$.[*].numeroPagare").value(hasItem(DEFAULT_NUMERO_PAGARE)))
                .andExpect(jsonPath("$.[*].numeroPago").value(hasItem(DEFAULT_NUMERO_PAGO)))
                .andExpect(jsonPath("$.[*].fechaVencimiento").value(hasItem(DEFAULT_FECHA_VENCIMIENTO.toString())))
                .andExpect(jsonPath("$.[*].fechaPago").value(hasItem(DEFAULT_FECHA_PAGO.toString())))
                .andExpect(jsonPath("$.[*].estatusMovimiento").value(hasItem(DEFAULT_ESTATUS_MOVIMIENTO.toString())))
                .andExpect(jsonPath("$.[*].secuenciaMovimiento").value(hasItem(DEFAULT_SECUENCIA_MOVIMIENTO.intValue())))
                .andExpect(jsonPath("$.[*].tipoPago").value(hasItem(DEFAULT_TIPO_PAGO.toString())))
                .andExpect(jsonPath("$.[*].tipoMovimiento").value(hasItem(DEFAULT_TIPO_MOVIMIENTO.toString())))
                .andExpect(jsonPath("$.[*].conceptoMovimiento").value(hasItem(DEFAULT_CONCEPTO_MOVIMIENTO.toString())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getMovimiento() throws Exception {
        // Initialize the database
        movimientoRepository.saveAndFlush(movimiento);

        // Get the movimiento
        restMovimientoMockMvc.perform(get("/api/movimientos/{id}", movimiento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(movimiento.getId().intValue()))
            .andExpect(jsonPath("$.monto").value(DEFAULT_MONTO.intValue()))
            .andExpect(jsonPath("$.numeroPagare").value(DEFAULT_NUMERO_PAGARE))
            .andExpect(jsonPath("$.numeroPago").value(DEFAULT_NUMERO_PAGO))
            .andExpect(jsonPath("$.fechaVencimiento").value(DEFAULT_FECHA_VENCIMIENTO.toString()))
            .andExpect(jsonPath("$.fechaPago").value(DEFAULT_FECHA_PAGO.toString()))
            .andExpect(jsonPath("$.estatusMovimiento").value(DEFAULT_ESTATUS_MOVIMIENTO.toString()))
            .andExpect(jsonPath("$.secuenciaMovimiento").value(DEFAULT_SECUENCIA_MOVIMIENTO.intValue()))
            .andExpect(jsonPath("$.tipoPago").value(DEFAULT_TIPO_PAGO.toString()))
            .andExpect(jsonPath("$.tipoMovimiento").value(DEFAULT_TIPO_MOVIMIENTO.toString()))
            .andExpect(jsonPath("$.conceptoMovimiento").value(DEFAULT_CONCEPTO_MOVIMIENTO.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE_STR))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingMovimiento() throws Exception {
        // Get the movimiento
        restMovimientoMockMvc.perform(get("/api/movimientos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMovimiento() throws Exception {
        // Initialize the database
        movimientoRepository.saveAndFlush(movimiento);

		int databaseSizeBeforeUpdate = movimientoRepository.findAll().size();

        // Update the movimiento
        movimiento.setMonto(UPDATED_MONTO);
        movimiento.setNumeroPagare(UPDATED_NUMERO_PAGARE);
        movimiento.setNumeroPago(UPDATED_NUMERO_PAGO);
        movimiento.setFechaVencimiento(UPDATED_FECHA_VENCIMIENTO);
        movimiento.setFechaPago(UPDATED_FECHA_PAGO);
        movimiento.setEstatusMovimiento(UPDATED_ESTATUS_MOVIMIENTO);
        movimiento.setSecuenciaMovimiento(UPDATED_SECUENCIA_MOVIMIENTO);
        movimiento.setTipoPago(UPDATED_TIPO_PAGO);
        movimiento.setTipoMovimiento(UPDATED_TIPO_MOVIMIENTO);
        movimiento.setConceptoMovimiento(UPDATED_CONCEPTO_MOVIMIENTO);
        movimiento.setCreatedDate(UPDATED_CREATED_DATE);
        movimiento.setLastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        restMovimientoMockMvc.perform(put("/api/movimientos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(movimiento)))
                .andExpect(status().isOk());

        // Validate the Movimiento in the database
        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeUpdate);
        Movimiento testMovimiento = movimientos.get(movimientos.size() - 1);
        assertThat(testMovimiento.getMonto()).isEqualTo(UPDATED_MONTO);
        assertThat(testMovimiento.getNumeroPagare()).isEqualTo(UPDATED_NUMERO_PAGARE);
        assertThat(testMovimiento.getNumeroPago()).isEqualTo(UPDATED_NUMERO_PAGO);
        assertThat(testMovimiento.getFechaVencimiento()).isEqualTo(UPDATED_FECHA_VENCIMIENTO);
        assertThat(testMovimiento.getFechaPago()).isEqualTo(UPDATED_FECHA_PAGO);
        assertThat(testMovimiento.getEstatusMovimiento()).isEqualTo(UPDATED_ESTATUS_MOVIMIENTO);
        assertThat(testMovimiento.getSecuenciaMovimiento()).isEqualTo(UPDATED_SECUENCIA_MOVIMIENTO);
        assertThat(testMovimiento.getTipoPago()).isEqualTo(UPDATED_TIPO_PAGO);
        assertThat(testMovimiento.getTipoMovimiento()).isEqualTo(UPDATED_TIPO_MOVIMIENTO);
        assertThat(testMovimiento.getConceptoMovimiento()).isEqualTo(UPDATED_CONCEPTO_MOVIMIENTO);
        assertThat(testMovimiento.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMovimiento.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void deleteMovimiento() throws Exception {
        // Initialize the database
        movimientoRepository.saveAndFlush(movimiento);

		int databaseSizeBeforeDelete = movimientoRepository.findAll().size();

        // Get the movimiento
        restMovimientoMockMvc.perform(delete("/api/movimientos/{id}", movimiento.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Movimiento> movimientos = movimientoRepository.findAll();
        assertThat(movimientos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

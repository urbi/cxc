package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Cuenta;
import com.kalitron.cxc.repository.CuentaRepository;
import com.kalitron.cxc.repository.search.CuentaSearchRepository;
import com.kalitron.cxc.web.rest.mapper.CuentaMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.TipoCuentaType;
import com.kalitron.cxc.domain.enumeration.EstatusCuentaType;

/**
 * Test class for the CuentaResource REST controller.
 *
 * @see CuentaResource
 */
//TODO: actualizar test
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CuentaResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_FECHA_CREACION = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_FECHA_CREACION = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_FECHA_CREACION_STR = dateTimeFormatter.print(DEFAULT_FECHA_CREACION);

    private static final BigDecimal DEFAULT_SALDO = new BigDecimal(0);
    private static final BigDecimal UPDATED_SALDO = new BigDecimal(1);

    private static final BigDecimal DEFAULT_MONTO_EXIGIBLE = new BigDecimal(0);
    private static final BigDecimal UPDATED_MONTO_EXIGIBLE = new BigDecimal(1);

    private static final BigDecimal DEFAULT_SALDO_INICIAL = new BigDecimal(0);
    private static final BigDecimal UPDATED_SALDO_INICIAL = new BigDecimal(1);

    private static final LocalDate DEFAULT_FECHA_ULTIMO_PAGO = new LocalDate(0L);
    private static final LocalDate UPDATED_FECHA_ULTIMO_PAGO = new LocalDate();
    private static final String DEFAULT_NUMERO_CONTRATO_ORIGEN = "SAMPLE_TEXT";
    private static final String UPDATED_NUMERO_CONTRATO_ORIGEN = "UPDATED_TEXT";
    private static final String DEFAULT_DESCRIPCION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPCION = "UPDATED_TEXT";
    private static final String DEFAULT_ATTRIBUTE1 = "SAMPLE_TEXT";
    private static final String UPDATED_ATTRIBUTE1 = "UPDATED_TEXT";
    private static final String DEFAULT_ATTRIBUTE2 = "SAMPLE_TEXT";
    private static final String UPDATED_ATTRIBUTE2 = "UPDATED_TEXT";

    private static final TipoCuentaType DEFAULT_TIPO_CUENTA = TipoCuentaType.VENCIMIENTO_FIJO;
    private static final TipoCuentaType UPDATED_TIPO_CUENTA = TipoCuentaType.SALDO_INSOLUTO;

    private static final EstatusCuentaType DEFAULT_ESTATUS_CUENTA = EstatusCuentaType.ACTIVADA;
    private static final EstatusCuentaType UPDATED_ESTATUS_CUENTA = EstatusCuentaType.CANCELADA;

    private static final DateTime DEFAULT_CREATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATED_DATE);

    private static final DateTime DEFAULT_LAST_MODIFIED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_LAST_MODIFIED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_LAST_MODIFIED_DATE_STR = dateTimeFormatter.print(DEFAULT_LAST_MODIFIED_DATE);

    private static final Long DEFAULT_CREATED_BY = 0L;
    private static final Long UPDATED_CREATED_BY = 1L;

    private static final Long DEFAULT_LAST_MODIFIED_BY = 0L;
    private static final Long UPDATED_LAST_MODIFIED_BY = 1L;

    @Inject
    private CuentaRepository cuentaRepository;

    @Inject
    private CuentaMapper cuentaMapper;

    @Inject
    private CuentaSearchRepository cuentaSearchRepository;

    private MockMvc restCuentaMockMvc;

    private Cuenta cuenta;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CuentaResource cuentaResource = new CuentaResource();
        ReflectionTestUtils.setField(cuentaResource, "cuentaRepository", cuentaRepository);
        ReflectionTestUtils.setField(cuentaResource, "cuentaMapper", cuentaMapper);
        ReflectionTestUtils.setField(cuentaResource, "cuentaSearchRepository", cuentaSearchRepository);
        this.restCuentaMockMvc = MockMvcBuilders.standaloneSetup(cuentaResource).build();
    }

    @Before
    public void initTest() {
        cuenta = new Cuenta();
        //cuenta.setFechaCreacion(DEFAULT_FECHA_CREACION);
        cuenta.setSaldo(DEFAULT_SALDO);
        cuenta.setMontoExigible(DEFAULT_MONTO_EXIGIBLE);
        cuenta.setSaldoInicial(DEFAULT_SALDO_INICIAL);
        cuenta.setFechaUltimoPago(DEFAULT_FECHA_ULTIMO_PAGO);
        cuenta.setNumeroContratoOrigen(DEFAULT_NUMERO_CONTRATO_ORIGEN);
        cuenta.setDescripcion(DEFAULT_DESCRIPCION);
        cuenta.setAttribute1(DEFAULT_ATTRIBUTE1);
        cuenta.setAttribute2(DEFAULT_ATTRIBUTE2);
      //  cuenta.setTipoCuenta(DEFAULT_TIPO_CUENTA);
        cuenta.setEstatusCuenta(DEFAULT_ESTATUS_CUENTA);
        cuenta.setCreatedDate(DEFAULT_CREATED_DATE);
        cuenta.setLastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
   //     cuenta.setCreated(DEFAULT_CREATED_BY);
     //   cuenta.setLastModifiedBy(DEFAULT_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createCuenta() throws Exception {
        int databaseSizeBeforeCreate = cuentaRepository.findAll().size();

        // Create the Cuenta
        restCuentaMockMvc.perform(post("/api/cuentas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cuenta)))
                .andExpect(status().isCreated());

        // Validate the Cuenta in the database
        List<Cuenta> cuentas = cuentaRepository.findAll();
        assertThat(cuentas).hasSize(databaseSizeBeforeCreate + 1);
        Cuenta testCuenta = cuentas.get(cuentas.size() - 1);
       // assertThat(testCuenta.getFechaCreacion().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_FECHA_CREACION);
        assertThat(testCuenta.getSaldo()).isEqualTo(DEFAULT_SALDO);
        assertThat(testCuenta.getMontoExigible()).isEqualTo(DEFAULT_MONTO_EXIGIBLE);
        assertThat(testCuenta.getSaldoInicial()).isEqualTo(DEFAULT_SALDO_INICIAL);
        assertThat(testCuenta.getFechaUltimoPago()).isEqualTo(DEFAULT_FECHA_ULTIMO_PAGO);
        assertThat(testCuenta.getNumeroContratoOrigen()).isEqualTo(DEFAULT_NUMERO_CONTRATO_ORIGEN);
        assertThat(testCuenta.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testCuenta.getAttribute1()).isEqualTo(DEFAULT_ATTRIBUTE1);
        assertThat(testCuenta.getAttribute2()).isEqualTo(DEFAULT_ATTRIBUTE2);

        assertThat(testCuenta.getEstatusCuenta()).isEqualTo(DEFAULT_ESTATUS_CUENTA);
        assertThat(testCuenta.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCuenta.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        //assertThat(testCuenta.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        //assertThat(testCuenta.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void checkSaldoIsRequired() throws Exception {
        int databaseSizeBeforeTest = cuentaRepository.findAll().size();
        // set the field null
        cuenta.setSaldo(null);

        // Create the Cuenta, which fails.
        restCuentaMockMvc.perform(post("/api/cuentas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cuenta)))
                .andExpect(status().isBadRequest());

        List<Cuenta> cuentas = cuentaRepository.findAll();
        assertThat(cuentas).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCuentas() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentas
        restCuentaMockMvc.perform(get("/api/cuentas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cuenta.getId().intValue())))
                .andExpect(jsonPath("$.[*].fechaCreacion").value(hasItem(DEFAULT_FECHA_CREACION_STR)))
                .andExpect(jsonPath("$.[*].saldo").value(hasItem(DEFAULT_SALDO.intValue())))
                .andExpect(jsonPath("$.[*].montoExigible").value(hasItem(DEFAULT_MONTO_EXIGIBLE.intValue())))
                .andExpect(jsonPath("$.[*].saldoInicial").value(hasItem(DEFAULT_SALDO_INICIAL.intValue())))
                .andExpect(jsonPath("$.[*].fechaUltimoPago").value(hasItem(DEFAULT_FECHA_ULTIMO_PAGO.toString())))
                .andExpect(jsonPath("$.[*].numeroContratoOrigen").value(hasItem(DEFAULT_NUMERO_CONTRATO_ORIGEN.toString())))
                .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
                .andExpect(jsonPath("$.[*].attribute1").value(hasItem(DEFAULT_ATTRIBUTE1.toString())))
                .andExpect(jsonPath("$.[*].attribute2").value(hasItem(DEFAULT_ATTRIBUTE2.toString())))
                .andExpect(jsonPath("$.[*].tipoCuenta").value(hasItem(DEFAULT_TIPO_CUENTA.toString())))
                .andExpect(jsonPath("$.[*].estatusCuenta").value(hasItem(DEFAULT_ESTATUS_CUENTA.toString())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE_STR)))
                .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
                .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY.intValue())));
    }

    @Test
    @Transactional
    public void getCuenta() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get the cuenta
        restCuentaMockMvc.perform(get("/api/cuentas/{id}", cuenta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cuenta.getId().intValue()))
            .andExpect(jsonPath("$.fechaCreacion").value(DEFAULT_FECHA_CREACION_STR))
            .andExpect(jsonPath("$.saldo").value(DEFAULT_SALDO.intValue()))
            .andExpect(jsonPath("$.montoExigible").value(DEFAULT_MONTO_EXIGIBLE.intValue()))
            .andExpect(jsonPath("$.saldoInicial").value(DEFAULT_SALDO_INICIAL.intValue()))
            .andExpect(jsonPath("$.fechaUltimoPago").value(DEFAULT_FECHA_ULTIMO_PAGO.toString()))
            .andExpect(jsonPath("$.numeroContratoOrigen").value(DEFAULT_NUMERO_CONTRATO_ORIGEN.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.attribute1").value(DEFAULT_ATTRIBUTE1.toString()))
            .andExpect(jsonPath("$.attribute2").value(DEFAULT_ATTRIBUTE2.toString()))
            .andExpect(jsonPath("$.tipoCuenta").value(DEFAULT_TIPO_CUENTA.toString()))
            .andExpect(jsonPath("$.estatusCuenta").value(DEFAULT_ESTATUS_CUENTA.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE_STR))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE_STR))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCuenta() throws Exception {
        // Get the cuenta
        restCuentaMockMvc.perform(get("/api/cuentas/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCuenta() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

		int databaseSizeBeforeUpdate = cuentaRepository.findAll().size();

        // Update the cuenta
        //cuenta.setFechaCreacion(UPDATED_FECHA_CREACION);
        cuenta.setSaldo(UPDATED_SALDO);
        cuenta.setMontoExigible(UPDATED_MONTO_EXIGIBLE);
        cuenta.setSaldoInicial(UPDATED_SALDO_INICIAL);
        cuenta.setFechaUltimoPago(UPDATED_FECHA_ULTIMO_PAGO);
        cuenta.setNumeroContratoOrigen(UPDATED_NUMERO_CONTRATO_ORIGEN);
        cuenta.setDescripcion(UPDATED_DESCRIPCION);
        cuenta.setAttribute1(UPDATED_ATTRIBUTE1);
        cuenta.setAttribute2(UPDATED_ATTRIBUTE2);

        cuenta.setEstatusCuenta(UPDATED_ESTATUS_CUENTA);
        cuenta.setCreatedDate(UPDATED_CREATED_DATE);
        cuenta.setLastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        //cuenta.setCreatedBy(UPDATED_CREATED_BY);
        //cuenta.setLastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        restCuentaMockMvc.perform(put("/api/cuentas")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cuenta)))
                .andExpect(status().isOk());

        // Validate the Cuenta in the database
        List<Cuenta> cuentas = cuentaRepository.findAll();
        assertThat(cuentas).hasSize(databaseSizeBeforeUpdate);
        Cuenta testCuenta = cuentas.get(cuentas.size() - 1);
        //assertThat(testCuenta.getFechaCreacion().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testCuenta.getSaldo()).isEqualTo(UPDATED_SALDO);
        assertThat(testCuenta.getMontoExigible()).isEqualTo(UPDATED_MONTO_EXIGIBLE);
        assertThat(testCuenta.getSaldoInicial()).isEqualTo(UPDATED_SALDO_INICIAL);
        assertThat(testCuenta.getFechaUltimoPago()).isEqualTo(UPDATED_FECHA_ULTIMO_PAGO);
        assertThat(testCuenta.getNumeroContratoOrigen()).isEqualTo(UPDATED_NUMERO_CONTRATO_ORIGEN);
        assertThat(testCuenta.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testCuenta.getAttribute1()).isEqualTo(UPDATED_ATTRIBUTE1);
        assertThat(testCuenta.getAttribute2()).isEqualTo(UPDATED_ATTRIBUTE2);

        assertThat(testCuenta.getEstatusCuenta()).isEqualTo(UPDATED_ESTATUS_CUENTA);
        assertThat(testCuenta.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCuenta.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        //assertThat(testCuenta.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        //assertThat(testCuenta.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void deleteCuenta() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

		int databaseSizeBeforeDelete = cuentaRepository.findAll().size();

        // Get the cuenta
        restCuentaMockMvc.perform(delete("/api/cuentas/{id}", cuenta.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Cuenta> cuentas = cuentaRepository.findAll();
        assertThat(cuentas).hasSize(databaseSizeBeforeDelete - 1);
    }
}

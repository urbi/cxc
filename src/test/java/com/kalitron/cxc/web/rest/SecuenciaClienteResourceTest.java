package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.SecuenciaCliente;
import com.kalitron.cxc.repository.SecuenciaClienteRepository;
import com.kalitron.cxc.repository.search.SecuenciaClienteSearchRepository;
import com.kalitron.cxc.web.rest.mapper.SecuenciaClienteMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.TipoClienteType;

/**
 * Test class for the SecuenciaClienteResource REST controller.
 *
 * @see SecuenciaClienteResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SecuenciaClienteResourceTest {


    private static final TipoClienteType DEFAULT_TIPO_CLIENTE = TipoClienteType.CLIENTE;
    private static final TipoClienteType UPDATED_TIPO_CLIENTE = TipoClienteType.EMPRESARIAL;

    private static final Long DEFAULT_SECUENCIA = 0L;
    private static final Long UPDATED_SECUENCIA = 1L;

    @Inject
    private SecuenciaClienteRepository secuenciaClienteRepository;

    @Inject
    private SecuenciaClienteMapper secuenciaClienteMapper;

    @Inject
    private SecuenciaClienteSearchRepository secuenciaClienteSearchRepository;

    private MockMvc restSecuenciaClienteMockMvc;

    private SecuenciaCliente secuenciaCliente;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SecuenciaClienteResource secuenciaClienteResource = new SecuenciaClienteResource();
        ReflectionTestUtils.setField(secuenciaClienteResource, "secuenciaClienteRepository", secuenciaClienteRepository);
        ReflectionTestUtils.setField(secuenciaClienteResource, "secuenciaClienteMapper", secuenciaClienteMapper);
        ReflectionTestUtils.setField(secuenciaClienteResource, "secuenciaClienteSearchRepository", secuenciaClienteSearchRepository);
        this.restSecuenciaClienteMockMvc = MockMvcBuilders.standaloneSetup(secuenciaClienteResource).build();
    }

    @Before
    public void initTest() {
        secuenciaCliente = new SecuenciaCliente();
        secuenciaCliente.setTipoCliente(DEFAULT_TIPO_CLIENTE);
        secuenciaCliente.setSecuencia(DEFAULT_SECUENCIA);
    }

    @Test
    @Transactional
    public void createSecuenciaCliente() throws Exception {
        int databaseSizeBeforeCreate = secuenciaClienteRepository.findAll().size();

        // Create the SecuenciaCliente
        restSecuenciaClienteMockMvc.perform(post("/api/secuenciaClientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(secuenciaCliente)))
                .andExpect(status().isCreated());

        // Validate the SecuenciaCliente in the database
        List<SecuenciaCliente> secuenciaClientes = secuenciaClienteRepository.findAll();
        assertThat(secuenciaClientes).hasSize(databaseSizeBeforeCreate + 1);
        SecuenciaCliente testSecuenciaCliente = secuenciaClientes.get(secuenciaClientes.size() - 1);
        assertThat(testSecuenciaCliente.getTipoCliente()).isEqualTo(DEFAULT_TIPO_CLIENTE);
        assertThat(testSecuenciaCliente.getSecuencia()).isEqualTo(DEFAULT_SECUENCIA);
    }

    @Test
    @Transactional
    public void getAllSecuenciaClientes() throws Exception {
        // Initialize the database
        secuenciaClienteRepository.saveAndFlush(secuenciaCliente);

        // Get all the secuenciaClientes
        restSecuenciaClienteMockMvc.perform(get("/api/secuenciaClientes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(secuenciaCliente.getId().intValue())))
                .andExpect(jsonPath("$.[*].tipoCliente").value(hasItem(DEFAULT_TIPO_CLIENTE.toString())))
                .andExpect(jsonPath("$.[*].secuencia").value(hasItem(DEFAULT_SECUENCIA.intValue())));
    }

    @Test
    @Transactional
    public void getSecuenciaCliente() throws Exception {
        // Initialize the database
        secuenciaClienteRepository.saveAndFlush(secuenciaCliente);

        // Get the secuenciaCliente
        restSecuenciaClienteMockMvc.perform(get("/api/secuenciaClientes/{id}", secuenciaCliente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(secuenciaCliente.getId().intValue()))
            .andExpect(jsonPath("$.tipoCliente").value(DEFAULT_TIPO_CLIENTE.toString()))
            .andExpect(jsonPath("$.secuencia").value(DEFAULT_SECUENCIA.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSecuenciaCliente() throws Exception {
        // Get the secuenciaCliente
        restSecuenciaClienteMockMvc.perform(get("/api/secuenciaClientes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSecuenciaCliente() throws Exception {
        // Initialize the database
        secuenciaClienteRepository.saveAndFlush(secuenciaCliente);

		int databaseSizeBeforeUpdate = secuenciaClienteRepository.findAll().size();

        // Update the secuenciaCliente
        secuenciaCliente.setTipoCliente(UPDATED_TIPO_CLIENTE);
        secuenciaCliente.setSecuencia(UPDATED_SECUENCIA);
        restSecuenciaClienteMockMvc.perform(put("/api/secuenciaClientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(secuenciaCliente)))
                .andExpect(status().isOk());

        // Validate the SecuenciaCliente in the database
        List<SecuenciaCliente> secuenciaClientes = secuenciaClienteRepository.findAll();
        assertThat(secuenciaClientes).hasSize(databaseSizeBeforeUpdate);
        SecuenciaCliente testSecuenciaCliente = secuenciaClientes.get(secuenciaClientes.size() - 1);
        assertThat(testSecuenciaCliente.getTipoCliente()).isEqualTo(UPDATED_TIPO_CLIENTE);
        assertThat(testSecuenciaCliente.getSecuencia()).isEqualTo(UPDATED_SECUENCIA);
    }

    @Test
    @Transactional
    public void deleteSecuenciaCliente() throws Exception {
        // Initialize the database
        secuenciaClienteRepository.saveAndFlush(secuenciaCliente);

		int databaseSizeBeforeDelete = secuenciaClienteRepository.findAll().size();

        // Get the secuenciaCliente
        restSecuenciaClienteMockMvc.perform(delete("/api/secuenciaClientes/{id}", secuenciaCliente.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SecuenciaCliente> secuenciaClientes = secuenciaClienteRepository.findAll();
        assertThat(secuenciaClientes).hasSize(databaseSizeBeforeDelete - 1);
    }
}

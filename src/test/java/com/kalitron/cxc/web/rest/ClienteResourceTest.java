package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Cliente;
import com.kalitron.cxc.repository.ClienteRepository;
import com.kalitron.cxc.repository.search.ClienteSearchRepository;
import com.kalitron.cxc.web.rest.dto.ClienteDTO;
import com.kalitron.cxc.web.rest.mapper.ClienteMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.SexoType;
import com.kalitron.cxc.domain.enumeration.SistemaOrigenType;
import com.kalitron.cxc.domain.enumeration.TipoClienteType;

/**
 * Test class for the ClienteResource REST controller.
 *
 * @see ClienteResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ClienteResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_NOMBRES = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRES = "UPDATED_TEXT";
    private static final String DEFAULT_APELLIDO_PATERNO = "SAMPLE_TEXT";
    private static final String UPDATED_APELLIDO_PATERNO = "UPDATED_TEXT";
    private static final String DEFAULT_APELLIDO_MATERNO = "SAMPLE_TEXT";
    private static final String UPDATED_APELLIDO_MATERNO = "UPDATED_TEXT";
    private static final String DEFAULT_RFC = "SAMPLE_TEXT";
    private static final String UPDATED_RFC = "UPDATED_TEXT";
    private static final String DEFAULT_CURP = "SAMPLE_TEXT";
    private static final String UPDATED_CURP = "UPDATED_TEXT";

    private static final SexoType DEFAULT_SEXO = SexoType.HOMBRE;
    private static final SexoType UPDATED_SEXO = SexoType.MUJER;
    private static final String DEFAULT_NSS = "SAMPLE_TEXT";
    private static final String UPDATED_NSS = "UPDATED_TEXT";
    private static final String DEFAULT_CORREO = "SAMPLE_TEXT";
    private static final String UPDATED_CORREO = "UPDATED_TEXT";
    private static final String DEFAULT_TELEFONO1 = "SAMPLE_TEXT";
    private static final String UPDATED_TELEFONO1 = "UPDATED_TEXT";
    private static final String DEFAULT_TELEFONO2 = "SAMPLE_TEXT";
    private static final String UPDATED_TELEFONO2 = "UPDATED_TEXT";
    private static final String DEFAULT_CELULAR = "SAMPLE_TEXT";
    private static final String UPDATED_CELULAR = "UPDATED_TEXT";

    private static final LocalDate DEFAULT_FECHA_NACIMIENTO = new LocalDate(0L);
    private static final LocalDate UPDATED_FECHA_NACIMIENTO = new LocalDate();
    private static final String DEFAULT_NUMERO_REF_EXTERNA = "SAMPLE_TEXT";
    private static final String UPDATED_NUMERO_REF_EXTERNA = "UPDATED_TEXT";

    private static final SistemaOrigenType DEFAULT_SISTEMA_ORIGEN = SistemaOrigenType.SVU2;
    private static final SistemaOrigenType UPDATED_SISTEMA_ORIGEN = SistemaOrigenType.SCU_MIGRACION;

    private static final TipoClienteType DEFAULT_TIPO_CLIENTE = TipoClienteType.CLIENTE;
    private static final TipoClienteType UPDATED_TIPO_CLIENTE = TipoClienteType.EMPRESARIAL;
    private static final String DEFAULT_CALLE = "SAMPLE_TEXT";
    private static final String UPDATED_CALLE = "UPDATED_TEXT";
    private static final String DEFAULT_NUMERO = "SAMPLE_TEXT";
    private static final String UPDATED_NUMERO = "UPDATED_TEXT";
    private static final String DEFAULT_CODIGO_POSTAL = "SAMPLE_TEXT";
    private static final String UPDATED_CODIGO_POSTAL = "UPDATED_TEXT";
    private static final String DEFAULT_COLONIA = "SAMPLE_TEXT";
    private static final String UPDATED_COLONIA = "UPDATED_TEXT";
    private static final String DEFAULT_CIUDAD = "SAMPLE_TEXT";
    private static final String UPDATED_CIUDAD = "UPDATED_TEXT";
    private static final String DEFAULT_ESTADO = "SAMPLE_TEXT";
    private static final String UPDATED_ESTADO = "UPDATED_TEXT";
    private static final String DEFAULT_MUNICIPIO = "SAMPLE_TEXT";
    private static final String UPDATED_MUNICIPIO = "UPDATED_TEXT";
    private static final String DEFAULT_GEO_LOCALIZACION = "SAMPLE_TEXT";
    private static final String UPDATED_GEO_LOCALIZACION = "UPDATED_TEXT";



    private static final DateTime DEFAULT_CREATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATED_DATE);



    private static final DateTime DEFAULT_LAST_MODIFIED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_LAST_MODIFIED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_LAST_MODIFIED_DATE_STR = dateTimeFormatter.print(DEFAULT_LAST_MODIFIED_DATE);

    private static final Long DEFAULT_SECUENCIA_CLIENTE = 0L;
    private static final Long UPDATED_SECUENCIA_CLIENTE = 1L;

    @Inject
    private ClienteRepository clienteRepository;

    @Inject
    private ClienteMapper clienteMapper;

    @Inject
    private ClienteSearchRepository clienteSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restClienteMockMvc;

    private Cliente cliente;

    private ClienteDTO clienteDTO;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClienteResource clienteResource = new ClienteResource();
        ReflectionTestUtils.setField(clienteResource, "clienteRepository", clienteRepository);
        ReflectionTestUtils.setField(clienteResource, "clienteMapper", clienteMapper);
        ReflectionTestUtils.setField(clienteResource, "clienteSearchRepository", clienteSearchRepository);
        this.restClienteMockMvc = MockMvcBuilders.standaloneSetup(clienteResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        cliente = new Cliente();
        cliente.setNombres(DEFAULT_NOMBRES);
        cliente.setApellidoPaterno(DEFAULT_APELLIDO_PATERNO);
        cliente.setApellidoMaterno(DEFAULT_APELLIDO_MATERNO);
        cliente.setRfc(DEFAULT_RFC);
        cliente.setCurp(DEFAULT_CURP);
        cliente.setSexo(DEFAULT_SEXO);
        cliente.setNss(DEFAULT_NSS);
        cliente.setCorreo(DEFAULT_CORREO);
        cliente.setTelefono1(DEFAULT_TELEFONO1);
        cliente.setTelefono2(DEFAULT_TELEFONO2);
        cliente.setCelular(DEFAULT_CELULAR);
        cliente.setFechaNacimiento(DEFAULT_FECHA_NACIMIENTO);
        cliente.setNumeroRefExterna(DEFAULT_NUMERO_REF_EXTERNA);
        cliente.setSistemaOrigen(DEFAULT_SISTEMA_ORIGEN);
        cliente.setTipoCliente(DEFAULT_TIPO_CLIENTE);
        cliente.setCalle(DEFAULT_CALLE);
        cliente.setNumero(DEFAULT_NUMERO);
        cliente.setCodigoPostal(DEFAULT_CODIGO_POSTAL);
        cliente.setColonia(DEFAULT_COLONIA);
        cliente.setCiudad(DEFAULT_CIUDAD);
        cliente.setEstado(DEFAULT_ESTADO);
        cliente.setMunicipio(DEFAULT_MUNICIPIO);
        cliente.setGeoLocalizacion(DEFAULT_GEO_LOCALIZACION);

        cliente.setCreatedDate(DEFAULT_CREATED_DATE);

        cliente.setLastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        cliente.setSecuenciaCliente(DEFAULT_SECUENCIA_CLIENTE);
    }

    @Test
    @Transactional
    public void createCliente() throws Exception {
        int databaseSizeBeforeCreate = clienteRepository.findAll().size();

        // Create the Cliente
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isCreated());

        // Validate the Cliente in the database
        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeCreate + 1);
        Cliente testCliente = clientes.get(clientes.size() - 1);
        assertThat(testCliente.getNombres()).isEqualTo(DEFAULT_NOMBRES);
        assertThat(testCliente.getApellidoPaterno()).isEqualTo(DEFAULT_APELLIDO_PATERNO);
        assertThat(testCliente.getApellidoMaterno()).isEqualTo(DEFAULT_APELLIDO_MATERNO);
        assertThat(testCliente.getRfc()).isEqualTo(DEFAULT_RFC);
        assertThat(testCliente.getCurp()).isEqualTo(DEFAULT_CURP);
        assertThat(testCliente.getSexo()).isEqualTo(DEFAULT_SEXO);
        assertThat(testCliente.getNss()).isEqualTo(DEFAULT_NSS);
        assertThat(testCliente.getCorreo()).isEqualTo(DEFAULT_CORREO);
        assertThat(testCliente.getTelefono1()).isEqualTo(DEFAULT_TELEFONO1);
        assertThat(testCliente.getTelefono2()).isEqualTo(DEFAULT_TELEFONO2);
        assertThat(testCliente.getCelular()).isEqualTo(DEFAULT_CELULAR);
        assertThat(testCliente.getFechaNacimiento()).isEqualTo(DEFAULT_FECHA_NACIMIENTO);
        assertThat(testCliente.getNumeroRefExterna()).isEqualTo(DEFAULT_NUMERO_REF_EXTERNA);
        assertThat(testCliente.getSistemaOrigen()).isEqualTo(DEFAULT_SISTEMA_ORIGEN);
        assertThat(testCliente.getTipoCliente()).isEqualTo(DEFAULT_TIPO_CLIENTE);
        assertThat(testCliente.getCalle()).isEqualTo(DEFAULT_CALLE);
        assertThat(testCliente.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testCliente.getCodigoPostal()).isEqualTo(DEFAULT_CODIGO_POSTAL);
        assertThat(testCliente.getColonia()).isEqualTo(DEFAULT_COLONIA);
        assertThat(testCliente.getCiudad()).isEqualTo(DEFAULT_CIUDAD);
        assertThat(testCliente.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testCliente.getMunicipio()).isEqualTo(DEFAULT_MUNICIPIO);
        assertThat(testCliente.getGeoLocalizacion()).isEqualTo(DEFAULT_GEO_LOCALIZACION);

        assertThat(testCliente.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATED_DATE);

        assertThat(testCliente.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCliente.getSecuenciaCliente()).isEqualTo(DEFAULT_SECUENCIA_CLIENTE);
    }

    @Test
    @Transactional
    public void checkNombresIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setNombres(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkApellidoPaternoIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setApellidoPaterno(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRfcIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setRfc(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCalleIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setCalle(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCodigoPostalIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setCodigoPostal(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkColoniaIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setColonia(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setEstado(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMunicipioIsRequired() throws Exception {
        int databaseSizeBeforeTest = clienteRepository.findAll().size();
        // set the field null
        cliente.setMunicipio(null);

        // Create the Cliente, which fails.
        ClienteDTO clienteDTO = clienteMapper.clienteToClienteDTO(cliente);

        restClienteMockMvc.perform(post("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isBadRequest());

        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientes() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get all the clientes
        restClienteMockMvc.perform(get("/api/clientes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cliente.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombres").value(hasItem(DEFAULT_NOMBRES.toString())))
                .andExpect(jsonPath("$.[*].apellidoPaterno").value(hasItem(DEFAULT_APELLIDO_PATERNO.toString())))
                .andExpect(jsonPath("$.[*].apellidoMaterno").value(hasItem(DEFAULT_APELLIDO_MATERNO.toString())))
                .andExpect(jsonPath("$.[*].rfc").value(hasItem(DEFAULT_RFC.toString())))
                .andExpect(jsonPath("$.[*].curp").value(hasItem(DEFAULT_CURP.toString())))
                .andExpect(jsonPath("$.[*].sexo").value(hasItem(DEFAULT_SEXO.toString())))
                .andExpect(jsonPath("$.[*].nss").value(hasItem(DEFAULT_NSS.toString())))
                .andExpect(jsonPath("$.[*].correo").value(hasItem(DEFAULT_CORREO.toString())))
                .andExpect(jsonPath("$.[*].telefono1").value(hasItem(DEFAULT_TELEFONO1.toString())))
                .andExpect(jsonPath("$.[*].telefono2").value(hasItem(DEFAULT_TELEFONO2.toString())))
                .andExpect(jsonPath("$.[*].celular").value(hasItem(DEFAULT_CELULAR.toString())))
                .andExpect(jsonPath("$.[*].fechaNacimiento").value(hasItem(DEFAULT_FECHA_NACIMIENTO.toString())))
                .andExpect(jsonPath("$.[*].numeroRefExterna").value(hasItem(DEFAULT_NUMERO_REF_EXTERNA.toString())))
                .andExpect(jsonPath("$.[*].sistemaOrigen").value(hasItem(DEFAULT_SISTEMA_ORIGEN.toString())))
                .andExpect(jsonPath("$.[*].tipoCliente").value(hasItem(DEFAULT_TIPO_CLIENTE.toString())))
                .andExpect(jsonPath("$.[*].calle").value(hasItem(DEFAULT_CALLE.toString())))
                .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO.toString())))
                .andExpect(jsonPath("$.[*].codigoPostal").value(hasItem(DEFAULT_CODIGO_POSTAL.toString())))
                .andExpect(jsonPath("$.[*].colonia").value(hasItem(DEFAULT_COLONIA.toString())))
                .andExpect(jsonPath("$.[*].ciudad").value(hasItem(DEFAULT_CIUDAD.toString())))
                .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.toString())))
                .andExpect(jsonPath("$.[*].municipio").value(hasItem(DEFAULT_MUNICIPIO.toString())))
                .andExpect(jsonPath("$.[*].geoLocalizacion").value(hasItem(DEFAULT_GEO_LOCALIZACION.toString())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE_STR)))
                .andExpect(jsonPath("$.[*].secuenciaCliente").value(hasItem(DEFAULT_SECUENCIA_CLIENTE.intValue())));
    }

    @Test
    @Transactional
    public void getCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", cliente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cliente.getId().intValue()))
            .andExpect(jsonPath("$.nombres").value(DEFAULT_NOMBRES.toString()))
            .andExpect(jsonPath("$.apellidoPaterno").value(DEFAULT_APELLIDO_PATERNO.toString()))
            .andExpect(jsonPath("$.apellidoMaterno").value(DEFAULT_APELLIDO_MATERNO.toString()))
            .andExpect(jsonPath("$.rfc").value(DEFAULT_RFC.toString()))
            .andExpect(jsonPath("$.curp").value(DEFAULT_CURP.toString()))
            .andExpect(jsonPath("$.sexo").value(DEFAULT_SEXO.toString()))
            .andExpect(jsonPath("$.nss").value(DEFAULT_NSS.toString()))
            .andExpect(jsonPath("$.correo").value(DEFAULT_CORREO.toString()))
            .andExpect(jsonPath("$.telefono1").value(DEFAULT_TELEFONO1.toString()))
            .andExpect(jsonPath("$.telefono2").value(DEFAULT_TELEFONO2.toString()))
            .andExpect(jsonPath("$.celular").value(DEFAULT_CELULAR.toString()))
            .andExpect(jsonPath("$.fechaNacimiento").value(DEFAULT_FECHA_NACIMIENTO.toString()))
            .andExpect(jsonPath("$.numeroRefExterna").value(DEFAULT_NUMERO_REF_EXTERNA.toString()))
            .andExpect(jsonPath("$.sistemaOrigen").value(DEFAULT_SISTEMA_ORIGEN.toString()))
            .andExpect(jsonPath("$.tipoCliente").value(DEFAULT_TIPO_CLIENTE.toString()))
            .andExpect(jsonPath("$.calle").value(DEFAULT_CALLE.toString()))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO.toString()))
            .andExpect(jsonPath("$.codigoPostal").value(DEFAULT_CODIGO_POSTAL.toString()))
            .andExpect(jsonPath("$.colonia").value(DEFAULT_COLONIA.toString()))
            .andExpect(jsonPath("$.ciudad").value(DEFAULT_CIUDAD.toString()))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.toString()))
            .andExpect(jsonPath("$.municipio").value(DEFAULT_MUNICIPIO.toString()))
            .andExpect(jsonPath("$.geoLocalizacion").value(DEFAULT_GEO_LOCALIZACION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE_STR))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE_STR))
            .andExpect(jsonPath("$.secuenciaCliente").value(DEFAULT_SECUENCIA_CLIENTE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCliente() throws Exception {
        // Get the cliente
        restClienteMockMvc.perform(get("/api/clientes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

		int databaseSizeBeforeUpdate = clienteRepository.findAll().size();

        // Update the cliente
        cliente.setNombres(UPDATED_NOMBRES);
        cliente.setApellidoPaterno(UPDATED_APELLIDO_PATERNO);
        cliente.setApellidoMaterno(UPDATED_APELLIDO_MATERNO);
        cliente.setRfc(UPDATED_RFC);
        cliente.setCurp(UPDATED_CURP);
        cliente.setSexo(UPDATED_SEXO);
        cliente.setNss(UPDATED_NSS);
        cliente.setCorreo(UPDATED_CORREO);
        cliente.setTelefono1(UPDATED_TELEFONO1);
        cliente.setTelefono2(UPDATED_TELEFONO2);
        cliente.setCelular(UPDATED_CELULAR);
        cliente.setFechaNacimiento(UPDATED_FECHA_NACIMIENTO);
        cliente.setNumeroRefExterna(UPDATED_NUMERO_REF_EXTERNA);
        cliente.setSistemaOrigen(UPDATED_SISTEMA_ORIGEN);
        cliente.setTipoCliente(UPDATED_TIPO_CLIENTE);
        cliente.setCalle(UPDATED_CALLE);
        cliente.setNumero(UPDATED_NUMERO);
        cliente.setCodigoPostal(UPDATED_CODIGO_POSTAL);
        cliente.setColonia(UPDATED_COLONIA);
        cliente.setCiudad(UPDATED_CIUDAD);
        cliente.setEstado(UPDATED_ESTADO);
        cliente.setMunicipio(UPDATED_MUNICIPIO);
        cliente.setGeoLocalizacion(UPDATED_GEO_LOCALIZACION);
        cliente.setCreatedDate(UPDATED_CREATED_DATE);
        cliente.setLastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        cliente.setSecuenciaCliente(UPDATED_SECUENCIA_CLIENTE);
        restClienteMockMvc.perform(put("/api/clientes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clienteDTO)))
                .andExpect(status().isOk());

        // Validate the Cliente in the database
        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeUpdate);
        Cliente testCliente = clientes.get(clientes.size() - 1);
        assertThat(testCliente.getNombres()).isEqualTo(UPDATED_NOMBRES);
        assertThat(testCliente.getApellidoPaterno()).isEqualTo(UPDATED_APELLIDO_PATERNO);
        assertThat(testCliente.getApellidoMaterno()).isEqualTo(UPDATED_APELLIDO_MATERNO);
        assertThat(testCliente.getRfc()).isEqualTo(UPDATED_RFC);
        assertThat(testCliente.getCurp()).isEqualTo(UPDATED_CURP);
        assertThat(testCliente.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testCliente.getNss()).isEqualTo(UPDATED_NSS);
        assertThat(testCliente.getCorreo()).isEqualTo(UPDATED_CORREO);
        assertThat(testCliente.getTelefono1()).isEqualTo(UPDATED_TELEFONO1);
        assertThat(testCliente.getTelefono2()).isEqualTo(UPDATED_TELEFONO2);
        assertThat(testCliente.getCelular()).isEqualTo(UPDATED_CELULAR);
        assertThat(testCliente.getFechaNacimiento()).isEqualTo(UPDATED_FECHA_NACIMIENTO);
        assertThat(testCliente.getNumeroRefExterna()).isEqualTo(UPDATED_NUMERO_REF_EXTERNA);
        assertThat(testCliente.getSistemaOrigen()).isEqualTo(UPDATED_SISTEMA_ORIGEN);
        assertThat(testCliente.getTipoCliente()).isEqualTo(UPDATED_TIPO_CLIENTE);
        assertThat(testCliente.getCalle()).isEqualTo(UPDATED_CALLE);
        assertThat(testCliente.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testCliente.getCodigoPostal()).isEqualTo(UPDATED_CODIGO_POSTAL);
        assertThat(testCliente.getColonia()).isEqualTo(UPDATED_COLONIA);
        assertThat(testCliente.getCiudad()).isEqualTo(UPDATED_CIUDAD);
        assertThat(testCliente.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testCliente.getMunicipio()).isEqualTo(UPDATED_MUNICIPIO);
        assertThat(testCliente.getGeoLocalizacion()).isEqualTo(UPDATED_GEO_LOCALIZACION);
        assertThat(testCliente.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCliente.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCliente.getSecuenciaCliente()).isEqualTo(UPDATED_SECUENCIA_CLIENTE);
    }

    @Test
    @Transactional
    public void deleteCliente() throws Exception {
        // Initialize the database
        clienteRepository.saveAndFlush(cliente);

		int databaseSizeBeforeDelete = clienteRepository.findAll().size();

        // Get the cliente
        restClienteMockMvc.perform(delete("/api/clientes/{id}", cliente.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Cliente> clientes = clienteRepository.findAll();
        assertThat(clientes).hasSize(databaseSizeBeforeDelete - 1);
    }
}

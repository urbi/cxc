package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.FileLoader;
import com.kalitron.cxc.repository.FileLoaderRepository;
import com.kalitron.cxc.repository.search.FileLoaderSearchRepository;
import com.kalitron.cxc.web.rest.dto.FileLoaderDTO;
import com.kalitron.cxc.web.rest.mapper.FileLoaderMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.ObjectNameEnum;

/**
 * Test class for the FileLoaderResource REST controller.
 *
 * @see FileLoaderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class FileLoaderResourceTest {


    private static final ObjectNameEnum DEFAULT_TIPO_DATO = ObjectNameEnum.CLIENTE;
    private static final ObjectNameEnum UPDATED_TIPO_DATO = ObjectNameEnum.CUENTA;

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(2, "1");

    @Inject
    private FileLoaderRepository fileLoaderRepository;

    @Inject
    private FileLoaderMapper fileLoaderMapper;

    @Inject
    private FileLoaderSearchRepository fileLoaderSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restFileLoaderMockMvc;

    private FileLoader fileLoader;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FileLoaderResource fileLoaderResource = new FileLoaderResource();
        ReflectionTestUtils.setField(fileLoaderResource, "fileLoaderRepository", fileLoaderRepository);
        ReflectionTestUtils.setField(fileLoaderResource, "fileLoaderMapper", fileLoaderMapper);
        ReflectionTestUtils.setField(fileLoaderResource, "fileLoaderSearchRepository", fileLoaderSearchRepository);
        this.restFileLoaderMockMvc = MockMvcBuilders.standaloneSetup(fileLoaderResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        fileLoader = new FileLoader();
        fileLoader.setTipoDato(DEFAULT_TIPO_DATO);
        fileLoader.setFile(DEFAULT_FILE);
    }

    @Test
    @Transactional
    public void createFileLoader() throws Exception {
        int databaseSizeBeforeCreate = fileLoaderRepository.findAll().size();

        // Create the FileLoader
        FileLoaderDTO fileLoaderDTO = fileLoaderMapper.fileLoaderToFileLoaderDTO(fileLoader);

        restFileLoaderMockMvc.perform(post("/api/fileLoaders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fileLoaderDTO)))
                .andExpect(status().isCreated());

        // Validate the FileLoader in the database
        List<FileLoader> fileLoaders = fileLoaderRepository.findAll();
        assertThat(fileLoaders).hasSize(databaseSizeBeforeCreate + 1);
        FileLoader testFileLoader = fileLoaders.get(fileLoaders.size() - 1);
        assertThat(testFileLoader.getTipoDato()).isEqualTo(DEFAULT_TIPO_DATO);
        assertThat(testFileLoader.getFile()).isEqualTo(DEFAULT_FILE);
    }

    @Test
    @Transactional
    public void checkTipoDatoIsRequired() throws Exception {
        int databaseSizeBeforeTest = fileLoaderRepository.findAll().size();
        // set the field null
        fileLoader.setTipoDato(null);

        // Create the FileLoader, which fails.
        FileLoaderDTO fileLoaderDTO = fileLoaderMapper.fileLoaderToFileLoaderDTO(fileLoader);

        restFileLoaderMockMvc.perform(post("/api/fileLoaders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fileLoaderDTO)))
                .andExpect(status().isBadRequest());

        List<FileLoader> fileLoaders = fileLoaderRepository.findAll();
        assertThat(fileLoaders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFileIsRequired() throws Exception {
        int databaseSizeBeforeTest = fileLoaderRepository.findAll().size();
        // set the field null
        fileLoader.setFile(null);

        // Create the FileLoader, which fails.
        FileLoaderDTO fileLoaderDTO = fileLoaderMapper.fileLoaderToFileLoaderDTO(fileLoader);

        restFileLoaderMockMvc.perform(post("/api/fileLoaders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fileLoaderDTO)))
                .andExpect(status().isBadRequest());

        List<FileLoader> fileLoaders = fileLoaderRepository.findAll();
        assertThat(fileLoaders).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFileLoaders() throws Exception {
        // Initialize the database
        fileLoaderRepository.saveAndFlush(fileLoader);

        // Get all the fileLoaders
        restFileLoaderMockMvc.perform(get("/api/fileLoaders"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(fileLoader.getId().intValue())))
                .andExpect(jsonPath("$.[*].tipoDato").value(hasItem(DEFAULT_TIPO_DATO.toString())))
                .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))));
    }

    @Test
    @Transactional
    public void getFileLoader() throws Exception {
        // Initialize the database
        fileLoaderRepository.saveAndFlush(fileLoader);

        // Get the fileLoader
        restFileLoaderMockMvc.perform(get("/api/fileLoaders/{id}", fileLoader.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(fileLoader.getId().intValue()))
            .andExpect(jsonPath("$.tipoDato").value(DEFAULT_TIPO_DATO.toString()))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)));
    }

    @Test
    @Transactional
    public void getNonExistingFileLoader() throws Exception {
        // Get the fileLoader
        restFileLoaderMockMvc.perform(get("/api/fileLoaders/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFileLoader() throws Exception {
        // Initialize the database
        fileLoaderRepository.saveAndFlush(fileLoader);

		int databaseSizeBeforeUpdate = fileLoaderRepository.findAll().size();

        // Update the fileLoader
        fileLoader.setTipoDato(UPDATED_TIPO_DATO);
        fileLoader.setFile(UPDATED_FILE);
        
        FileLoaderDTO fileLoaderDTO = fileLoaderMapper.fileLoaderToFileLoaderDTO(fileLoader);

        restFileLoaderMockMvc.perform(put("/api/fileLoaders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(fileLoaderDTO)))
                .andExpect(status().isOk());

        // Validate the FileLoader in the database
        List<FileLoader> fileLoaders = fileLoaderRepository.findAll();
        assertThat(fileLoaders).hasSize(databaseSizeBeforeUpdate);
        FileLoader testFileLoader = fileLoaders.get(fileLoaders.size() - 1);
        assertThat(testFileLoader.getTipoDato()).isEqualTo(UPDATED_TIPO_DATO);
        assertThat(testFileLoader.getFile()).isEqualTo(UPDATED_FILE);
    }

    @Test
    @Transactional
    public void deleteFileLoader() throws Exception {
        // Initialize the database
        fileLoaderRepository.saveAndFlush(fileLoader);

		int databaseSizeBeforeDelete = fileLoaderRepository.findAll().size();

        // Get the fileLoader
        restFileLoaderMockMvc.perform(delete("/api/fileLoaders/{id}", fileLoader.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<FileLoader> fileLoaders = fileLoaderRepository.findAll();
        assertThat(fileLoaders).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.FoliadorReciboPago;
import com.kalitron.cxc.repository.FoliadorReciboPagoRepository;
import com.kalitron.cxc.repository.search.FoliadorReciboPagoSearchRepository;
import com.kalitron.cxc.web.rest.dto.FoliadorReciboPagoDTO;
import com.kalitron.cxc.web.rest.mapper.FoliadorReciboPagoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the FoliadorReciboPagoResource REST controller.
 *
 * @see FoliadorReciboPagoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class FoliadorReciboPagoResourceTest {


    private static final Long DEFAULT_SECUENCIA = 1L;
    private static final Long UPDATED_SECUENCIA = 2L;

    private static final Long DEFAULT_EMPRESA = 1L;
    private static final Long UPDATED_EMPRESA = 2L;

    private static final Long DEFAULT_PLAZA = 1L;
    private static final Long UPDATED_PLAZA = 2L;

    @Inject
    private FoliadorReciboPagoRepository foliadorReciboPagoRepository;

    @Inject
    private FoliadorReciboPagoMapper foliadorReciboPagoMapper;

    @Inject
    private FoliadorReciboPagoSearchRepository foliadorReciboPagoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restFoliadorReciboPagoMockMvc;

    private FoliadorReciboPago foliadorReciboPago;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FoliadorReciboPagoResource foliadorReciboPagoResource = new FoliadorReciboPagoResource();
        ReflectionTestUtils.setField(foliadorReciboPagoResource, "foliadorReciboPagoRepository", foliadorReciboPagoRepository);
        ReflectionTestUtils.setField(foliadorReciboPagoResource, "foliadorReciboPagoMapper", foliadorReciboPagoMapper);
        ReflectionTestUtils.setField(foliadorReciboPagoResource, "foliadorReciboPagoSearchRepository", foliadorReciboPagoSearchRepository);
        this.restFoliadorReciboPagoMockMvc = MockMvcBuilders.standaloneSetup(foliadorReciboPagoResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        foliadorReciboPago = new FoliadorReciboPago();
        foliadorReciboPago.setSecuencia(DEFAULT_SECUENCIA);
        foliadorReciboPago.setEmpresa(DEFAULT_EMPRESA);
        foliadorReciboPago.setPlaza(DEFAULT_PLAZA);
    }

    @Test
    @Transactional
    public void createFoliadorReciboPago() throws Exception {
        int databaseSizeBeforeCreate = foliadorReciboPagoRepository.findAll().size();

        // Create the FoliadorReciboPago
        FoliadorReciboPagoDTO foliadorReciboPagoDTO = foliadorReciboPagoMapper.foliadorReciboPagoToFoliadorReciboPagoDTO(foliadorReciboPago);

        restFoliadorReciboPagoMockMvc.perform(post("/api/foliadorReciboPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(foliadorReciboPagoDTO)))
                .andExpect(status().isCreated());

        // Validate the FoliadorReciboPago in the database
        List<FoliadorReciboPago> foliadorReciboPagos = foliadorReciboPagoRepository.findAll();
        assertThat(foliadorReciboPagos).hasSize(databaseSizeBeforeCreate + 1);
        FoliadorReciboPago testFoliadorReciboPago = foliadorReciboPagos.get(foliadorReciboPagos.size() - 1);
        assertThat(testFoliadorReciboPago.getSecuencia()).isEqualTo(DEFAULT_SECUENCIA);
        assertThat(testFoliadorReciboPago.getEmpresa()).isEqualTo(DEFAULT_EMPRESA);
        assertThat(testFoliadorReciboPago.getPlaza()).isEqualTo(DEFAULT_PLAZA);
    }

    @Test
    @Transactional
    public void getAllFoliadorReciboPagos() throws Exception {
        // Initialize the database
        foliadorReciboPagoRepository.saveAndFlush(foliadorReciboPago);

        // Get all the foliadorReciboPagos
        restFoliadorReciboPagoMockMvc.perform(get("/api/foliadorReciboPagos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(foliadorReciboPago.getId().intValue())))
                .andExpect(jsonPath("$.[*].secuencia").value(hasItem(DEFAULT_SECUENCIA.intValue())))
                .andExpect(jsonPath("$.[*].empresa").value(hasItem(DEFAULT_EMPRESA.intValue())))
                .andExpect(jsonPath("$.[*].plaza").value(hasItem(DEFAULT_PLAZA.intValue())));
    }

    @Test
    @Transactional
    public void getFoliadorReciboPago() throws Exception {
        // Initialize the database
        foliadorReciboPagoRepository.saveAndFlush(foliadorReciboPago);

        // Get the foliadorReciboPago
        restFoliadorReciboPagoMockMvc.perform(get("/api/foliadorReciboPagos/{id}", foliadorReciboPago.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(foliadorReciboPago.getId().intValue()))
            .andExpect(jsonPath("$.secuencia").value(DEFAULT_SECUENCIA.intValue()))
            .andExpect(jsonPath("$.empresa").value(DEFAULT_EMPRESA.intValue()))
            .andExpect(jsonPath("$.plaza").value(DEFAULT_PLAZA.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingFoliadorReciboPago() throws Exception {
        // Get the foliadorReciboPago
        restFoliadorReciboPagoMockMvc.perform(get("/api/foliadorReciboPagos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFoliadorReciboPago() throws Exception {
        // Initialize the database
        foliadorReciboPagoRepository.saveAndFlush(foliadorReciboPago);

		int databaseSizeBeforeUpdate = foliadorReciboPagoRepository.findAll().size();

        // Update the foliadorReciboPago
        foliadorReciboPago.setSecuencia(UPDATED_SECUENCIA);
        foliadorReciboPago.setEmpresa(UPDATED_EMPRESA);
        foliadorReciboPago.setPlaza(UPDATED_PLAZA);
        
        FoliadorReciboPagoDTO foliadorReciboPagoDTO = foliadorReciboPagoMapper.foliadorReciboPagoToFoliadorReciboPagoDTO(foliadorReciboPago);

        restFoliadorReciboPagoMockMvc.perform(put("/api/foliadorReciboPagos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(foliadorReciboPagoDTO)))
                .andExpect(status().isOk());

        // Validate the FoliadorReciboPago in the database
        List<FoliadorReciboPago> foliadorReciboPagos = foliadorReciboPagoRepository.findAll();
        assertThat(foliadorReciboPagos).hasSize(databaseSizeBeforeUpdate);
        FoliadorReciboPago testFoliadorReciboPago = foliadorReciboPagos.get(foliadorReciboPagos.size() - 1);
        assertThat(testFoliadorReciboPago.getSecuencia()).isEqualTo(UPDATED_SECUENCIA);
        assertThat(testFoliadorReciboPago.getEmpresa()).isEqualTo(UPDATED_EMPRESA);
        assertThat(testFoliadorReciboPago.getPlaza()).isEqualTo(UPDATED_PLAZA);
    }

    @Test
    @Transactional
    public void deleteFoliadorReciboPago() throws Exception {
        // Initialize the database
        foliadorReciboPagoRepository.saveAndFlush(foliadorReciboPago);

		int databaseSizeBeforeDelete = foliadorReciboPagoRepository.findAll().size();

        // Get the foliadorReciboPago
        restFoliadorReciboPagoMockMvc.perform(delete("/api/foliadorReciboPagos/{id}", foliadorReciboPago.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<FoliadorReciboPago> foliadorReciboPagos = foliadorReciboPagoRepository.findAll();
        assertThat(foliadorReciboPagos).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.Estado;
import com.kalitron.cxc.repository.EstadoRepository;
import com.kalitron.cxc.repository.search.EstadoSearchRepository;
import com.kalitron.cxc.web.rest.mapper.EstadoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.RegionType;

/**
 * Test class for the EstadoResource REST controller.
 *
 * @see EstadoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class EstadoResourceTest {

    private static final String DEFAULT_NOMBRE = "SAMPLE_TEXT";
    private static final String UPDATED_NOMBRE = "UPDATED_TEXT";

    private static final RegionType DEFAULT_REGION = RegionType.PACIFICO;
    private static final RegionType UPDATED_REGION = RegionType.NORTE;

    @Inject
    private EstadoRepository estadoRepository;

    @Inject
    private EstadoMapper estadoMapper;

    @Inject
    private EstadoSearchRepository estadoSearchRepository;

    private MockMvc restEstadoMockMvc;

    private Estado estado;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        EstadoResource estadoResource = new EstadoResource();
        ReflectionTestUtils.setField(estadoResource, "estadoRepository", estadoRepository);
        ReflectionTestUtils.setField(estadoResource, "estadoMapper", estadoMapper);
        ReflectionTestUtils.setField(estadoResource, "estadoSearchRepository", estadoSearchRepository);
        this.restEstadoMockMvc = MockMvcBuilders.standaloneSetup(estadoResource).build();
    }

    @Before
    public void initTest() {
        estado = new Estado();
        estado.setNombre(DEFAULT_NOMBRE);
        estado.setRegion(DEFAULT_REGION);
    }

    @Test
    @Transactional
    public void createEstado() throws Exception {
        int databaseSizeBeforeCreate = estadoRepository.findAll().size();

        // Create the Estado
        restEstadoMockMvc.perform(post("/api/estados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(estado)))
                .andExpect(status().isCreated());

        // Validate the Estado in the database
        List<Estado> estados = estadoRepository.findAll();
        assertThat(estados).hasSize(databaseSizeBeforeCreate + 1);
        Estado testEstado = estados.get(estados.size() - 1);
        assertThat(testEstado.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testEstado.getRegion()).isEqualTo(DEFAULT_REGION);
    }

    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = estadoRepository.findAll().size();
        // set the field null
        estado.setNombre(null);

        // Create the Estado, which fails.
        restEstadoMockMvc.perform(post("/api/estados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(estado)))
                .andExpect(status().isBadRequest());

        List<Estado> estados = estadoRepository.findAll();
        assertThat(estados).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEstados() throws Exception {
        // Initialize the database
        estadoRepository.saveAndFlush(estado);

        // Get all the estados
        restEstadoMockMvc.perform(get("/api/estados"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(estado.getId().intValue())))
                .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
                .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION.toString())));
    }

    @Test
    @Transactional
    public void getEstado() throws Exception {
        // Initialize the database
        estadoRepository.saveAndFlush(estado);

        // Get the estado
        restEstadoMockMvc.perform(get("/api/estados/{id}", estado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(estado.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.region").value(DEFAULT_REGION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEstado() throws Exception {
        // Get the estado
        restEstadoMockMvc.perform(get("/api/estados/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEstado() throws Exception {
        // Initialize the database
        estadoRepository.saveAndFlush(estado);

		int databaseSizeBeforeUpdate = estadoRepository.findAll().size();

        // Update the estado
        estado.setNombre(UPDATED_NOMBRE);
        estado.setRegion(UPDATED_REGION);
        restEstadoMockMvc.perform(put("/api/estados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(estado)))
                .andExpect(status().isOk());

        // Validate the Estado in the database
        List<Estado> estados = estadoRepository.findAll();
        assertThat(estados).hasSize(databaseSizeBeforeUpdate);
        Estado testEstado = estados.get(estados.size() - 1);
        assertThat(testEstado.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testEstado.getRegion()).isEqualTo(UPDATED_REGION);
    }

    @Test
    @Transactional
    public void deleteEstado() throws Exception {
        // Initialize the database
        estadoRepository.saveAndFlush(estado);

		int databaseSizeBeforeDelete = estadoRepository.findAll().size();

        // Get the estado
        restEstadoMockMvc.perform(delete("/api/estados/{id}", estado.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Estado> estados = estadoRepository.findAll();
        assertThat(estados).hasSize(databaseSizeBeforeDelete - 1);
    }
}

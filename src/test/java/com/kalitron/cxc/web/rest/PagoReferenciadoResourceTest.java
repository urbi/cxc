package com.kalitron.cxc.web.rest;

import com.kalitron.cxc.Application;
import com.kalitron.cxc.domain.PagoReferenciado;
import com.kalitron.cxc.repository.PagoReferenciadoRepository;
import com.kalitron.cxc.repository.search.PagoReferenciadoSearchRepository;
import com.kalitron.cxc.web.rest.mapper.PagoReferenciadoMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.LocalDate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kalitron.cxc.domain.enumeration.EstatusPagoType;

/**
 * Test class for the PagoReferenciadoResource REST controller.
 *
 * @see PagoReferenciadoResource
 */
//TODO: actualizar test
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class PagoReferenciadoResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final BigDecimal DEFAULT_MONTO_PAGO = new BigDecimal(0);
    private static final BigDecimal UPDATED_MONTO_PAGO = new BigDecimal(1);
    private static final String DEFAULT_REFERENCIA = "SAMPLE_TEXT";
    private static final String UPDATED_REFERENCIA = "UPDATED_TEXT";
    private static final String DEFAULT_CUENTA_CLABE = "SAMPLE_TEXT";
    private static final String UPDATED_CUENTA_CLABE = "UPDATED_TEXT";
    private static final String DEFAULT_OBSERVACIONES = "SAMPLE_TEXT";
    private static final String UPDATED_OBSERVACIONES = "UPDATED_TEXT";

    private static final LocalDate DEFAULT_FECHA_PAGO = new LocalDate(0L);
    private static final LocalDate UPDATED_FECHA_PAGO = new LocalDate();

    private static final DateTime DEFAULT_FECHA_CARGA_PAGO = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_FECHA_CARGA_PAGO = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_FECHA_CARGA_PAGO_STR = dateTimeFormatter.print(DEFAULT_FECHA_CARGA_PAGO);
    private static final String DEFAULT_FOLIO_EXTERNO_CONCILIACION = "SAMPLE_TEXT";
    private static final String UPDATED_FOLIO_EXTERNO_CONCILIACION = "UPDATED_TEXT";
    private static final String DEFAULT_FOLIO_CARGA = "SAMPLE_TEXT";
    private static final String UPDATED_FOLIO_CARGA = "UPDATED_TEXT";
    private static final String DEFAULT_CONVENIO = "SAMPLE_TEXT";
    private static final String UPDATED_CONVENIO = "UPDATED_TEXT";

    private static final DateTime DEFAULT_FECHA_APLICACION_PAGO = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_FECHA_APLICACION_PAGO = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_FECHA_APLICACION_PAGO_STR = dateTimeFormatter.print(DEFAULT_FECHA_APLICACION_PAGO);
    private static final String DEFAULT_COMENTARIO = "SAMPLE_TEXT";
    private static final String UPDATED_COMENTARIO = "UPDATED_TEXT";

    private static final EstatusPagoType DEFAULT_ESTATUS_PAGO = EstatusPagoType.APLICADO;
    private static final EstatusPagoType UPDATED_ESTATUS_PAGO = EstatusPagoType.DUPLICADO;

    private static final Long DEFAULT_CREATED_BY = 0L;
    private static final Long UPDATED_CREATED_BY = 1L;

    private static final DateTime DEFAULT_CREATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATED_DATE);

    private static final Long DEFAULT_LAST_MODIFIED_BY = 0L;
    private static final Long UPDATED_LAST_MODIFIED_BY = 1L;

    private static final DateTime DEFAULT_LAST_MODIFIED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_LAST_MODIFIED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_LAST_MODIFIED_DATE_STR = dateTimeFormatter.print(DEFAULT_LAST_MODIFIED_DATE);

    @Inject
    private PagoReferenciadoRepository pagoReferenciadoRepository;

    @Inject
    private PagoReferenciadoMapper pagoReferenciadoMapper;

    @Inject
    private PagoReferenciadoSearchRepository pagoReferenciadoSearchRepository;

    private MockMvc restPagoReferenciadoMockMvc;

    private PagoReferenciado pagoReferenciado;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PagoReferenciadoResource pagoReferenciadoResource = new PagoReferenciadoResource();
        ReflectionTestUtils.setField(pagoReferenciadoResource, "pagoReferenciadoRepository", pagoReferenciadoRepository);
        ReflectionTestUtils.setField(pagoReferenciadoResource, "pagoReferenciadoMapper", pagoReferenciadoMapper);
        ReflectionTestUtils.setField(pagoReferenciadoResource, "pagoReferenciadoSearchRepository", pagoReferenciadoSearchRepository);
        this.restPagoReferenciadoMockMvc = MockMvcBuilders.standaloneSetup(pagoReferenciadoResource).build();
    }

    @Before
    public void initTest() {
        pagoReferenciado = new PagoReferenciado();
        pagoReferenciado.setMontoPago(DEFAULT_MONTO_PAGO);
        pagoReferenciado.setReferencia(DEFAULT_REFERENCIA);
        pagoReferenciado.setCuentaClabe(DEFAULT_CUENTA_CLABE);
        pagoReferenciado.setObservaciones(DEFAULT_OBSERVACIONES);
        pagoReferenciado.setFechaPago(DEFAULT_FECHA_PAGO);
        pagoReferenciado.setFechaCargaPago(DEFAULT_FECHA_CARGA_PAGO);
        pagoReferenciado.setFolioExternoConciliacion(DEFAULT_FOLIO_EXTERNO_CONCILIACION);
        pagoReferenciado.setFolioCarga(DEFAULT_FOLIO_CARGA);
        pagoReferenciado.setConvenio(DEFAULT_CONVENIO);
        pagoReferenciado.setFechaAplicacionPago(DEFAULT_FECHA_APLICACION_PAGO);
        pagoReferenciado.setComentario(DEFAULT_COMENTARIO);
        pagoReferenciado.setEstatusPago(DEFAULT_ESTATUS_PAGO);

        pagoReferenciado.setCreatedDate(DEFAULT_CREATED_DATE);

        pagoReferenciado.setLastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createPagoReferenciado() throws Exception {
        int databaseSizeBeforeCreate = pagoReferenciadoRepository.findAll().size();

        // Create the PagoReferenciado
        restPagoReferenciadoMockMvc.perform(post("/api/pagoReferenciados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagoReferenciado)))
                .andExpect(status().isCreated());

        // Validate the PagoReferenciado in the database
        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        assertThat(pagoReferenciados).hasSize(databaseSizeBeforeCreate + 1);
        PagoReferenciado testPagoReferenciado = pagoReferenciados.get(pagoReferenciados.size() - 1);
        assertThat(testPagoReferenciado.getMontoPago()).isEqualTo(DEFAULT_MONTO_PAGO);
        assertThat(testPagoReferenciado.getReferencia()).isEqualTo(DEFAULT_REFERENCIA);
        assertThat(testPagoReferenciado.getCuentaClabe()).isEqualTo(DEFAULT_CUENTA_CLABE);
        assertThat(testPagoReferenciado.getObservaciones()).isEqualTo(DEFAULT_OBSERVACIONES);
        assertThat(testPagoReferenciado.getFechaPago()).isEqualTo(DEFAULT_FECHA_PAGO);
        assertThat(testPagoReferenciado.getFechaCargaPago().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_FECHA_CARGA_PAGO);
        assertThat(testPagoReferenciado.getFolioExternoConciliacion()).isEqualTo(DEFAULT_FOLIO_EXTERNO_CONCILIACION);
        assertThat(testPagoReferenciado.getFolioCarga()).isEqualTo(DEFAULT_FOLIO_CARGA);
        assertThat(testPagoReferenciado.getConvenio()).isEqualTo(DEFAULT_CONVENIO);
        assertThat(testPagoReferenciado.getFechaAplicacionPago().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_FECHA_APLICACION_PAGO);
        assertThat(testPagoReferenciado.getComentario()).isEqualTo(DEFAULT_COMENTARIO);
        assertThat(testPagoReferenciado.getEstatusPago()).isEqualTo(DEFAULT_ESTATUS_PAGO);

        assertThat(testPagoReferenciado.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATED_DATE);

        assertThat(testPagoReferenciado.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void checkReferenciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = pagoReferenciadoRepository.findAll().size();
        // set the field null
        pagoReferenciado.setReferencia(null);

        // Create the PagoReferenciado, which fails.
        restPagoReferenciadoMockMvc.perform(post("/api/pagoReferenciados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagoReferenciado)))
                .andExpect(status().isBadRequest());

        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        assertThat(pagoReferenciados).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCuentaClabeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pagoReferenciadoRepository.findAll().size();
        // set the field null
        pagoReferenciado.setCuentaClabe(null);

        // Create the PagoReferenciado, which fails.
        restPagoReferenciadoMockMvc.perform(post("/api/pagoReferenciados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagoReferenciado)))
                .andExpect(status().isBadRequest());

        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        assertThat(pagoReferenciados).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFolioCargaIsRequired() throws Exception {
        int databaseSizeBeforeTest = pagoReferenciadoRepository.findAll().size();
        // set the field null
        pagoReferenciado.setFolioCarga(null);

        // Create the PagoReferenciado, which fails.
        restPagoReferenciadoMockMvc.perform(post("/api/pagoReferenciados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagoReferenciado)))
                .andExpect(status().isBadRequest());

        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        assertThat(pagoReferenciados).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPagoReferenciados() throws Exception {
        // Initialize the database
        pagoReferenciadoRepository.saveAndFlush(pagoReferenciado);

        // Get all the pagoReferenciados
        restPagoReferenciadoMockMvc.perform(get("/api/pagoReferenciados"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pagoReferenciado.getId().intValue())))
                .andExpect(jsonPath("$.[*].montoPago").value(hasItem(DEFAULT_MONTO_PAGO.intValue())))
                .andExpect(jsonPath("$.[*].referencia").value(hasItem(DEFAULT_REFERENCIA.toString())))
                .andExpect(jsonPath("$.[*].cuentaClabe").value(hasItem(DEFAULT_CUENTA_CLABE.toString())))
                .andExpect(jsonPath("$.[*].observaciones").value(hasItem(DEFAULT_OBSERVACIONES.toString())))
                .andExpect(jsonPath("$.[*].fechaPago").value(hasItem(DEFAULT_FECHA_PAGO.toString())))
                .andExpect(jsonPath("$.[*].fechaCargaPago").value(hasItem(DEFAULT_FECHA_CARGA_PAGO_STR)))
                .andExpect(jsonPath("$.[*].folioExternoConciliacion").value(hasItem(DEFAULT_FOLIO_EXTERNO_CONCILIACION.toString())))
                .andExpect(jsonPath("$.[*].folioCarga").value(hasItem(DEFAULT_FOLIO_CARGA.toString())))
                .andExpect(jsonPath("$.[*].convenio").value(hasItem(DEFAULT_CONVENIO.toString())))
                .andExpect(jsonPath("$.[*].fechaAplicacionPago").value(hasItem(DEFAULT_FECHA_APLICACION_PAGO_STR)))
                .andExpect(jsonPath("$.[*].comentario").value(hasItem(DEFAULT_COMENTARIO.toString())))
                .andExpect(jsonPath("$.[*].estatusPago").value(hasItem(DEFAULT_ESTATUS_PAGO.toString())))
                .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.intValue())))
                .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY.intValue())))
                .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getPagoReferenciado() throws Exception {
        // Initialize the database
        pagoReferenciadoRepository.saveAndFlush(pagoReferenciado);

        // Get the pagoReferenciado
        restPagoReferenciadoMockMvc.perform(get("/api/pagoReferenciados/{id}", pagoReferenciado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(pagoReferenciado.getId().intValue()))
            .andExpect(jsonPath("$.montoPago").value(DEFAULT_MONTO_PAGO.intValue()))
            .andExpect(jsonPath("$.referencia").value(DEFAULT_REFERENCIA.toString()))
            .andExpect(jsonPath("$.cuentaClabe").value(DEFAULT_CUENTA_CLABE.toString()))
            .andExpect(jsonPath("$.observaciones").value(DEFAULT_OBSERVACIONES.toString()))
            .andExpect(jsonPath("$.fechaPago").value(DEFAULT_FECHA_PAGO.toString()))
            .andExpect(jsonPath("$.fechaCargaPago").value(DEFAULT_FECHA_CARGA_PAGO_STR))
            .andExpect(jsonPath("$.folioExternoConciliacion").value(DEFAULT_FOLIO_EXTERNO_CONCILIACION.toString()))
            .andExpect(jsonPath("$.folioCarga").value(DEFAULT_FOLIO_CARGA.toString()))
            .andExpect(jsonPath("$.convenio").value(DEFAULT_CONVENIO.toString()))
            .andExpect(jsonPath("$.fechaAplicacionPago").value(DEFAULT_FECHA_APLICACION_PAGO_STR))
            .andExpect(jsonPath("$.comentario").value(DEFAULT_COMENTARIO.toString()))
            .andExpect(jsonPath("$.estatusPago").value(DEFAULT_ESTATUS_PAGO.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.intValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE_STR))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY.intValue()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingPagoReferenciado() throws Exception {
        // Get the pagoReferenciado
        restPagoReferenciadoMockMvc.perform(get("/api/pagoReferenciados/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePagoReferenciado() throws Exception {
        // Initialize the database
        pagoReferenciadoRepository.saveAndFlush(pagoReferenciado);

		int databaseSizeBeforeUpdate = pagoReferenciadoRepository.findAll().size();

        // Update the pagoReferenciado
        pagoReferenciado.setMontoPago(UPDATED_MONTO_PAGO);
        pagoReferenciado.setReferencia(UPDATED_REFERENCIA);
        pagoReferenciado.setCuentaClabe(UPDATED_CUENTA_CLABE);
        pagoReferenciado.setObservaciones(UPDATED_OBSERVACIONES);
        pagoReferenciado.setFechaPago(UPDATED_FECHA_PAGO);
        pagoReferenciado.setFechaCargaPago(UPDATED_FECHA_CARGA_PAGO);
        pagoReferenciado.setFolioExternoConciliacion(UPDATED_FOLIO_EXTERNO_CONCILIACION);
        pagoReferenciado.setFolioCarga(UPDATED_FOLIO_CARGA);
        pagoReferenciado.setConvenio(UPDATED_CONVENIO);
        pagoReferenciado.setFechaAplicacionPago(UPDATED_FECHA_APLICACION_PAGO);
        pagoReferenciado.setComentario(UPDATED_COMENTARIO);
        pagoReferenciado.setEstatusPago(UPDATED_ESTATUS_PAGO);

        pagoReferenciado.setCreatedDate(UPDATED_CREATED_DATE);

        pagoReferenciado.setLastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        restPagoReferenciadoMockMvc.perform(put("/api/pagoReferenciados")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pagoReferenciado)))
                .andExpect(status().isOk());

        // Validate the PagoReferenciado in the database
        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        assertThat(pagoReferenciados).hasSize(databaseSizeBeforeUpdate);
        PagoReferenciado testPagoReferenciado = pagoReferenciados.get(pagoReferenciados.size() - 1);
        assertThat(testPagoReferenciado.getMontoPago()).isEqualTo(UPDATED_MONTO_PAGO);
        assertThat(testPagoReferenciado.getReferencia()).isEqualTo(UPDATED_REFERENCIA);
        assertThat(testPagoReferenciado.getCuentaClabe()).isEqualTo(UPDATED_CUENTA_CLABE);
        assertThat(testPagoReferenciado.getObservaciones()).isEqualTo(UPDATED_OBSERVACIONES);
        assertThat(testPagoReferenciado.getFechaPago()).isEqualTo(UPDATED_FECHA_PAGO);
        assertThat(testPagoReferenciado.getFechaCargaPago().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_FECHA_CARGA_PAGO);
        assertThat(testPagoReferenciado.getFolioExternoConciliacion()).isEqualTo(UPDATED_FOLIO_EXTERNO_CONCILIACION);
        assertThat(testPagoReferenciado.getFolioCarga()).isEqualTo(UPDATED_FOLIO_CARGA);
        assertThat(testPagoReferenciado.getConvenio()).isEqualTo(UPDATED_CONVENIO);
        assertThat(testPagoReferenciado.getFechaAplicacionPago().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_FECHA_APLICACION_PAGO);
        assertThat(testPagoReferenciado.getComentario()).isEqualTo(UPDATED_COMENTARIO);
        assertThat(testPagoReferenciado.getEstatusPago()).isEqualTo(UPDATED_ESTATUS_PAGO);
//        assertThat(testPagoReferenciado.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPagoReferenciado.getCreatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATED_DATE);
  //      assertThat(testPagoReferenciado.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPagoReferenciado.getLastModifiedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void deletePagoReferenciado() throws Exception {
        // Initialize the database
        pagoReferenciadoRepository.saveAndFlush(pagoReferenciado);

		int databaseSizeBeforeDelete = pagoReferenciadoRepository.findAll().size();

        // Get the pagoReferenciado
        restPagoReferenciadoMockMvc.perform(delete("/api/pagoReferenciados/{id}", pagoReferenciado.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PagoReferenciado> pagoReferenciados = pagoReferenciadoRepository.findAll();
        assertThat(pagoReferenciados).hasSize(databaseSizeBeforeDelete - 1);
    }
}
